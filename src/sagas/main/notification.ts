import {ApolloClient, ApolloQueryResult, NormalizedCacheObject} from 'apollo-boost';
import * as Notifications from 'expo-notifications';
import {put, takeEvery} from 'redux-saga/effects';
import {API_ENDPOINT} from '../../../config';
import * as actionTypes from '../../actions/main/notification';
import {NotificationSubscriberRecord} from '../../actions/main/notification';
import * as settingsActionTypes from '../../actions/main/settings';
import initApollo from '../../lib/initApollo';
import {getNotificationSubscriberByIdentifier} from '../../lib/notification/queries/index.graphql';
import {NotificationSubscriberConnection} from '../../lib/notification/types';
import {Action} from '../../types/types';

function* onRequestNotificationIdentifier() {
    try {
        const identifier = yield Notifications.getExpoPushTokenAsync();

        yield put(actionTypes.defineNotificationIdentifier(identifier.data));
    } catch (e) {
        console.error('sagas/main/notification.ts@onRequestNotificationIdentifier => ', e);
        return;
    }
}

function* onRegisterNotificationSubscriber(action: Action) {
    try {
        const subscriber: NotificationSubscriberRecord = action.payload;

        yield fetch(API_ENDPOINT + '/notification_subscribers', {
            body: JSON.stringify(subscriber),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            method: 'POST',
        });
    } catch (e) {
        console.error('sagas/main/notification.ts@onRegisterNotificationSubscriber => ', e);
        return;
    }
}

function* onRequestNotificationSubscriber(action: Action) {
    try {
        const apolloClient: ApolloClient<NormalizedCacheObject> = initApollo();

        let identifier = action.payload;

        if (!identifier) {
            const identifierData = yield Notifications.getExpoPushTokenAsync();
            identifier = identifierData.data;
        }

        const {data}: ApolloQueryResult<{
            notificationSubscribers: NotificationSubscriberConnection,
        }> = yield apolloClient.query({
            query: getNotificationSubscriberByIdentifier,
            variables: {identifier},
        });

        const notificationSubscriber = data.notificationSubscribers.edges[0].node;

        yield put(actionTypes.receivedNotificationSubscriber(notificationSubscriber));
    } catch (e) {
        console.error('sagas/main/notification.ts@onRequestNotificationSubscriber => ', e.graphQLErrors ?? e);
        yield put(actionTypes.receivedNotificationSubscriber(null));
        return;
    }
}

function* onUpdateNotificationSubscriber(action: Action) {
    try {
        const subscriber: { record: NotificationSubscriberRecord, id: string } = action.payload;

        const res: Response =  yield fetch(API_ENDPOINT + '/notification_subscribers/' + subscriber.id, {
            body: JSON.stringify(subscriber.record),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            method: 'PUT',
        });

        if (res.status >= 200 && res.status < 300) {
            yield put(settingsActionTypes.setToaster('Vos préférences ont été enregistrées'));
        } else {
            yield put(settingsActionTypes.setToasterError('Une erreur est survenue lors de l\'enregistrement de vos ' +
                'préférences'));
        }

    } catch (e) {
        console.error('sagas/main/notification.ts@onUpdateNotificationSubscriber => ', e);
        yield put(settingsActionTypes.setToasterError('Une erreur est survenue lors de l\'enregistrement de vos ' +
            'préférences'));
    }
}

export default function* notificationSaga() {
    yield takeEvery(actionTypes.REQUEST_NOTIFICATION_IDENTIFIER, onRequestNotificationIdentifier);
    yield takeEvery(actionTypes.REGISTER_NOTIFICATION_SUBSCRIBER, onRegisterNotificationSubscriber);
    yield takeEvery(actionTypes.REQUEST_NOTIFICATION_SUBSCRIBER, onRequestNotificationSubscriber);
    yield takeEvery(actionTypes.UPDATE_NOTIFICATION_SUBSCRIBER, onUpdateNotificationSubscriber);
}
