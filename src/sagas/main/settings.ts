import {put, takeEvery} from 'redux-saga/effects';
import * as mainActionTypes from '../../actions/main/main';
import * as actionTypes from '../../actions/main/settings';

function* onActivateGodMode() {
    yield put(actionTypes.setToaster('Vous avez maintenant accès aux notifications de test !'));
}

export default function* settingsSaga() {
    yield takeEvery(mainActionTypes.ACTIVATE_GOD_MODE, onActivateGodMode);
}
