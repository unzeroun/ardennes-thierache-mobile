import * as Location from 'expo-location';
import {LocationObject} from 'expo-location';
import {put, takeEvery} from 'redux-saga/effects';
import * as actionTypes from '../../actions/main/location';

function* onGetCurrentPosition() {
    try {
        const currentPosition: LocationObject = yield Location.getCurrentPositionAsync();

        yield put(actionTypes.currentPositionRetrieved(currentPosition));
    } catch (e) {
        return;
    }
}

export default function* locationSaga() {
    yield takeEvery(actionTypes.GET_CURRENT_POSITION, onGetCurrentPosition);
}
