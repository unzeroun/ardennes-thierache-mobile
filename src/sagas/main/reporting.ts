import {Camera} from 'expo-camera';
import {CapturedPicture} from 'expo-camera/build/Camera.types';
import {put, takeEvery} from 'redux-saga/effects';
import {API_ENDPOINT} from '../../../config';
import * as actionTypes from '../../actions/main/reporting';
import * as settingsActionTypes from '../../actions/main/settings';
import {Action, Report} from '../../types/types';

function* onTakeReportPicture(action: Action) {
    try {
        const camera: Camera = action.payload;

        if (camera) {
            const picture: CapturedPicture = yield camera.takePictureAsync({
                base64: true,
                exif: true,
                quality: 1,
            });

            yield put(actionTypes.pictureTaken(picture));
        }
    } catch (e) {
        return;
    }
}

function* onSendReport(action: Action) {
    try {
        const report: Report = action.payload;

        const response: Response = yield fetch(API_ENDPOINT + '/reports', {
            body: JSON.stringify({
                file: report.picture ? report.picture.base64 : null,
                location: report.location || {latitude: 0, longitude: 0},
                text: report.message,
                type: report.type,
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            method: 'POST',
        });

        if (response.status >= 200 && response.status < 300) {
            yield put(settingsActionTypes.setToaster('Le rapport a été envoyé avec succès'));
        } else {
            yield put(settingsActionTypes.setToasterError('Une erreur est survenue lors de l\'envoi du rapport'));
        }

        yield put(actionTypes.reportSent());
    } catch (e) {
        yield put(settingsActionTypes.setToasterError('Une erreur est survenue lors de l\'envoi du rapport'));
        yield put(actionTypes.reportSent());
    }
}

export default function* reportingSaga() {
    yield takeEvery(actionTypes.TAKE_REPORT_PICTURE, onTakeReportPicture);
    yield takeEvery(actionTypes.SEND_REPORT, onSendReport);
}
