import * as Permissions from 'expo-permissions';
import {put, takeEvery} from 'redux-saga/effects';
import * as actionTypes from '../../actions/main/permission';

export function* onGetLocationPermission() {
    try {
        const {status} = yield Permissions.getAsync(Permissions.LOCATION);

        if ('granted' === status) {
            yield put(actionTypes.locationPermissionGranted());
            return;
        }

        const {status: finalStatus} = yield Permissions.askAsync(Permissions.LOCATION);

        if ('granted' === finalStatus) {
            yield put(actionTypes.locationPermissionGranted());
            return;
        }

        yield put(actionTypes.locationPermissionDenied());
    } catch (e) {
        return;
    }
}

export function* onGetCameraPermission() {
    try {
        const {status} = yield Permissions.getAsync(Permissions.CAMERA);

        if ('granted' === status) {
            yield put(actionTypes.cameraPermissionGranted());
            return;
        }

        const {status: finalStatus} = yield Permissions.askAsync(Permissions.CAMERA);

        if ('granted' === finalStatus) {
            yield put(actionTypes.cameraPermissionGranted());
            return;
        }

        yield put(actionTypes.cameraPermissionDenied());
    } catch (e) {
        return;
    }
}

export function* onGetNotificationPermission() {
    try {
        const {status} = yield Permissions.getAsync(Permissions.NOTIFICATIONS);

        if ('granted' === status) {
            yield put(actionTypes.notificationPermissionGranted());
            return;
        }

        const {status: finalStatus} = yield Permissions.askAsync(Permissions.NOTIFICATIONS);

        if ('granted' === finalStatus) {
            yield put(actionTypes.notificationPermissionGranted());
            return;
        }

        yield put(actionTypes.notificationPermissionDenied());
    } catch (e) {
        return;
    }
}

export default function* permissionSaga() {
    yield takeEvery(actionTypes.GET_LOCATION_PERMISSION, onGetLocationPermission);
    yield takeEvery(actionTypes.GET_CAMERA_PERMISSION, onGetCameraPermission);
    yield takeEvery(actionTypes.GET_NOTIFICATION_PERMISSION, onGetNotificationPermission);
}
