import {call, put, takeEvery} from 'redux-saga/effects';
import {REACHY_BASE_URL, REACHY_CLIENT_ID} from '../../../config';
import * as actionTypes from '../../actions/main/poi';

const fetchFromApi = (uri: string) => fetch(
    REACHY_BASE_URL + uri,
    {
        headers: {
            'X-Client-Id': REACHY_CLIENT_ID,
        },
    },
);

export function* apiFetch(uri: string) {
    const res = yield call(fetchFromApi, uri);

    if (res.status !== 200) {
        return null;
    }

    return yield call([res, 'json']);
}

function* onRequestPois() {
    try {
        const data = yield call(apiFetch, '/points');

        if (data) {
            yield put(actionTypes.receivedPois(data['hydra:member']));
        }
    } catch (e) {
        return;
    }
}

export default function* poiSaga() {
    yield takeEvery(actionTypes.REQUEST_POIS, onRequestPois);
}
