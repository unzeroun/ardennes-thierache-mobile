import * as Notifications from 'expo-notifications';
import {put, select, takeEvery} from 'redux-saga/effects';
import * as profileActionTypes from '../../actions/carpooling/profile';
import * as permissionTypes from '../../actions/main/permission';
import {State} from '../../reducers';
import {User} from '../../types/types';

function* onNotificationPermissionGranted() {
    try {
        const user: User = yield select((state: State) => state.profile.user);
        const identifier = yield Notifications.getExpoPushTokenAsync();

        if (!user || !identifier) {
            return;
        }

        if (user.devicesIdentifiers.indexOf(identifier) !== -1) {
            return;
        }

        yield put(profileActionTypes.submitUserProfileUpdate({
            ...user,
            devicesIdentifiers: [...user.devicesIdentifiers, identifier],
        }));

    } catch (e) {
        return;
    }
}

export default function* notificationSaga() {
    yield takeEvery(permissionTypes.NOTIFICATION_PERMISSION_GRANTED, onNotificationPermissionGranted);
}
