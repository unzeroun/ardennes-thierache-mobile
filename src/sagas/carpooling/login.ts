import {call, put, select, takeEvery} from 'redux-saga/effects';
import {USED_MAIL_ERROR_LABEL} from '../../../config';
import * as actionTypes from '../../actions/carpooling/login';
import * as tokenTypes from '../../actions/carpooling/token';
import {setToasterError} from '../../actions/main/settings';
import {State} from '../../reducers';
import {Action, Token} from '../../types/types';
import {apiFetch} from '../index';

function* onRequestLogin(action: Action) {
    try {
        const data = yield call(apiFetch, '/api/end_users', {
            body: JSON.stringify({
                email: action.payload,
            }),
            headers: {
                'accept': 'application/ld+json',
                'content-type': 'application/ld+json',
            },
            method: 'POST',
        });

        if (data.violations) {
            yield put(actionTypes.receivedLoginRequestError());
            if (data['hydra:description'] === USED_MAIL_ERROR_LABEL) {
                yield put(actionTypes.requestNewMagicLink(action.payload));
            } else {
                yield put(setToasterError(data.violations[0].message));
            }
        } else {
            yield put(actionTypes.receivedLoginRequestSuccess());
        }
    } catch (e) {
        yield put(actionTypes.receivedLoginRequestError());
        yield put(setToasterError('Une erreur est survenue'));
        return;
    }
}

function* onRequestNewMagicLink(action: Action) {
    try {
        const data = yield call(apiFetch, '/api/login/' + action.payload + '/get_link', {});

        if (data) {
            yield put(actionTypes.receivedLoginRequestSuccess());
        }

    } catch (e) {
        yield put(setToasterError('Une erreur est survenue'));
        return;
    }
}

function* onRequestCodeVerification(action: Action) {
    try {
        const requestEmail = yield select((state: State) => state.login.userEmail);

        const data = yield call(
            apiFetch,
            '/api/login/' + requestEmail + '/' + action.payload.requestCode,
            {},
        );

        if (data) {
            const token: Token = {
                refresh_token: data.refresh_token,
                token: data.token,
            };

            yield put(tokenTypes.storeToken(token));
        } else {
            yield put(setToasterError('Le code entré est incorrect'));
            yield put(actionTypes.codeVerificationError());
        }
    } catch (e) {
        yield put(setToasterError('Une erreur est survenue'));
        return;
    }
}

export default function* loginSaga() {
    yield takeEvery(actionTypes.REQUEST_LOGIN_REQUEST, onRequestLogin);
    yield takeEvery(actionTypes.REQUEST_CODE_VERIFICATION, onRequestCodeVerification);
    yield takeEvery(actionTypes.REQUEST_NEW_MAGIC_LINK, onRequestNewMagicLink);
}
