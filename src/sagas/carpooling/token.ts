import AsyncStorage from '@react-native-community/async-storage';
import {call, put, takeEvery} from 'redux-saga/effects';
import {API_BASE_URL} from '../../../config';
import * as actionTypes from '../../actions/carpooling/token';
import {setToasterError} from '../../actions/main/settings';
import {isJWTValid, willJWTExpire} from '../../lib/tokenManagement';
import {Action, Token} from '../../types/types';

function* onGetToken() {
    try {
        const rawToken: string = yield call([AsyncStorage, 'getItem'], '@at:user_token');

        if (!rawToken) {
            return;
        }

        const token = JSON.parse(rawToken);

        if (!isJWTValid) {
            yield put(actionTypes.setTokenValidity(false));
            return;
        }

        if (willJWTExpire(token)) {
            yield put(actionTypes.refreshToken(token));
        }

        yield put(actionTypes.getTokenSuccess(token));
    } catch (e) {
        yield put(actionTypes.getTokenError());
        return;
    }
}

function* onStoreToken(action: Action) {
    try {
        yield AsyncStorage.setItem('@at:user_token', JSON.stringify(action.payload));

        yield put(actionTypes.storeTokenSuccess(action.payload));
    } catch (e) {
        yield put(actionTypes.storeTokenError());
        return;
    }
}

function* onRefreshToken(action: Action) {
    try {
        const token: Token = action.payload;

        if (token) {
            const req = yield fetch(API_BASE_URL + '/api/token/refresh', {
                body: JSON.stringify({
                    refresh_token: token.refresh_token,
                }),
                headers: {
                    authorization: 'Bearer ' + token.token,
                },
                method: 'POST',
            });

            const data = yield call([req, 'json']);
            yield put(actionTypes.refreshTokenSuccess(data));
        }
    } catch (e) {
        yield put(actionTypes.refreshTokenError());
        return;
    }
}

function* onSetTokenValidity(action: Action) {
    try {
        if (!action.payload) {
            yield put(setToasterError('Connexion expirée'));
            yield AsyncStorage.removeItem('@at:user_token');
        }
    } catch (e) {
        return;
    }
}

export default function* tokenSaga() {
    yield takeEvery(actionTypes.STORE_TOKEN, onStoreToken);
    yield takeEvery(actionTypes.GET_TOKEN, onGetToken);
    yield takeEvery(actionTypes.REFRESH_TOKEN, onRefreshToken);
    yield takeEvery(actionTypes.SET_TOKEN_VALIDITY, onSetTokenValidity);
}
