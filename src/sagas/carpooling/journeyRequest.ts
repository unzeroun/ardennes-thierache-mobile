import {call, put, select, takeEvery} from 'redux-saga/effects';
import * as actionTypes from '../../actions/carpooling/journeyRequest';
import {State} from '../../reducers';
import {Action, JourneyRequest, Token, User} from '../../types/types';
import {apiFetch} from '../index';

function* onRequestCreateJourneyRequest(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);
        const user: User = yield select((state: State) => state.profile.user);

        if (!token || !user) {
            return;
        }

        const data = yield call(apiFetch, '/api/journey_requests', {
            body: JSON.stringify({
                journey: action.payload,
            }),
            headers: {
                'accept': 'application/ld+json',
                'authorization': 'Bearer ' + token.token,
                'content-type': 'application/ld+json',
            },
            method: 'POST',
        });

        if (data) {
            yield put(actionTypes.createJourneyRequestSuccess(action.payload));
        }
    } catch (e) {
        return;
    }
}

function* onGetJourneyRequests(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        yield call(apiFetch, action.payload + '/requests', {
            headers: {
                authorization: 'Bearer ' + token.token,
            },
        });

        if (data) {
            const journeyRequests: JourneyRequest[] = data['hydra:member'].map((request: JourneyRequest) => {
                return {...request};
            });

            yield put(actionTypes.getJourneyRequestSuccess(journeyRequests));
        }
    } catch (e) {
        return;
    }
}

function* onAcceptJourneyRequest(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(apiFetch, action.payload.journeyRequestIri + '/accept', {
            body: JSON.stringify({}),
            headers: {
                'authorization': 'Bearer ' + token.token,
                'content-type': 'application/ld+json',
            },
            method: 'POST',
        });

        if (data) {
            yield put(actionTypes.setCurrentJourneyRequest(data));
            yield put(actionTypes.acceptJourneyRequestSuccess(action.payload.journeyIri));
        }
    } catch (e) {
        return;
    }
}

function* onDenyJourneyRequest(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(apiFetch, action.payload.journeyRequestIri + '/deny', {
            body: JSON.stringify({}),
            headers: {
                'authorization': 'Bearer ' + token.token,
                'content-type': 'application/ld+json',
            },
            method: 'POST',
        });

        if (data) {
            yield put(actionTypes.denyJourneyRequestSuccess(action.payload.journeyIri));
        }
    } catch (e) {
        return;
    }
}

export default function* journeyRequestSaga() {
    yield takeEvery(actionTypes.REQUEST_CREATE_JOURNEY_REQUEST, onRequestCreateJourneyRequest);
    yield takeEvery([
        actionTypes.GET_JOURNEY_REQUESTS,
        actionTypes.CREATE_JOURNEY_REQUEST_SUCCESS,
        actionTypes.ACCEPT_JOURNEY_REQUEST_SUCCESS,
        actionTypes.DENY_JOURNEY_REQUEST_SUCCESS,
    ], onGetJourneyRequests);
    yield takeEvery(actionTypes.ACCEPT_JOURNEY_REQUEST, onAcceptJourneyRequest);
    yield takeEvery(actionTypes.DENY_JOURNEY_REQUEST, onDenyJourneyRequest);
}
