import moment from 'moment';
import {call, put, select, takeEvery} from 'redux-saga/effects';
import {GEO_API_GOUV_URL} from '../../../config';
import * as actionTypes from '../../actions/carpooling/journey';
import * as miscTypes from '../../actions/carpooling/misc';
import {setToaster, setToasterError} from '../../actions/main/settings';
import {CitySuggestionType} from '../../components/carpooling/journey/CitySuggestion';
import {State} from '../../reducers';
import {Action, Journey, Token} from '../../types/types';
import {apiFetch} from '../index';

function* onRequestUserJourneyList() {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(apiFetch, '/api/journeys/valid', {
            headers: {
                authorization: 'Bearer ' + token.token,
            },
        });

        if (data) {
            const journeys: Journey[] = data['hydra:member'].map((journey: any) => {
                return {...journey};
            });

            yield put(actionTypes.userJourneyListReceivedSuccess(journeys));
        }
    } catch (e) {
        return;
    }
}

function* onRequestAllJourneysList() {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(apiFetch, '/api/journeys?departureDate[after]=now&order[departureDate]', {
            headers: {
                authorization: 'Bearer ' + token.token,
            },
        });

        if (data) {
            const journeys: Journey[] = data['hydra:member'].map((journey: any) => {
                return {...journey};
            });

            if (data['hydra:view']['hydra:next']) {
                yield put(actionTypes.setNextAllJourneysPageIri(data['hydra:view']['hydra:next']));
            }

            const sortedJourneys: Journey[] = journeys.sort((a, b) => {
                return moment(a.departureDate).isBefore(moment(b.departureDate)) ? -1 : 1;
            });

            yield put(actionTypes.allJourneysListRetrievedSuccess(sortedJourneys));
        }
    } catch (e) {
        return;
    }
}

function* onRequestAllJourneysListNextPage(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(apiFetch, action.payload, {
            headers: {
                authorization: 'Bearer ' + token.token,
            },
        });

        if (data) {
            const journeys: Journey[] = data['hydra:member'].map((journey: any) => {
                return {...journey};
            });

            if (data['hydra:view']['hydra:next']) {
                yield put(actionTypes.setNextAllJourneysPageIri(data['hydra:view']['hydra:next']));
            } else {
                yield put(actionTypes.setNextAllJourneysPageIri(null));
            }

            const sortedJourneys: Journey[] = journeys.sort((a, b) => {
                return moment(a.departureDate).isBefore(moment(b.departureDate)) ? -1 : 1;
            });

            yield put(actionTypes.requestAllJourneysListNextPageSuccess(sortedJourneys));
        }
    } catch (e) {
        return;
    }
}

function* onRequestJourneyById(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(apiFetch, action.payload, {
            headers: {
                authorization: 'Bearer ' + token.token,
            },
        });

        if (data) {
            yield put(actionTypes.journeyReceivedSuccess({...data}));
        }
    } catch (e) {
        return;
    }
}

function* onSubmitJourneyCreation(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(apiFetch, '/api/journeys', {
            body: JSON.stringify(action.payload),
            headers: {
                'accept': 'application/ld+json',
                'authorization': 'Bearer ' + token.token,
                'content-type': 'application/json',
            },
            method: 'POST',
        });

        if (data.violations) {
            yield put(actionTypes.journeyCreationError());
            yield put(miscTypes.setViolationsArray(data.violations));
        } else {
            if (data['@id']) {
                yield put(setToaster('Itinéraire créé avec succès'));
                yield put(actionTypes.journeyCreationSuccess());
            } else {
                yield put(actionTypes.journeyCreationError());
                yield put(setToasterError('Une erreur est survenue'));
            }
        }
    } catch (e) {
        yield put(actionTypes.journeyCreationError());
        return;
    }
}

function* onRequestGeoDepartureSearch(action: Action) {
    try {
        const req = yield fetch(
            GEO_API_GOUV_URL + '/search/?q=' + action.payload,
        );

        const data = yield call([req, 'json']);

        if (data && data.features.length > 0) {
            const citySuggestions: CitySuggestionType[] = [];

            data.features.map((suggestion: any) => {
                citySuggestions.push({
                    city: suggestion.properties.city,
                    coordinates: {
                        latitude: suggestion.geometry.coordinates[1],
                        longitude: suggestion.geometry.coordinates[0],
                    },
                    postcode: suggestion.properties.postcode,
                });
            });
            yield put(actionTypes.setDepartureCitySuggestions(citySuggestions));
        }
    } catch (e) {
        return;
    }
}

function* onRequestArrivalSearch(action: Action) {
    try {
        const req = yield fetch(
            GEO_API_GOUV_URL + '/search/?q=' + action.payload,
        );

        const data = yield call([req, 'json']);

        if (data && data.features.length > 0) {
            const citySuggestions: CitySuggestionType[] = [];

            data.features.map((suggestion: any) => {
                citySuggestions.push({
                    city: suggestion.properties.city,
                    coordinates: {
                        latitude: suggestion.geometry.coordinates[1],
                        longitude: suggestion.geometry.coordinates[0],
                    },
                    postcode: suggestion.properties.postcode,
                });
            });
            yield put(actionTypes.setArrivalCitySuggestions(citySuggestions));
        }
    } catch (e) {
        return;
    }
}

function* onRequestPendingJourneyList() {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(apiFetch, '/api/journeys/pending', {
            headers: {
                authorization: 'Bearer ' + token.token,
            },
        });

        if (data) {
            const journeys: Journey[] = data['hydra:member'].map((journey: any) => {
                return {...journey};
            });

            yield put(actionTypes.pendingJourneyListSuccess(journeys));
        }
    } catch (e) {
        return;
    }
}

export default function* journeySaga() {
    yield takeEvery(actionTypes.SUBMIT_JOURNEY_CREATION, onSubmitJourneyCreation);
    yield takeEvery(actionTypes.REQUEST_ALL_JOURNEYS_LIST, onRequestAllJourneysList);
    yield takeEvery(actionTypes.REQUEST_ALL_JOURNEYS_LIST_NEXT_PAGE, onRequestAllJourneysListNextPage);
    yield takeEvery(actionTypes.REQUEST_JOURNEY_BY_ID, onRequestJourneyById);
    yield takeEvery(actionTypes.REQUEST_USER_JOURNEY_LIST, onRequestUserJourneyList);
    yield takeEvery(actionTypes.REQUEST_GEO_DEPARTURE_SEARCH, onRequestGeoDepartureSearch);
    yield takeEvery(actionTypes.REQUEST_GEO_ARRIVAL_SEARCH, onRequestArrivalSearch);
    yield takeEvery(actionTypes.REQUEST_PENDING_JOURNEY_LIST, onRequestPendingJourneyList);
}
