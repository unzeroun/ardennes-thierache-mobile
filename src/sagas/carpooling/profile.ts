import {call, put, select, takeEvery} from 'redux-saga/effects';
import * as miscActionTypes from '../../actions/carpooling/misc';
import * as actionTypes from '../../actions/carpooling/profile';
import {setToaster, setToasterError} from '../../actions/main/settings';
import {getIdFromToken} from '../../lib/tokenManagement';
import {State} from '../../reducers';
import {Action, Car, Token} from '../../types/types';
import {apiFetch} from '../index';

function* onRequestUserProfile() {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const userId = getIdFromToken(token);

        const data = yield call(apiFetch, '/api/end_users/' + userId, {
            headers: {
                accept: 'application/ld+json',
                authorization: 'Bearer ' + token.token,
            },
            method: 'GET',
        });

        if ('hydra:Error' === data['@type']) {
            yield put(actionTypes.requestUserProfileError());
        } else {
            yield put(actionTypes.requestUserProfileSuccess({
                address1: data.address1,
                address2: data.address2,
                address3: data.address3,
                birthDate: data.birthDate,
                city: data.city,
                description: data.description,
                devicesIdentifiers: data.devicesIdentifiers,
                drivingLicenseDate: data.drivingLicenseDate,
                email: data.email,
                firstName: data.firstName,
                gender: data.gender,
                hasDrivingLicense: data.hasDrivingLicense,
                iri: data['@id'],
                lastName: data.lastName,
                phoneNumber: data.phoneNumber,
                postCode: data.postCode,
            }));
        }
    } catch (e) {
        yield put(actionTypes.requestUserProfileError());
        return;
    }
}

function* onSubmitUserProfileUpdate(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const userId = getIdFromToken(token);

        const data = yield call(apiFetch, '/api/end_users/' + userId, {
            body: JSON.stringify(action.payload),
            headers: {
                'accept': 'application/ld+json',
                'authorization': 'Bearer ' + token.token,
                'content-type': 'application/ld+json',
            },
            method: 'PUT',
        });

        if (data.violations) {
            yield put(actionTypes.userProfileUpdateError());
            yield put(miscActionTypes.setViolationsArray(data.violations));
        } else {
            yield put(setToaster('Profil mis à jour'));
            yield put(actionTypes.userProfileUpdateSuccess({
                address1: data.address1,
                address2: data.address2,
                address3: data.address3,
                birthDate: data.birthDate,
                city: data.city,
                description: data.description,
                devicesIdentifiers: data.devicesIdentifiers,
                drivingLicenseDate: data.drivingLicenseDate,
                email: data.email,
                firstName: data.firstName,
                gender: data.gender,
                hasDrivingLicense: data.hasDrivingLicense,
                iri: data['@id'],
                lastName: data.lastName,
                phoneNumber: data.phoneNumber,
                postCode: data.postCode,
            }));
        }

    } catch (e) {
        yield put(actionTypes.userProfileUpdateError());
        yield put(setToasterError('Une erreur est survenue'));
        return;
    }
}

function* onRequestUserCarProfile() {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const userId = getIdFromToken(token);

        const data = yield call(apiFetch, '/api/end_users/' + userId + '/car', {
            headers: {
                accept: 'application/ld+json',
                authorization: 'Bearer ' + token.token,
            },
            method: 'GET',
        });

        if ('hydra:Error' === data['@type']) {
            yield put(actionTypes.requestUserCarProfileError());
        } else {
            yield put(actionTypes.requestUserCarProfileSuccess({
                brand: data.brand,
                color: data.color,
                iri: data['@id'],
                model: data.model,
                preferences: data.preferences,
                year: data.year,
            }));
        }

    } catch (e) {
        yield put(actionTypes.requestUserCarProfileError());
        return;
    }
}

function* onSubmitCarProfileUpdate(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);
        const car: Car = action.payload;

        if (!token || !car) {
            return;
        }

        const data = yield call(apiFetch, car.iri, {
            body: JSON.stringify({
                ...action.payload,
                preferences: [...action.payload.preferences],
            }),
            headers: {
                'accept': 'application/ld+json',
                'authorization': 'Bearer ' + token.token,
                'content-type': 'application/ld+json',
            },
            method: 'PUT',
        });

        if (data) {
            if (data.violations) {
                yield put(actionTypes.userProfileUpdateError());
                yield put(miscActionTypes.setViolationsArray(data.violations));
            } else {
                yield put(setToaster('Véhicule mis à jour'));
                yield put(actionTypes.carProfileUpdateSuccess({
                    brand: data.brand,
                    color: data.color,
                    iri: data['@id'],
                    model: data.model,
                    preferences: data.preferences,
                    year: data.year,
                }));
            }
        }
    } catch (e) {
        yield put(actionTypes.userProfileUpdateError());
        yield put(setToasterError('Veuillez vérifier les informations renseignées'));
        return;
    }
}

export default function* profileSaga() {
    yield takeEvery(actionTypes.REQUEST_USER_PROFILE, onRequestUserProfile);
    yield takeEvery(actionTypes.SUBMIT_USER_PROFILE_UPDATE, onSubmitUserProfileUpdate);
    yield takeEvery([
        actionTypes.REQUEST_USER_CAR_PROFILE,
        actionTypes.USER_PROFILE_UPDATE_SUCCESS,
    ], onRequestUserCarProfile);
    yield takeEvery(actionTypes.SUBMIT_CAR_PROFILE_UPDATE, onSubmitCarProfileUpdate);
}
