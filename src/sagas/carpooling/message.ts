import moment from 'moment';
import {call, put, select, takeEvery} from 'redux-saga/effects';
import * as actionTypes from '../../actions/carpooling/message';
import {State} from '../../reducers';
import {Action, Message, Token} from '../../types/types';
import {apiFetch} from '../index';

function* onGetMessages(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(apiFetch, action.payload + '/messages', {
            headers: {
                authorization: 'Bearer ' + token.token,
            },
        });

        if (data) {
            const messages: Message[] = data['hydra:member'].map((item: Message) => {
                return item;
            });

            yield put(actionTypes.getMessagesSuccess(messages.sort((a, b) => {
                return moment(a.sentAt).unix() < moment(b.sentAt).unix() ? -1 : 1;
            })));
        }
    } catch (e) {
        return;
    }
}

function* onMarkMessagesAsRead(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(apiFetch, action.payload.journeyIri + '/messages/mark_as_read', {
            body: JSON.stringify({
                withEndUser: action.payload.endUserIri,
            }),
            headers: {
                'authorization': 'Bearer ' + token.token,
                'content-type': 'application/json',
            },
            method: 'POST',
        });
    } catch (e) {
        return;
    }
}

function* onSendMessage(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(apiFetch, '/api/messages', {
            body: JSON.stringify({
                content: action.payload.content,
                journey: action.payload.journeyIri,
                withEndUser: action.payload.withEndUserIri,
            }),
            headers: {
                'authorization': 'Bearer ' + token.token,
                'content-type': 'application/json',
            },
            method: 'POST',
        });
    } catch (e) {
        return;
    }
}

function* onGetUnreadMessages() {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(apiFetch, '/api/messages/unread', {
            headers: {
                authorization: 'Bearer ' + token.token,
            },
        });

        if (data) {
            yield put(actionTypes.getAllUnreadMessagesSuccess(data['hydra:member']));
        }
    } catch (e) {
        return;
    }
}

export default function* messageSaga() {
    yield takeEvery(actionTypes.GET_MESSAGES, onGetMessages);
    yield takeEvery(actionTypes.MARK_MESSAGES_AS_READ, onMarkMessagesAsRead);
    yield takeEvery(actionTypes.SEND_MESSAGE, onSendMessage);
    yield takeEvery(actionTypes.GET_ALL_UNREAD_MESSAGES, onGetUnreadMessages);
}
