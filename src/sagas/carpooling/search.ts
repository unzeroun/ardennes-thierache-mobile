import moment from 'moment';
import {call, put, select, takeEvery} from 'redux-saga/effects';
import * as actionTypes from '../../actions/carpooling/serach';
import {State} from '../../reducers';
import {Action, Journey, Token} from '../../types/types';
import {apiFetch} from '../index';

function* onRequestJourneySearch(action: Action) {
    try {
        const token: Token = yield select((state: State) => state.token.token);

        if (!token) {
            return;
        }

        const data = yield call(
            apiFetch,
            '/api/journeys?departureDate[after]=now&order[departureDate]&departure.city=' + action.payload.departure
            + '&arrival.city=' + action.payload.arrival,
            {
                headers: {
                    authorization: 'Bearer ' + token.token,
                },
            });

        if (data) {
            const journeys: Journey[] = data['hydra:member'].map((journey: any) => {
                return {...journey};
            });

            const sortedJourneys: Journey[] = journeys.sort((a, b) => {
                return moment(a.departureDate).isBefore(moment(b.departureDate)) ? -1 : 1;
            });

            yield put(actionTypes.journeyResearchSuccess(sortedJourneys));
        } else {
            yield put(actionTypes.journeyResearchError());
        }
    } catch (e) {
        yield put(actionTypes.journeyResearchError());
        return;
    }
}

export default function* searchSaga() {
    yield takeEvery(actionTypes.REQUEST_JOURNEY_SEARCH, onRequestJourneySearch);
}
