import AsyncStorage from '@react-native-community/async-storage';
import {all, call, put} from 'redux-saga/effects';
import {API_BASE_URL} from '../../config';
import * as tokenActionTypes from '../actions/carpooling/token';
import {willJWTExpire} from '../lib/tokenManagement';
import journeySaga from './carpooling/journey';
import journeyRequestSaga from './carpooling/journeyRequest';
import loginSaga from './carpooling/login';
import messageSaga from './carpooling/message';
import carpoolingNotifications from './carpooling/notifications';
import profileSaga from './carpooling/profile';
import searchSaga from './carpooling/search';
import tokenSaga from './carpooling/token';
import locationSaga from './main/location';
import notificationSaga from './main/notification';
import permissionSaga from './main/permission';
import poiSaga from './main/poi';
import reportingSaga from './main/reporting';
import settingsSaga from './main/settings';

export type Params = {
    [key: string]: any,
};

export const fetchFromApi = (uri: string, params: Params = {
    headers: {
        accept: 'application/ld+json',
    },
    method: 'GET',
}) => fetch(API_BASE_URL + uri, params);

export function* apiFetch(uri: string, params: Params) {
    const rawToken: string = yield call([AsyncStorage, 'getItem'], '@at:user_token');
    const token = JSON.parse(rawToken);

    if (token && willJWTExpire(token)) {
        const refresh = yield fetch(API_BASE_URL + '/api/token/refresh', {
            body: JSON.stringify({
                refresh_token: token.refresh_token,
            }),
            headers: {
                'authorization': 'Bearer ' + token.token,
                'content-type': 'application/json',
            },
            method: 'POST',
        });

        if (refresh) {
            const data = yield call([refresh, 'json']);
            yield AsyncStorage.setItem('@at:user_token', JSON.stringify(data));
        }
    }

    const res = yield call(fetchFromApi, uri, params);

    if (res.status === 401) {
        yield put(tokenActionTypes.setTokenValidity(false));
    }

    if (res.status < 200 || (res.status > 299 && res.status !== 400)) {
        return null;
    }

    return yield call([res, 'json']);
}

export default function* rootSaga() {
    yield all([
        carpoolingNotifications(),
        journeySaga(),
        journeyRequestSaga(),
        locationSaga(),
        loginSaga(),
        messageSaga(),
        notificationSaga(),
        permissionSaga(),
        poiSaga(),
        profileSaga(),
        reportingSaga(),
        searchSaga(),
        settingsSaga(),
        tokenSaga(),
    ]);
}
