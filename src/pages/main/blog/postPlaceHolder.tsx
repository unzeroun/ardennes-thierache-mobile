import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import PlaceHolder from '../../../components/main/PlaceHolder';
import styles from '../../../styles/main/pages/blog/postPlaceHolder';

export default function PostPlaceHolder() {
    return (
        <ScrollView contentContainerStyle={styles.container}>
            <Text style={styles.title}>ACTUS</Text>
            <View style={styles.separator}/>
            <View style={styles.content}>
                <PlaceHolder lines={2} type="text" outerStyle={styles.header}/>
            </View>
            <PlaceHolder lines={1} type="image" outerStyle={styles.imageContainer}/>
            <View style={styles.content}>
                <PlaceHolder lines={4} type="text" outerStyle={styles.textContainer}/>
            </View>
        </ScrollView>
    );
}
