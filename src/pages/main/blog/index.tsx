import React, {Fragment, useState} from 'react';
import {ScrollView, Text, View} from 'react-native';
import BlogPostListItem from '../../../components/main/blog/BlogPostListItem';
import LoadMore from '../../../components/main/LoadMore';
import PlaceHolder from '../../../components/main/PlaceHolder';
import withPosts, {WrappedProps} from '../../../concertosdk/blog/containers/withPosts';
import {OwnRouteComponentProps} from '../../../MainRouter';
import styles from '../../../styles/main/pages/blog/index';

type OuterProps = {
    first?: number,
};

type Props = WrappedProps & OuterProps;

export default function Component({first, location}: OuterProps & OwnRouteComponentProps) {

    const [showPlaceHolder, setShowPlaceHolder] = useState<boolean>(true);

    const BlogPostIndexComponent = withPosts(
        function BlogPostIndex({loadMore, posts}: Props) {

            setShowPlaceHolder(false);

            return (
                <ScrollView contentContainerStyle={styles.listContainer}>
                    {
                        posts.edges.map(({node: post}) => <BlogPostListItem key={post.id} post={post}/>)
                    }
                    {
                        loadMore && <LoadMore loadMore={loadMore} outerStyle={styles.loadMore}/>
                    }
                </ScrollView>
            );
        },
    );

    return (
        <Fragment>
            <Text style={styles.title}>ACTUS</Text>
            <View style={styles.separator}/>
            {
                showPlaceHolder && (
                    <ScrollView>
                        <PlaceHolder lines={5} type="image"/>
                    </ScrollView>
                )

            }
            <BlogPostIndexComponent first={first} categories_slug={location.state.slug}/>
        </Fragment>
    );
}
