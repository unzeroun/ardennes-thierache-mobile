import moment from 'moment';
import React, {useState} from 'react';
import {IsocontentNative} from 'react-isocontent-native';
import {ScrollView, Text, View} from 'react-native';
import {withRouter} from 'react-router-native';
import withPost, {PostProps} from '../../../concertosdk/blog/containers/withPost';
import Media from '../../../lib/media';
import {getMediaUrl} from '../../../lib/mediaHelper';
import {OwnRouteComponentProps} from '../../../MainRouter';
import styles from '../../../styles/main/pages/blog/post';
import PostPlaceHolder from './postPlaceHolder';

type Props = OwnRouteComponentProps;

export default withRouter(
    function BlogPost({location}: Props) {

        const [showPlaceHolder, setShowPlaceHolder] = useState<boolean>(true);

        const postId = location.state.id;

        const PostComponent = withPost(
            function Post({post}: PostProps) {
                const publishedAt = moment(post.publishedAt);

                setShowPlaceHolder(false);

                const parsedDescription = JSON.parse(post.description);
                const parsedBody = JSON.parse(post.body);

                return (
                    <ScrollView contentContainerStyle={styles.container}>
                        <Text style={styles.title}>ACTUS</Text>
                        <View style={styles.separator}/>
                        <View style={styles.content}>
                            <View style={styles.header}>
                                <Text style={styles.date}>{publishedAt.fromNow()},
                                    le {publishedAt.format('DD MMM')}</Text>
                                <Text style={styles.postTitle}>{post.title}</Text>
                            </View>
                        </View>
                        {
                            post.media && (
                                <View style={styles.imageContainer}>
                                    <Media mediaUrl={getMediaUrl(post.media)}/>
                                </View>
                            )
                        }
                        <View>
                            <View style={styles.textContainer}>
                                <IsocontentNative content={parsedDescription}/>
                                <IsocontentNative content={parsedBody}/>
                            </View>
                        </View>
                    </ScrollView>
                );
            },
        );

        return (
            <View>
                {
                    showPlaceHolder && <PostPlaceHolder/>
                }
                <PostComponent id={postId}/>
            </View>
        );
    },
);
