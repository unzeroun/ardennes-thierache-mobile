import {Camera} from 'expo-camera';
import {CapturedPicture} from 'expo-camera/build/Camera.types';
import React, {Fragment, useEffect, useState} from 'react';
import {ActivityIndicator, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {Link} from 'react-router-native';
import Redux, {Action, bindActionCreators} from 'redux';
import {CAMERA_RATIO} from '../../../../config';
import {getCameraPermission} from '../../../actions/main/permission';
import {resetReporting, takeReportPicture} from '../../../actions/main/reporting';
import {OwnRouteComponentProps} from '../../../MainRouter';
import {State} from '../../../reducers';
import styles from '../../../styles/main/pages/reporting/ReportCamera';

type StateProps = {
    cameraPermission: boolean,
    loading: boolean,
    reportingPicture: CapturedPicture | null,
};

type DispatchProps = {
    getCameraPermission: () => void,
    resetReporting: () => void,
    takeReportPicture: (camera: Camera) => void,
};

type Props = StateProps & DispatchProps & OwnRouteComponentProps;

function mapStateToProps(state: State): StateProps {
    return {
        cameraPermission: state.permission.cameraPermission,
        loading: state.reporting.loading,
        reportingPicture: state.reporting.reportingPicture,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        getCameraPermission,
        resetReporting,
        takeReportPicture,
    }, dispatch);
}

export default withRouter(
    connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
        function ReportingCamera({
                                     cameraPermission,
                                     getCameraPermission,
                                     history,
                                     loading,
                                     reportingPicture,
                                     resetReporting,
                                     takeReportPicture,
                                 }: Props) {
            const [camera, setCamera] = useState<Camera | null>(null);
            const [cameraHeight, setCameraHeight] = useState<number | null>(null);
            const [cameraSize, setCameraSize] = useState<string>('');
            const [canRedirect, setCanRedirect] = useState<boolean>(false);

            useEffect(() => {
                resetReporting();
                setCanRedirect(true);
                getCameraPermission();
            }, []);

            useEffect(() => {
                if (reportingPicture && canRedirect) {
                    history.push('/reporting/form');
                }
            }, [reportingPicture]);

            function onTakePicturePress() {
                if (camera) {
                    takeReportPicture(camera);
                }
            }

            function onCameraRef(camera: Camera | null) {
                setCamera(camera);
            }

            async function onCameraReady() {
                if (!camera) {
                    return;
                }

                const sizes = await camera.getAvailablePictureSizesAsync(CAMERA_RATIO);
                const size = sizes.reduce(
                    (memo, size) => {
                        if (!size.match(/\d+x\d+/)) {
                            return memo;
                        }

                        const [width] = size.split('x');
                        if (parseInt(width, 10) <= 1280 && parseInt(width, 10) >= 640) {
                            return size;
                        }

                        return memo;
                    },
                );

                setCameraSize(size);
            }

            function getCameraWidth(): number {
                if (!cameraHeight) {
                    return 0;
                }

                const heightRatio = parseInt(CAMERA_RATIO.split(':')[0], 10);
                const widthRatio = parseInt(CAMERA_RATIO.split(':')[1], 10);

                return cameraHeight * widthRatio / heightRatio;
            }

            if (!cameraPermission) {
                return (
                    <View style={styles.noCameraContainer}>
                        <Text  style={styles.noCameraLabel}>
                            Veuillez autoriser l'utilisation de la caméra afin d'ajouter une photo à votre
                            rapport
                        </Text>
                        <Link style={styles.noCameraLinkContainer} to="/reporting/form">
                            <Text style={styles.noCameraLinkLabel}>Passer</Text>
                        </Link>
                    </View>
                );
            }

            return (
                <View style={styles.container}>
                    <Fragment>
                        <Camera
                            ref={(ref) => onCameraRef(ref)}
                            onCameraReady={() => onCameraReady()}
                            onLayout={(event) => setCameraHeight(event.nativeEvent.layout.height)}
                            pictureSize={cameraSize}
                            style={[styles.camera, {width: getCameraWidth()}]}
                            ratio={CAMERA_RATIO}
                            type={Camera.Constants.Type.back}
                        />
                        <Link style={styles.linkContainer} to="/reporting/form">
                            <Text>Passer</Text>
                        </Link>
                        {
                            loading && (
                                <ActivityIndicator color="#5883bd" style={styles.activityIndicator}
                                                   size="large"/>
                            ) || (
                                <TouchableOpacity
                                    onPress={() => onTakePicturePress()}
                                    style={styles.snapButton}
                                />
                            )
                        }
                    </Fragment>
                </View>
            );
        },
    ),
);
