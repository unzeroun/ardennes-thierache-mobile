import {LocationObject} from 'expo-location';
import React, {Fragment, useEffect} from 'react';
import {Image, Text, TouchableHighlight, View} from 'react-native';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-native';
import Redux, {Action, bindActionCreators} from 'redux';
import {REPORT_TYPES, Type} from '../../../../config';
import {getCurrentPosition} from '../../../actions/main/location';
import {getLocationPermission} from '../../../actions/main/permission';
import {resetReporting, setReportingType} from '../../../actions/main/reporting';
import {OwnRouteComponentProps} from '../../../MainRouter';
import {State} from '../../../reducers';
import styles from '../../../styles/main/pages/reporting/ReportTypes';

type StateProps = {
    currentPosition: LocationObject | null,
};

type DispatchProps = {
    getCurrentPosition: () => void,
    getLocationPermission: () => void,
    resetReporting: () => void,
    setReportingType: (type: string) => void,
};

type Props = StateProps & DispatchProps & OwnRouteComponentProps;

function mapStateToProps(state: State): StateProps {
    return {
        currentPosition: state.location.currentPosition,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        getCurrentPosition,
        getLocationPermission,
        resetReporting,
        setReportingType,
    }, dispatch);
}

export default withRouter(
    connect<StateProps, DispatchProps, OwnRouteComponentProps, State>(mapStateToProps, mapDispatchToProps)(
        function ReportTypes({
                                 currentPosition,
                                 getCurrentPosition,
                                 getLocationPermission,
                                 history,
                                 resetReporting,
                                 setReportingType,
                             }: Props) {

            useEffect(() => {
                if (!currentPosition) {
                    getLocationPermission();
                }

                resetReporting();
            }, []);

            function onTypePress(type: Type) {
                getCurrentPosition();
                setReportingType(type.label);
                history.push('/reporting/camera');
            }

            return (
                <View style={styles.container}>
                    {
                        REPORT_TYPES.map((type: Type, index: number) => (
                            <TouchableHighlight
                                key={index}
                                style={styles.button}
                                onPress={() => onTypePress(type)}
                            >
                                <Fragment>
                                    <Image
                                        source={type.image}
                                        style={styles.image}
                                        resizeMode="contain"
                                    />
                                    <Text style={styles.label}>{type.label}</Text>
                                </Fragment>
                            </TouchableHighlight>
                        ))
                    }
                </View>
            );
        },
    ),
);
