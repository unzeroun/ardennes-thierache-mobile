import React from 'react';
import {Redirect, Route, Switch} from 'react-router-native';
import {OwnRouteComponentProps} from '../../../MainRouter';

type Props = OwnRouteComponentProps;

export default function Reporting({match}: Props) {
    return (
        <Switch>
            <Route exact path={match.url} render={() => <Redirect to="/reporting/types"/>}/>
        </Switch>
    );
}
