import {CapturedPicture} from 'expo-camera/build/Camera.types';
import {LocationObject} from 'expo-location';
import React, {useEffect} from 'react';
import {
    ActivityIndicator,
    Image,
    KeyboardAvoidingView,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {getCurrentPosition} from '../../../actions/main/location';
import {sendReport, setReportingMessage} from '../../../actions/main/reporting';
import {State} from '../../../reducers';
import styles from '../../../styles/main/pages/reporting/ReportForm';
import {Report} from '../../../types/types';

type StateProps = {
    currentPosition: LocationObject | null,
    reportingMessage: string,
    reportingPicture: CapturedPicture | null,
    reportingType: string | null,
    sendingReport: boolean,
};

type DispatchProps = {
    getCurrentPosition: () => void,
    sendReport: (report: Report) => void,
    setReportingMessage: (message: string) => void,
};

type Props = StateProps & DispatchProps;

function mapStateToProps(state: State): StateProps {
    return {
        currentPosition: state.location.currentPosition,
        reportingMessage: state.reporting.reportingMessage,
        reportingPicture: state.reporting.reportingPicture,
        reportingType: state.reporting.reportingType,
        sendingReport: state.reporting.sendingReport,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        getCurrentPosition,
        sendReport,
        setReportingMessage,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function ReportForm({
                            currentPosition,
                            getCurrentPosition,
                            reportingMessage,
                            reportingPicture,
                            reportingType,
                            setReportingMessage,
                            sendReport,
                            sendingReport,
                        }: Props) {

        useEffect(() => {
            getCurrentPosition();
        }, []);

        function onSendReport() {
            if (reportingType) {
                sendReport({
                    location: currentPosition ? {
                        latitude: currentPosition.coords.latitude,
                        longitude: currentPosition.coords.longitude,
                    } : null,
                    message: reportingMessage,
                    picture: reportingPicture ? {
                        base64: reportingPicture.base64,
                        path: reportingPicture.uri,
                    } : null,
                    type: reportingType,
                });
            }
        }

        return (
            <KeyboardAvoidingView style={styles.container}>
                <ScrollView>
                    <Text style={styles.title}>Envoyer un rapport</Text>
                    {
                        reportingPicture && <Image
                            resizeMode="contain"
                            style={styles.image}
                            source={{uri: reportingPicture.uri}}
                        />
                    }
                    <View>
                        <Text style={styles.inputLabel}>Message :</Text>
                        <TextInput
                            style={styles.input}
                            multiline={true}
                            onChangeText={(message: string) => setReportingMessage(message)}
                            value={reportingMessage}
                            placeholder="Entrez votre message ici"
                            placeholderTextColor="#FFF"
                            underlineColorAndroid="transparent"
                        />
                        {
                            currentPosition && (
                                <MapView
                                    style={styles.map}
                                    scrollEnabled={false}
                                    rotateEnabled={false}
                                    zoomEnabled={false}
                                    initialRegion={{
                                        latitude: currentPosition.coords.latitude,
                                        latitudeDelta: .005,
                                        longitude: currentPosition.coords.longitude,
                                        longitudeDelta: .005,
                                    }}
                                >
                                    <Marker
                                        coordinate={{
                                            latitude: currentPosition.coords.latitude,
                                            longitude: currentPosition.coords.longitude,
                                        }}>
                                        <Image
                                            source={require('../../../../assets/images/map_marker.png')}
                                            style={styles.marker}
                                        />
                                    </Marker>
                                </MapView>
                            )
                        }
                    </View>
                    {
                        sendingReport && (
                            <ActivityIndicator size="large" color="#2C3840"/>
                        ) || (
                            <TouchableOpacity style={styles.button} onPress={() => onSendReport()}>
                                <Text style={styles.buttonLabel}>Envoyer le rapport</Text>
                            </TouchableOpacity>
                        )
                    }
                </ScrollView>
            </KeyboardAvoidingView>
        );
    },
);
