import React from 'react';
import {ScrollView} from 'react-native';
import {withRouter} from 'react-router-native';
import DocumentListItem from '../../../../components/main/document/category/DocumentListItem';
import withDocumentCategory,
{DocumentCategoryProps} from '../../../../concertosdk/document/containers/withDocumentCategory';
import withDocuments, {WrappedProps} from '../../../../concertosdk/document/containers/withDocuments';
import {OwnRouteComponentProps} from '../../../../MainRouter';

type Props = OwnRouteComponentProps;

export default withRouter(
    function DocumentCategory({location}: Props) {
        const documentCategorySlug = location.state.slug;

        if (!documentCategorySlug) {
            return null;
        }

        const DocumentCategoryComponent = withDocumentCategory(
            function Category({documentCategory}: DocumentCategoryProps) {

                const Component = withDocuments(function DocumentList({documents}: WrappedProps) {
                    return (
                        <ScrollView>
                            {
                                documents.edges.map(({node: document}) => (
                                    <DocumentListItem key={document.id} document={document}/>
                                ))
                            }
                        </ScrollView>
                    );
                });

                return <Component category={documentCategory.id} order={{title: 'DESC'}}/>;
            },
        );

        return <DocumentCategoryComponent slug={documentCategorySlug}/>;
    },
);
