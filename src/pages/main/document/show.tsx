import React from 'react';
import {IsocontentNative} from 'react-isocontent-native';
import {ScrollView, Text} from 'react-native';
import {withRouter} from 'react-router';
import DocumentFileLayout from '../../../components/main/document/DocumentFileLayout';
import withDocument, {DocumentProps} from '../../../concertosdk/document/containers/withDocument';
import {OwnRouteComponentProps} from '../../../MainRouter';
import styles from '../../../styles/main/pages/document/show';

type Props = OwnRouteComponentProps;

export default withRouter(
    function Document({location}: Props) {
        const documentSlug = location.state.slug;

        if (!documentSlug) {
            return null;
        }

        const DocumentComponent = withDocument(
            function Document({document}: DocumentProps) {
                const parsedDescription = JSON.parse(document.description);
                return (
                    <ScrollView contentContainerStyle={styles.container}>
                        <Text style={styles.title}>{document.title}</Text>
                        <IsocontentNative content={parsedDescription} />
                        {
                            document.files.edges.map(({node: documentFile}) => (
                                <DocumentFileLayout key={documentFile.id} documentFile={documentFile}/>
                            ))
                        }
                    </ScrollView>
                );
            },
        );

        return <DocumentComponent slug={documentSlug}/>;
    },
);
