import React from 'react';
import {ScrollView, Text} from 'react-native';
import {withRouter} from 'react-router-native';
import PageBlock from '../../../components/main/core/PageBlock';
import withPage, {MenuPageProps} from '../../../concertosdk/core/containers/withPage';
import {OwnRouteComponentProps} from '../../../MainRouter';
import styles from '../../../styles/main/pages/core/page';

type Props = OwnRouteComponentProps;

export default withRouter(
    function MenuPage({location}: Props) {
        const pageSlug = location.state.slug;

        if (!pageSlug) {
            return null;
        }

        const PageComponent = withPage(
            function Page({page}: MenuPageProps) {
                return (
                    <ScrollView contentContainerStyle={styles.container}>
                        <Text style={styles.title}>{page.title}</Text>

                        {
                            page.blocks.edges.map(({node: block}) => (
                                <PageBlock
                                    key={block.id}
                                    block={block}
                                    imageResizeMode="contain"
                                    imageStyle={styles.imageBlock}
                                />
                            ))
                        }
                    </ScrollView>
                );
            },
        );

        return <PageComponent slug={pageSlug}/>;
    },
);
