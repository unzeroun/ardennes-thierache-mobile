import React, {useEffect, useState} from 'react';
import {Animated, Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {setPoiPanelExpansion} from '../../../actions/main/poi';
import Media from '../../../lib/media';
import {isIOS} from '../../../lib/Platform';
import {State} from '../../../reducers';
import styles from '../../../styles/main/pages/poi/PoiPanel';
import {Poi} from '../../../types/types';

type StateProps = {
    poiPanelExpanded: boolean,
};

type DispatchProps = {
    setPoiPanelExpansion: (expanded: boolean) => void,
};

type OwnProps = {
    offset: number,
    poi: Poi,
};

type Props = StateProps & DispatchProps & OwnProps;

function mapStateToProps(state: State): StateProps {
    return {
        poiPanelExpanded: state.poi.poiPanelExpanded,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        setPoiPanelExpansion,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, OwnProps, State>(mapStateToProps, mapDispatchToProps)(
    function PoiPanel({offset, poi, poiPanelExpanded, setPoiPanelExpansion}: Props) {

        const [position] = useState<Animated.Value>(new Animated.Value(isIOS ? 0 : offset));

        useEffect(() => {
            if (poiPanelExpanded) {
                Animated.timing(position, {
                    duration: 500,
                    toValue: 0,
                    useNativeDriver: !isIOS,
                }).start();
            } else {
                Animated.timing(position, {
                    duration: 500,
                    toValue: isIOS ? -offset : offset,
                    useNativeDriver: !isIOS,
                }).start();
            }
        }, [poiPanelExpanded]);

        const {
            address1,
            address2,
            postCode,
            city,
            phoneNumber,
            mobilePhone,
        } = poi.additionalData.contacts[0].addresses[0];

        return (
            <Animated.View style={[styles.container, isIOS ? {height: offset, bottom: position}
                                                           : {height: offset, translateY: position}]}>
                <ScrollView>
                    <View style={styles.headerExpandButtonContainer}>
                        <TouchableOpacity
                            style={styles.headerExpandButton}
                            onPress={() => setPoiPanelExpansion(false)}
                        >
                            <Image
                                source={require('../../../../assets/images/arrow_up.png')}
                                style={styles.arrow}
                            />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.poiName}>{poi.name}</Text>
                    <ScrollView horizontal>
                        {
                            poi.content.blocks.map((media, index) => (
                                <View key={index} style={styles.mediaContainer}>
                                    <Media mediaUrl={{type: 'image', url: media.path}}/>
                                </View>
                            ))
                        }
                    </ScrollView>
                    <Text style={styles.poiDescription}>{poi.description}</Text>
                    <View>
                        {
                            address1 && <Text style={styles.additionalData}>{address1}</Text>
                        }
                        {
                            address2 && <Text style={styles.additionalData}>{address2}</Text>
                        }
                        <View style={styles.city}>
                            {
                                postCode && <Text style={styles.additionalData}>{postCode}</Text>
                            }
                            {
                                city && <Text style={styles.additionalData}>{city}</Text>
                            }
                        </View>
                        {
                            phoneNumber && <Text style={styles.additionalData}>{phoneNumber}</Text>
                        }
                        {
                            mobilePhone && <Text style={styles.additionalData}>{mobilePhone}</Text>
                        }
                    </View>
                </ScrollView>
            </Animated.View>
        );
    },
);
