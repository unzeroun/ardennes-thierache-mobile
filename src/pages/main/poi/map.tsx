import {LocationObject} from 'expo-location';
import React, {Fragment, useEffect, useState} from 'react';
import {Animated, Image, TouchableOpacity, View} from 'react-native';
import MapView, {Marker, Region} from 'react-native-maps';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {getCurrentPosition} from '../../../actions/main/location';
import {getLocationPermission} from '../../../actions/main/permission';
import {requestPois, setCurrentPoi, setPoiPanelExpansion} from '../../../actions/main/poi';
import {State} from '../../../reducers';
import styles from '../../../styles/main/pages/poi/map';
import {Poi} from '../../../types/types';
import PoiPanel from './PoiPanel';
import PoiPanelHeader from './PoiPanelHeader';

const initialRegion: Region = {
    latitude: 49.8790905701822,
    latitudeDelta: 0.09306744311047055,
    longitude: 4.395748843967677,
    longitudeDelta: 0.09910178768180344,
};

type StateProps = {
    currentPosition: LocationObject | null,
    currentPoi: Poi | null,
    locationPermission: boolean,
    mapLoading: boolean,
    pois: Poi[],
};

type DispatchProps = {
    getCurrentPosition: () => void,
    getLocationPermission: () => void,
    requestPois: () => void,
    setCurrentPoi: (poi: Poi | null) => void,
    setPoiPanelExpansion: (expanded: boolean) => void,
};

type Props = DispatchProps & StateProps;

function mapStateToProps(state: State): StateProps {
    return {
        currentPoi: state.poi.currentPoi,
        currentPosition: state.location.currentPosition,
        locationPermission: state.permission.locationPermission,
        mapLoading: state.poi.loading,
        pois: state.poi.pois,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        getCurrentPosition,
        getLocationPermission,
        requestPois,
        setCurrentPoi,
        setPoiPanelExpansion,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function Map({
                     currentPosition,
                     currentPoi,
                     getCurrentPosition,
                     getLocationPermission,
                     locationPermission,
                     pois,
                     requestPois,
                     setCurrentPoi,
                     setPoiPanelExpansion,
                 }: Props) {

        const [mapRef, setMapRef] = useState<MapView | null>(null);
        const [mapHeight, setMapHeight] = useState<number | null>(null);
        const [dotCirclePosition] = useState<Animated.Value>(new Animated.Value(10));

        useEffect(() => {
            setCurrentPoi(null);
            requestPois();
            getLocationPermission();
            setPoiPanelExpansion(false);
        }, []);

        useEffect(() => {
            if (locationPermission) {
                getCurrentPosition();
            }
        }, [locationPermission]);

        useEffect(() => {
            setTimeout(() => {
                if (currentPoi) {
                    Animated.timing(dotCirclePosition, {
                        duration: 250,
                        toValue: 150,
                        useNativeDriver: true,
                    }).start();
                } else {
                    Animated.timing(dotCirclePosition, {
                        duration: 250,
                        toValue: 10,
                        useNativeDriver: true,
                    }).start();
                }
            }, 410);
        }, [currentPoi]);

        function updateMapRegion(region: Region) {
            if (!mapRef) {
                return;
            }

            mapRef.animateToRegion(region, 250);
        }

        function onMarkerPressed(poi: Poi) {
            updateMapRegion({
                latitude: poi.position.latitude,
                latitudeDelta: .05,
                longitude: poi.position.longitude,
                longitudeDelta: .05,
            });
            setCurrentPoi(poi);
        }

        function onCurrentLocationPress() {
            if (currentPosition) {
                updateMapRegion({
                    latitude: currentPosition.coords.latitude,
                    latitudeDelta: .02,
                    longitude: currentPosition.coords.longitude,
                    longitudeDelta: .02,
                });
            }
        }

        return (
            <View>
                <MapView
                    style={styles.map}
                    initialRegion={initialRegion}
                    onLayout={(event) => setMapHeight(event.nativeEvent.layout.height)}
                    ref={(ref) => setMapRef(ref)}
                >
                    {
                        currentPosition && (
                            <Marker
                                coordinate={{
                                    latitude: currentPosition.coords.latitude,
                                    longitude: currentPosition.coords.longitude,
                                }}
                            >
                                <Image
                                    source={require('../../../../assets/images/map_location.png')}
                                    style={styles.icon}
                                />
                            </Marker>
                        )
                    }
                    {
                        pois.map(poi => (
                            <Marker
                                key={poi.id}
                                coordinate={poi.position}
                                onPress={() => onMarkerPressed(poi)}
                            >
                                <Image
                                    source={require('../../../../assets/images/map_marker.png')}
                                    style={styles.icon}
                                />
                            </Marker>
                        ))
                    }
                </MapView>
                {
                    currentPosition && (
                        <Animated.View style={[styles.dotCircleContainer, {translateY: dotCirclePosition}]}>
                            <TouchableOpacity onPress={() => onCurrentLocationPress()}>
                                <Image
                                    source={require('../../../../assets/images/dot_circle.png')}
                                    style={styles.dotCircle}
                                />
                            </TouchableOpacity>
                        </Animated.View>
                    )
                }
                {
                    currentPoi && mapHeight && (
                        <Fragment>
                            <PoiPanelHeader poi={currentPoi}/>
                            <PoiPanel offset={mapHeight} poi={currentPoi}/>
                        </Fragment>
                    )
                }
            </View>
        );
    },
);
