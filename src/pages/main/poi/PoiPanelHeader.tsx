import {LocationObject} from 'expo-location';
import React, {useEffect, useState} from 'react';
import {Animated, Image, Linking, Platform, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {setPoiPanelExpansion} from '../../../actions/main/poi';
import {isIOS} from '../../../lib/Platform';
import {truncate} from '../../../lib/textUtils';
import {State} from '../../../reducers';
import styles from '../../../styles/main/pages/poi/PoiPanelHeader';
import {Poi} from '../../../types/types';

type StateProps = {
    currentPosition: LocationObject | null,
    poiPanelExpanded: boolean,
};

type DispatchProps = {
    setPoiPanelExpansion: (expanded: boolean) => void,
};

type OwnProps = {
    poi: Poi,
};

type Props = StateProps & DispatchProps & OwnProps;

function mapStateToProps(state: State): StateProps {
    return {
        currentPosition: state.location.currentPosition,
        poiPanelExpanded: state.poi.poiPanelExpanded,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        setPoiPanelExpansion,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, OwnProps, State>(mapStateToProps, mapDispatchToProps)(
    function PoiPanelHeader({currentPosition, poi, poiPanelExpanded, setPoiPanelExpansion}: Props) {

        const [bottomPosition] = useState<Animated.Value>(new Animated.Value(0));

        useEffect(() => {
            if (poiPanelExpanded) {
                Animated.timing(bottomPosition, {
                    duration: 250,
                    toValue: isIOS ? -140 : 140,
                    useNativeDriver: !isIOS,
                }).start();
            } else {
                setTimeout(() => {
                    Animated.timing(bottomPosition, {
                        duration: 250,
                        toValue: 0,
                        useNativeDriver: !isIOS,
                    }).start();
                }, 400);
            }
        }, [poiPanelExpanded]);

        function onRoutePress() {
            if (currentPosition) {
                const sourceParams = `?saddr=${currentPosition.coords.latitude},${currentPosition.coords.longitude}`;
                const destinationParams = `&daddr=${poi.position.latitude},${poi.position.longitude}`;

                if (Platform.OS === 'ios') {
                    Linking.openURL(`http://maps.apple.com/${sourceParams}${destinationParams}`);
                } else {
                    Linking.openURL(`http://maps.google.com/maps/${sourceParams}${destinationParams}`);
                }
            }
        }

        return (
            <Animated.View style={[styles.header, isIOS ? {bottom: bottomPosition}
                                                        : {translateY: bottomPosition}]}>
                <View style={styles.headerExpandButtonContainer}>
                    <TouchableOpacity
                        style={styles.headerExpandButton}
                        onPress={() => setPoiPanelExpansion(true)}
                    >
                        <Image
                            source={require('../../../../assets/images/arrow_up.png')}
                            style={styles.arrow}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.contentContainer}>
                    <View style={styles.textContainer}>
                        <Text style={styles.headerLabel}>{poi.name}</Text>
                        {
                            poi.description.length > 0 && (
                                <Text style={styles.headerDescription}>{truncate(poi.description, 100)}</Text>
                            )
                        }
                    </View>
                    {
                        currentPosition && (
                            <TouchableOpacity onPress={() => onRoutePress()} style={styles.carButton}>
                                <Image
                                    source={require('../../../../assets/images/car_icon.png')}
                                    style={styles.carIcon}
                                />
                            </TouchableOpacity>
                        )
                    }
                </View>
            </Animated.View>
        );
    },
);
