import React, {Fragment, useState} from 'react';
import {ScrollView, Text, View} from 'react-native';
import EventListItem from '../../../components/main/calendar/EventListItem';
import LoadMore from '../../../components/main/LoadMore';
import PlaceHolder from '../../../components/main/PlaceHolder';
import withEvents, {WrappedProps} from '../../../concertosdk/calendar/containers/withEvents';
import styles from '../../../styles/main/pages/calendar/index';

type OuterProps = {
    first?: number,
};

type Props = WrappedProps & OuterProps;

export default function Component({first}: OuterProps) {

    const [showPlaceHolder, setShowPlaceHolder] = useState<boolean>(true);

    const CalendarEventIndexComponent = withEvents(
        function CalendarEventIndex({events, loadMore}: Props) {

            setShowPlaceHolder(false);

            return (
                <ScrollView contentContainerStyle={styles.content}>
                    {
                        events.edges.map(({node: event}) => <EventListItem key={event.id} event={event}/>)
                    }
                    {
                        loadMore && <LoadMore loadMore={loadMore} outerStyle={styles.loadMore}/>
                    }
                </ScrollView>
            );
        },
    );

    return (
        <Fragment>
            <Text style={styles.title}>A VENIR</Text>
            <View style={styles.separator}/>
            {
                showPlaceHolder && (
                    <ScrollView>
                        <PlaceHolder lines={5} type="image"/>
                    </ScrollView>
                )

            }
            <CalendarEventIndexComponent first={first}/>
        </Fragment>
    );
}
