import React, {Fragment, useState} from 'react';
import {IsocontentNative} from 'react-isocontent-native';
import {Image, ScrollView, Text, View} from 'react-native';
import MapView, {Marker, Region} from 'react-native-maps';
import {withRouter} from 'react-router';
import withEvent, {EventProps} from '../../../concertosdk/calendar/containers/withEvent';
import {dateWithHour} from '../../../lib/date';
import Media from '../../../lib/media';
import {getMediaUrl} from '../../../lib/mediaHelper';
import {OwnRouteComponentProps} from '../../../MainRouter';
import styles from '../../../styles/main/pages/calendar/event';
import EventPlaceHolder from './eventPlaceHolder';

type Props = OwnRouteComponentProps;

export default withRouter(
    function CalendarEvent({location}: Props) {

        const [showPlaceHolder, setShowPlaceHolder] = useState<boolean>(true);

        const eventSlug = location.state.slug;

        const EventComponent = withEvent(
            function Event({event}: EventProps) {

                setShowPlaceHolder(false);

                const {place, contact_name, contact_phone, contact_email} = event.content;
                const eventRegion: Region = {
                    latitude: event.latitude,
                    latitudeDelta: .005,
                    longitude: event.longitude,
                    longitudeDelta: .005,
                };

                return (
                    <ScrollView contentContainerStyle={styles.container}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.title}>A VENIR</Text>
                        </View>
                        <View style={styles.separator}/>
                        <View style={styles.content}>
                            <Text style={styles.eventTitle}>{event.title}</Text>
                        </View>
                        {
                            event.media && (
                                <View style={styles.imageContainer}>
                                    <Media mediaUrl={getMediaUrl(event.media)}/>
                                </View>
                            )
                        }
                        <View>
                            <View style={styles.textContainer}>
                                <IsocontentNative content={event.description}/>
                                <Text style={styles.date}>{dateWithHour({begin: event.begin, end: event.end})}</Text>
                                {
                                    place && (
                                        <Text style={styles.place}>{place}</Text>
                                    )
                                }
                                {
                                    (contact_name || contact_phone || contact_email) && (
                                        <Fragment>
                                            <Text style={styles.contactLabel}>Contact :</Text>
                                            <View>
                                                {
                                                    contact_name &&
                                                    <Text style={styles.organizerData}>{contact_name}</Text>
                                                }
                                                {
                                                    contact_phone &&
                                                    <Text style={styles.organizerData}>{contact_phone}</Text>
                                                }
                                                {
                                                    contact_email &&
                                                    <Text style={styles.organizerData}>{contact_email}</Text>
                                                }
                                            </View>
                                        </Fragment>
                                    )
                                }
                            </View>
                        </View>
                        {
                            event.latitude && event.longitude && (
                                <MapView
                                    scrollEnabled={false}
                                    rotateEnabled={false}
                                    zoomEnabled={false}
                                    style={styles.map}
                                    initialRegion={eventRegion}
                                >
                                    <Marker
                                        coordinate={{
                                            latitude: event.latitude,
                                            longitude: event.longitude,
                                        }}>
                                        <Image
                                            source={require('../../../../assets/images/map_marker.png')}
                                            style={styles.marker}
                                        />
                                    </Marker>
                                </MapView>
                            )
                        }
                    </ScrollView>
                );
            },
        );

        return (
            <Fragment>
                {
                    showPlaceHolder && <EventPlaceHolder/>
                }
                <EventComponent slug={eventSlug}/>
            </Fragment>
        );
    },
);
