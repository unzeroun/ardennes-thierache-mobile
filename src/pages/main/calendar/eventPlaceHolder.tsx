import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import PlaceHolder from '../../../components/main/PlaceHolder';
import styles from '../../../styles/main/pages/calendar/eventPlaceHolder';

export default function CalendarEvent() {

    return (
        <ScrollView contentContainerStyle={styles.container}>
            <View style={styles.titleContainer}>
                <Text style={styles.title}>A VENIR</Text>
            </View>
            <View style={styles.separator}/>
            <View style={styles.content}>
                <PlaceHolder lines={2} type="text" outerStyle={styles.titlePlaceholder}/>
            </View>
            <PlaceHolder lines={1} type="image" outerStyle={styles.imageContainer}/>
            <View style={styles.content}>
                <PlaceHolder lines={4} type="text" outerStyle={styles.textPlaceholder}/>
                <PlaceHolder lines={1} type="image" outerStyle={styles.mapPlaceholder}/>
            </View>
        </ScrollView>
    );
}
