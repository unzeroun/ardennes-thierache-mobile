import React from 'react';
import {View} from 'react-native';
import NotificationList from '../../components/main/settings/NotificationList';
import styles from '../../styles/main/pages/Settings';

export default function Settings() {

    return (
        <View style={styles.container}>
            <NotificationList/>
        </View>
    );
}
