import React from 'react';
import {ScrollView} from 'react-native';
import HomepageBlogPostListBlock from '../../components/main/homepage/HomepageBlogPostListBlock';
import HomepageFlashInfoBlock from '../../components/main/homepage/HomepageFlashInfoBlock';
import HomepageSocialLinksBlock from '../../components/main/homepage/HomepageSocialLinksBlock';
import HomepageUpcomingEventsBlock from '../../components/main/homepage/HomepageUpcomingEventsBlock';

export default function Home() {

    return (
        <ScrollView>
            <HomepageFlashInfoBlock first={3}/>
            <HomepageSocialLinksBlock/>
            <HomepageUpcomingEventsBlock first={15}/>
            <HomepageBlogPostListBlock first={15}/>
        </ScrollView>
    );
}
