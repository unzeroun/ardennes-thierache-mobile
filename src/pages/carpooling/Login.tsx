import {Linking} from 'expo';
import React, {useEffect, useState} from 'react';
import {
    Image, KeyboardAvoidingView, ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {requestCodeVerification, requestLoginRequest} from '../../actions/carpooling/login';
import {getToken} from '../../actions/carpooling/token';
import {setAppSwitchingModalVisibility, setMainRoute} from '../../actions/routing';
import AppSwitchingModal from '../../components/AppSwitchingModal';
import LoadingComponent from '../../components/main/LoadingComponent';
import {isJWTValid} from '../../lib/tokenManagement';
import TopOval from '../../lib/TopOval';
import {OwnRouteComponentProps} from '../../MainRouter';
import {State} from '../../reducers';
import styles from '../../styles/carpooling/pages/Login';
import {Token} from '../../types/types';

type StateProps = {
    loading: boolean,
    requestedLogin: boolean,
    token: Token | null,
};

type DispatchProps = {
    getToken: () => void,
    requestLoginRequest: (email: string) => void,
    requestCodeVerification: (code: string, email?: string) => void,
    setAppSwitchingModalVisibility: (visibility: boolean) => void,
    setMainRoute: () => void,
};

type Props = StateProps & DispatchProps & OwnRouteComponentProps;

function mapStateToProps(state: State): StateProps {
    return {
        loading: state.login.loading,
        requestedLogin: state.login.requestedLogin,
        token: state.token.token,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        getToken,
        requestCodeVerification,
        requestLoginRequest,
        setAppSwitchingModalVisibility,
        setMainRoute,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function Login({
                       getToken,
                       history,
                       loading,
                       requestedLogin,
                       requestLoginRequest,
                       requestCodeVerification,
                       setAppSwitchingModalVisibility,
                       setMainRoute,
                       token,
                   }: Props) {

        const [emailInput, setEmailInput] = useState<string>('');
        const [verificationCode, setVerificationCode] = useState<string>('');

        useEffect(() => {
            Linking.addEventListener('url', (payload: { [key: string]: string }) => {
                if (payload.url) {
                    const {queryParams} = Linking.parse(payload.url);

                    if (Object.keys(queryParams).length > 0) {
                        onMagicLink(queryParams);
                    }
                }
            });

            Linking.getInitialURL().then((url: string) => {
                if (url) {
                    const {queryParams} = Linking.parse(url);

                    if (Object.keys(queryParams).length > 0) {
                        onMagicLink(queryParams);
                    }
                }
            });
        }, []);

        useEffect(() => {
            getToken();
        }, []);

        useEffect(() => {
            if (token && isJWTValid(token)) {
                history.replace('/routes');
            }
        }, [token]);

        function onMagicLink(params: { [key: string]: string }) {
            requestCodeVerification(params.code, params.email);
        }

        function onQuitApp() {
            setMainRoute();
        }

        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <ScrollView keyboardShouldPersistTaps="handled" bounces={false}>
                    <View style={styles.header}>
                        <View style={styles.logoContainer}>
                            <Image
                                source={require('../../../assets/images/logo.png')}
                                style={styles.logo}
                                resizeMode="contain"
                            />
                            <Image
                                source={require('../../../assets/images/login_baseline.png')}
                                style={styles.baseline}
                                resizeMode="contain"
                            />
                        </View>
                    </View>
                    <TopOval backgroundColor="#F0BE2B"/>
                    <View style={styles.content}>
                        <Text style={styles.label}>VOTRE MAIL</Text>
                        <View style={styles.separator}/>
                        <TextInput
                            autoCapitalize="none"
                            autoCompleteType="off"
                            autoCorrect={false}
                            value={emailInput}
                            keyboardType="email-address"
                            multiline={true}
                            placeholder="?"
                            onChangeText={(input: string) => setEmailInput(input)}
                            textAlignVertical="center"
                            style={[styles.input, requestedLogin ? styles.requestedInput : {}]}
                            editable={!requestedLogin}
                        />
                        {
                            !requestedLogin && (
                                loading && <LoadingComponent/> || (
                                    <TouchableOpacity
                                        style={styles.submit}
                                        onPress={() => requestLoginRequest(emailInput)}
                                    >
                                        <Text style={styles.submitLabel}>SE CONNECTER</Text>
                                    </TouchableOpacity>
                                )
                            ) || (
                                <View style={styles.confirmContainer}>
                                    <Text style={styles.confirmLabel}>Un code de confirmation vous a été envoyé</Text>
                                    <TextInput
                                        autoCapitalize="none"
                                        placeholder="Code de vérification"
                                        onChangeText={(input) => setVerificationCode(input)}
                                        style={styles.confirmInput}
                                    />
                                    <View style={styles.inputUnderline}/>
                                    {
                                        loading && <LoadingComponent/> || (
                                            <TouchableOpacity
                                                onPress={() => requestCodeVerification(verificationCode)}
                                                style={styles.submit}
                                            >
                                                <Text style={styles.submitLabel}>Confirmer le code</Text>
                                            </TouchableOpacity>
                                        )
                                    }
                                </View>
                            )
                        }
                    </View>
                    <TouchableOpacity
                        style={styles.submit}
                        onPress={() => setAppSwitchingModalVisibility(true)}
                    >
                        <Text style={styles.submitLabel}>RETOUR</Text>
                    </TouchableOpacity>
                </ScrollView>
                <AppSwitchingModal
                    callback={() => onQuitApp()}
                    text="Souhaitez vous revenir sur l'application Ardennes Thiérache ?"
                />
            </KeyboardAvoidingView>
        );
    },
);
