import {Picker} from '@react-native-picker/picker';
import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {
    Image, KeyboardAvoidingView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {resetUserProfileRequestState, submitUserProfileUpdate} from '../../../actions/carpooling/profile';
import {setCloseButtonStyle, setCloseButtonVisibility} from '../../../actions/carpooling/ui';
import LoadingComponent from '../../../components/main/LoadingComponent';
import FormPicker from '../../../lib/FormPicker';
import TopOval from '../../../lib/TopOval';
import WithErrorComponent from '../../../lib/withErrorComponent';
import {OwnRouteComponentProps} from '../../../MainRouter';
import {State} from '../../../reducers';
import {CLOSE_BUTTON_STYLES} from '../../../reducers/carpooling/ui';
import styles from '../../../styles/carpooling/pages/profile/User';
import {ErrorMap, User} from '../../../types/types';

type StateProps = {
    errorMap: ErrorMap,
    loading: boolean,
    user: User | null,
    userProfileUpdated: boolean,
};

type DispatchProps = {
    setCloseButtonStyle: (style: CLOSE_BUTTON_STYLES) => void,
    setCloseButtonVisibility: (visibility: boolean) => void,
    submitUserProfileUpdate: (user: User) => void,
    resetUserProfileRequestState: () => void,
};

type Props = StateProps & DispatchProps & OwnRouteComponentProps;

function mapStateToProps(state: State): StateProps {
    return {
        errorMap: state.misc.errorMap,
        loading: state.profile.loading,
        user: state.profile.user,
        userProfileUpdated: state.profile.userProfileUpdated,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        resetUserProfileRequestState,
        setCloseButtonStyle,
        setCloseButtonVisibility,
        submitUserProfileUpdate,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function User({
                      errorMap,
                      history,
                      loading,
                      resetUserProfileRequestState,
                      setCloseButtonStyle,
                      setCloseButtonVisibility,
                      submitUserProfileUpdate,
                      user,
                      userProfileUpdated,
                  }: Props) {

        useEffect(() => {
            setCloseButtonVisibility(true);
            setCloseButtonStyle(CLOSE_BUTTON_STYLES.WHITE);

            return () => setCloseButtonVisibility(false);
        }, []);

        useEffect(() => {
            if (!loading && userProfileUpdated) {
                resetUserProfileRequestState();

                if (hasDrivingLicense) {
                    history.push('/profile/car');
                } else {
                    history.push('/routes');
                }
            }
        }, [loading]);

        useEffect(() => {
            resetUserProfileRequestState();
            if (user) {
                setLastName(user.lastName);
                setFirstName(user.firstName);
                setBirthDate(user.birthDate);
                setAddress1(user.address1);
                setAddress2(user.address2);
                setAddress3(user.address3);
                setPostCode(user.postCode);
                setCity(user.city);
                setPhoneNumber(user.phoneNumber);
                setHasDrivingLicense(user.hasDrivingLicense);
                setDrivingLicenseDate(user.drivingLicenseDate);
                setGender(user.gender);
                setDescription(user.description);
            }
        }, []);

        const [lastName, setLastName] = useState<string>('');
        const [firstName, setFirstName] = useState<string>('');
        const [birthDate, setBirthDate] = useState<string>(moment().toISOString());
        const [address1, setAddress1] = useState<string>('');
        const [address2, setAddress2] = useState<string>('');
        const [address3, setAddress3] = useState<string>('');
        const [postCode, setPostCode] = useState<string>('');
        const [city, setCity] = useState<string>('');
        const [phoneNumber, setPhoneNumber] = useState<string>('');
        const [hasDrivingLicense, setHasDrivingLicense] = useState<boolean>(false);
        const [drivingLicenseDate, setDrivingLicenseDate] = useState<string>(moment().toISOString());
        const [gender, setGender] = useState<string>('F');
        const [description, setDescription] = useState<string>('');

        function onSubmit() {
            submitUserProfileUpdate({
                address1,
                address2,
                address3,
                birthDate,
                city,
                description,
                devicesIdentifiers: user ? user.devicesIdentifiers : [],
                drivingLicenseDate,
                email: '',
                firstName,
                gender,
                hasDrivingLicense,
                iri: '',
                lastName,
                phoneNumber,
                postCode,
            });
        }

        type GenderMapType = {
            [key: string]: string,
        };

        const genderMap: GenderMapType = {
            F: 'Femme',
            H: 'Homme',
        };

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerLabel}>VOTRE PROFIL</Text>
                </View>
                <TopOval backgroundColor="#F3C045" color="#404B52"/>
                <KeyboardAvoidingView style={{flex: 1}} behavior="padding" keyboardVerticalOffset={130}>
                    <View
                        style={styles.scrollContainer}
                    >
                        <View style={styles.nameContainer}>
                            <WithErrorComponent error={errorMap.firstName}>
                                <View style={[styles.inputContainer, styles.inputNameContainer]}>
                                    <TextInput
                                        value={firstName}
                                        onChangeText={(input) => setFirstName(input)}
                                        placeholder="Prénom"
                                        placeholderTextColor="#DCE7EE"
                                        style={styles.nameLabel}
                                    />
                                </View>
                            </WithErrorComponent>
                            <WithErrorComponent error={errorMap.lastName}>
                                <View style={[styles.inputContainer, styles.inputNameContainer]}>
                                    <TextInput
                                        value={lastName}
                                        onChangeText={(input) => setLastName(input)}
                                        placeholder="Nom"
                                        placeholderTextColor="#DCE7EE"
                                        style={[styles.nameLabel, styles.nameOffset]}
                                    />
                                </View>
                            </WithErrorComponent>
                            <Image
                                style={styles.editIcon}
                                source={require('../../../../assets/images/edit_pencil.png')}
                                resizeMode="contain"
                            />
                        </View>
                        <WithErrorComponent error={errorMap.birthDate}>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>DATE DE NAISSANCE</Text>
                                <View style={styles.separator}/>
                                <DatePicker
                                    date={moment(birthDate)}
                                    onDateChange={(_, date) => setBirthDate(moment(date).toISOString())}
                                    showIcon={false}
                                    customStyles={{
                                        dateInput: styles.dateContainer,
                                        dateText: styles.dateInput,
                                    }}
                                    locale="fr"
                                    format="DD-MM-YYYY"
                                    confirmBtnText="Valider"
                                    cancelBtnText="Annuler"
                                />
                            </View>
                        </WithErrorComponent>
                        <WithErrorComponent error={errorMap.address1}>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>ADRESSE</Text>
                                <View style={styles.separator}/>
                                <TextInput
                                    value={address1}
                                    multiline={true}
                                    onChangeText={(input) => setAddress1(input)}
                                    placeholder="?"
                                    placeholderTextColor="#DCE7EE"
                                    style={styles.input}
                                />
                            </View>
                        </WithErrorComponent>
                        <WithErrorComponent error={errorMap.postCode}>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>CODE POSTAL</Text>
                                <View style={styles.separator}/>
                                <TextInput
                                    keyboardType="number-pad"
                                    value={postCode}
                                    onChangeText={(input) => setPostCode(input)}
                                    placeholder="?"
                                    placeholderTextColor="#DCE7EE"
                                    style={styles.input}
                                />
                            </View>
                        </WithErrorComponent>
                        <WithErrorComponent error={errorMap.city}>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>VILLE</Text>
                                <View style={styles.separator}/>
                                <TextInput
                                    value={city}
                                    onChangeText={(input) => setCity(input)}
                                    placeholder="?"
                                    placeholderTextColor="#DCE7EE"
                                    style={styles.input}
                                />
                            </View>
                        </WithErrorComponent>
                        <WithErrorComponent error={errorMap.phoneNumber}>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>TÉLÉPHONE</Text>
                                <View style={styles.separator}/>
                                <TextInput
                                    value={phoneNumber}
                                    keyboardType="number-pad"
                                    onChangeText={(input) => setPhoneNumber(input)}
                                    placeholder="?"
                                    placeholderTextColor="#DCE7EE"
                                    style={styles.input}
                                />
                            </View>
                        </WithErrorComponent>
                        <View style={styles.inputContainer}>
                            <Text style={styles.inputLabel}>PERMIS</Text>
                            <View style={styles.separator}/>
                            <TouchableOpacity
                                onPress={() => setHasDrivingLicense(!hasDrivingLicense)}
                            >
                                <Text style={styles.input}>{hasDrivingLicense ? 'OUI' : 'NON'}</Text>
                            </TouchableOpacity>
                        </View>
                        {
                            hasDrivingLicense && (
                                <WithErrorComponent error={errorMap.drivingLicenseDate}>
                                    <View style={styles.inputContainer}>
                                        <Text style={styles.inputLabel}>DATE D'OBTENTION</Text>
                                        <View style={styles.separator}/>
                                        <DatePicker
                                            date={moment(drivingLicenseDate)}
                                            format="DD-MM-YYYY"
                                            onDateChange={(_, date) => {
                                                setDrivingLicenseDate(moment(date).toISOString());
                                            }}
                                            showIcon={false}
                                            customStyles={{
                                                dateInput: styles.dateContainer,
                                                dateText: styles.dateInput,
                                            }}
                                            locale="fr"
                                            confirmBtnText="Valider"
                                            cancelBtnText="Annuler"
                                        />
                                    </View>
                                </WithErrorComponent>
                            )
                        }
                        <WithErrorComponent error={errorMap.gender}>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>SEXE</Text>
                                <View style={styles.separator}/>
                                <View style={styles.genderContainer}>
                                    <FormPicker
                                        label={genderMap[gender]}
                                        labelStyle={styles.genderLabel}
                                        onValueChange={setGender}
                                        outerStyle={styles.genderPicker}
                                        selectedValue={gender}
                                    >
                                        <Picker.Item color="#000" label="Femme" value="F"/>
                                        <Picker.Item color="#000" label="Homme" value="H"/>
                                    </FormPicker>
                                </View>
                            </View>
                        </WithErrorComponent>
                        <WithErrorComponent error={errorMap.description}>
                            <View style={[styles.inputContainer, styles.descriptionContainer]}>
                                <Text style={styles.inputLabel}>DESCRIPTION</Text>
                                <View style={styles.separator}/>
                                <TextInput
                                    value={description}
                                    multiline={true}
                                    onChangeText={(input) => setDescription(input)}
                                    style={[styles.input, styles.descriptionInput]}
                                    placeholder="?"
                                    placeholderTextColor="#DCE7EE"
                                />
                            </View>
                        </WithErrorComponent>
                        <View/>

                        {
                            loading && <LoadingComponent/> || (
                                <TouchableOpacity onPress={() => onSubmit()} style={styles.submit}>
                                    <Text style={styles.submitLabel}>{hasDrivingLicense ? 'VÉHICULE' : 'VALIDER'}</Text>
                                </TouchableOpacity>
                            )
                        }
                    </View>
                </KeyboardAvoidingView>
            </View>
        );
    },
);
