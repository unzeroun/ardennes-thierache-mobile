import React, {useEffect, useState} from 'react';
import {
    Image, KeyboardAvoidingView,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {DRIVER_CAR_PREFERENCES} from '../../../../config';
import {
    requestUserCarProfile,
    resetCarProfileRequestState,
    submitCarProfileUpdate,
} from '../../../actions/carpooling/profile';
import {setCloseButtonStyle, setCloseButtonVisibility} from '../../../actions/carpooling/ui';
import PreferencesList from '../../../components/carpooling/profile/PreferencesList';
import LoadingComponent from '../../../components/main/LoadingComponent';
import TopOval from '../../../lib/TopOval';
import WithErrorComponent from '../../../lib/withErrorComponent';
import {OwnRouteComponentProps} from '../../../MainRouter';
import {State} from '../../../reducers';
import {CLOSE_BUTTON_STYLES} from '../../../reducers/carpooling/ui';
import styles from '../../../styles/carpooling/pages/profile/Car';
import {Car, ErrorMap, User} from '../../../types/types';

type StateProps = {
    car: Car | null,
    carProfileUpdated: boolean,
    errorMap: ErrorMap,
    loading: boolean,
    user: User | null,
};

type DispatchProps = {
    requestUserCarProfile: (userIri: string) => void,
    resetCarProfileRequestState: () => void,
    setCloseButtonStyle: (style: CLOSE_BUTTON_STYLES) => void,
    setCloseButtonVisibility: (visibility: boolean) => void,
    submitCarProfileUpdate: (car: Car) => void,
};

type Props = StateProps & DispatchProps & OwnRouteComponentProps;

function mapStateToProps(state: State): StateProps {
    return {
        car: state.profile.car,
        carProfileUpdated: state.profile.carProfileUpdated,
        errorMap: state.misc.errorMap,
        loading: state.profile.loading,
        user: state.profile.user,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        requestUserCarProfile,
        resetCarProfileRequestState,
        setCloseButtonStyle,
        setCloseButtonVisibility,
        submitCarProfileUpdate,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function Car({
                     car,
                     carProfileUpdated,
                     errorMap,
                     history,
                     loading,
                     requestUserCarProfile,
                     resetCarProfileRequestState,
                     setCloseButtonStyle,
                     setCloseButtonVisibility,
                     submitCarProfileUpdate,
                     user,
                 }: Props) {

        useEffect(() => {
            setCloseButtonVisibility(true);
            setCloseButtonStyle(CLOSE_BUTTON_STYLES.WHITE);

            return () => setCloseButtonVisibility(false);
        }, []);

        useEffect(() => {
            resetCarProfileRequestState();
        }, []);

        useEffect(() => {
            if (user) {
                requestUserCarProfile(user.iri);
            }
        }, [user]);

        useEffect(() => {
            if (car) {
                setBrand(car.brand || '');
                setModel(car.model || '');
                setYear(car.year || '');
                setColor(car.color || '');
                setPreferences(car.preferences);
            }
        }, [car]);

        useEffect(() => {
            if (!loading && carProfileUpdated) {
                resetCarProfileRequestState();
                history.go(-2);
            }
        }, [loading]);

        const [brand, setBrand] = useState<string>('');
        const [model, setModel] = useState<string>('');
        const [year, setYear] = useState<string>('');
        const [color, setColor] = useState<string>('');
        const [preferences, setPreferences] = useState<string[]>([]);

        function addPreference(preference: string) {
            setPreferences([
                ...preferences,
                preference,
            ]);
        }

        function removePreference(value: string) {
            setPreferences(preferences.filter(preference => {
                return preference !== value;
            }));
        }

        function onSubmit() {
            if (car) {
                submitCarProfileUpdate({
                    brand,
                    color,
                    iri: car.iri,
                    model,
                    preferences,
                    year,
                });
            }
        }

        return (
            <View style={{backgroundColor: '#F00', flex: 1}}>
                <ScrollView contentContainerStyle={{flex: 1}}>
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <Text style={styles.headerLabel}>VOTRE VÉHICULE</Text>
                        </View>
                            <TopOval backgroundColor="#F3C045" color="#404B52"/>
                        <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={140}>
                            <View
                                style={styles.scrollContainer}
                            >
                                <WithErrorComponent error={errorMap.model}>
                                    <View style={styles.modelContainer}>
                                        <TextInput
                                            value={model}
                                            onChangeText={(input) => setModel(input)}
                                            placeholder="MODÈLE"
                                            placeholderTextColor="#DCE7EE"
                                            style={styles.modelInput}
                                        />
                                        <Image
                                            style={styles.editIcon}
                                            source={require('../../../../assets/images/edit_pencil.png')}
                                            resizeMode="contain"
                                        />
                                    </View>
                                </WithErrorComponent>
                                <WithErrorComponent error={errorMap.brand}>
                                    <View style={styles.inputContainer}>
                                        <Text style={styles.inputLabel}>MARQUE</Text>
                                        <View style={styles.separator}/>
                                        <TextInput
                                            value={brand}
                                            onChangeText={(input) => setBrand(input)}
                                            placeholder="?"
                                            placeholderTextColor="#DCE7EE"
                                            style={styles.input}
                                        />
                                    </View>
                                </WithErrorComponent>
                                <WithErrorComponent error={errorMap.year}>
                                    <View style={styles.inputContainer}>
                                        <Text style={styles.inputLabel}>ANNÉE</Text>
                                        <View style={styles.separator}/>
                                        <TextInput
                                            keyboardType="number-pad"
                                            value={year.toString()}
                                            onChangeText={(input) => setYear(input)}
                                            placeholder="?"
                                            placeholderTextColor="#DCE7EE"
                                            style={styles.input}
                                        />
                                    </View>
                                </WithErrorComponent>
                                <WithErrorComponent error={errorMap.color}>
                                    <View style={styles.inputContainer}>
                                        <Text style={styles.inputLabel}>COULEUR</Text>
                                        <View style={styles.separator}/>
                                        <TextInput
                                            value={color}
                                            onChangeText={(input) => setColor(input)}
                                            placeholder="?"
                                            placeholderTextColor="#DCE7EE"
                                            style={styles.input}
                                        />
                                    </View>
                                </WithErrorComponent>
                                <View>
                                    <PreferencesList
                                        addPreference={addPreference}
                                        outerPreferencesList={preferences}
                                        preferences={DRIVER_CAR_PREFERENCES}
                                        removePreference={removePreference}
                                        outerStyle={styles.preferencesList}
                                    />
                                    {
                                        loading && <LoadingComponent/> || (
                                            <TouchableOpacity
                                                onPress={() => onSubmit()}
                                                style={styles.submit}
                                            >
                                                <Text style={styles.submitLabel}>VALIDER</Text>
                                            </TouchableOpacity>
                                        )
                                    }
                                </View>
                            </View>
                        </KeyboardAvoidingView>
                    </View>
                </ScrollView>
                <View style={styles.backButtonContainer}>
                    <TouchableOpacity style={styles.backButton} onPress={() => history.go(-2)}>
                        <Image source={require('../../../../assets/images/plus_icon.png')}
                               style={styles.backIcon}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    },
);
