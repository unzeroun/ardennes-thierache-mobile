import React, {useEffect} from 'react';
import {Image, View} from 'react-native';
import {OwnRouteComponentProps} from '../../MainRouter';
import styles from '../../styles/carpooling/pages/SplashScreen';

export default function SplashScreen({history}: OwnRouteComponentProps) {

    useEffect(() => {
        setTimeout(() => history.replace('/login'), 3000);
    });

    return (
        <View style={styles.container}>
            <Image
                style={styles.logo}
                source={require('../../../assets/images/header_logo.png')}
                resizeMode="contain"
            />
        </View>
    );
}
