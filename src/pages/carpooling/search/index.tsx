import React, {useState} from 'react';
import {
    Image,
    KeyboardAvoidingView,
    ScrollView,
    Text,
    TextInput, TouchableOpacity,
    View,
} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {requestJourneySearch} from '../../../actions/carpooling/serach';
import JourneyList from '../../../components/carpooling/journey/JourneyList';
import LoadingComponent from '../../../components/main/LoadingComponent';
import {State} from '../../../reducers';
import styles from '../../../styles/carpooling/pages/search/index';
import {Journey} from '../../../types/types';

type StateProps = {
    journeyList: Journey[],
    loading: boolean,
};

type DispatchProps = {
    requestJourneySearch: (departure: string, arrival: string) => void,
};

type Props = StateProps & DispatchProps;

function mapStateToProps(state: State): StateProps {
    return {
        journeyList: state.search.journeyList,
        loading: state.search.loading,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        requestJourneySearch,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function SearchIndex({journeyList, loading, requestJourneySearch}: Props) {

        const [searchDepartureInput, setSearchDepartureInput] = useState<string>('');
        const [searchArrivalInput, setSearchArrivalInput] = useState<string>('');

        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <ScrollView
                    keyboardShouldPersistTaps="handled"
                    bounces={false}
                    contentContainerStyle={styles.content}
                >
                    <View style={styles.inputContainer}>
                        <View>
                            <Text style={styles.label}>Ville de départ :</Text>
                            <TextInput
                                value={searchDepartureInput}
                                onChangeText={(input) => setSearchDepartureInput(input)}
                                style={styles.input}
                            />
                            <Text style={styles.label}>Ville d'arrivée :</Text>
                            <TextInput
                                value={searchArrivalInput}
                                onChangeText={(input) => setSearchArrivalInput(input)}
                                style={styles.input}
                            />
                        </View>
                        <TouchableOpacity
                            onPress={() => requestJourneySearch(searchDepartureInput, searchArrivalInput)}
                            style={styles.btn}
                        >
                            <Image
                                style={styles.icon}
                                source={require('../../../../assets/images/search_icon.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.title}>{'Résultats de la recherche'.toUpperCase()}</Text>
                    <View>
                        {
                            loading && <LoadingComponent/> || (
                                <JourneyList journeys={journeyList} itemStyle={styles.item}/>
                            )
                        }
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    },
);
