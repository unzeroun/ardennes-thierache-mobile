import React, {useEffect, useState} from 'react';
import {
    Image, Keyboard, KeyboardAvoidingView,
    ScrollView,
    Text,
    TextInput,
    View,
} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {CARPOOLING_FOOTER_NAVIGATION_ITEMS, JOURNEY_REQUEST_STATUSES} from '../../../../config';
import {requestJourneyById} from '../../../actions/carpooling/journey';
import {markMessagesAsRead, sendMessage} from '../../../actions/carpooling/message';
import {setCloseButtonStyle, setCloseButtonVisibility} from '../../../actions/carpooling/ui';
import JourneyListItem from '../../../components/carpooling/journey/JourneyListItem';
import MessageInput from '../../../components/carpooling/message/MessageInput';
import MessageList from '../../../components/carpooling/message/MessageList';
import RequestAccepter from '../../../components/carpooling/message/RequestAccepter';
import CloseButton from '../../../components/carpooling/ui/CloseButton';
import Footer from '../../../components/carpooling/ui/Footer';
import Header from '../../../components/carpooling/ui/Header';
import ChatUpdater from '../../../lib/chatUpdater';
import TopOval from '../../../lib/TopOval';
import {OwnRouteComponentProps} from '../../../MainRouter';
import {State} from '../../../reducers';
import {CLOSE_BUTTON_STYLES} from '../../../reducers/carpooling/ui';
import styles from '../../../styles/carpooling/pages/message/show';
import {Journey, JourneyRequest, Message, MessageListType, User} from '../../../types/types';

type StateProps = {
    currentJourney: Journey | null,
    messagesDisplayedCount: number,
    journeyRequest: JourneyRequest | null,
    messageList: MessageListType,
    user: User | null,
};

type DispatchProps = {
    markMessagesAsRead: (journeyIri: string, endUserIri: string) => void,
    requestJourneyById: (journeyIri: string) => void,
    sendMessage: (journeyIri: string, withEndUserIri: string, content: string) => void,
    setCloseButtonStyle: (style: CLOSE_BUTTON_STYLES) => void,
    setCloseButtonVisibility: (visible: boolean) => void,
};

type MergeProps = StateProps & DispatchProps & {
    addressee: {
        '@id': string,
        firstName: string,
        lastName: string,
    },
    endUserIri: string,
    journeyIri: string,
    messages: Message[],
};

type Props = MergeProps;

function mapStateToProps(state: State): StateProps {
    return {
        currentJourney: state.journey.currentJourney,
        journeyRequest: state.journeyRequest.currentJourneyRequest,
        messageList: state.message.messageList,
        messagesDisplayedCount: state.message.messagesDisplayedCount,
        user: state.profile.user,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        markMessagesAsRead,
        requestJourneyById,
        sendMessage,
        setCloseButtonStyle,
        setCloseButtonVisibility,
    }, dispatch);
}

function mergeProps(state: StateProps, dispatch: DispatchProps, ownProps: OwnRouteComponentProps): MergeProps {
    return {
        addressee: ownProps.history.location.state.addressee,
        endUserIri: ownProps.history.location.state.endUserIri,
        journeyIri: ownProps.history.location.state.journeyIri,
        messages: state.messageList[ownProps.history.location.state.endUserIri],
        ...ownProps,
        ...state,
        ...dispatch,
    };
}

export default connect<StateProps, DispatchProps, OwnRouteComponentProps, MergeProps, State>(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps,
)(
    function showChat({
                          addressee,
                          currentJourney,
                          endUserIri,
                          journeyIri,
                          journeyRequest,
                          markMessagesAsRead,
                          messages,
                          messagesDisplayedCount,
                          requestJourneyById,
                          sendMessage,
                          setCloseButtonStyle,
                          setCloseButtonVisibility,
                          user,
                      }: Props) {

        const [typing, setTyping] = useState<string>('');
        const [addresseeIri, setAddresseeIri] = useState<string>('');
        const [messageInput, setMessageInput] = useState<TextInput | null>(null);
        const [scrollView, setScrollView] = useState<ScrollView | null>(null);
        const [isKeyboardVisible, setKeyboardVisible] = useState<boolean>(false);

        useEffect(() => {
            Keyboard.addListener('keyboardWillShow', () => {
                setKeyboardVisible(true);
            });
            Keyboard.addListener('keyboardWillHide', () => {
                setKeyboardVisible(false);
            });
        }, []);

        useEffect(() => {
            setCloseButtonVisibility(true);
            setCloseButtonStyle(CLOSE_BUTTON_STYLES.YELLOW);

            return () => setCloseButtonVisibility(false);
        }, []);

        useEffect(() => {
            if (journeyIri && (!currentJourney || currentJourney['@id'] !== journeyIri)) {
                requestJourneyById(journeyIri);
            }
        }, [journeyIri]);

        useEffect(() => {
            if (user && currentJourney) {
                const addressee = user.iri === currentJourney.endUser['@id'] ?
                    endUserIri : currentJourney.endUser['@id'];

                setAddresseeIri(addressee);
                markMessagesAsRead(journeyIri, addressee);
            }
        }, [user, currentJourney]);

        useEffect(() => {
            markMessagesAsRead(journeyIri, addresseeIri);
        }, [messages]);

        useEffect(() => {
            if (scrollView) {
                scrollView.scrollToEnd({animated: true});
            }
        }, [messagesDisplayedCount]);

        function onSendMessage() {
            if (messageInput) {
                messageInput.blur();
            }

            setTyping('');
            sendMessage(journeyIri, addresseeIri, typing);
        }

        if (!currentJourney || !user || !addressee) {
            return null;
        }

        return (
            <>
                <KeyboardAvoidingView behavior="padding" style={styles.container}>
                    <ScrollView
                        style={styles.scroller}
                        keyboardShouldPersistTaps="handled"
                        bounces={false}
                        ref={(scrollViewRef) => setScrollView(scrollViewRef)}
                    >
                        <Header/>
                        <View style={styles.ovalContainer}>
                            <TopOval backgroundColor="#F0BE2B"/>
                        </View>
                        <View style={styles.header}>
                            <ChatUpdater journeyIri={journeyIri}/>
                            <JourneyListItem journey={currentJourney} outerStyle={styles.journey}/>
                        </View>
                        <TopOval color="#404B52"/>
                        <View style={styles.contentContainer}>
                            <View style={styles.innerContainer}>
                                <Text style={styles.driverLabel}>
                                    {
                                        currentJourney.endUser['@id'] !== user.iri ? 'CONDUCTEUR' : 'PASSAGER'
                                    }
                                </Text>
                                <Text style={styles.driverName}>
                                    {addressee.firstName} {addressee.lastName}
                                </Text>
                            </View>
                            {
                                journeyRequest && journeyRequest.status === JOURNEY_REQUEST_STATUSES.PENDING && (
                                    <View style={styles.requestContainer}>
                                        <Image
                                            style={styles.requestIcon}
                                            source={require('../../../../assets/images/question_mark_bubble.png')}/>
                                        <Text style={styles.request}>est interessé(e) par votre trajet</Text>
                                    </View>
                                )
                            }

                            {
                                messages && (
                                    <View style={styles.messageContainer}>
                                        <MessageList messages={messages}/>
                                    </View>
                                )
                            }
                        </View>
                    </ScrollView>
                    <KeyboardAvoidingView style={[
                        styles.inputContainer,
                        {paddingBottom: !isKeyboardVisible ? 80 : 10},
                    ]}>
                        <MessageInput
                            sendMessage={onSendMessage}
                            setInput={setMessageInput}
                            setValue={setTyping}
                            value={typing}
                        />
                    </KeyboardAvoidingView>
                    {
                        journeyRequest && journeyRequest.status === JOURNEY_REQUEST_STATUSES.PENDING && (
                            <RequestAccepter journeyRequest={journeyRequest} journey={currentJourney}/>
                        )
                    }
                    <CloseButton containerStyle={styles.backButtonContainer}/>
                </KeyboardAvoidingView>
                <Footer navigationItems={CARPOOLING_FOOTER_NAVIGATION_ITEMS}/>
            </>
        );
    },
);
