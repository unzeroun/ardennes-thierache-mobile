import React, {useEffect, useState} from 'react';
import {Image, LayoutRectangle, ScrollView, Text, View} from 'react-native';
import {connect} from 'react-redux';
import {Link} from 'react-router-native';
import Redux, {Action, bindActionCreators} from 'redux';
import {CARPOOLING_FOOTER_NAVIGATION_ITEMS} from '../../../../config';
import {
    requestAllJourneysList,
    requestAllJourneysListNextPage,
    requestPendingJourneyList,
    requestUserJourneyList,
} from '../../../actions/carpooling/journey';
import {requestUserCarProfile, requestUserProfile} from '../../../actions/carpooling/profile';
import {getNotificationPermission} from '../../../actions/main/permission';
import JourneyList from '../../../components/carpooling/journey/JourneyList';
import Footer from '../../../components/carpooling/ui/Footer';
import CarpoolingHeader from '../../../components/carpooling/ui/Header';
import LoadingComponent from '../../../components/main/LoadingComponent';
import {isComponentInViewPort} from '../../../lib/scrollViewHelper';
import TopOval from '../../../lib/TopOval';
import UnreadMessagesUpdater from '../../../lib/unreadMessagesUpdater';
import {OwnRouteComponentProps} from '../../../MainRouter';
import {State} from '../../../reducers';
import styles from '../../../styles/carpooling/pages/journey/index';
import {Car, Journey, MessageListType, Token, User} from '../../../types/types';

type StateProps = {
    allJourneys: Journey[],
    car: Car | null,
    firstConnection: boolean,
    loadingAllList: boolean,
    loadingNextList: boolean,
    loadingPendingList: boolean,
    loadingUserList: boolean,
    nextPageIri: string | null,
    pendingJourneys: Journey[],
    profileRequestError: boolean,
    unreadMessages: MessageListType,
    user: User | null,
    userJourneys: Journey[],
    token: Token | null,
};

type DispatchProps = {
    getNotificationPermission: () => void,
    requestAllJourneysList: () => void,
    requestAllJourneysListNextPage: (nextPageIri: string) => void,
    requestPendingJourneyList: () => void,
    requestUserJourneyList: () => void,
    requestUserProfile: () => void,
    requestUserCarProfile: (id: string) => void,
};

type Props = StateProps & DispatchProps & OwnRouteComponentProps;

function mapStateToProps(state: State): StateProps {
    return {
        allJourneys: state.journey.allJourneys,
        car: state.profile.car,
        firstConnection: state.login.firstConnection,
        loadingAllList: state.journey.loadingAllList,
        loadingNextList: state.journey.loadingNextList,
        loadingPendingList: state.journey.loadingPendingList,
        loadingUserList: state.journey.loadingUserList,
        nextPageIri: state.journey.nextPageIri,
        pendingJourneys: state.journey.pendingJourneys,
        profileRequestError: state.profile.profileRequestError,
        token: state.token.token,
        unreadMessages: state.message.unreadMessages,
        user: state.profile.user,
        userJourneys: state.journey.userJourneys,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        getNotificationPermission,
        requestAllJourneysList,
        requestAllJourneysListNextPage,
        requestPendingJourneyList,
        requestUserCarProfile,
        requestUserJourneyList,
        requestUserProfile,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function Routes({
                        allJourneys,
                        getNotificationPermission,
                        history,
                        loadingAllList,
                        loadingNextList,
                        loadingPendingList,
                        loadingUserList,
                        nextPageIri,
                        pendingJourneys,
                        profileRequestError,
                        userJourneys,
                        requestAllJourneysList,
                        requestAllJourneysListNextPage,
                        requestPendingJourneyList,
                        requestUserJourneyList,
                        requestUserProfile,
                        requestUserCarProfile,
                        user,
                        unreadMessages,
                    }: Props) {

        useEffect(() => {
            if (!user) {
                requestUserProfile();
            }
        }, []);

        useEffect(() => {
            if (user) {
                requestUserCarProfile(user.iri);
                requestAllJourneysList();
                requestPendingJourneyList();
                requestUserJourneyList();
                getNotificationPermission();
            }
        }, [user]);

        useEffect(() => {
            if (profileRequestError) {
                history.push('/profile/user');
            }
        }, [profileRequestError]);

        const [viewportLayout, setViewportLayout] = useState<LayoutRectangle | null>(null);
        const [loadingLayout, setLoadingLayout] = useState<LayoutRectangle | null>(null);
        const [isComponentVisible, setComponentVisible] = useState<boolean>(false);

        function onScroll(componentVisibility: boolean) {
            setComponentVisible(componentVisibility);
            if (!isComponentVisible && componentVisibility && nextPageIri && !loadingNextList) {
                requestAllJourneysListNextPage(nextPageIri);
            }
        }

        return (
            <View
                onLayout={(ev) => setViewportLayout(ev.nativeEvent.layout)}
                style={styles.container}
            >
                <UnreadMessagesUpdater/>
                <ScrollView
                    bounces={false}
                    onScroll={(ev) => {
                        if (loadingLayout && viewportLayout) {
                            onScroll(isComponentInViewPort(viewportLayout, ev.nativeEvent, loadingLayout));
                        }
                    }}
                >
                    <CarpoolingHeader/>
                    {
                        loadingUserList && <LoadingComponent/> || userJourneys.length > 0 && (
                            <View style={[styles.journeyList, styles.userJourneys]}>
                                <View style={styles.header}>
                                    <Text style={styles.headerText}>VOS PROCHAINS TRAJETS</Text>
                                </View>
                                <View style={styles.upcomingTargets}>
                                    {
                                        (
                                            <JourneyList
                                                journeys={userJourneys}
                                                itemStyle={styles.item}
                                                unreadMessages={unreadMessages}
                                            />
                                        )
                                    }
                                </View>
                            </View>
                        )
                    }
                    <View>
                        {
                            loadingPendingList && <LoadingComponent/> || pendingJourneys.length > 0 && (
                                <View style={[styles.journeyList, styles.pendingList]}>
                                    <TopOval backgroundColor="#F3C045"/>
                                    <Text style={[styles.headerText, styles.requestsHeadTitle]}>VOS DEMANDES</Text>
                                    <View style={styles.pendingTargets}>
                                        {
                                            pendingJourneys.length > 0 && (
                                                <JourneyList
                                                    journeys={pendingJourneys}
                                                    itemStyle={styles.item}
                                                    itemTimeContainerStyle={styles.pendingTimeContainer}
                                                    unreadMessages={unreadMessages}
                                                />
                                            )
                                        }
                                    </View>
                                </View>
                            )
                        }
                    </View>
                    {
                        loadingAllList && <LoadingComponent/> || allJourneys.length > 0 && (
                            <View style={[styles.journeyList, styles.closeTargetsContainer]}>
                                <TopOval
                                    color="#F8F8F8"
                                    backgroundColor={
                                        userJourneys.length > 0 || pendingJourneys.length > 0 ? '#FFF' : '#F3C045'
                                    }
                                />
                                <Text style={[styles.headerText, styles.requestsHeadTitle]}>TRAJETS PROCHES</Text>
                                <View style={styles.closeTargets}>
                                    {
                                        allJourneys.length > 0 && (
                                            <JourneyList
                                                journeys={allJourneys}
                                                itemStyle={styles.item}
                                                itemTimeContainerStyle={styles.closeTimeContainer}
                                                unreadMessages={unreadMessages}
                                            />
                                        )
                                    }
                                    <View
                                        onLayout={(ev) => setLoadingLayout(ev.nativeEvent.layout)}
                                    />
                                    {
                                        loadingNextList && <LoadingComponent/>
                                    }
                                </View>
                            </View>
                        )
                    }
                </ScrollView>
                <Footer navigationItems={CARPOOLING_FOOTER_NAVIGATION_ITEMS}/>
                <Link to="/journey/create" style={styles.addContainer}>
                    <Image source={require('../../../../assets/images/plus_icon.png')} style={styles.addIcon}/>
                </Link>
            </View>
        );
    },
);
