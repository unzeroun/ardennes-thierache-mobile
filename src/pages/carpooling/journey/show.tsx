import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {Image, Text, View} from 'react-native';
import MapView, {Marker, Polyline} from 'react-native-maps';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {DRIVER_CAR_PREFERENCES, JOURNEY_REQUEST_STATUSES} from '../../../../config';
import {requestJourneyById, resetCurrentJourney} from '../../../actions/carpooling/journey';
import {
    getJourneyRequests,
    requestCreateJourneyRequest,
    resetJourneyRequestReducer,
} from '../../../actions/carpooling/journeyRequest';
import {getMessages} from '../../../actions/carpooling/message';
import {setCloseButtonStyle, setCloseButtonVisibility} from '../../../actions/carpooling/ui';
import JourneyMessageList from '../../../components/carpooling/journey/JourneyMessageList';
import JourneyRequestList from '../../../components/carpooling/journey/JourneyRequestList';
import PostJourneyRequestStatusButton from '../../../components/carpooling/journey/PostJourneyRequestStatusButton';
import ChatButton from '../../../components/carpooling/ui/ChatButton';
import PhoneButton from '../../../components/carpooling/ui/PhoneButton';
import LoadingComponent from '../../../components/main/LoadingComponent';
import ChatUpdater from '../../../lib/chatUpdater';
import TopOval from '../../../lib/TopOval';
import {OwnRouteComponentProps} from '../../../MainRouter';
import {State} from '../../../reducers';
import {CLOSE_BUTTON_STYLES} from '../../../reducers/carpooling/ui';
import styles from '../../../styles/carpooling/pages/journey/show';
import {Journey, JourneyRequest, Message, MessageListType, User} from '../../../types/types';

type StateProps = {
    allJourneyRequests: JourneyRequest[],
    journey: Journey | null,
    loading: boolean,
    messageList: MessageListType,
    requestSent: boolean,
    user: User | null,
};

type DispatchProps = {
    requestCreateJourneyRequest: (journeyId: string) => void,
    requestJourneyById: (journeyId: string) => void,
    resetCurrentJourney: () => void,
    resetJourneyRequestReducer: () => void,
    setCloseButtonStyle: (style: CLOSE_BUTTON_STYLES) => void,
    setCloseButtonVisibility: (visible: boolean) => void,
    getJourneyRequests: (journeyId: string) => void,
    getMessages: (journeyIri: string) => void,
};

type Props = StateProps & DispatchProps & OwnRouteComponentProps;

function mapStateToProps(state: State): StateProps {
    return {
        allJourneyRequests: state.journeyRequest.allJourneyRequests,
        journey: state.journey.currentJourney,
        loading: state.journeyRequest.loading,
        messageList: state.message.messageList,
        requestSent: state.journeyRequest.requestSent,
        user: state.profile.user,
    };
}

function mapDispatchProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        getJourneyRequests,
        getMessages,
        requestCreateJourneyRequest,
        requestJourneyById,
        resetCurrentJourney,
        resetJourneyRequestReducer,
        setCloseButtonStyle,
        setCloseButtonVisibility,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchProps)(
    function showJourney({
                             allJourneyRequests,
                             journey,
                             history,
                             loading,
                             requestJourneyById,
                             getJourneyRequests,
                             getMessages,
                             messageList,
                             requestCreateJourneyRequest,
                             requestSent,
                             resetCurrentJourney,
                             resetJourneyRequestReducer,
                             setCloseButtonStyle,
                             setCloseButtonVisibility,
                             user,
                         }: Props) {
        const [postedRequest, setPostedRequest] = useState<JourneyRequest | null>(null);
        const [mapReference, setMapReference] = useState<MapView | null>(null);

        useEffect(() => {
            setCloseButtonVisibility(true);

            setCloseButtonStyle(CLOSE_BUTTON_STYLES.YELLOW);

            return () => setCloseButtonVisibility(false);
        }, []);

        useEffect(() => {
            resetCurrentJourney();
            resetJourneyRequestReducer();
            if (history.location.state.journeyId) {
                requestJourneyById(history.location.state.journeyId);
            }
        }, []);

        useEffect(() => {
            if (requestSent) {
                history.push({
                    pathname: '/journey/request/confirmation',
                    state: {journey},
                });
            }
        }, [requestSent]);

        useEffect(() => {
            if (journey) {
                getJourneyRequests(journey['@id']);
                getMessages(journey['@id']);
                onMapReady();
            }
        }, [journey]);

        useEffect(() => {
            if (allJourneyRequests && allJourneyRequests.length > 0) {
                setPostedRequest(allJourneyRequests[0]);
            } else {
                setPostedRequest(null);
            }
        }, [allJourneyRequests]);

        function getAcceptedRequests() {
            return allJourneyRequests.filter(request => {
                return request.status === JOURNEY_REQUEST_STATUSES.ACCEPTED;
            });
        }

        function getPendingRequests() {
            return allJourneyRequests.filter(request => {
                return request.status === JOURNEY_REQUEST_STATUSES.PENDING;
            });
        }

        function onMapReady() {
            if (mapReference && journey) {
                mapReference.fitToElements(false);
            }
        }

        function getNonRequestedMessages(): Message[][] {
            if (Object.keys(messageList).length === 0) {
                return [];
            }
            const journeyRequesters = allJourneyRequests.map(request => request.requester['@id']);
            return Object.keys(messageList).filter(messagesIndex => {
                return journeyRequesters.indexOf(messagesIndex) === -1;
            }).map(key => messageList[key]);
        }

        if (!journey) {
            return null;
        }

        function isOwnedByUser(): boolean {
            if (!journey || !user || !journey.endUser) {
                return false;
            }

            return (journey.endUser as any)['@id'] === user.iri;
        }

        function onRequestJourneyRequestCreation() {
            if (journey) {
                requestCreateJourneyRequest(journey['@id']);
            }
        }

        if (loading) {
            return (
                <View style={styles.loadingContainer}>
                    <LoadingComponent/>
                </View>
            );
        }

        return (
            <View style={styles.container}>
                <TopOval backgroundColor="#F3C045"/>
                {
                    journey && <ChatUpdater journeyIri={journey['@id']}/>
                }
                <View style={styles.topContent}>
                    <View style={styles.timeAndPlaceContainer}>
                        <View style={styles.timeContainer}>
                            <Text style={styles.day}>{moment(journey.departureDate).format('DD MMM')}</Text>
                            <Text style={styles.hour}>
                                à {moment(journey.departureDate).format('HH')}
                                h{moment(journey.departureDate).format('mm')}
                            </Text>
                        </View>
                        <View style={styles.availablePlaces}>
                            <Image
                                style={styles.carSeat}
                                source={require('../../../../assets/images/car_seat.png')}
                                resizeMode="contain"
                            />
                            <Text style={styles.placesLabel}>{journey.availablePlaces} places libres</Text>
                        </View>
                    </View>
                    <View style={styles.stepContainer}>
                        <Image
                            style={styles.stepIcon}
                            source={require('../../../../assets/images/journey_list_item_icon.png')}
                            resizeMode="contain"
                        />
                        <View>
                            <Text style={styles.stepLabel}>{journey.departure.city}</Text>
                            <Text style={styles.stepLabel}>{journey.arrival.city}</Text>
                        </View>
                    </View>
                    <Text style={styles.price}>{journey.price}€</Text>
                </View>
                <View>
                    {
                        !isOwnedByUser() && (
                            <>
                                {
                                    loading && <LoadingComponent/> || (
                                        <View style={styles.requestButtonContainer}>
                                            <PostJourneyRequestStatusButton
                                                requestMethod={onRequestJourneyRequestCreation}
                                                status={postedRequest ? postedRequest.status : null}
                                                outerStyle={styles.statusButton}
                                            />
                                        </View>
                                    )
                                }
                            </>
                        ) || (
                            <View>
                                {
                                    getAcceptedRequests().length > 0 && (
                                        <View style={styles.requestList}>
                                            <Text style={styles.requestListLabel}>PASSAGERS</Text>
                                            <JourneyRequestList
                                                journey={journey}
                                                journeyRequests={getAcceptedRequests()}
                                            />
                                        </View>
                                    )
                                }
                                {
                                    getPendingRequests().length > 0 && (
                                        <View style={[styles.requestList, styles.pendingRequestList]}>
                                            <TopOval color="#F8F8F8"/>
                                            <Text style={styles.requestListLabel}>DEMANDES</Text>
                                            <JourneyRequestList
                                                journey={journey}
                                                journeyRequests={getPendingRequests()}
                                            />
                                        </View>
                                    )
                                }
                                {
                                    getNonRequestedMessages().length > 0 && (
                                        <View style={[styles.requestList, styles.pendingRequestList]}>
                                            <TopOval/>
                                            <Text style={styles.requestListLabel}>MESSAGES</Text>
                                            {
                                                <JourneyMessageList
                                                    journeyIri={journey['@id']}
                                                    messageList={getNonRequestedMessages()}
                                                />
                                            }
                                        </View>
                                    )
                                }
                            </View>
                        )
                    }
                </View>
                <TopOval color="#404C52"/>
                <View style={styles.driverData}>
                    <Text style={styles.driverHeaderLabel}>CONDUCTEUR</Text>
                    {
                        journey.endUser && user && (
                            <>
                                {
                                    !isOwnedByUser() && (
                                        <Text style={styles.driverName}>{journey.endUser.firstName}</Text>
                                    ) || (
                                        <Text style={styles.driverName}>VOUS</Text>
                                    )
                                }
                                <View>
                                    {
                                        !isOwnedByUser() && <ChatButton
                                            addressee={journey.endUser}
                                            journeyIri={journey['@id']}
                                            endUserIri={user.iri}
                                        />
                                    }
                                    {
                                        !isOwnedByUser() && journey.endUser.phoneNumber && postedRequest &&
                                        JOURNEY_REQUEST_STATUSES.ACCEPTED === postedRequest.status && (
                                            <PhoneButton
                                                phoneNumber={journey.endUser.phoneNumber}
                                                outerStyle={styles.phoneButton}
                                            />
                                        )
                                    }
                                </View>
                            </>
                        )
                    }
                    {
                        journey.endUser.car && (
                            <View style={styles.carData}>
                                <View style={styles.carIdentity}>
                                    <Text style={styles.carBrand}>{journey.endUser.car.brand}</Text>
                                    <Text style={styles.carModel}>{journey.endUser.car.model}</Text>
                                </View>
                                <View style={styles.carPreferences}>
                                    {
                                        journey.endUser.car.preferences.map(preference => {
                                            return (
                                                <View key={preference} style={styles.carPreferenceContainer}>
                                                    <Image
                                                        style={styles.carPreference}
                                                        resizeMode="contain"
                                                        source={DRIVER_CAR_PREFERENCES[preference].enabledIcon}
                                                    />
                                                </View>
                                            );
                                        })
                                    }
                                </View>
                            </View>
                        )
                    }
                </View>
                {
                    journey.departure.latitude && journey.departure.longitude &&
                    journey.arrival.latitude && journey.arrival.longitude && (
                        <MapView
                            style={styles.map}
                            ref={mapReference => setMapReference(mapReference)}
                            onMapReady={() => onMapReady()}
                            zoomEnabled={false}
                            rotateEnabled={false}
                            scrollEnabled={false}
                        >
                            <Marker coordinate={{
                                latitude: journey.departure.latitude,
                                longitude: journey.departure.longitude,
                            }}/>
                            <Marker coordinate={{
                                latitude: journey.arrival.latitude,
                                longitude: journey.arrival.longitude,
                            }}/>
                            <Polyline coordinates={[
                                {
                                    latitude: journey.departure.latitude,
                                    longitude: journey.departure.longitude,
                                },
                                {
                                    latitude: journey.arrival.latitude,
                                    longitude: journey.arrival.longitude,
                                },
                            ]}/>
                        </MapView>
                    )
                }
            </View>
        );
    },
);
