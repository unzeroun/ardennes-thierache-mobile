import React, {useEffect} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {CARPOOLING_FOOTER_NAVIGATION_ITEMS} from '../../../../config';
import {resetJourneyRequestReducer} from '../../../actions/carpooling/journeyRequest';
import JourneyListItem from '../../../components/carpooling/journey/JourneyListItem';
import Footer from '../../../components/carpooling/ui/Footer';
import CarpoolingHeader from '../../../components/carpooling/ui/Header';
import TopOval from '../../../lib/TopOval';
import {OwnRouteComponentProps} from '../../../MainRouter';
import styles from '../../../styles/carpooling/pages/journey/RequestPostedConfirmation';
import {Journey} from '../../../types/types';

type DispatchProps = {
    resetJourneyRequestReducer: () => void,
};

type MergeProps = DispatchProps & OwnRouteComponentProps & {
    journey: Journey,
};

type Props = MergeProps;

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        resetJourneyRequestReducer,
    }, dispatch);
}

function mergeProps(_: {}, dispatchProps: DispatchProps, ownProps: OwnRouteComponentProps): MergeProps {
    return {
        ...dispatchProps,
        ...ownProps,
        journey: ownProps.history.location.state.journey,
    };
}

export default connect<{}, DispatchProps, OwnRouteComponentProps, MergeProps>(null, mapDispatchToProps, mergeProps)(
    function RequestPostedConfirmation({history, journey, resetJourneyRequestReducer}: Props) {

        useEffect(() => {
            resetJourneyRequestReducer();
        }, []);

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <CarpoolingHeader/>
                </View>
                <JourneyListItem journey={journey} outerStyle={styles.journey}/>
                <TopOval color="#2C3840"/>
                <View style={styles.content}>
                    <Image
                        source={require('../../../../assets/images/question_mark_bubble.png')}
                        style={styles.image}
                        resizeMode="contain"
                    />
                    <Text style={styles.label}>Votre demande a bien été envoyée à</Text>
                    <Text style={styles.name}>{journey.endUser.firstName} {journey.endUser.lastName}</Text>
                    <TouchableOpacity style={styles.submit} onPress={() => history.goBack()}>
                        <Text style={styles.submitLabel}>D'ACCORD</Text>
                    </TouchableOpacity>
                </View>
                <Footer navigationItems={CARPOOLING_FOOTER_NAVIGATION_ITEMS}/>
            </View>
        );
    },
);
