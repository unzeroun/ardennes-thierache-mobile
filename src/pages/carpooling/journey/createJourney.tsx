import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {KeyboardAvoidingView, ScrollView, Text, TextInput, TouchableOpacity, View} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {
    requestGeoArrivalSearch,
    requestGeoDepartureSearch,
    resetJourneyCreationState,
    setArrivalCitySuggestions,
    setArrivalSelectedCitySuggestion,
    setDepartureCitySuggestions,
    setDepartureSelectedCitySuggestion,
    submitJourneyCreation,
} from '../../../actions/carpooling/journey';
import {setCloseButtonStyle, setCloseButtonVisibility} from '../../../actions/carpooling/ui';
import {CitySuggestionType} from '../../../components/carpooling/journey/CitySuggestion';
import CitySuggestionList from '../../../components/carpooling/journey/CitySuggestionList';
import LoadingComponent from '../../../components/main/LoadingComponent';
import TopOval from '../../../lib/TopOval';
import WithErrorComponent from '../../../lib/withErrorComponent';
import {OwnRouteComponentProps} from '../../../MainRouter';
import {State} from '../../../reducers';
import {CLOSE_BUTTON_STYLES} from '../../../reducers/carpooling/ui';
import styles from '../../../styles/carpooling/pages/journey/CreateJourney';
import {ErrorMap, Journey, User} from '../../../types/types';

type StateProps = {
    arrivalCitySuggestions: CitySuggestionType[],
    arrivalSelectedSuggestion: CitySuggestionType | null,
    departureCitySuggestions: CitySuggestionType[],
    departureSelectedSuggestion: CitySuggestionType | null,
    errorMap: ErrorMap,
    journeyCreated: boolean,
    loading: boolean,
    user: User | null,
};

type DispatchProps = {
    requestGeoArrivalSearch: (arrival: string) => void,
    requestGeoDepartureSearch: (address: string) => void,
    resetJourneyCreationState: () => void,
    setArrivalCitySuggestions: (suggestions: CitySuggestionType[]) => void,
    setArrivalSelectedCitySuggestion: (citySuggestion: CitySuggestionType | null) => void,
    setCloseButtonStyle: (style: CLOSE_BUTTON_STYLES) => void,
    setCloseButtonVisibility: (visible: boolean) => void,
    setDepartureCitySuggestions: (suggestions: CitySuggestionType[]) => void,
    setDepartureSelectedCitySuggestion: (citySuggestion: CitySuggestionType | null) => void,
    submitJourneyCreation: (journey: Journey) => void,
};

type Props = StateProps & DispatchProps & OwnRouteComponentProps;

function mapStateToProps(state: State): StateProps {
    return {
        arrivalCitySuggestions: state.journey.arrivalCitySuggestions,
        arrivalSelectedSuggestion: state.journey.arrivalSelectedSuggestion,
        departureCitySuggestions: state.journey.departureCitySuggestions,
        departureSelectedSuggestion: state.journey.departureSelectedSuggestion,
        errorMap: state.misc.errorMap,
        journeyCreated: state.journey.journeyCreated,
        loading: state.journey.loading,
        user: state.profile.user,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        requestGeoArrivalSearch,
        requestGeoDepartureSearch,
        resetJourneyCreationState,
        setArrivalCitySuggestions,
        setArrivalSelectedCitySuggestion,
        setCloseButtonStyle,
        setCloseButtonVisibility,
        setDepartureCitySuggestions,
        setDepartureSelectedCitySuggestion,
        submitJourneyCreation,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function CreateJourney({
                               arrivalCitySuggestions,
                               arrivalSelectedSuggestion,
                               departureCitySuggestions,
                               departureSelectedSuggestion,
                               errorMap,
                               history,
                               journeyCreated,
                               loading,
                               requestGeoArrivalSearch,
                               requestGeoDepartureSearch,
                               resetJourneyCreationState,
                               setArrivalSelectedCitySuggestion,
                               setCloseButtonStyle,
                               setCloseButtonVisibility,
                               setDepartureSelectedCitySuggestion,
                               submitJourneyCreation,
                               user,
                           }: Props) {

        const [departureEditionTimer, setDepartureEditionTimer] = useState<number | null>(null);
        const [arrivalEditionTimer, setArrivalEditionTimer] = useState<number | null>(null);
        const [arrivalCityValue, setArrivalCityValue] = useState<string>('');
        const [availablePlacesValue, setAvailablePlacesValue] = useState<number | null>(null);
        const [departureCityValue, setDepartureCityValue] = useState<string>('');
        const [departureDateValue, setDepartureDateValue] = useState<string>(moment().format());
        const [luggagePossibleValue, setLuggagePossibleValue] = useState<boolean>(false);
        const [priceValue, setPriceValue] = useState<number | null>(null);
        const [hasBeenSubmitted, setHasBeenSubmitted] = useState<boolean>(false);

        useEffect(() => {
            setCloseButtonVisibility(true);
            setCloseButtonStyle(CLOSE_BUTTON_STYLES.YELLOW);

            return () => setCloseButtonVisibility(false);
        }, []);

        useEffect(() => {
            if (!loading && journeyCreated) {
                history.goBack();
            }
        }, [journeyCreated]);

        useEffect(() => {
            resetJourneyCreationState();
        }, []);

        useEffect(() => {
            if (departureSelectedSuggestion) {
                setDepartureCityValue(departureSelectedSuggestion.city + ' - ' + departureSelectedSuggestion.postcode);
            }
        }, [departureSelectedSuggestion]);

        useEffect(() => {
            if (arrivalSelectedSuggestion) {
                setArrivalCityValue(arrivalSelectedSuggestion.city + ' - ' + arrivalSelectedSuggestion.postcode);
            }
        }, [arrivalSelectedSuggestion]);

        function onDepartureChange(input: string) {
            setDepartureCityValue(input);
            if (departureEditionTimer) {
                clearTimeout(departureEditionTimer);
                setDepartureEditionTimer(null);
            }
            setDepartureEditionTimer(setTimeout(() => {
                requestGeoDepartureSearch(input);
            }, 1000));
        }

        function onArrivalChange(input: string) {
            setArrivalCityValue(input);
            if (arrivalEditionTimer) {
                clearTimeout(arrivalEditionTimer);
                setArrivalEditionTimer(null);
            }
            setArrivalEditionTimer(setTimeout(() => {
                requestGeoArrivalSearch(input);
            }, 1000));
        }

        function onDepartureSuggestionClick(citySuggestion: CitySuggestionType | null) {
            if (citySuggestion) {
                setDepartureSelectedCitySuggestion(citySuggestion);
            }
        }

        function onArrivalSuggestionClick(citySuggestion: CitySuggestionType | null) {
            if (citySuggestion) {
                setArrivalSelectedCitySuggestion(citySuggestion);
            }
        }

        function getIntValue(value: string, initialValue: number | null) {
            if (value.length === 0) {
                return null;
            }

            return parseInt(value, 0) || initialValue;
        }

        function isDepartureInPast(): boolean {
            return moment(departureDateValue).isBefore(moment());
        }

        return (
            <>
                <View style={styles.header}>
                    <Text style={styles.headerLabel}>Nouveau trajet</Text>
                </View>
                <View style={styles.container}>
                    <TopOval backgroundColor="#F3C045"/>
                    <KeyboardAvoidingView style={{width: '100%'}} behavior="padding" keyboardVerticalOffset={190}>
                        <ScrollView
                            contentContainerStyle={styles.scrollContainer}
                            keyboardShouldPersistTaps="handled"
                            bounces={false}
                        >
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>{'Ville de départ'.toUpperCase()}</Text>
                                <View style={styles.separator}/>
                                <TextInput
                                    style={styles.input}
                                    value={departureCityValue}
                                    onChangeText={(input) => onDepartureChange(input)}
                                    placeholder="?"
                                    placeholderTextColor="#404B52"
                                />
                                <CitySuggestionList callback={onDepartureSuggestionClick}
                                                    citySuggestions={departureCitySuggestions}
                                />
                            </View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>{"Ville d'arrivée".toUpperCase()}</Text>
                                <View style={styles.separator}/>
                                <TextInput
                                    style={styles.input}
                                    value={arrivalCityValue}
                                    onChangeText={(input) => onArrivalChange(input)}
                                    placeholder="?"
                                    placeholderTextColor="#404B52"
                                />
                                <CitySuggestionList callback={onArrivalSuggestionClick}
                                                    citySuggestions={arrivalCitySuggestions}
                                />
                            </View>
                            <WithErrorComponent>
                                <View style={styles.inputContainer}>
                                    <Text style={styles.inputLabel}>{'Date de départ'.toUpperCase()}</Text>
                                    <View style={styles.separator}/>
                                    <DatePicker
                                        date={moment(departureDateValue)}
                                        onDateChange={(_, date) => setDepartureDateValue(moment(date).format())}
                                        mode="datetime"
                                        is24Hour={true}
                                        showIcon={false}
                                        locale="fr"
                                        format="DD-MM-YYYY HH[h]mm"
                                        confirmBtnText="Valider"
                                        cancelBtnText="Annuler"
                                        customStyles={{
                                            dateInput: styles.dateContainer,
                                            dateText: styles.dateInput,
                                        }}
                                    />
                                    {
                                        isDepartureInPast() && hasBeenSubmitted && (
                                            <Text style={styles.errorLabel}>
                                                La date de départ ne peut être dans le passé
                                            </Text>
                                        )
                                    }
                                </View>
                            </WithErrorComponent>
                            <WithErrorComponent error={errorMap.availablePlaces}>
                                <View style={styles.inputContainer}>
                                    <Text style={styles.inputLabel}>NOMBRE DE PLACES</Text>
                                    <View style={styles.separator}/>
                                    <TextInput
                                        keyboardType="number-pad"
                                        placeholder="0"
                                        placeholderTextColor="#404B52"
                                        style={styles.input}
                                        value={availablePlacesValue ? availablePlacesValue.toString() : undefined}
                                        onChangeText={(input) => {
                                            setAvailablePlacesValue(getIntValue(input, availablePlacesValue));
                                        }}
                                    />
                                </View>
                            </WithErrorComponent>
                            <WithErrorComponent>
                                <View style={styles.inputContainer}>
                                    <Text style={styles.inputLabel}>PRIX</Text>
                                    <View style={styles.separator}/>
                                    <TextInput
                                        keyboardType="number-pad"
                                        style={styles.input}
                                        value={priceValue ? priceValue.toString() : undefined}
                                        onChangeText={(input) => setPriceValue(getIntValue(input, priceValue))}
                                        placeholder="0"
                                        placeholderTextColor="#404B52"
                                    />
                                </View>
                            </WithErrorComponent>
                            <WithErrorComponent>
                                <View style={styles.inputContainer}>
                                    <Text style={styles.inputLabel}>BAGAGES ?</Text>
                                    <View style={styles.separator}/>
                                    <TouchableOpacity
                                        onPress={() => setLuggagePossibleValue(!luggagePossibleValue)}
                                    >
                                        <Text style={styles.input}>{luggagePossibleValue ? 'oui' : 'non'}</Text>
                                    </TouchableOpacity>
                                </View>
                            </WithErrorComponent>
                            <View>
                                {
                                    loading && <LoadingComponent/> || (
                                        <TouchableOpacity
                                            style={styles.submit}
                                            onPress={() => {
                                                setHasBeenSubmitted(true);
                                                if (!user || !availablePlacesValue || !priceValue ||
                                                    isDepartureInPast()) {
                                                    return;
                                                }

                                                submitJourneyCreation({
                                                    '@id': '',
                                                    'arrival': {
                                                        address1: '',
                                                        address2: '',
                                                        address3: '',
                                                        city: arrivalSelectedSuggestion ? arrivalSelectedSuggestion.city
                                                            : arrivalCityValue,
                                                        latitude: arrivalSelectedSuggestion ?
                                                            arrivalSelectedSuggestion.coordinates.latitude : null,
                                                        longitude: arrivalSelectedSuggestion ?
                                                            arrivalSelectedSuggestion.coordinates.longitude : null,
                                                        postCode: arrivalSelectedSuggestion ?
                                                            arrivalSelectedSuggestion.postcode : '',
                                                    },
                                                    'availablePlaces': availablePlacesValue,
                                                    'departure': {
                                                        address1: '',
                                                        address2: '',
                                                        address3: '',
                                                        city: departureSelectedSuggestion
                                                            ? departureSelectedSuggestion.city : departureCityValue,
                                                        latitude: departureSelectedSuggestion ?
                                                            departureSelectedSuggestion.coordinates.latitude : null,
                                                        longitude: departureSelectedSuggestion ?
                                                            departureSelectedSuggestion.coordinates.longitude : null,
                                                        postCode: departureSelectedSuggestion
                                                            ? departureSelectedSuggestion.postcode : '',
                                                    },
                                                    'departureDate': departureDateValue,
                                                    'endUser': {
                                                        '@id': user.iri,
                                                        'firstName': user.firstName,
                                                        'lastName': user.lastName,
                                                        'phoneNumber': user.phoneNumber,
                                                    },
                                                    'luggagePossible': luggagePossibleValue,
                                                    'price': priceValue,
                                                    'type': 'driver',
                                                    'unreadMessages': 0,
                                                });
                                            }}
                                        >
                                            <Text style={styles.submitLabel}>CRÉER LE TRAJET</Text>
                                        </TouchableOpacity>
                                    )
                                }
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </>
        );
    },
);
