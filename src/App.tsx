import {ApolloClient, NormalizedCacheObject} from 'apollo-boost/lib/index';
import * as Font from 'expo-font';
import React, {useState} from 'react';
import {ApolloProvider} from 'react-apollo';
import {LogBox, Text, View} from 'react-native';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import ErrorComponent from './components/main/ErrorComponent';
import LoadingComponent from './components/main/LoadingComponent';
import RegisterNotificationSubscriber from './components/main/RegisterNotificationSubscriber';
import LifecycleComponentsProvider from './concertosdk/common/containers/LifecycleComponentsProvider';
import Entrypoint from './Entrypoint';
import initApollo from './lib/initApollo';
import defaultReducer from './reducers';
import saga from './sagas/index';
import styles from './styles/main/App';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

const store = createStore(defaultReducer, applyMiddleware(...middlewares));
sagaMiddleware.run(saga);

export default function App() {
    const [ready, setReady] = useState<boolean>(false);

    async function loadFont() {
        await Font.loadAsync({
            ubuntu_bold: require('../assets/fonts/Ubuntu-Bold.ttf'),
            ubuntu_bold_italic: require('../assets/fonts/Ubuntu-BoldItalic.ttf'),
            ubuntu_italic: require('../assets/fonts/Ubuntu-Italic.ttf'),
            ubuntu_light: require('../assets/fonts/Ubuntu-Light.ttf'),
            ubuntu_light_italic: require('../assets/fonts/Ubuntu-LightItalic.ttf'),
            ubuntu_medium: require('../assets/fonts/Ubuntu-Medium.ttf'),
            ubuntu_medium_italic: require('../assets/fonts/Ubuntu-MediumItalic.ttf'),
            ubuntu_regular: require('../assets/fonts/Ubuntu-Regular.ttf'),
        });
    }

    const apolloClient: ApolloClient<NormalizedCacheObject> = initApollo();
    loadFont().then(() => setReady(true));

    if (!ready) {
        return null;
    }

    LogBox.ignoreAllLogs(true);

    return (
        <Provider store={store}>
            <ApolloProvider client={apolloClient}>
                <LifecycleComponentsProvider
                    loadingComponent={() => <LoadingComponent/>}
                    errorComponent={() => <ErrorComponent/>}
                    emptyComponent={() => <Text>Rien à afficher</Text>}
                >
                    <RegisterNotificationSubscriber/>
                    <View style={styles.statusBar}/>
                    <Entrypoint/>
                </LifecycleComponentsProvider>
            </ApolloProvider>
        </Provider>
    );
}
