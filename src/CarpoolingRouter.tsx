import moment from 'moment';
import React, {useEffect} from 'react';
import {ScrollView, View} from 'react-native';
import {connect} from 'react-redux';
import {BackButton, NativeRouter, Redirect, Route, Switch} from 'react-router-native';
import Redux, {Action, bindActionCreators} from 'redux';
import {CARPOOLING_FOOTER_NAVIGATION_ITEMS, NOTIFICATION_TYPES} from '../config';
import {setTokenValidity} from './actions/carpooling/token';
import {setOpenedNotification} from './actions/routing';
import CloseButton from './components/carpooling/ui/CloseButton';
import Footer from './components/carpooling/ui/Footer';
import Header from './components/carpooling/ui/Header';
import Toaster from './components/main/settings/Toaster';
import CreateJourney from './pages/carpooling/journey/createJourney';
import RouteList from './pages/carpooling/journey/index';
import RequestPostedConfirmation from './pages/carpooling/journey/RequestPostedConfirmation';
import ShowJourney from './pages/carpooling/journey/show';
import Login from './pages/carpooling/Login';
import MessageShow from './pages/carpooling/message/show';
import CarProfileForm from './pages/carpooling/profile/Car';
import UserProfileForm from './pages/carpooling/profile/User';
import SearchIndex from './pages/carpooling/search';
import SplashScreen from './pages/carpooling/SplashScreen';
import {State} from './reducers';
import styles from './styles/carpooling/Router';
import {NotificationParamsType, User} from './types/types';

type StateProps = {
    isTokenValid: boolean,
    openedNotification: NotificationParamsType | null,
    user: User | null,
};

type DispatchToProps = {
    setOpenedNotification: (notification: NotificationParamsType | null) => void,
    setTokenValidity: (validity: boolean) => void,
};

type Props = StateProps & DispatchToProps;

function mapStateToProps(state: State): StateProps {
    return {
        isTokenValid: state.token.isTokenValid,
        openedNotification: state.routing.openedNotification,
        user: state.profile.user,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        setOpenedNotification,
        setTokenValidity,
    }, dispatch);
}

export default connect<StateProps, DispatchToProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function Router({
                        isTokenValid,
                        openedNotification,
                        setOpenedNotification,
                        setTokenValidity,
                        user,
                    }: Props) {

        require('moment/locale/fr');
        moment.locale('fr');

        useEffect(() => {
            if (!isTokenValid) {
                setTokenValidity(true);
            }
        }, [isTokenValid]);

        useEffect(() => {
            if (openedNotification && user) {
                setOpenedNotification(null);
            }
        }, [openedNotification, user]);

        function onNotificationOpening() {
            if (!openedNotification || !user) {
                return;
            }

            switch (openedNotification.type) {
                case NOTIFICATION_TYPES.JOURNEY_REQUEST_RECEIVED:
                    return <Redirect
                        to={{
                            pathname: '/journey/show/' + openedNotification.data.journeyId,
                            state: {
                                journeyId: '/api/journeys/' + openedNotification.data.journeyId,
                            },
                        }}
                    />;
                case NOTIFICATION_TYPES.NEW_MESSAGE_RECEIVED:
                    return <Redirect
                        to={{
                            pathname: '/message/show/:iri',
                            state: {
                                addressee: openedNotification.data.addressee,
                                endUserIri: openedNotification.data.endUserIri,
                                journeyIri: openedNotification.data.journeyIri,
                            },
                        }}
                    />;
                default:
                    return null;
            }
        }

        return (
            <NativeRouter>
                <BackButton/>
                {
                    onNotificationOpening()
                }
                <Switch>
                    {
                        !isTokenValid && <Redirect to="/login"/>
                    }
                    <Route exact path="/" component={SplashScreen}/>
                    <Route exact path="/login" component={Login}/>
                    <Route path="/message/show/:iri" component={MessageShow}/>
                    <Route exact path="/routes" component={RouteList}/>
                    <Route exact path="/journey/request/confirmation"
                           component={RequestPostedConfirmation}/>
                    <View style={styles.filler}>
                        <View style={{backgroundColor: '#404B52', flex: 1}}>
                            <ScrollView
                                keyboardShouldPersistTaps="handled"
                                bounces={false}
                            >
                                <Header/>
                                <Switch>
                                    <Route exact path="/profile/user" component={UserProfileForm}/>
                                    <Route exact path="/profile/car" component={CarProfileForm}/>
                                    <Route exact path="/journey/create" component={CreateJourney}/>
                                    <Route exact path="/journey/show/:iri" component={ShowJourney}/>
                                    <Route path="/search" component={SearchIndex}/>
                                </Switch>
                            </ScrollView>
                            <CloseButton/>
                        </View>
                        <Footer navigationItems={CARPOOLING_FOOTER_NAVIGATION_ITEMS}/>
                    </View>
                </Switch>
                <Toaster/>
            </NativeRouter>
        );
    },
);
