import {Action, ActiveRoute, NotificationParamsType} from '../types/types';

export const SET_MAIN_ROUTE = '@at/routing/SET_MAIN_ROUTE';
export const setMainRoute = (): Action => {
    return {
        payload: null,
        type: SET_MAIN_ROUTE,
    };
};

export const SET_CARPOOLING_ROUTE = '@at/routing/SET_CARPOOLING_ROUTE';
export const setCarpoolingRoute = (): Action => {
    return {
        payload: null,
        type: SET_CARPOOLING_ROUTE,
    };
};

export const SET_ACTIVE_ROUTER = '@at/routing/set_active_router';
export const setActiveRouter = (activeRoute: ActiveRoute): Action => {
    return {
        payload: activeRoute,
        type: SET_ACTIVE_ROUTER,
    };
};

export const SET_OPENED_NOTIFICATION = '@at/routing/set_opened_notification';
export const setOpenedNotification = (notification: NotificationParamsType | null): Action => {
    return {
        payload: notification,
        type: SET_OPENED_NOTIFICATION,
    };
};

export const SET_APP_SWITCHING_MODAL_VISIBILITY = '@at/routing/set_app_switching_modal_visibility';
export const setAppSwitchingModalVisibility = (visible: boolean): Action => {
    return {
        payload: visible,
        type: SET_APP_SWITCHING_MODAL_VISIBILITY,
    };
};
