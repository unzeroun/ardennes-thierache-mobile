import {Action} from '../../types/types';

export const GET_LOCATION_PERMISSION = '@at/permission/get_location_permission';
export const getLocationPermission = (): Action => {
    return {
        payload: null,
        type: GET_LOCATION_PERMISSION,
    };
};

export const LOCATION_PERMISSION_GRANTED = '@at/permission/location_permission_granted';
export const locationPermissionGranted = (): Action => {
    return {
        payload: null,
        type: LOCATION_PERMISSION_GRANTED,
    };
};

export const LOCATION_PERMISSION_DENIED = '@at/permission/location_permission_denied';
export const locationPermissionDenied = (): Action => {
    return {
        payload: null,
        type: LOCATION_PERMISSION_DENIED,
    };
};

export const GET_CAMERA_PERMISSION = '@at/permission/get_camera_permission';
export const getCameraPermission = (): Action => {
    return {
        payload: null,
        type: GET_CAMERA_PERMISSION,
    };
};

export const CAMERA_PERMISSION_GRANTED = '@at/permission/camera_permission_granted';
export const cameraPermissionGranted = (): Action => {
    return {
        payload: null,
        type: CAMERA_PERMISSION_GRANTED,
    };
};

export const CAMERA_PERMISSION_DENIED = '@at/permission/camera_permission_denied';
export const cameraPermissionDenied = (): Action => {
    return {
        payload: null,
        type: CAMERA_PERMISSION_DENIED,
    };
};

export const GET_NOTIFICATION_PERMISSION = '@at/permission/get_notification_permission';
export const getNotificationPermission = (): Action => {
    return {
        payload: null,
        type: GET_NOTIFICATION_PERMISSION,
    };
};

export const NOTIFICATION_PERMISSION_GRANTED = '@at/permission/notification_permission_granted';
export const notificationPermissionGranted = (): Action => {
    return {
        payload: null,
        type: NOTIFICATION_PERMISSION_GRANTED,
    };
};

export const NOTIFICATION_PERMISSION_DENIED = '@at/permission/notification_permission_denied';
export const notificationPermissionDenied = (): Action => {
    return {
        payload: null,
        type: NOTIFICATION_PERMISSION_DENIED,
    };
};
