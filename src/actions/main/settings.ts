import {Action} from '../../types/types';

export const SET_TOASTER = '@at/settings/set_toaster';
export const setToaster = (message: string | null): Action => {
    return {
        payload: message,
        type: SET_TOASTER,
    };
};

export const SET_TOASTER_ERROR = '@at/settings/set_toaster_error';
export const setToasterError = (message: string | null): Action => {
    return {
        payload: message,
        type: SET_TOASTER_ERROR,
    };
};
