import {Camera} from 'expo-camera';
import {CapturedPicture} from 'expo-camera/build/Camera.types';
import {Action, Report} from '../../types/types';

export const RESET_REPORTING = '@at/reporting/reset_reporting';
export const resetReporting = (): Action => {
    return {
        payload: null,
        type: RESET_REPORTING,
    };
};

export const SET_REPORTING_TYPE = '@at/reporting/set_reporting_type';
export const setReportingType = (type: string): Action => {
    return {
        payload: type,
        type: SET_REPORTING_TYPE,
    };
};

export const TAKE_REPORT_PICTURE = '@at/reporting/take_picture';
export const takeReportPicture = (camera: Camera): Action => {
    return {
        payload: camera,
        type: TAKE_REPORT_PICTURE,
    };
};

export const PICTURE_TAKEN = '@at/reporting/picture_taken';
export const pictureTaken = (picture: CapturedPicture): Action => {
    return {
        payload: picture,
        type: PICTURE_TAKEN,
    };
};

export const SET_REPORTING_MESSAGE = '@at/reporting/set_reporting_message';
export const setReportingMessage = (message: string): Action => {
    return {
        payload: message,
        type: SET_REPORTING_MESSAGE,
    };
};

export const SEND_REPORT = '@at/reporting/send_report';
export const sendReport = (report: Report): Action => {
    return {
        payload: report,
        type: SEND_REPORT,
    };
};

export const REPORT_SENT = '@at/reporting/report_sent';
export const reportSent = (): Action => {
    return {
        payload: null,
        type: REPORT_SENT,
    };
};
