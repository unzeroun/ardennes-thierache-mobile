import {NotificationSubscriber} from '../../lib/notification/types/index';
import {Action} from '../../types/types';

export type NotificationSubscriberRecord = {
    identifier: string,
    provider: string,
    topics: string[],
};

export const REQUEST_NOTIFICATION_IDENTIFIER = '@at/notification/request_notification_identifier';
export const requestNotificationIdentifier = (): Action => {
    return {
        payload: null,
        type: REQUEST_NOTIFICATION_IDENTIFIER,
    };
};

export const DEFINE_NOTIFICATION_IDENTIFIER = '@at/notification/define_notification_identifier';
export const defineNotificationIdentifier = (identifier: string): Action => {
    return {
        payload: identifier,
        type: DEFINE_NOTIFICATION_IDENTIFIER,
    };
};

export const REGISTER_NOTIFICATION_SUBSCRIBER = '@at/notification/register_notification_subscriber';
export const registerNotificationSubscriber = (subscriber: NotificationSubscriberRecord): Action => {
    return {
        payload: subscriber,
        type: REGISTER_NOTIFICATION_SUBSCRIBER,
    };
};

export const REQUEST_NOTIFICATION_SUBSCRIBER = '@at/notification/request_notification_subscriber';
export const requestNotificationSubscriber = (identifier: string | null): Action => {
    return {
        payload: identifier,
        type: REQUEST_NOTIFICATION_SUBSCRIBER,
    };
};

export const RECEIVED_NOTIFICATION_SUBSCRIBER = '@at/notification/received_notification_subscriber';
export const receivedNotificationSubscriber = (subscriber: NotificationSubscriber | null): Action => {
    return {
        payload: subscriber,
        type: RECEIVED_NOTIFICATION_SUBSCRIBER,
    };
};

export const UPDATE_NOTIFICATION_SUBSCRIBER = '@at/notification/update_notification_subscriber';
export const updateNotificationSubscriber = (record: NotificationSubscriberRecord, id: number): Action => {
    return {
        payload: {record, id},
        type: UPDATE_NOTIFICATION_SUBSCRIBER,
    };
};
