import {LocationData} from 'expo-location';
import {Action} from '../../types/types';

export const GET_CURRENT_POSITION = '@at/location/get_current_position';
export const getCurrentPosition = (): Action => {
    return {
        payload: null,
        type: GET_CURRENT_POSITION,
    };
};

export const CURRENT_POSITION_RETRIEVED = '@at/location/current_position_retrieved';
export const currentPositionRetrieved = (currentPosition: LocationData): Action => {
    return {
        payload: currentPosition,
        type: CURRENT_POSITION_RETRIEVED,
    };
};
