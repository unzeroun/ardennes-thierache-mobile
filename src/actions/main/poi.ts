import {Action, Poi} from '../../types/types';

export const REQUEST_POIS = '@at/poi/request_pois';
export const requestPois = (): Action => {
    return {
        payload: null,
        type: REQUEST_POIS,
    };
};

export const RECEIVED_POIS = '@at/poi/received_pois';
export const receivedPois = (pois: Poi[]): Action => {
    return {
        payload: pois,
        type: RECEIVED_POIS,
    };
};

export const SET_CURRENT_POI = '@at/poi/set_current_poi';
export const setCurrentPoi = (poi: Poi | null): Action => {
    return {
        payload: poi,
        type: SET_CURRENT_POI,
    };
};

export const SET_POI_PANEL_EXPANSION = '@at/poi/set_poi_panel_expansion';
export const setPoiPanelExpansion = (expanded: boolean): Action => {
    return {
        payload: expanded,
        type: SET_POI_PANEL_EXPANSION,
    };
};
