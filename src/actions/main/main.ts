import {Action} from '../../types/types';

export const OPEN_SIDE_MENU = '@at/main/open_side_menu';
export const openSideMenu = (): Action => {
    return {
        payload: null,
        type: OPEN_SIDE_MENU,
    };
};

export const CLOSE_SIDE_MENU = '@at/main/close_side_menu';
export const closeSideMenu = (): Action => {
    return {
        payload: null,
        type: CLOSE_SIDE_MENU,
    };
};

export const ACTIVATE_GOD_MODE = '@at/main/activate_god_mode';
export const activateGodMode = (): Action => {
    return {
        payload: null,
        type: ACTIVATE_GOD_MODE,
    };
};

export const DISABLE_GOD_MODE = '@at/main/disable_god_mode';
export const disableGodMode = (): Action => {
    return {
        payload: null,
        type: DISABLE_GOD_MODE,
    };
};
