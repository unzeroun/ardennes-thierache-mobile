import {CLOSE_BUTTON_STYLES} from '../../reducers/carpooling/ui';
import {Action} from '../../types/types';

export const SET_CLOSE_BUTTON_VISIBILITY = '@at/carpooling/set_close_button_visibility';
export const setCloseButtonVisibility = (visible: boolean): Action => {
    return {
        payload: visible,
        type: SET_CLOSE_BUTTON_VISIBILITY,
    };
};

export const SET_CLOSE_BUTTON_STYLE = '@at/carpooling/set_close_button_style';
export const setCloseButtonStyle = (style: CLOSE_BUTTON_STYLES): Action => {
    return {
        payload: style,
        type: SET_CLOSE_BUTTON_STYLE,
    };
};
