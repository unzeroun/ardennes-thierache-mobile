import {Action, Token} from '../../types/types';

export const REQUEST_LOGIN_REQUEST = '@at/login/request_login_request';
export const requestLoginRequest = (email: string): Action => {
    return {
        payload: email,
        type: REQUEST_LOGIN_REQUEST,
    };
};

export const RECEIVED_LOGIN_REQUEST_SUCCESS = '@at/login/received_login_request_success';
export const receivedLoginRequestSuccess = (): Action => {
    return {
        payload: null,
        type: RECEIVED_LOGIN_REQUEST_SUCCESS,
    };
};

export const RECEIVED_LOGIN_REQUEST_ERROR = '@at/login/received_login_request_error';
export const receivedLoginRequestError = (): Action => {
    return {
        payload: null,
        type: RECEIVED_LOGIN_REQUEST_ERROR,
    };
};

export const REQUEST_NEW_MAGIC_LINK = '@at/login/request_new_magic_link';
export const requestNewMagicLink = (email: string): Action => {
    return {
        payload: email,
        type: REQUEST_NEW_MAGIC_LINK,
    };
};

export const REQUEST_CODE_VERIFICATION = '@at/login/request_code_verification';
export const requestCodeVerification = (requestCode: string, requestEmail?: string): Action => {
    return {
        payload: {requestCode, requestEmail},
        type: REQUEST_CODE_VERIFICATION,
    };
};

export const CODE_VERIFICATION_SUCCESS = '@at/login/code_verification_success';
export const codeVerificationSuccess = (token: Token): Action => {
    return {
        payload: token,
        type: CODE_VERIFICATION_SUCCESS,
    };
};

export const CODE_VERIFICATION_ERROR = '@at/login/code_verification_error';
export const codeVerificationError = (): Action => {
    return {
        payload: null,
        type: CODE_VERIFICATION_ERROR,
    };
};
