import {Action, Car, Token, User} from '../../types/types';

export const SUBMIT_USER_PROFILE_UPDATE = '@at/profile/submit_user_profile_update';
export const submitUserProfileUpdate = (user: User): Action => {
    return {
        payload: user,
        type: SUBMIT_USER_PROFILE_UPDATE,
    };
};

export const USER_PROFILE_UPDATE_SUCCESS = '@at/profile/user_profile_update_success';
export const userProfileUpdateSuccess = (user: User): Action => {
    return {
        payload: user,
        type: USER_PROFILE_UPDATE_SUCCESS,
    };
};

export const USER_PROFILE_UPDATE_ERROR = '@at/profile/user_profile_update_error';
export const userProfileUpdateError = (): Action => {
    return {
        payload: null,
        type: USER_PROFILE_UPDATE_ERROR,
    };
};

export const REQUEST_USER_PROFILE = '@at/profile/request_user_profile';
export const requestUserProfile = (): Action => {
    return {
        payload: null,
        type: REQUEST_USER_PROFILE,
    };
};

export const REQUEST_USER_PROFILE_SUCCESS = '@at/profile/request_user_profile_success';
export const requestUserProfileSuccess = (user: User): Action => {
    return {
        payload: user,
        type: REQUEST_USER_PROFILE_SUCCESS,
    };
};

export const REQUEST_USER_PROFILE_ERROR = '@at/profile/request_user_profile_error';
export const requestUserProfileError = (): Action => {
    return {
        payload: null,
        type: REQUEST_USER_PROFILE_ERROR,
    };
};

export const RESET_USER_PROFILE_REQUEST_STATE = '@at/profile/reset_user_profile_request_state';
export const resetUserProfileRequestState = (): Action => {
    return {
        payload: null,
        type: RESET_USER_PROFILE_REQUEST_STATE,
    };
};

export const REQUEST_TOKEN_USER_DATA = '@at/profile/request_token_user_data';
export const requestTokenUserData = (token: Token): Action => {
    return {
        payload: token,
        type: REQUEST_TOKEN_USER_DATA,
    };
};

export const REQUEST_USER_CAR_PROFILE = '@at/profile/request_user_car_profile';
export const requestUserCarProfile = (iri: string): Action => {
    return {
        payload: iri,
        type: REQUEST_USER_CAR_PROFILE,
    };
};

export const REQUEST_USER_CAR_PROFILE_SUCCESS = '@at/profile/request_user_car_profile_success';
export const requestUserCarProfileSuccess = (car: Car): Action => {
    return {
        payload: car,
        type: REQUEST_USER_CAR_PROFILE_SUCCESS,
    };
};

export const REQUEST_USER_CAR_PROFILE_ERROR = '@at/profile/request_user_car_profile_error';
export const requestUserCarProfileError = (): Action => {
    return {
        payload: null,
        type: REQUEST_USER_CAR_PROFILE_ERROR,
    };
};

export const SUBMIT_CAR_PROFILE_UPDATE = '@at/profile/submit_car_profile_update';
export const submitCarProfileUpdate = (car: Car): Action => {
    return {
        payload: car,
        type: SUBMIT_CAR_PROFILE_UPDATE,
    };
};

export const CAR_PROFILE_UPDATE_SUCCESS = '@at/car_profile_update_success';
export const carProfileUpdateSuccess = (car: Car): Action => {
    return {
        payload: car,
        type: CAR_PROFILE_UPDATE_SUCCESS,
    };
};

export const CAR_PROFILE_UPDATE_ERROR = '@at/car_profile_update_error';
export const carProfileUpdateError = (): Action => {
    return {
        payload: null,
        type: REQUEST_USER_CAR_PROFILE_ERROR,
    };
};

export const RESET_CAR_PROFILE_REQUEST_STATE = '@at/reset_car_profile_request_state';
export const resetCarProfileRequestState = (): Action => {
    return {
        payload: null,
        type: RESET_CAR_PROFILE_REQUEST_STATE,
    };
};
