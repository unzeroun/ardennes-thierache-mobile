import {Action, Journey} from '../../types/types';

export const REQUEST_JOURNEY_SEARCH = '@at/search/request_journey_search';
export const requestJourneySearch = (departure: string, arrival: string): Action => {
    return {
        payload: {
            arrival,
            departure,
        },
        type: REQUEST_JOURNEY_SEARCH,
    };
};

export const JOURNEY_SEARCH_SUCCESS = '@at/search/journey_search_request';
export const journeyResearchSuccess = (journeyList: Journey[]): Action => {
    return {
        payload: journeyList,
        type: JOURNEY_SEARCH_SUCCESS,
    };
};

export const JOURNEY_SEARCH_ERROR = '@at/search/journey_search_error';
export const journeyResearchError = (): Action => {
    return {
        payload: null,
        type: JOURNEY_SEARCH_ERROR,
    };
};
