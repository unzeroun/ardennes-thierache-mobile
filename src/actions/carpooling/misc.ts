import {Action, Violation} from '../../types/types';

export const SET_ERROR = '@at/misc/set_error';
export const setError = (error: string | null): Action => {
    return {
        payload: error,
        type: SET_ERROR,
    };
};

export const SET_VIOLATIONS_ARRAY = '@at/misc/set_violations_array';
export const setViolationsArray = (violations: Violation[]): Action => {
    return {
        payload: violations,
        type: SET_VIOLATIONS_ARRAY,
    };
};
