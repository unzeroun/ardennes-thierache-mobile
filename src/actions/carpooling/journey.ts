import {CitySuggestionType} from '../../components/carpooling/journey/CitySuggestion';
import {Action, Journey} from '../../types/types';

export const REQUEST_USER_JOURNEY_LIST = '@at/journey/request_user_journey_list';
export const requestUserJourneyList = (): Action => {
    return {
        payload: null,
        type: REQUEST_USER_JOURNEY_LIST,
    };
};

export const USER_JOURNEY_LIST_RECEIVED_SUCCESS = '@at/journey/user_journey_list_received_success';
export const userJourneyListReceivedSuccess = (journeyList: Journey[]): Action => {
    return {
        payload: journeyList,
        type: USER_JOURNEY_LIST_RECEIVED_SUCCESS,
    };
};

export const REQUEST_ALL_JOURNEYS_LIST = '@at/journey/request_all_journeys_list';
export const requestAllJourneysList = (): Action => {
    return {
        payload: null,
        type: REQUEST_ALL_JOURNEYS_LIST,
    };
};

export const ALL_JOURNEYS_LIST_RETRIEVED_SUCCESS = '@at/journey/all_journeys_list_retrieved_success';
export const allJourneysListRetrievedSuccess = (journeys: Journey[]): Action => {
    return {
        payload: journeys,
        type: ALL_JOURNEYS_LIST_RETRIEVED_SUCCESS,
    };
};

export const REQUEST_PENDING_JOURNEY_LIST = '@at/journey/request_pending_journey_list';
export const requestPendingJourneyList = (): Action => {
    return {
        payload: null,
        type: REQUEST_PENDING_JOURNEY_LIST,
    };
};

export const PENDING_JOURNEY_LIST_SUCCESS = '@at/journey/pending_journey_list_success';
export const pendingJourneyListSuccess = (journeys: Journey[]): Action => {
    return {
        payload: journeys,
        type: PENDING_JOURNEY_LIST_SUCCESS,
    };
};

export const SET_NEXT_ALL_JOURNEYS_PAGE_IRI = '@at/journey/set_next_all_journeys_page_iri';
export const setNextAllJourneysPageIri = (nextPageIri: string | null): Action => {
    return {
        payload: nextPageIri,
        type: SET_NEXT_ALL_JOURNEYS_PAGE_IRI,
    };
};

export const REQUEST_ALL_JOURNEYS_LIST_NEXT_PAGE = '@at/journey/request_all_journeys_list_next_page';
export const requestAllJourneysListNextPage = (nextPageIri: string): Action => {
    return {
        payload: nextPageIri,
        type: REQUEST_ALL_JOURNEYS_LIST_NEXT_PAGE,
    };
};

export const REQUEST_ALL_JOURNEYS_LIST_NEXT_PAGE_SUCCESS = '@at/journey/request_all_journeys_list_next_page_success';
export const requestAllJourneysListNextPageSuccess = (journeys: Journey[]): Action => {
    return {
        payload: journeys,
        type: REQUEST_ALL_JOURNEYS_LIST_NEXT_PAGE_SUCCESS,
    };
};

export const REQUEST_JOURNEY_BY_ID = '@at/journey/request_journey_by_id';
export const requestJourneyById = (iri: string): Action => {
    return {
        payload: iri,
        type: REQUEST_JOURNEY_BY_ID,
    };
};

export const JOURNEY_RECEIVED_SUCCESS = '@at/journey/journey_received_success';
export const journeyReceivedSuccess = (journey: Journey): Action => {
    return {
        payload: journey,
        type: JOURNEY_RECEIVED_SUCCESS,
    };
};

export const SUBMIT_JOURNEY_CREATION = '@at/journey/submit_journey_creation';
export const submitJourneyCreation = (journey: Journey): Action => {
    return {
        payload: journey,
        type: SUBMIT_JOURNEY_CREATION,
    };
};

export const JOURNEY_CREATION_SUCCESS = '@at/journey/journey_creation_success';
export const journeyCreationSuccess = (): Action => {
    return {
        payload: null,
        type: JOURNEY_CREATION_SUCCESS,
    };
};

export const JOURNEY_CREATION_ERROR = '@at/journey/journey_creation_error';
export const journeyCreationError = (): Action => {
    return {
        payload: null,
        type: JOURNEY_CREATION_ERROR,
    };
};

export const RESET_JOURNEY_CREATION_STATE = '@at/journey/reset_journey_creation_state';
export const resetJourneyCreationState = (): Action => {
    return {
        payload: null,
        type: RESET_JOURNEY_CREATION_STATE,
    };
};

export const REQUEST_GEO_DEPARTURE_SEARCH = '@at/journey/request_geo_departure_search';
export const requestGeoDepartureSearch = (address: string): Action => {
    return {
        payload: address,
        type: REQUEST_GEO_DEPARTURE_SEARCH,
    };
};

export const REQUEST_GEO_ARRIVAL_SEARCH = '@at/journey/request_geo_arrival_search';
export const requestGeoArrivalSearch = (address: string): Action => {
    return {
        payload: address,
        type: REQUEST_GEO_ARRIVAL_SEARCH,
    };
};

export const SET_DEPARTURE_CITY_SUGGESTIONS = '@at/journey/set_departure_city_suggestions';
export const setDepartureCitySuggestions = (citySuggestions: CitySuggestionType[]): Action => {
    return {
        payload: citySuggestions,
        type: SET_DEPARTURE_CITY_SUGGESTIONS,
    };
};

export const SET_DEPARTURE_SELECTED_CITY_SUGGESTION = '@at/journey/set_departure_selected_city_suggestion';
export const setDepartureSelectedCitySuggestion = (citySuggestion: CitySuggestionType | null): Action => {
    return {
        payload: citySuggestion,
        type: SET_DEPARTURE_SELECTED_CITY_SUGGESTION,
    };
};

export const SET_ARRIVAL_CITY_SUGGESTIONS = '@at/journey/set_arrival_city_suggestions';
export const setArrivalCitySuggestions = (citySuggestions: CitySuggestionType[]): Action => {
    return {
        payload: citySuggestions,
        type: SET_ARRIVAL_CITY_SUGGESTIONS,
    };
};

export const SET_ARRIVAL_SELECTED_CITY_SUGGESTION = '@at/journey/set_arrival_selected_city_suggestion';
export const setArrivalSelectedCitySuggestion = (citySuggestion: CitySuggestionType | null): Action => {
    return {
        payload: citySuggestion,
        type: SET_ARRIVAL_SELECTED_CITY_SUGGESTION,
    };
};

export const RESET_CITY_SUGGESTIONS = '@at/journey/reset_city_suggestions';
export const resetCitySuggestions = (): Action => {
    return {
        payload: null,
        type: RESET_CITY_SUGGESTIONS,
    };
};

export const RESET_CURRENT_JOURNEY = '@at/journey/reset_current_journey';
export const resetCurrentJourney = (): Action => {
    return {
        payload: null,
        type: RESET_CURRENT_JOURNEY,
    };
};
