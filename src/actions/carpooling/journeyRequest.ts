import {Action, JourneyRequest} from '../../types/types';

export const REQUEST_CREATE_JOURNEY_REQUEST = '@at/journey_request/request_create_journey_request';
export const requestCreateJourneyRequest = (journeyIri: string): Action => {
    return {
        payload: journeyIri,
        type: REQUEST_CREATE_JOURNEY_REQUEST,
    };
};

export const CREATE_JOURNEY_REQUEST_SUCCESS = '@at/journey_request/create_journey_request_success';
export const createJourneyRequestSuccess = (journeyIri?: string): Action => {
    return {
        payload: journeyIri,
        type: CREATE_JOURNEY_REQUEST_SUCCESS,
    };
};

export const GET_JOURNEY_REQUESTS = '@at/journey_request/get_journey_requests';
export const getJourneyRequests = (journeyId: string): Action => {
    return {
        payload: journeyId,
        type: GET_JOURNEY_REQUESTS,
    };
};

export const GET_JOURNEY_REQUEST_SUCCESS = '@at/journey_request/get_journey_request_success';
export const getJourneyRequestSuccess = (journeyRequests: JourneyRequest[]): Action => {
    return {
        payload: journeyRequests,
        type: GET_JOURNEY_REQUEST_SUCCESS,
    };
};

export const GET_JOURNEY_REQUEST_ERROR = '@at/journey_request/get_journey_request_error';
export const getJourneyRequestError = (): Action => {
    return {
        payload: null,
        type: GET_JOURNEY_REQUEST_ERROR,
    };
};

export const ACCEPT_JOURNEY_REQUEST = '@at/journey_request/accept_journey_request';
export const acceptJourneyRequest = (journeyRequestIri: string, journeyIri: string): Action => {
    return {
        payload: {
            journeyIri,
            journeyRequestIri,
        },
        type: ACCEPT_JOURNEY_REQUEST,
    };
};

export const ACCEPT_JOURNEY_REQUEST_SUCCESS = '@at/journey_request/accept_journey_request_success';
export const acceptJourneyRequestSuccess = (journeyIri: string): Action => {
    return {
        payload: journeyIri,
        type: ACCEPT_JOURNEY_REQUEST_SUCCESS,
    };
};

export const DENY_JOURNEY_REQUEST = '@at/journey_request/deny_journey_request';
export const denyJourneyRequest = (journeyRequestIri: string, journeyIri: string): Action => {
    return {
        payload: {
            journeyIri,
            journeyRequestIri,
        },
        type: DENY_JOURNEY_REQUEST,
    };
};

export const DENY_JOURNEY_REQUEST_SUCCESS = '@at/journey_request/deny_journey_request_success';
export const denyJourneyRequestSuccess = (journeyIri: string): Action => {
    return {
        payload: journeyIri,
        type: DENY_JOURNEY_REQUEST_SUCCESS,
    };
};

export const RESET_JOURNEY_REQUEST_REDUCER = '@at/reset_journey_request_reducer';
export const resetJourneyRequestReducer = (): Action => {
    return {
        payload: null,
        type: RESET_JOURNEY_REQUEST_REDUCER,
    };
};

export const SET_CURRENT_JOURNEY_REQUEST = '@at/set_current_journey_request';
export const setCurrentJourneyRequest = (journeyRequest: JourneyRequest | null): Action => {
    return {
        payload: journeyRequest,
        type: SET_CURRENT_JOURNEY_REQUEST,
    };
};
