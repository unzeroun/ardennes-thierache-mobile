import {Action, Message} from '../../types/types';

export const GET_MESSAGES = '@at/message/get_messages';
export const getMessages = (journeyIri: string): Action => {
    return {
        payload: journeyIri,
        type: GET_MESSAGES,
    };
};

export const GET_MESSAGES_SUCCESS = '@at/message/get_messages_success';
export const getMessagesSuccess = (messages: Message[]): Action => {
    return {
        payload: messages,
        type: GET_MESSAGES_SUCCESS,
    };
};

export const GET_MESSAGES_ERROR = '@at/message/get_message_error';
export const getMessageError = (): Action => {
    return {
        payload: null,
        type: GET_MESSAGES_ERROR,
    };
};

export const MARK_MESSAGES_AS_READ = '@at/message/mark_messages_as_read';
export const markMessagesAsRead = (journeyIri: string, endUserIri: string): Action => {
    return {
        payload: {
            endUserIri,
            journeyIri,
        },
        type: MARK_MESSAGES_AS_READ,
    };
};

export const MARK_MESSAGES_AS_READ_SUCCESS = '@at/message/mark_messages_as_read_success';
export const markMessagesAsReadSuccess = (): Action => {
    return {
        payload: null,
        type: MARK_MESSAGES_AS_READ_SUCCESS,
    };
};

export const SEND_MESSAGE = '@at/message/send_message';
export const sendMessage = (journeyIri: string, withEndUserIri: string, content: string): Action => {
    return {
        payload: {
            content,
            journeyIri,
            withEndUserIri,
        },
        type: SEND_MESSAGE,
    };
};

export const SEND_MESSAGE_SUCCESS = '@at/message/send_message_success';
export const sendMessageSuccess = (): Action => {
    return {
        payload: null,
        type: SEND_MESSAGE_SUCCESS,
    };
};

export const INCREASE_DISPLAYED_MESSAGES_COUNT = '@at/message/increase_displayed_messages_count';
export const increaseDisplayedMessagesCount = (): Action => {
    return {
        payload: null,
        type: INCREASE_DISPLAYED_MESSAGES_COUNT,
    };
};

export const RESET_DISPLAYED_MESSAGES_COUNT = '@at/message/reset_displayed_messages_count';
export const resetDisplayedMessagesCount = (): Action => {
    return {
        payload: null,
        type: RESET_DISPLAYED_MESSAGES_COUNT,
    };
};

export const GET_ALL_UNREAD_MESSAGES = '@at/message/get_all_unread_messages';
export const getAllUnreadMessages = (): Action => {
    return {
        payload: null,
        type: GET_ALL_UNREAD_MESSAGES,
    };
};

export const GET_ALL_UNREAD_MESSAGES_SUCCESS = '@at/messages/get_all_unread_messages_success';
export const getAllUnreadMessagesSuccess = (messages: Message[]): Action => {
    return {
        payload: messages,
        type: GET_ALL_UNREAD_MESSAGES_SUCCESS,
    };
};

export const GET_ALL_UNREAD_MESSAGES_ERROR = '@at/messages/get_all_unread_messages_error';
export const getAllUnreadMessagesError = (): Action => {
    return {
        payload: null,
        type: GET_ALL_UNREAD_MESSAGES_ERROR,
    };
};
