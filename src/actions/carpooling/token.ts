import {Action, Token} from '../../types/types';

export const GET_TOKEN = '@at/token/get_token';
export const getToken = (): Action => {
    return {
        payload: null,
        type: GET_TOKEN,
    };
};

export const GET_TOKEN_SUCCESS = '@at/token/get_token_success';
export const getTokenSuccess = (token: Token): Action => {
    return {
        payload: token,
        type: GET_TOKEN_SUCCESS,
    };
};

export const GET_TOKEN_ERROR = '@at/token/get_token_error';
export const getTokenError = (): Action => {
    return {
        payload: null,
        type: GET_TOKEN_ERROR,
    };
};

export const STORE_TOKEN = '@at/token/store_token';
export const storeToken = (token: Token): Action => {
    return {
        payload: token,
        type: STORE_TOKEN,
    };

};

export const STORE_TOKEN_SUCCESS = '@at/token/store_token_success';
export const storeTokenSuccess = (token: Token): Action => {
    return {
        payload: token,
        type: STORE_TOKEN_SUCCESS,
    };
};

export const STORE_TOKEN_ERROR = '@at/token/store_token_error';
export const storeTokenError = (): Action => {
    return {
        payload: null,
        type: STORE_TOKEN_ERROR,
    };
};

export const REFRESH_TOKEN = '@at/token/refresh_token';
export const refreshToken = (token: Token): Action => {
    return {
        payload: token,
        type: REFRESH_TOKEN,
    };
};

export const REFRESH_TOKEN_SUCCESS = '@at/token/refresh_token_success';
export const refreshTokenSuccess = (token: Token): Action => {
    return {
        payload: token,
        type: REFRESH_TOKEN_SUCCESS,
    };
};

export const REFRESH_TOKEN_ERROR = '@at/token/refresh_token_error';
export const refreshTokenError = (): Action => {
    return {
        payload: null,
        type: REFRESH_TOKEN_ERROR,
    };
};

export const SET_TOKEN_VALIDITY = '@at/token/set_token_validity';
export const setTokenValidity = (isValid: boolean): Action => {
    return {
        payload: isValid,
        type: SET_TOKEN_VALIDITY,
    };
};
