import {combineReducers} from 'redux';
import journey, {JourneyState} from './carpooling/journey';
import journeyRequest, {JourneyRequestState} from './carpooling/journeyRequest';
import login, {LoginState} from './carpooling/login';
import message, {MessageState} from './carpooling/message';
import misc, {MiscState} from './carpooling/misc';
import profile, {ProfileState} from './carpooling/profile';
import search, {SearchState} from './carpooling/search';
import token, {TokenState} from './carpooling/token';
import ui, {UiState} from './carpooling/ui';
import location, {LocationState} from './main/location';
import main, {MainState} from './main/main';
import notification, {NotificationState} from './main/notification';
import permission, {PermissionState} from './main/permission';
import poi, {PoiState} from './main/poi';
import reporting, {ReportingState} from './main/reporting';
import settings, {SettingsState} from './main/settings';
import routing, {RoutingState} from './routing';

export type State = {
    journey: JourneyState,
    journeyRequest: JourneyRequestState,
    location: LocationState,
    login: LoginState,
    main: MainState,
    message: MessageState,
    misc: MiscState,
    notification: NotificationState,
    permission: PermissionState,
    poi: PoiState,
    profile: ProfileState,
    reporting: ReportingState,
    routing: RoutingState,
    search: SearchState,
    settings: SettingsState,
    token: TokenState,
    ui: UiState,
};

export default combineReducers({
    journey,
    journeyRequest,
    location,
    login,
    main,
    message,
    misc,
    notification,
    permission,
    poi,
    profile,
    reporting,
    routing,
    search,
    settings,
    token,
    ui,
});
