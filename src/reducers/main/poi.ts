import * as actionTypes from '../../actions/main/poi';
import {Action, Poi} from '../../types/types';

export type PoiState = {
    currentPoi: Poi | null,
    loading: boolean,
    poiPanelExpanded: boolean,
    pois: Poi[],
};

const initialState: PoiState = {
    currentPoi: null,
    loading: false,
    poiPanelExpanded: false,
    pois: [],
};

export default function poiReducer(state: PoiState = initialState, action: Action): PoiState {
    switch (action.type) {
        case actionTypes.REQUEST_POIS:
            return {
                ...state,
                loading: true,
            };
        case actionTypes.RECEIVED_POIS:
            return {
                ...state,
                loading: false,
                pois: action.payload,
            };
        case actionTypes.SET_CURRENT_POI:
            return {
                ...state,
                currentPoi: action.payload,
            };
        case actionTypes.SET_POI_PANEL_EXPANSION:
            return {
                ...state,
                poiPanelExpanded: action.payload,
            };
        default:
            return state;
    }
}
