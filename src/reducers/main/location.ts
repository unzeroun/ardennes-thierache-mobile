import {LocationObject} from 'expo-location';
import * as actionTypes from '../../actions/main/location';
import {Action} from '../../types/types';

export type LocationState = {
    currentPosition: LocationObject | null,
    loading: boolean,
};

const initialState: LocationState = {
    currentPosition: null,
    loading: false,
};

export default function locationReducer(state: LocationState = initialState, action: Action): LocationState {
    switch (action.type) {
        case actionTypes.GET_CURRENT_POSITION:
            return {
                ...state,
                loading: true,
            };
        case actionTypes.CURRENT_POSITION_RETRIEVED:
            return {
                ...state,
                currentPosition: action.payload,
                loading: false,
            };
        default:
            return state;
    }
}
