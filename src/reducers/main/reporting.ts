import {CapturedPicture} from 'expo-camera/build/Camera.types';
import {LocationObject} from 'expo-location';
import * as actionTypes from '../../actions/main/reporting';
import {Action} from '../../types/types';

export type ReportingState = {
    loading: boolean,
    reportingLocation: LocationObject | null,
    reportingMessage: string,
    reportingPicture: CapturedPicture | null,
    reportingType: string | null,
    sendingReport: boolean,
};

const initialState: ReportingState = {
    loading: false,
    reportingLocation: null,
    reportingMessage: '',
    reportingPicture: null,
    reportingType: null,
    sendingReport: false,
};

export default function ReportingReducer(state: ReportingState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.RESET_REPORTING:
            return {
                ...state,
                loading: false,
                reportingLocation: null,
                reportingPicture: null,
            };
        case actionTypes.SET_REPORTING_TYPE:
            return {
                ...state,
                reportingType: action.payload,
            };
        case actionTypes.TAKE_REPORT_PICTURE:
            return {
                ...state,
                loading: true,
            };
        case actionTypes.PICTURE_TAKEN:
            return {
                ...state,
                loading: false,
                reportingPicture: action.payload,
            };
        case actionTypes.SET_REPORTING_MESSAGE:
            return {
                ...state,
                reportingMessage: action.payload,
            };
        case actionTypes.SEND_REPORT:
            return {
                ...state,
                sendingReport: true,
            };
        case actionTypes.REPORT_SENT:
            return {
                ...state,
                sendingReport: false,
            };
        default:
            return state;
    }
}
