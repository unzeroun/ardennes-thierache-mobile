import * as actionTypes from '../../actions/main/main';
import {Action} from '../../types/types';

export type MainState = {
    godMode: boolean,
    isSideMenuOpen: boolean,
    loading: boolean,
};

const initialState: MainState = {
    godMode: false,
    isSideMenuOpen: false,
    loading: false,
};

export default function defaultReducer(state: MainState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.OPEN_SIDE_MENU:
            return {
                ...state,
                isSideMenuOpen: true,
            };
        case actionTypes.CLOSE_SIDE_MENU:
            return {
                ...state,
                isSideMenuOpen: false,
            };
        case actionTypes.ACTIVATE_GOD_MODE:
            return {
                ...state,
                godMode: true,
            };
        case actionTypes.DISABLE_GOD_MODE:
            return {
                ...state,
                godMode: false,
            };
        default:
            return state;
    }
}
