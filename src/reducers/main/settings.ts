import * as actionTypes from '../../actions/main/settings';
import {Action} from '../../types/types';

export type SettingsState = {
    errored: boolean,
    message: string | null,
};

const initialState: SettingsState = {
    errored: false,
    message: null,
};

export default function settingsReducer(state: SettingsState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.SET_TOASTER:
            return {
                ...state,
                errored: false,
                message: action.payload,
            };
        case actionTypes.SET_TOASTER_ERROR:
            return {
                ...state,
                errored: true,
                message: state.message ? state.message : action.payload,
            };
        default:
            return state;
    }
}
