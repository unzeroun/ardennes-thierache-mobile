import * as actionTypes from '../../actions/main/permission';
import {Action} from '../../types/types';

export type PermissionState = {
    cameraPermission: boolean,
    loading: boolean,
    locationPermission: boolean,
    notificationPermission: boolean,
};

const initialState: PermissionState = {
    cameraPermission: false,
    loading: false,
    locationPermission: false,
    notificationPermission: false,
};

export default function PermissionReducer(state: PermissionState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.LOCATION_PERMISSION_GRANTED:
            return {
                ...state,
                locationPermission: true,
            };
        case actionTypes.LOCATION_PERMISSION_DENIED:
            return {
                ...state,
                locationPermission: false,
            };
        case actionTypes.CAMERA_PERMISSION_GRANTED:
            return {
                ...state,
                cameraPermission: true,
            };
        case actionTypes.CAMERA_PERMISSION_DENIED:
            return {
                ...state,
                cameraPermission: false,
            };
        case actionTypes.NOTIFICATION_PERMISSION_GRANTED:
            return {
                ...state,
                notificationPermission: true,
            };
        case actionTypes.NOTIFICATION_PERMISSION_DENIED:
            return {
                ...state,
                notificationPermission: false,
            };
        default:
            return state;
    }
}
