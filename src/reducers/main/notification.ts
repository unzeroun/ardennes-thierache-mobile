import * as actionTypes from '../../actions/main/notification';
import {NotificationSubscriber} from '../../lib/notification/types';
import {Action} from '../../types/types';

export type NotificationState = {
    notificationIdentifier: string | null,
    notificationSubscriber: NotificationSubscriber| null,
    requestingSubscriber: boolean,
};

const initialState: NotificationState = {
    notificationIdentifier: null,
    notificationSubscriber: null,
    requestingSubscriber: false,
};

export default function notificationReducer(state: NotificationState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.DEFINE_NOTIFICATION_IDENTIFIER:
            return {
                ...state,
                notificationIdentifier: action.payload,
            };
        case actionTypes.REQUEST_NOTIFICATION_SUBSCRIBER:
            return {
                ...state,
                requestingSubscriber: true,
            };
        case actionTypes.RECEIVED_NOTIFICATION_SUBSCRIBER:
            return {
                ...state,
                notificationSubscriber: action.payload,
                requestingSubscriber: false,
            };
        default:
            return state;
    }
}
