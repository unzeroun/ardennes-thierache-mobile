import * as actionTypes from '../../actions/carpooling/message';
import {SET_TOKEN_VALIDITY} from '../../actions/carpooling/token';
import {Action, Message, MessageListType} from '../../types/types';

export type MessageState = {
    messageList: MessageListType,
    messagesDisplayedCount: number,
    unreadMessages: MessageListType,
};

const initialState: MessageState = {
    messageList: {},
    messagesDisplayedCount: 0,
    unreadMessages: {},
};

export default function messageReducer(state: MessageState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.GET_MESSAGES_SUCCESS:
            return {
                ...state,
                messageList: getOrderMessages(action.payload),
            };
        case actionTypes.INCREASE_DISPLAYED_MESSAGES_COUNT:
            return {
                ...state,
                messagesDisplayedCount: state.messagesDisplayedCount + 1,
            };
        case actionTypes.RESET_DISPLAYED_MESSAGES_COUNT:
            return {
                ...state,
                messagesDisplayedCount: 0,
            };
        case actionTypes.GET_ALL_UNREAD_MESSAGES_SUCCESS:
            return {
                ...state,
                unreadMessages: getUnreadMessages(action.payload),
            };
        case SET_TOKEN_VALIDITY:
            return action.payload ? state : initialState;
        default:
            return state;
    }
}

function getOrderMessages(messages: Message[]): MessageListType {
    const messageList: MessageListType = {};
    messages.filter(message => {
        if (!messageList[message.withEndUser['@id']]) {
            messageList[message.withEndUser['@id']] = [];
        }

        messageList[message.withEndUser['@id']].push(message);
    });

    return messageList;
}

function getUnreadMessages(messages: Message[]): MessageListType {
    const messageList: MessageListType = {};
    messages.filter(message => {
        if (!messageList[message.journey]) {
            messageList[message.journey] = [];
        }

        messageList[message.journey].push(message);
    });

    return messageList;
}
