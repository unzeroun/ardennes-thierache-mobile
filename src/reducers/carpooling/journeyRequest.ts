import * as actionTypes from '../../actions/carpooling/journeyRequest';
import {SET_TOKEN_VALIDITY} from '../../actions/carpooling/token';
import {Action, JourneyRequest} from '../../types/types';

export type JourneyRequestState = {
    allJourneyRequests: JourneyRequest[],
    currentJourneyRequest: JourneyRequest | null,
    loading: boolean,
    requestSent: boolean,
};

const initialState: JourneyRequestState = {
    allJourneyRequests: [],
    currentJourneyRequest: null,
    loading: false,
    requestSent: false,
};

export default function journeyRequestReducer(state: JourneyRequestState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.GET_JOURNEY_REQUESTS: {
            return {
                ...state,
                loading: true,
            };
        }
        case actionTypes.GET_JOURNEY_REQUEST_SUCCESS: {
            return {
                ...state,
                allJourneyRequests: action.payload,
                loading: false,
            };
        }
        case actionTypes.REQUEST_CREATE_JOURNEY_REQUEST: {
            return {
                ...state,
                loading: true,
            };
        }
        case actionTypes.CREATE_JOURNEY_REQUEST_SUCCESS: {
            return {
                ...state,
                loading: false,
                requestSent: true,
            };
        }
        case actionTypes.RESET_JOURNEY_REQUEST_REDUCER: {
            return {
                ...state,
                requestSent: false,
            };
        }
        case actionTypes.SET_CURRENT_JOURNEY_REQUEST: {
            return {
                ...state,
                currentJourneyRequest: action.payload,
            };
        }
        case actionTypes.ACCEPT_JOURNEY_REQUEST: {
            return {
                ...state,
                loading: true,
            };
        }
        case actionTypes.ACCEPT_JOURNEY_REQUEST_SUCCESS: {
            return {
                ...state,
                loading: false,
            };
        }
        case SET_TOKEN_VALIDITY:
            return action.payload ? state : initialState;
        default:
            return state;
    }
}
