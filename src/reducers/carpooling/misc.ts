import * as journeyTypes from '../../actions/carpooling/journey';
import * as actionTypes from '../../actions/carpooling/misc';
import {SET_TOKEN_VALIDITY} from '../../actions/carpooling/token';
import {Action, ErrorMap, Violation} from '../../types/types';

export type MiscState = {
    error: string | null,
    errorMap: ErrorMap,
};

const initialState: MiscState = {
    error: null,
    errorMap: {},
};

export default function miscReducer(state: MiscState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.SET_ERROR:
            return {
                ...state,
                error: action.payload,
            };
        case actionTypes.SET_VIOLATIONS_ARRAY:
            return {
                ...state,
                errorMap: formatViolationsArray(action.payload),
            };
        case journeyTypes.RESET_JOURNEY_CREATION_STATE:
            return initialState;
        case SET_TOKEN_VALIDITY:
            return action.payload ? state : initialState;
        default:
            return state;
    }
}

function formatViolationsArray(violations: Violation[]) {
    return violations.reduce((prev: ErrorMap, current: Violation) => {
        prev[current.propertyPath] = current.message;
        return prev;
    }, {});
}
