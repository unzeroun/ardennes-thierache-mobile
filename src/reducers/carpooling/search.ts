import * as actionTypes from '../../actions/carpooling/serach';
import {Action, Journey} from '../../types/types';

export type SearchState = {
    journeyList: Journey[],
    loading: boolean,
};

const initialState: SearchState = {
    journeyList: [],
    loading: false,
};

export default function searchReducer(state: SearchState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.REQUEST_JOURNEY_SEARCH: {
            return {
                ...state,
                journeyList: [],
                loading: true,
            };
        }
        case actionTypes.JOURNEY_SEARCH_SUCCESS: {
            return {
                ...state,
                journeyList: action.payload,
                loading: false,
            };
        }
        case actionTypes.JOURNEY_SEARCH_ERROR: {
            return {
                ...state,
                loading: false,
            };
        }
        default:
            return state;
    }
}
