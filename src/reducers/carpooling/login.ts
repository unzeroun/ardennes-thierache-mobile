import * as actionTypes from '../../actions/carpooling/login';
import * as profileActionTypes from '../../actions/carpooling/profile';
import * as tokenActionTypes from '../../actions/carpooling/token';
import {Action} from '../../types/types';

export type LoginState = {
    firstConnection: boolean,
    loading: boolean,
    requestCode: string | null,
    requestedLogin: boolean,
    userEmail: string | null,
};

const initialState: LoginState = {
    firstConnection: true,
    loading: false,
    requestCode: null,
    requestedLogin: false,
    userEmail: null,
};

export default function loginReducer(state: LoginState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.REQUEST_LOGIN_REQUEST: {
            return {
                ...state,
                firstConnection: true,
                loading: true,
                requestedLogin: false,
                userEmail: action.payload,
            };
        }
        case actionTypes.RECEIVED_LOGIN_REQUEST_SUCCESS: {
            return {
                ...state,
                loading: false,
                requestedLogin: true,
            };
        }
        case actionTypes.RECEIVED_LOGIN_REQUEST_ERROR: {
            return {
                ...state,
                loading: false,
            };
        }
        case actionTypes.REQUEST_NEW_MAGIC_LINK: {
            return {
                ...state,
                // firstConnection: false,
            };
        }
        case actionTypes.REQUEST_CODE_VERIFICATION: {
            return {
                ...state,
                loading: true,
                requestCode: action.payload.requestCode,
                userEmail: action.payload.requestEmail ? action.payload.requestEmail : state.userEmail,
            };
        }
        case actionTypes.CODE_VERIFICATION_SUCCESS: {
            return {
                ...state,
                loading: false,
            };
        }
        case actionTypes.CODE_VERIFICATION_ERROR: {
            return {
                ...state,
                loading: false,
            };
        }
        case profileActionTypes.USER_PROFILE_UPDATE_SUCCESS:
            return {
                ...state,
                firstConnection: false,
            };
        case tokenActionTypes.GET_TOKEN_SUCCESS:
            return {
                ...state,
                // firstConnection: false,
            };
        case tokenActionTypes.SET_TOKEN_VALIDITY:
            if (action.payload) {
                return state;
            }
            return initialState;
        default:
            return state;
    }
}
