import * as actionTypes from '../../actions/carpooling/ui';
import {Action} from '../../types/types';

export enum CLOSE_BUTTON_STYLES {
    WHITE = 'white',
    YELLOW = 'yellow',
}

export type UiState = {
    closeButtonStyle: CLOSE_BUTTON_STYLES,
    isCloseButtonVisible: boolean,
};

const initialState: UiState = {
    closeButtonStyle: CLOSE_BUTTON_STYLES.WHITE,
    isCloseButtonVisible: false,
};

export default function uiReducer(state: UiState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.SET_CLOSE_BUTTON_VISIBILITY:
            return {
                ...state,
                isCloseButtonVisible: action.payload,
            };
        case actionTypes.SET_CLOSE_BUTTON_STYLE:
            return {
                ...state,
                closeButtonStyle: action.payload,
            };
        default:
            return state;
    }
}
