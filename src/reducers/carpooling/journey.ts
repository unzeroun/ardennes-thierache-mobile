import {LatLng} from 'react-native-maps';
import * as actionTypes from '../../actions/carpooling/journey';
import {SET_TOKEN_VALIDITY} from '../../actions/carpooling/token';
import {CitySuggestionType} from '../../components/carpooling/journey/CitySuggestion';
import {Action, Journey} from '../../types/types';

export type JourneyState = {
    allJourneys: Journey[],
    arrivalCitySuggestions: CitySuggestionType[],
    arrivalSelectedSuggestion: CitySuggestionType | null,
    currentJourney: Journey | null,
    departureCitySuggestions: CitySuggestionType[],
    departureSelectedSuggestion: CitySuggestionType | null,
    journeyCreated: boolean,
    loading: boolean,
    loadingUserList: boolean,
    loadingAllList: boolean,
    loadingNextList: boolean,
    loadingPendingList: boolean,
    nextPageIri: string | null,
    pendingJourneys: Journey[],
    arrivalCoordinate: LatLng | null,
    departureCoordinate: LatLng | null,
    userJourneys: Journey[],
};

const initialState: JourneyState = {
    allJourneys: [],
    arrivalCitySuggestions: [],
    arrivalCoordinate: null,
    arrivalSelectedSuggestion: null,
    currentJourney: null,
    departureCitySuggestions: [],
    departureCoordinate: null,
    departureSelectedSuggestion: null,
    journeyCreated: false,
    loading: false,
    loadingAllList: true,
    loadingNextList: false,
    loadingPendingList: true,
    loadingUserList: true,
    nextPageIri: null,
    pendingJourneys: [],
    userJourneys: [],
};

export default function journeyReducer(state: JourneyState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.SUBMIT_JOURNEY_CREATION:
            return {
                ...state,
                loading: true,
            };
        case actionTypes.SET_ARRIVAL_CITY_SUGGESTIONS:
            return {
                ...state,
                arrivalCitySuggestions: action.payload,
            };
        case actionTypes.SET_ARRIVAL_SELECTED_CITY_SUGGESTION:
            return {
                ...state,
                arrivalCitySuggestions: [],
                arrivalSelectedSuggestion: action.payload,
            };
        case actionTypes.SET_DEPARTURE_CITY_SUGGESTIONS:
            return {
                ...state,
                departureCitySuggestions: action.payload,
                departureSelectedSuggestion: null,
            };
        case actionTypes.SET_DEPARTURE_SELECTED_CITY_SUGGESTION:
            return {
                ...state,
                departureCitySuggestions: [],
                departureSelectedSuggestion: action.payload,
            };
        case actionTypes.RESET_CITY_SUGGESTIONS:
            return {
                ...state,
                arrivalCitySuggestions: [],
                departureCitySuggestions: [],
            };
        case actionTypes.JOURNEY_CREATION_SUCCESS:
            return {
                ...state,
                arrivalCitySuggestions: [],
                arrivalSelectedSuggestion: null,
                currentJourney: null,
                departureCitySuggestions: [],
                departureSelectedSuggestion: null,
                journeyCreated: true,
                loading: false,
                selectedArrival: null,
                selectedDeparture: null,
            };
        case actionTypes.JOURNEY_CREATION_ERROR:
            return {
                ...state,
                loading: false,
            };
        case actionTypes.RESET_JOURNEY_CREATION_STATE:
            return {
                ...state,
                journeyCreated: false,
                loading: false,
            };
        case actionTypes.REQUEST_USER_JOURNEY_LIST:
            return {
                ...state,
                loadingUserList: true,
            };
        case actionTypes.USER_JOURNEY_LIST_RECEIVED_SUCCESS:
            return {
                ...state,
                loadingUserList: false,
                userJourneys: action.payload,
            };
        case actionTypes.REQUEST_ALL_JOURNEYS_LIST: {
            return {
                ...state,
                loadingAllList: true,
            };
        }
        case actionTypes.ALL_JOURNEYS_LIST_RETRIEVED_SUCCESS: {
            return {
                ...state,
                allJourneys: action.payload,
                loadingAllList: false,
            };
        }
        case actionTypes.SET_NEXT_ALL_JOURNEYS_PAGE_IRI: {
            return {
                ...state,
                nextPageIri: action.payload,
            };
        }
        case actionTypes.REQUEST_ALL_JOURNEYS_LIST_NEXT_PAGE: {
            return {
                ...state,
                loadingNextList: true,
            };
        }
        case actionTypes.REQUEST_ALL_JOURNEYS_LIST_NEXT_PAGE_SUCCESS: {
            const journeys = state.allJourneys;
            journeys.push(...action.payload);
            return {
                ...state,
                allJourneys: journeys,
                loadingNextList: false,
            };
        }
        case actionTypes.JOURNEY_RECEIVED_SUCCESS:
            return {
                ...state,
                currentJourney: action.payload,
            };
        case actionTypes.REQUEST_PENDING_JOURNEY_LIST: {
            return {
                ...state,
                loadingPendingList: true,
            };
        }
        case actionTypes.PENDING_JOURNEY_LIST_SUCCESS:
            return {
                ...state,
                loadingPendingList: false,
                pendingJourneys: action.payload,
            };
        case actionTypes.RESET_CURRENT_JOURNEY:
            return {
                ...state,
                currentJourney: null,
            };
        case SET_TOKEN_VALIDITY:
            return action.payload ? state : initialState;
        default:
            return state;
    }
}
