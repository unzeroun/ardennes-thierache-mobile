import * as loginActionTypes from '../../actions/carpooling/login';
import * as actionTypes from '../../actions/carpooling/token';
import {Action, Token} from '../../types/types';

export type TokenState = {
    isTokenValid: boolean,
    token: Token | null,
};

const initialState: TokenState = {
    isTokenValid: true,
    token: null,
};

export default function tokenReducer(state: TokenState = initialState, action: Action) {
    switch (action.type) {
        case loginActionTypes.CODE_VERIFICATION_SUCCESS: {
            return {
                ...state,
                token: action.payload,
            };
        }
        case loginActionTypes.REQUEST_LOGIN_REQUEST: {
            return {
                ...state,
                isTokenValid: true,
            };
        }
        case actionTypes.GET_TOKEN: {
            return {
                ...state,
                isTokenValid: true,
            };
        }
        case actionTypes.GET_TOKEN_SUCCESS: {
            return {
                ...state,
                token: action.payload,
            };
        }
        case actionTypes.STORE_TOKEN: {
            return {
                ...state,
                token: action.payload,
            };
        }
        case actionTypes.SET_TOKEN_VALIDITY: {
            return {
                ...state,
                isTokenValid: action.payload,
                token: null,
            };
        }
        default:
            return state;
    }
}
