import * as loginActions from '../../actions/carpooling/login';
import * as actionTypes from '../../actions/carpooling/profile';
import * as tokenActions from '../../actions/carpooling/token';
import {SET_TOKEN_VALIDITY} from '../../actions/carpooling/token';
import {getIdFromToken, getUsernameFromToken} from '../../lib/tokenManagement';
import {Action, Car, User} from '../../types/types';

export type ProfileState = {
    car: Car | null,
    carProfileUpdated: boolean,
    loading: boolean,
    profileRequestError: boolean,
    user: User | null,
    userProfileUpdated: boolean,
};

const initialState: ProfileState = {
    car: null,
    carProfileUpdated: false,
    loading: false,
    profileRequestError: false,
    user: null,
    userProfileUpdated: false,
};

export default function profileReducer(state: ProfileState = initialState, action: Action) {
    switch (action.type) {
        case tokenActions.SET_TOKEN_VALIDITY:
            return action.type ? state : initialState;
        case actionTypes.SUBMIT_USER_PROFILE_UPDATE:
            return {
                ...state,
                loading: true,
                profileRequestError: false,
            };
        case actionTypes.USER_PROFILE_UPDATE_SUCCESS:
            return {
                ...state,
                loading: false,
                user: action.payload,
                userProfileUpdated: true,
            };
        case actionTypes.USER_PROFILE_UPDATE_ERROR:
            return {
                ...state,
                loading: false,
            };
        case actionTypes.REQUEST_TOKEN_USER_DATA:
            return {
                ...state,
                user: {
                    ...state.user,
                    email: getUsernameFromToken(action.payload),
                    id: getIdFromToken(action.payload),
                },
            };
        case actionTypes.REQUEST_USER_PROFILE: {
            return {
                ...state,
                profileRequestError: false,
            };
        }
        case actionTypes.REQUEST_USER_PROFILE_SUCCESS: {
            return {
                ...state,
                user: action.payload,
            };
        }
        case actionTypes.REQUEST_USER_PROFILE_ERROR:
            return {
                ...state,
                profileRequestError: true,
            };
        case actionTypes.RESET_USER_PROFILE_REQUEST_STATE:
            return {
                ...state,
                userProfileUpdated: false,
            };
        case actionTypes.REQUEST_USER_CAR_PROFILE:
            return {
                ...state,
                profileRequestError: false,
            };
        case actionTypes.REQUEST_USER_CAR_PROFILE_SUCCESS:
            return {
                ...state,
                car: action.payload,
            };
        case actionTypes.REQUEST_USER_CAR_PROFILE_ERROR:
            return {
                ...state,
                profileRequestError: true,
            };
        case actionTypes.SUBMIT_CAR_PROFILE_UPDATE:
            return {
                ...state,
                loading: true,
            };
        case actionTypes.CAR_PROFILE_UPDATE_SUCCESS:
            return {
                ...state,
                car: action.payload,
                carProfileUpdated: true,
                loading: false,
            };
        case actionTypes.CAR_PROFILE_UPDATE_ERROR:
            return {
                ...state,
                loading: false,
            };
        case actionTypes.RESET_CAR_PROFILE_REQUEST_STATE:
            return {
                ...state,
                carProfileUpdated: false,
            };
        case loginActions.REQUEST_LOGIN_REQUEST:
            return initialState;
        case SET_TOKEN_VALIDITY:
            return action.payload ? state : initialState;
        default:
            return state;
    }
}
