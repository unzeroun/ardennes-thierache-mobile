import * as actionTypes from '../actions/routing';
import {Action, ActiveRoute, NotificationParamsType} from '../types/types';

export type RoutingState = {
    activeRoute: ActiveRoute,
    appSwitchingModalVisible: boolean,
    openedNotification: NotificationParamsType | null,
};

const initialState: RoutingState = {
    activeRoute: ActiveRoute.MAIN,
    appSwitchingModalVisible: false,
    openedNotification: null,
};

export default function routingReducer(state: RoutingState = initialState, action: Action) {
    switch (action.type) {
        case actionTypes.SET_MAIN_ROUTE:
            return {
                ...state,
                activeRoute: ActiveRoute.MAIN,
            };
        case actionTypes.SET_CARPOOLING_ROUTE:
            return {
                ...state,
                activeRoute: ActiveRoute.CARPOOLING,
            };
        case actionTypes.SET_ACTIVE_ROUTER:
            return {
                ...state,
                activeRoute: action.payload,
            };
        case actionTypes.SET_OPENED_NOTIFICATION:
            return {
                ...state,
                openedNotification: action.payload,
            };
        case actionTypes.SET_APP_SWITCHING_MODAL_VISIBILITY:
            return {
                ...state,
                appSwitchingModalVisible: action.payload,
            };
        default:
            return state;
    }
}
