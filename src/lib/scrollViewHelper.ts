import {LayoutRectangle, NativeScrollEvent} from 'react-native';

export function isComponentInViewPort(
    viewportLayout: LayoutRectangle,
    scrollViewEvent: NativeScrollEvent,
    component: LayoutRectangle,
): boolean {
    return component.y - viewportLayout.height - scrollViewEvent.contentOffset.y <= 0;
}
