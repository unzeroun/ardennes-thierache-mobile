import {useEffect} from 'react';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {getMessages} from '../actions/carpooling/message';
import useInterval from './useInterval';

type DispatchProps = {
    getMessages: (journeyIri: string) => void,
};

type OwnProps = {
    journeyIri: string,
};

type Props = DispatchProps & OwnProps;

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        getMessages,
    }, dispatch);
}

export default connect<{}, DispatchProps, OwnProps>(null, mapDispatchToProps)(
    function ChatUpdater({getMessages, journeyIri}: Props) {

        const [setUpdater, clearUpdater] = useInterval(() => {
            getMessages(journeyIri);
        }, 5000);

        useEffect(() => {
            return () => clearUpdater();
        }, []);

        useEffect(() => {
            restartUpdateCycle();
        }, [journeyIri]);

        function startUpdateCycle() {
            setUpdater();
        }

        function stopUpdateCycle() {
            clearUpdater();
        }

        function restartUpdateCycle() {
            stopUpdateCycle();
            startUpdateCycle();
        }

        return null;
    },
);
