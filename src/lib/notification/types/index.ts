import {Connection, Edge, HasTimestamp, PageInfo} from '../../../concertosdk/common/types';

export interface NotificationTopic extends HasTimestamp {
    _id: number;
    id: string;
    slug: string;
    name: string;
    private: boolean;
}

export interface NotificationTopicEdge extends Edge {
    node: NotificationTopic;
}

export interface NotificationTopicConnection extends Connection {
    edges: NotificationTopicEdge[];
    pageInfo: NotificationTopicPageInfo;
}

interface NotificationTopicPageInfo extends PageInfo {
}

export interface NotificationSubscriber extends HasTimestamp {
    _id: number;
    id: string;
    identifier: string;
    provider: string;
    topics: NotificationTopicConnection;
}

export interface NotificationSubscriberEdge extends Edge {
    node: NotificationSubscriber;
}

export interface NotificationSubscriberConnection extends Connection {
    edges: NotificationSubscriberEdge[];
    pageInfo: NotificationSubscriberPageInfo;
}

interface NotificationSubscriberPageInfo extends PageInfo {
}
