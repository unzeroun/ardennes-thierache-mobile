import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';
import {ConsolidatedQueryResult} from '../../../concertosdk/common/containers/withCommonLifecycle';
import {getNotificationTopics} from '../queries/index.graphql';
import {NotificationTopicConnection} from '../types';

export type NotificationTopicsProps = {
    notificationTopics: NotificationTopicConnection,
};

export type WrappedProps = NotificationTopicsProps;

export default function withNotificationTopics(WrappedComponent: ComponentType<WrappedProps>) {
    return ({...props}) => {
        return (
            <Query query={getNotificationTopics}>
                {
                    ({data}: ConsolidatedQueryResult<NotificationTopicsProps>) => {
                        if (!data) {
                            return null;
                        }

                        return (
                            <WrappedComponent notificationTopics={data.notificationTopics} {...props}/>
                        );
                    }
                }
            </Query>
        );
    };
}
