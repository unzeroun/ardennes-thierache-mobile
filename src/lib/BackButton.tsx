import { LocationState} from 'history';
import React, {useState} from 'react';
import {BackHandler} from 'react-native';
import {connect} from 'react-redux';
import {__RouterContext as RouterContext} from 'react-router';
import Redux, {Action, bindActionCreators} from 'redux';
import {setPoiPanelExpansion} from '../actions/main/poi';
import {OwnHistory} from '../MainRouter';
import {State} from '../reducers';

type StateProps = {
    poiPanelExpanded: boolean,
};

type DispatchProps = {
    setPoiPanelExpansion: (expanded: boolean) => void,
};

type Props = StateProps & DispatchProps;

function mapStateToProps(state: State): StateProps {
    return {
        poiPanelExpanded: state.poi.poiPanelExpanded,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        setPoiPanelExpansion,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function BackHandlerButton({poiPanelExpanded, setPoiPanelExpansion}: Props) {
        const [history, setHistory] = useState<OwnHistory<LocationState> | null>(null);

        function initHandler(history: OwnHistory<LocationState>) {
            setHistory(history);
            BackHandler.addEventListener('hardwareBackPress', () => handleBack());
        }

        function handleBack() {

            if (!history) {
                return false;
            }

            if (history.index === 0) {
                return false;
            } else {
                if (poiPanelExpanded) {
                    setPoiPanelExpansion(false);
                } else {
                    history.goBack();
                }

                return true;
            }
        }

        return (
            <RouterContext.Consumer>
                {context => {
                    initHandler(context.history);
                    return null;
                }}
            </RouterContext.Consumer>
        );
    },
);
