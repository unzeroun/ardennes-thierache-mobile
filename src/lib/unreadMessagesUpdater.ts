import {useEffect} from 'react';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {getAllUnreadMessages} from '../actions/carpooling/message';
import useInterval from './useInterval';

type DispatchProps = {
    getAllUnreadMessages: () => void,
};

type Props = DispatchProps;

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        getAllUnreadMessages,
    }, dispatch);
}

export default connect<{}, DispatchProps>(null, mapDispatchToProps)(
    function ChatUpdater({getAllUnreadMessages}: Props) {

        const [setUpdater, clearUpdater] = useInterval(() => {
            getAllUnreadMessages();
        }, 5000);

        useEffect(() => {
            return () => clearUpdater();
        }, []);

        useEffect(() => {
            setUpdater();
        }, []);

        return null;
    },
);
