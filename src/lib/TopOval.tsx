import React from 'react';
import {Dimensions} from 'react-native';
import {Defs, Ellipse, RadialGradient, Stop, Svg} from 'react-native-svg';

type OwnProps = {
    backgroundColor?: string,
    color?: string,
    height?: number,
    widthFactor?: number,
};

type Props = OwnProps;

export default function OvalSvg({
                                    color = '#FFF',
                                    height = 50,
                                    backgroundColor = '#FFF',
                                    widthFactor = 1.5,
                                }: Props) {
    return (
        <Svg
            height={height}
            width={Dimensions.get('window').width}
            style={{backgroundColor}}
        >
            <Defs>
                <RadialGradient
                    id="grad"
                    cx={Dimensions.get('window').width / 2}
                    cy={height + height * .1}
                    rx={Dimensions.get('window').width / widthFactor}
                    ry={height}
                    fx="50%"
                    fy="50%"
                    gradientUnits="userSpaceOnUse"
                >
                    <Stop offset="100%" stopColor="#000" stopOpacity="0"/>
                    <Stop offset="25%" stopColor="#000" stopOpacity="1"/>
                </RadialGradient>
            </Defs>
            <Ellipse
                cx={Dimensions.get('window').width / 2}
                cy={height}
                rx={Dimensions.get('window').width / widthFactor}
                ry={height}
                rotation={0}
                fill="url(#grad)"
            />
            <Ellipse
                cx={Dimensions.get('window').width / 2}
                cy={height + height * .2}
                rx={Dimensions.get('window').width / widthFactor}
                ry={height}
                rotation={0}
                fill={color}
            />
        </Svg>
    );
}
