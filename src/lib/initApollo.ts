import {ApolloClient, HttpLink, InMemoryCache} from 'apollo-boost/lib/index';
import {API_ENTRYPOINT} from '../../config';

export default function initApollo() {

    return new ApolloClient({
        cache: new InMemoryCache(),
        link: new HttpLink({
            uri: API_ENTRYPOINT,
        }),
    });
}
