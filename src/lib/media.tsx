import React from 'react';
import {Image, ImageResizeMode, ImageStyle, StyleProp, ViewStyle} from 'react-native';
import WebView from 'react-native-webview';
import styles from '../styles/main/lib/media';
import {MediaUrl} from './mediaHelper';

type Props = {
    imageResizeMode?: ImageResizeMode | null,
    imageStyle?: StyleProp<ImageStyle> | null,
    mediaUrl: MediaUrl | null,
    webViewStyle?: StyleProp<ViewStyle> | null,
};

export default function Media({
                                  imageResizeMode = null,
                                  imageStyle = null,
                                  mediaUrl,
                                  webViewStyle = null,
                              }: Props) {
    if (!mediaUrl) {
        return null;
    }

    if ('hosted_video' === mediaUrl.type) {
        return (
            <WebView
                source={{uri: mediaUrl.url}}
                style={[styles.webView, webViewStyle]}
            />
        );
    }

    if ('image' === mediaUrl.type) {
        return (
            <Image
                resizeMode={imageResizeMode || 'cover'}
                source={{uri: mediaUrl.url}}
                style={[styles.image, imageStyle]}
            />
        );
    }

    return null;
}
