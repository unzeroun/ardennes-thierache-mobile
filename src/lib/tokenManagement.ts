import jwtDecode from 'jwt-decode';
import moment from 'moment';
import {JWT_EXPIRE_TIME_LIMIT} from '../../config';
import {Token} from '../types/types';

export type DecodedToken = {
    iat: number,
    exp: number,
    roles: string[],
    username: string,
    id: string,
};

export function decodeJWT(token: string): DecodedToken {
    return jwtDecode(token);
}

export function isJWTValid(token: Token) {
    const decodedToken = decodeJWT(token.token);
    return moment.unix(decodedToken.exp).isAfter(moment());
}

export function willJWTExpire(token: Token) {
    const decodedToken = decodeJWT(token.token);
    return moment.unix(decodedToken.exp).isAfter(moment().add(JWT_EXPIRE_TIME_LIMIT, 's').subtract(2, 'm'));
}

export function getIdFromToken(token: Token): string {
    const decodedToken = decodeJWT(token.token);
    return decodedToken.id;
}

export function getUsernameFromToken(token: Token): string {
    const decodedToken = decodeJWT(token.token);
    return decodedToken.username;
}
