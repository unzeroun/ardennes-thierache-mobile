import {THUMBNAIL_BASE_URL, YOUTUBE_THUMBNAIL_URL} from '../../config';
import {ResourceNode} from '../concertosdk/common/types';
import {Medium} from '../concertosdk/media/types';

export function getMediaThumbnailUrl(resource: ResourceNode) {
    if (!resource.media) {
        return '';
    }

    if (resource.media.data.medias
        && resource.media.data.medias.length > 0
        && 'hosted_video' === resource.media.data.medias[0].type
    ) {
        if (!resource.media.data.medias[0].ref) {
            return '';
        }

        return YOUTUBE_THUMBNAIL_URL.replace(':id', resource.media.data.medias[0].ref);
    }

    if ('aggregate_media' === resource.media.type
        && resource.media.data.medias
        && resource.media.data.medias.length > 0
    ) {
        return THUMBNAIL_BASE_URL + resource.media.data.medias[0].path;
    }

    return THUMBNAIL_BASE_URL + resource.media.thumbnail;
}

export type MediaUrl = {
    type: string,
    url: string,
};

export function getMediaUrl(media: Medium | null): MediaUrl | null {
    if (!media) {
        return null;
    }

    if (media.data.medias
        && media.data.medias.length > 0
        && 'hosted_video' === media.data.medias[0].type
    ) {
        if (!media.data.medias[0].ref) {
            return null;
        }

        return {
            type: 'hosted_video',
            url: 'https://www.youtube.com/embed/' + media.data.medias[0].ref,
        };
    }

    if ('aggregate_media' === media.type && media.data.medias) {
        if (media.data.medias.length === 0) {
            return null;
        }

        return {
            type: 'image',
            url: THUMBNAIL_BASE_URL + media.data.medias[0].path,
        };
    }

    return {
        type: 'image',
        url: THUMBNAIL_BASE_URL + media.thumbnail,
    };
}
