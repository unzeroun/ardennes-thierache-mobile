import React, {ReactElement} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-native';
import Redux, {Action, bindActionCreators} from 'redux';
import {closeSideMenu} from '../actions/main/main';

type DispatchProps = {
    closeSideMenu: () => void,
};

type OwnProps = {
    children: ReactElement,
    page: string,
    route: string,
    slug: string,
};

type Props = DispatchProps & OwnProps;

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        closeSideMenu,
    }, dispatch);
}

function getPathname(route: string, slug: string): string {
    return route.replace(/:slug/i, slug);
}

export default connect<{}, DispatchProps>(null, mapDispatchToProps)(
    function LinkComponent({children, closeSideMenu, page, route, slug}: Props) {

        return (
            <Link onPress={() => closeSideMenu()} to={{
                pathname: getPathname(route, slug),
                state: {
                    page,
                    slug,
                },
            }}>
                {children}
            </Link>
        );
    },
);
