import React, {ReactElement} from 'react';
import {Text, View} from 'react-native';
import styles from '../styles/lib/withErrorComponent';

type OwnProps = {
    children?: ReactElement,
    error?: string,
};

type Props = OwnProps;

export default function WithErrorComponent({children, error}: Props) {
    return (
        <View style={styles.container}>
            {children}
            {
                error && <Text style={styles.errorLabel}>{error}</Text>
            }
        </View>
    );
}
