import {useRef} from 'react';

export default function useTimeout(callback: (args?: any) => any, time: number) {
    const timer = useRef<number | null>(null);

    function clear() {
        if (null !== timer.current) {
            clearTimeout(timer.current);
        }
    }

    function set(args?: any) {
        clear();

        timer.current = setTimeout(() => callback(args), time);
    }

    return [set, clear];
}
