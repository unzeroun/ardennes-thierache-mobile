import moment, {Moment} from 'moment';

const getMonth = (date: string | Moment) => moment(date).format('MMMM');

const year = (date: Moment) => (date.year() > moment().year()) ? ` ${date.year()}` : '';

const hour = (date: string | Moment) => (moment(date).format('H[h]mm'));

type DateProps = {
    begin: string,
    end: string,
};

export function dateWithHour({begin, end}: DateProps) {
    if (begin === null || end === null) {
        return;
    }

    const beginDate = moment(begin);
    const endDate = moment(end);

    if (beginDate.date() === endDate.date()) {
        return `Le ${beginDate.date()} ${getMonth(beginDate)}${year(endDate)} de ${hour(beginDate)} à ${hour(endDate)}`;
    }

    return `Du ${beginDate.date()} ${getMonth(beginDate)} à ${hour(beginDate)} au ${endDate.date()} ` +
        `${getMonth(endDate)} ${year(endDate)} à ${hour(endDate)}`;
}
