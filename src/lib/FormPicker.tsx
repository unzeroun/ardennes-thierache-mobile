import React, {ReactElement, useState} from 'react';
import {
    Modal,
    Picker,
    PickerItemProps,
    Platform,
    StyleProp,
    Text, TextStyle,
    TouchableOpacity,
    View,
    ViewStyle,
} from 'react-native';
import styles from '../styles/lib/FormPicker';

type OwnProps = {
    children: Array<ReactElement<PickerItemProps>>,
    label: string,
    onValueChange: (value: string) => void,
    outerStyle?: StyleProp<ViewStyle>,
    labelStyle?: StyleProp<TextStyle>,
    selectedValue: string,
};

type Props = OwnProps;

export default function FormPicker({
                                       children,
                                       label,
                                       onValueChange,
                                       labelStyle = {},
                                       outerStyle = {},
                                       selectedValue,
                                   }: Props) {

    const [isModalOpened, setModalOpened] = useState<boolean>(false);

    if (Platform.OS === 'ios') {
        return (
            <>
                <TouchableOpacity style={styles.labelContainer} onPress={() => setModalOpened(true)}>
                    <Text style={styles.label}>{label}</Text>
                </TouchableOpacity>
                <Modal
                    transparent
                    visible={isModalOpened}
                    onRequestClose={() => setModalOpened(false)}>
                    <View style={styles.container}>
                        <TouchableOpacity style={styles.backdrop} onPress={() => setModalOpened(false)}/>
                        <View>
                            <View style={styles.pickerContainer}>
                                <Picker
                                    selectedValue={selectedValue}
                                    onValueChange={(value) => onValueChange(value)}
                                    style={outerStyle}
                                >
                                    {children}
                                </Picker>
                            </View>
                            <TouchableOpacity onPress={() => setModalOpened(false)} style={styles.submit}>
                                <Text style={styles.submitLabel}>FERMER</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </>
        );
    }

    return (
        <>
            <Text style={labelStyle}>{label}</Text>
            <Picker
                selectedValue={selectedValue}
                onValueChange={(value) => onValueChange(value)}
                style={outerStyle}
            >
                {children}
            </Picker>
        </>
    );
}
