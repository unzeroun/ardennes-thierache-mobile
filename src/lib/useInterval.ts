import {useRef} from 'react';

export default function useInterval(callback: (args?: any) => any, time: number) {
    const timer = useRef<number | null>(null);

    function clear() {
        if (null !== timer.current) {
            clearInterval(timer.current);
        }
    }

    function set(args?: any) {
        clear();

        timer.current = setInterval(() => callback(args), time);
    }

    return [set, clear];
}
