export function truncate(text: string, limit: number): string {

    if (text.length <= limit) {
        return text;
    }

    const truncateIndex = text.indexOf(' ', limit);

    if (-1 !== truncateIndex) {
        return text.substring(0, truncateIndex) + '...';
    }

    return text;
}
