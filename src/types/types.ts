import {Moment} from 'moment';
import {NOTIFICATION_TYPES} from '../../config';

export type Action = {
    type: string,
    payload: any,
};

type Block = {
    credit: string,
    legend: string,
    path: string,
    type: string,
};

type Position = {
    latitude: number,
    longitude: number,
};

export type Poi = {
    additionalData: {
        contacts: {
            [key: string]: any,
        },
    }
    content: {
        blocks: Block[],
    },
    description: string,
    id: string,
    name: string,
    position: Position,
    [key: string]: any,
};

export type Report = {
    location: Position | null,
    message: string,
    picture: {
        base64?: string,
        path: string,
    } | null,
    type: string,
};

export enum ActiveRoute {
    MAIN = 0,
    CARPOOLING = 1,
}

export type User = {
    address1: string,
    address2: string,
    address3: string,
    birthDate: string,
    city: string,
    description: string,
    devicesIdentifiers: string[],
    drivingLicenseDate: string,
    email: string,
    firstName: string,
    gender: string,
    hasDrivingLicense: boolean,
    iri: string,
    lastName: string,
    phoneNumber: string,
    postCode: string,
};

export type Car = {
    brand: string | null,
    color: string | null,
    iri: string,
    model: string | null,
    preferences: string[],
    year: string | null,
};

export type Route = {
    booked: boolean,
    departureDate: Moment,
    departurePlace: string,
    arrivalPlace: string,
    travelingPreferences: string[],
    user: User,
};

export type Token = {
    token: string,
    refresh_token: string,
};

export type Violation = {
    propertyPath: string,
    message: string,
};

export type ErrorMap = {
    [key: string]: string,
};

export type LocalizedAddress = {
    address1: string,
    address2?: string | null,
    address3?: string | null,
    city: string,
    latitude: number | null,
    longitude: number | null,
    postCode: string,
};

export type GeolocatedAddress = {
    '@id': string,
    latitude: string,
    longitude: string,
    address1: string,
    address2: string,
    address3: string,
    city: string,
    postCode: string,
};

export type Journey = {
    '@id': string,
    arrival: LocalizedAddress,
    availablePlaces: number,
    departure: LocalizedAddress,
    departureDate: string,
    endUser: {
        '@id': string,
        car?: {
            brand: string,
            model: string,
            preferences: string[],
        },
        firstName: string,
        lastName: string,
        phoneNumber: string,
    },
    luggagePossible: boolean,
    price: number,
    type: string,
    unreadMessages: number,
};

export type JourneyRequest = {
    '@id': string,
    requester: {
        '@id': string,
        firstName: string,
        lastName: string,
    },
    status: string,
};

export type Message = {
    '@id': string,
    journey: string,
    withEndUser: {
        '@id': string,
        firstName: string,
        lastName: string,
    },
    writer: {
        '@id': string,
        firstName: string,
        lastName: string,
    },
    content: string,
    sentAt: string,
    read: boolean,
};

export type MessageListType = {
    [key: string]: Message[],
};

export type NotificationParamsType = {
    data: {[key: string]: any},
    type: NOTIFICATION_TYPES,
};
