const block = require('./block/routes');
const blog = require('./blog/routes');
const core = require('./core/routes');
const calendar = require('./calendar/routes');
const directory = require('./directory/routes');
const document = require('./document/routes');

module.exports = {
    ...block,
    ...blog,
    ...calendar,
    ...directory,
    ...document,
    ...core,
    soloist_index: { pattern: '/', page: 'index' },
};
