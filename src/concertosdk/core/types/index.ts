import {RouteParams} from 'next-routes';
import {Connection, Edge, PageInfo, ResourceNode} from '../../common/types';
import {Medium} from '../../media/types';
import {EmbeddedResource} from '../../resource/types';

export interface MenuNode extends Node {
    _id: number;
    id: string;
    type: string;
    root: number;
    level: number;
    lft: number;
    rgt: number;
    parent: MenuNode;
    visibleInMenu: boolean;
    specialKey: string;
    slug: string;
    shortTitle: string;
    title: string;
    description: string;
    media: Medium;
    children?: MenuNode[];
    createdAt: string;
    updatedAt: string;
    specificData: {
        uri: string,
        params: {route: string} & RouteParams,
        node?: MenuNode,
    };
}

export interface MenuNodeConnection extends Connection {
    edges: MenuNodeEdge[];
    pageInfo: MenuNodePageInfo;
}

export interface MenuNodeEdge extends Edge {
    node: MenuNode;
}

export interface MenuNodePageInfo extends PageInfo {
}

export interface MenuPage extends ResourceNode {
    id: string;
    type: string;
    pageType: string;
    level: number;
    lft: number;
    parent: MenuNode;
    blocks: MenuPageBlockConnection;
    rgt: number;
    root: number;
    shortTitle: string;
    specialKey: string;
    visibleInMenu: boolean;
    placementMethod: string;
    refererNode: MenuNode;
    identifier: string;
    createdAt: string;
    updatedAt: string;
    specificData: {
        uri: string,
    };
    _metadata: EmbeddedResource;
}

export interface MenuPageConnection extends Connection {
    edges: MenuPageEdge[];
    pageInfo: MenuPagePageInfo;
}

export interface MenuPageEdge extends Edge {
    node: MenuPage;
}

export interface MenuPagePageInfo extends PageInfo {
}

export interface MenuPageBlockConnection {
    edges: MenuPageBlockEdge[];
}

export interface MenuPageBlockEdge {
    node: MenuPageBlock;
}

export interface MenuPageBlock {
    id: string;
    _id: number;
    value: string;
    displayableData: {
        [key: string]: any;
    };
    name: string;
    type: string;
}
