import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';
import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';
import {getMenuNodes, getMenuNodesBranch} from '../queries/index.graphql';
import {MenuNode, MenuNodeConnection} from '../types';

export type MenuNodesProps = { menuNodes: MenuNodeConnection };

type DecoratedProps = {
    asTree?: boolean,
    specialKey?: string | null,
    id?: string | null,
};

export function makeTree(menuNodes: MenuNodeConnection): MenuNodeConnection {
    function appendChildren(menuNode: MenuNode, currentLevel: number): MenuNode {
        return {
            ...menuNode,
            children: menuNodes.edges
                .filter(({node}) => currentLevel === node.level && node.lft > menuNode.lft && node.rgt < menuNode.rgt)
                .filter(({node}) => node.visibleInMenu)
                .map(({node}) => node)
                .map(node => appendChildren(node, currentLevel + 1)),
        };
    }

    return {
        ...menuNodes,
        edges: menuNodes.edges
            .filter(({node}) => (menuNodes.edges[0].node.level + 1) === node.level)
            .filter(({node}) => node.visibleInMenu)
            .map(edge => ({...edge, node: appendChildren(edge.node, menuNodes.edges[0].node.level + 2)})),
    };
}

export function getQuery({specialKey, id}: {specialKey?: string | null, id?: string | null}) {
    if (specialKey) {
        return getMenuNodesBranch;
    }

    if (id) {
        return getMenuNodesBranch;
    }

    return getMenuNodes;
}

export default function withMenuNodes<T extends object>(
    WrappedComponent: ComponentType<MenuNodesProps>): ComponentType<DecoratedProps & T> {
    return ({asTree = false, id = null, specialKey = null, ...props}: DecoratedProps & T) => {
        return (
            <Query query={getQuery({id, specialKey})} variables={{specialKey, id}}>
                {withCommonLifecycle(
                    ({data}: ConsolidatedQueryResult<MenuNodesProps>) => {
                        if (asTree) {
                            return <WrappedComponent menuNodes={makeTree(data.menuNodes)} {...props} />;
                        }

                        return <WrappedComponent menuNodes={data.menuNodes} {...props} />;
                    },
                )}
            </Query>
        );
    };
}
