import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';
import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';
import {NoResultException} from '../../common/exceptions';
import {HasResourceProps, ResourceConnection} from '../../resource/types';
import routes from '../../routes';
import {getMenuPageBySlug} from '../queries/index.graphql';
import {MenuPage, MenuPageConnection} from '../types';

export type MenuPageDecoratedProps = {
    slug: string,
};

export type MenuPageProps = { page: MenuPage } & HasResourceProps;

export default function withPage(WrappedComponent: ComponentType<MenuPageProps>):
    ComponentType<MenuPageDecoratedProps> {
    return ({slug}: MenuPageDecoratedProps) => {
        const url = routes.soloist_core_page.pattern.replace(/:slug/i, slug);

        return (
            <Query query={getMenuPageBySlug} variables={{slug, url}}>
                {withCommonLifecycle(
                    ({data}: ConsolidatedQueryResult<{
                        menuPages: MenuPageConnection,
                        resources?: ResourceConnection,
                    }>) => {
                        if (0 === data.menuPages.edges.length) {
                            throw new NoResultException('core/page', {slug});
                        }

                        return <WrappedComponent page={data.menuPages.edges[0].node} resources={data.resources}/>;
                    },
                )}
            </Query>
        );
    };
}
