import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';

import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';
import {MenuNode, MenuNodeConnection} from '../types';

const {getMenuNodeById} = require('../queries/index.graphql');

export type MenuNodesProps = { menuNodes: MenuNodeConnection };

type DecoratedProps = {
    id?: number | null,
};

export type QueryResultMenuNode = {
    menuNode: MenuNode,
};

export default function withMenuNode(WrappedComponent: ComponentType<QueryResultMenuNode>) {
    return ({id = null}: DecoratedProps) => {
        return (
            <Query query={getMenuNodeById} variables={{id: '/api/menu_nodes/' + id}}>
                {withCommonLifecycle(
                    ({data}: ConsolidatedQueryResult<QueryResultMenuNode>) => {
                        return <WrappedComponent menuNode={data.menuNode}/>;
                    },
                )}
            </Query>
        );
    };
}
