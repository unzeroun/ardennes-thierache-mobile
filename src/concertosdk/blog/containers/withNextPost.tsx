import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';

import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';

import {Post, PostConnection} from '../types';

const {getNextPost} = require('../queries/index.graphql');

export type PostDecoratedProps = {
    post: Post,
};

export type PostProps = { post: Post };

export default function withNextPost(WrappedComponent: ComponentType<PostProps>): ComponentType<PostDecoratedProps> {
    return ({post}: PostDecoratedProps) => {
        return (
            <Query query={getNextPost} variables={post}>
                {withCommonLifecycle(
                    ({data}: ConsolidatedQueryResult<{posts: PostConnection}>) => data.posts.edges[0] && (
                        <WrappedComponent post={data.posts.edges[0].node} />
                    ) || null,
                )}
            </Query>
        );
    };
}
