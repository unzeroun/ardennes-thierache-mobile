import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';
import withCommonLifecycle, {ConsolidatedQueryResult, fetchMore} from '../../common/containers/withCommonLifecycle';
import {getPosts} from '../queries/index.graphql';
import {PostConnection} from '../types';

export type VariableProps = {
    first?: number,
    after?: string | null,
    afterDate?: string | null,
    categories_id?: number | null,
    categories_slug?: string | null,
};

export type PostsDecoratedProps = {
    [key: string]: any,
} & VariableProps;

export type PostsProps = {
    posts: PostConnection,
};

export type WrappedProps = PostsProps & {
    loadMore: null | (() => void),
};

function loadMore(data: PostsProps, fetchMore: fetchMore<PostsProps, VariableProps>) {
    return () => {
        if (data.posts.edges.length >= data.posts.totalCount || !data.posts.pageInfo.hasNextPage) {
            return;
        }

        const lastCursor = data.posts.edges[data.posts.edges.length - 1].cursor;

        fetchMore({
            updateQuery: (prev, {fetchMoreResult}) => {
                if (!fetchMoreResult) {
                    return prev;
                }

                return {
                    ...prev,
                    posts: {
                        ...prev.posts,
                        edges: [...prev.posts.edges, ...fetchMoreResult.posts.edges],
                        pageInfo: {
                            ...prev.posts.pageInfo,
                            hasNextPage: fetchMoreResult.posts.pageInfo.hasNextPage,
                        },
                    },
                };
            },
            variables: {
                after: lastCursor,
            },
        });
    };
}

export default function withPosts(WrappedComponent: ComponentType<WrappedProps>) {
    return ({
                first = 30, after = null, afterDate = null, categories_id = null, categories_slug = null, ...props
            }: PostsDecoratedProps) => {
        return (
            <Query query={getPosts} variables={{first, after, afterDate, categories_id, categories_slug}}>
                {withCommonLifecycle(
                    ({data, fetchMore}: ConsolidatedQueryResult<PostsProps, VariableProps>) => {
                        return (
                            <WrappedComponent posts={data.posts}
                                              loadMore={data.posts.pageInfo.hasNextPage
                                                  ? loadMore(data, fetchMore) : null} {...props} />
                        );
                    },
                )}
            </Query>
        );
    };
}
