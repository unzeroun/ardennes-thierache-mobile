import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';
import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';
import {HasResourceProps, ResourceConnection} from '../../resource/types';
import routes from '../../routes';
import {getPostBySlug, getPostsById} from '../queries/index.graphql';
import {Post, PostConnection} from '../types';

export type PostDecoratedProps = {
    id?: number
    slug?: string
    url?: string,
};

export type PostProps = { post: Post } & HasResourceProps;

export default function withPost(WrappedComponent: ComponentType<PostProps>): ComponentType<PostDecoratedProps> {
    return (props: PostDecoratedProps) => {
        if (props.id) {
            return (
                <Query query={getPostsById} variables={{id: '/api/posts/' + props.id}}>
                    {withCommonLifecycle(
                        ({data}: ConsolidatedQueryResult<{post: Post}>) => (
                            <WrappedComponent post={data.post} />
                        ),
                    )}
                </Query>
            );
        }

        let url: string | null = null;
        if (props.slug) {
            url = routes.soloist_blog_post_show.pattern.replace(/:slug/i, props.slug);
        }
        if (props.url) {
            url = props.url;
        }

        return (
            <Query query={getPostBySlug} variables={{...props, url}}>
                {withCommonLifecycle(
                    ({data}: ConsolidatedQueryResult<{posts: PostConnection, resources?: ResourceConnection}>) => (
                        <WrappedComponent post={data.posts.edges[0].node} resources={data.resources}/>
                    ),
                )}
            </Query>
        );
    };
}
