import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';

import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';

import {Post, PostConnection} from '../types';

const {getPreviousPost} = require('../queries/index.graphql');

export type PostDecoratedProps = {
    post: Post,
};

export type PostProps = { post: Post };

export default function withPreviousPost(WrappedComponent: ComponentType<PostProps>):
    ComponentType<PostDecoratedProps> {
    return ({post}: PostDecoratedProps) => {
        return (
            <Query query={getPreviousPost} variables={post}>
                {withCommonLifecycle(
                    ({data}: ConsolidatedQueryResult<{posts: PostConnection}>) => data.posts.edges[0] && (
                        <WrappedComponent post={data.posts.edges[0].node} />
                    ) || null,
                )}
            </Query>
        );
    };
}
