module.exports = {
    soloist_blog_post_show: { pattern: '/blog/posts/:slug', page: 'blog/post' },
    soloist_blog_index: { pattern: '/blog/posts', page: 'blog/index' },
    soloist_blog_category_show: { pattern: '/blog/category/:slug', page: 'blog/index' }
};
