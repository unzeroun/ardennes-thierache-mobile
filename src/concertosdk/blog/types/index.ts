import {Connection, ContentResourceNode, Edge, PageInfo} from '../../common/types';
export interface Post extends ContentResourceNode {
    body: string;
    publishedAt: string;
    lead: boolean;
    workflowState: string;
    categories: CategoryConnection;
}

export interface PostConnection extends Connection {
    edges: PostEdge[];
    pageInfo: PostPageInfo;
}

export interface PostEdge extends Edge {
    node: Post;
}

interface PostPageInfo extends PageInfo {
}

export interface Category extends ContentResourceNode {
}

export interface CategoryEdge extends Edge {
    node: Category;
}

export interface CategoryConnection extends Connection {
    edges: CategoryEdge[];
}
