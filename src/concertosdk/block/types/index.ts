import {Connection, ContentResourceNode, Edge, HasTimestamp, Node, PageInfo} from '../../common/types';

export interface PageBlock extends ContentResourceNode {
    template: string;
    routeParams: { [key: string]: any };
    specialKey: string;
    blocks: PageBlockBlockConnection;
}

export interface PageBlockBlock extends Node, HasTimestamp {
    _id: number;
    page: PageBlock;
    type: string;
    position: number;
    alternativeTemplate?: string;
    content: { [key: string]: any };
    settings: { [key: string]: any };
    contentType?: string;
}

// Connection for PageBlockBlock.
export interface PageBlockBlockConnection extends Connection {
    edges: PageBlockBlockEdge[];
    pageInfo: PageBlockBlockPageInfo;
}

// Edge of PageBlockBlock.
export interface PageBlockBlockEdge extends Edge {
    node: PageBlockBlock;
}

// Information about the current page.
export interface PageBlockBlockPageInfo extends PageInfo {
}

// Connection for PageBlock.
export interface PageBlockConnection extends Connection {
    edges: PageBlockEdge[];
    pageInfo: PageBlockPageInfo;
}

// Edge of PageBlock.
export interface PageBlockEdge extends Edge {
    node: PageBlock;
}

// Information about the current page.
export interface PageBlockPageInfo extends PageInfo {
}
