import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';

import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';
import {HasResourceProps, ResourceConnection} from '../../resource/types';
import routes from '../../routes';
import {PageBlock, PageBlockConnection} from '../types';

const {getPageBySpecialKey, getPageBySlug, getPageById} = require('../queries/index.graphql');

export type PageBlockDecoratedProps = {
    id?: number
    slug?: string
    specialKey?: string
    url?: string,
};

export type PageBlockProps = {
    pageBlock: PageBlock
    [key: string]: any,
} & HasResourceProps;

export default function withPage(
    WrappedComponent: ComponentType<PageBlockProps>,
): ComponentType<PageBlockDecoratedProps> {
    return ({id, slug, url, specialKey, ...props}: PageBlockDecoratedProps) => {
        if (id) {
            return (
                <Query query={getPageById} variables={{id: '/api/page_blocks/' + id}}>
                    {withCommonLifecycle(
                        ({data}: ConsolidatedQueryResult<{pageBlock: PageBlock}>) => (
                            <WrappedComponent pageBlock={data.pageBlock} {...props} />
                        ),
                    )}
                </Query>
            );
        }

        let resolvedUrl: string | null = null;
        if (slug) {
            resolvedUrl = routes.soloist_block_page.pattern.replace(/:slug/i, slug);
        }
        if (url) {
            resolvedUrl = url;
        }

        return (
            <Query query={slug ? getPageBySlug : getPageBySpecialKey} variables={{specialKey, slug, url: resolvedUrl}}>
                {withCommonLifecycle(
                    ({data}: ConsolidatedQueryResult<{
                        pageBlocks: PageBlockConnection,
                        resources?: ResourceConnection,
                    }>) => 0 !== data.pageBlocks.edges.length ? (
                        <WrappedComponent pageBlock={data.pageBlocks.edges[0].node}
                                          resources={data.resources} {...props} />
                    ) : null,
                )}
            </Query>
        );
    };
}
