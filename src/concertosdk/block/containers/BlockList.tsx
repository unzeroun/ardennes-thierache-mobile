import React, {ComponentType} from 'react';
import {PageBlock} from '../types';

type Props = {
    page: PageBlock
    components: {
        [key: string]: ComponentType<any>,
    },
};

export default function BlockList({page, components}: Props) {
    return (
        <>
            {page.blocks.edges
                .sort(({node: block1}, {node: block2}) => block1.position > block2.position ? 1 : -1)
                .map(({node: block}) => {
                    const Component = block.alternativeTemplate ?
                        components[block.alternativeTemplate] : components[block.type];

                    if (!Component) {
                        console.error('Unknown block component', block);

                        return null;
                    }

                    return <Component block={block} key={block.id} />;
                },
            )}
        </>
    );
}
