import dynamic from 'next-server/dynamic';
import Head from 'next/head';
import {ComponentType} from 'react';
import {MenuPage} from '../../core/types';

import {HasResourceProps} from '../types';

const LinkedContent = dynamic(() => import('../../../components/resource/LinkedContent'), {ssr: false});

type Props = HasResourceProps & { page?: MenuPage };

export default function withHeader<P extends Props>(WrappedComponent: ComponentType<P>): ComponentType<P> {
    return (props: P) => {
        if (!props.resources) {
            return <WrappedComponent {...props} />;
        }

        if (0 === props.resources.edges.length) {
            return <WrappedComponent {...props} />;
        }

        const resource = props.resources.edges[0].node;

        return (
            <>
                <Head>
                    <title>{resource.seoInformation.title || resource.description.name}</title>
                    {resource.seoInformation.description || resource.description.description ? (
                        <>
                            <meta name="description"
                                  content={resource.seoInformation.description || resource.description.description}/>
                            <meta name="og:description"
                                  content={resource.seoInformation.description || resource.description.description}/>

                            {resource.seoInformation.keywords ?
                                <meta name="keywords" content={resource.seoInformation.keywords}/>
                                : null
                            }
                        </>
                    ) : null
                    }
                </Head>

                <WrappedComponent {...props} />

                {(resource.links && resource.links.edges.length > 0 || props.page) && (
                    <LinkedContent resource={resource} page={props.page} />
                )}
            </>
        );
    };
}
