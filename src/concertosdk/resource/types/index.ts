import {Connection, Edge, HasTimestamp, Node, PageInfo} from '../../common/types';

export interface Resource extends Node, HasTimestamp {
    id: string;
    _id: number;
    url: string;
    canonicalUrl: string;
    adminUrl: string;
    description: ResourceDescription;
    seoInformation: ResourceSeoInformation;
    links: ResourceLinkConnection;
    tags: ResourceTagConnection;
}

export interface EmbeddedResource extends Node, HasTimestamp {
    id: string;
    url: string;
    canonicalUrl: string;
    adminUrl: string;
    description: ResourceDescription;
    tags: ResourceTag[];
}

// Connection for Resource.
export interface ResourceConnection extends Connection {
    edges: ResourceEdge[];
    pageInfo: ResourcePageInfo;
}

// Edge of Resource.
export interface ResourceEdge extends Edge {
    node: Resource;
}

// Information about the current page.
export interface ResourcePageInfo extends PageInfo {
}

export interface HasResourceProps {
    resources?: ResourceConnection;
}

export interface ResourceDescription extends Node {
    description: string;
    media: number;
    name: string;
    type: string;
    locale: string;
}

export interface ResourceSeoInformation extends Node {
    description: string;
    keywords: string;
    title: string;
}

export interface ResourceLink extends Node {
    id: string;
    _id: number;
    rank: number;
    target: Resource;
}

export interface ResourceLinkConnection extends Connection {
    edges: ResourceLinkEdge[];
    pageInfo: ResourceLinkPageInfo;
}

export interface ResourceLinkEdge extends Edge {
    node: ResourceLink;
}

export interface ResourceLinkPageInfo extends PageInfo {
}

export interface ResourceTag extends Node {
    id: string;
    _id: number;
    namespace: string;
    attribute: string;
    value: string;
}

export interface ResourceTagConnection extends Connection {
    edges: ResourceTagEdge[];
    pageInfo: ResourceTagPageInfo;
}

export interface ResourceTagEdge extends Edge {
    node: ResourceTag;
}

export interface ResourceTagPageInfo extends PageInfo {
}
