import {Medium} from '../../media/types';
import {EmbeddedResource} from '../../resource/types';

export interface Node {
    id: string;
}

// Connection for Node.
export interface NodeConnection extends Connection {
    edges: [NodeEdge];
    pageInfo: NodePageInfo;
}

// Edge of Node.
export interface NodeEdge extends Edge {
    node: Node;
}

// Information about the current page.
export interface NodePageInfo extends PageInfo {
}

export interface HasTimestamp {
    createdAt: string;
    updatedAt: string;
}

export interface ResourceNode extends Node, HasTimestamp {
    _id: number;
    _metadata: EmbeddedResource;
    title: string;
    slug: string;
    description: string;
    media: Medium | null;
}

export interface ContentResourceNode extends ResourceNode {
    content: { [key: string]: any };
}

export interface Connection {
    totalCount: number;
}

export interface Edge {
    cursor: string;
}

export interface PageInfo {
    endCursor: string;
    hasNextPage: boolean;
}
