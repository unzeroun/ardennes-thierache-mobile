import React, {ComponentType, createContext, ReactNode} from 'react';

type Props = {
    children: ReactNode,
    loadingComponent?: ComponentType,
    errorComponent?: ComponentType,
    emptyComponent?: ComponentType,
};

export type ErrorContext = { errorComponent?: ComponentType };
export type LoadingContext = { loadingComponent?: ComponentType };
export type EmptyContext = { emptyComponent?: ComponentType };

export type LifecycleContext = ErrorContext & LoadingContext & EmptyContext;

export const LifecycleComponentsContext = createContext<LifecycleContext | null>(null);

export default function LifecycleComponentsProvider({
                                                        children,
                                                        loadingComponent,
                                                        errorComponent,
                                                        emptyComponent,
                                                    }: Props) {
    return (
        <LifecycleComponentsContext.Provider value={{loadingComponent, errorComponent, emptyComponent}}>
            {children}
        </LifecycleComponentsContext.Provider>
    );
}
