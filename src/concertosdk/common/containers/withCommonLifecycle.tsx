import {ApolloQueryResult, FetchMoreOptions, FetchMoreQueryOptions} from 'apollo-client';
import {DocumentNode} from 'graphql';
import React, {ComponentType} from 'react';
import {QueryResult} from 'react-apollo';
import {LifecycleComponentsContext} from './LifecycleComponentsProvider';

export type fetchMore<TData, TVariables> =
(
    <K extends keyof TVariables>(
        fetchMoreOptions: FetchMoreQueryOptions<TVariables, K> &
            FetchMoreOptions<TData, TVariables>,
    ) => Promise<ApolloQueryResult<TData>>
) &
(
    <TData2, TVariables2, K extends keyof TVariables2>(
        fetchMoreOptions: { query: DocumentNode } &
            FetchMoreQueryOptions<TVariables2, K> &
            FetchMoreOptions<TData2, TVariables2>,
    ) => Promise<ApolloQueryResult<TData2>>
);

export interface ConsolidatedQueryResult<TData, TVariables = {}> {
    data: TData;
    fetchMore: fetchMore<TData, TVariables>;
}

export default function withCommonLifecycle<TData, TVariables>(
    WrappedComponent: ComponentType<ConsolidatedQueryResult<TData, TVariables>>,
) {
    return ({data, loading, error, fetchMore, ...props}: QueryResult<TData, TVariables>) => {
        return (
            <LifecycleComponentsContext.Consumer>
                {context => (() => {
                    if (error) {
                        console.error(error);

                        return context && context.errorComponent && <context.errorComponent/>;
                    }

                    if (loading) {
                        return context && context.loadingComponent && <context.loadingComponent/>;
                    }

                    if (!data) {
                        console.error('Empty data');

                        return context && context.errorComponent && <context.errorComponent/>;
                    }

                    return <WrappedComponent data={data} fetchMore={fetchMore} {...props} />;
                })()}
            </LifecycleComponentsContext.Consumer>
        );
    };
}
