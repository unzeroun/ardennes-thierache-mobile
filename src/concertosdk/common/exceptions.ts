export interface ConcertoException {}

export class NoResultException implements ConcertoException {
    private readonly _queryParams: { [key: string]: string | number };

    private readonly _resource: string;

    constructor(resource: string, queryParams: {[key: string]: string | number}) {
        this._resource = resource;
        this._queryParams = queryParams;
    }

    get queryParams() {
        return this._queryParams;
    }

    get resource() {
        return this._resource;
    }
}
