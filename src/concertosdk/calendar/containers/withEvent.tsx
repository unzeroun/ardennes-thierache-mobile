import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';
import withCommonLifeCycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';
import {HasResourceProps, ResourceConnection} from '../../resource/types';
import routes from '../../routes';
import {getEventsBySlug} from '../queries/index.graphql';
import {Event, EventConnection} from '../types';

export type EventDecoratedProps = {
    id?: number,
    slug?: string,
    url?: string,
};

export type EventProps = { event: Event } & HasResourceProps;

export default function withEvent(WrappedComponent: ComponentType<EventProps>): ComponentType<EventDecoratedProps> {
    return (props: EventDecoratedProps) => {
        if (props.id) {
            return (
                <Query query={getEventById} variables={{id: '/api/events/' + props.id}}>
                    {withCommonLifeCycle(
                        ({data}: ConsolidatedQueryResult<{event: Event}>) => (
                            <WrappedComponent event={data.event}/>
                        ),
                    )}
                </Query>
            );
        }

        let url: string | null = null;

        if (props.slug) {
            url = routes.soloist_calendar_event_show.pattern.replace(/:slug/i, props.slug);
        }

        if (props.url) {
            url = props.url;
        }

        return (
            <Query query={getEventsBySlug} variables={{...props, url}}>
                {withCommonLifeCycle(
                    ({data}: ConsolidatedQueryResult<{events: EventConnection, resources?: ResourceConnection}>) => (
                        <WrappedComponent event={data.events.edges[0].node} resources={data.resources}/>
                    ),
                )}
            </Query>
        );
    };
}
