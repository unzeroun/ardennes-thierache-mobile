import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';

import {VariableProps} from '../../blog/containers/withPosts';
import withCommonLifecycle, {ConsolidatedQueryResult, fetchMore} from '../../common/containers/withCommonLifecycle';
import {getEvents} from '../queries/index.graphql';
import {EventConnection} from '../types';

export type VariableProps = {
    first?: number,
    after?: string | null,
};

export type EventsDecoratedProps = {
    calendarSlug?: string,
    [key: string]: any,
} & VariableProps;

export type EventsProps = { events: EventConnection };

export type WrappedProps = EventsProps & {
    loadMore: null | (() => void),
};

function loadMore(data: EventsProps, fetchMore: fetchMore<EventsProps, VariableProps>) {
    return () => {
        if (data.events.edges.length >= data.events.totalCount || !data.events.pageInfo.hasNextPage) {
            return;
        }

        const lastCursor = data.events.edges[data.events.edges.length - 1].cursor;

        return fetchMore({
            updateQuery: (prev, { fetchMoreResult }) => {
                if (!fetchMoreResult) {
                    return prev;
                }

                return {
                    ...prev,
                    events: {
                        ...prev.events,
                        edges: [...prev.events.edges, ...fetchMoreResult.events.edges],
                        pageInfo: {
                            ...prev.events.pageInfo,
                            hasNextPage: fetchMoreResult.events.pageInfo.hasNextPage,
                        },
                    },
                };
            },
            variables: {
                after: lastCursor,
            },
        });
    };
}

export default function withEvents(WrappedComponent: ComponentType<WrappedProps>) {
    return ({first = 30, after = null, calendarSlug, ...props}: EventsDecoratedProps) => {
        return (
            <Query query={getEvents} variables={{first, after, calendarSlug}}>
                {withCommonLifecycle(
                    ({data, fetchMore}: ConsolidatedQueryResult<EventsProps, VariableProps>) => {
                        return (
                            <WrappedComponent events={data.events} loadMore={data.events.pageInfo.hasNextPage
                                ? loadMore(data, fetchMore) : null} {...props} />
                        );
                    },
                )}
            </Query>
        );
    };
}
