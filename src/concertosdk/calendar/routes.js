module.exports = {
    soloist_calendar_event_show: { pattern: '/calendrier/evenement/:slug', page: 'calendar/event' },
    soloist_calendar_calendar_index: { pattern: '/calendrier/:slug' , page: 'calendar/index'},
    soloist_calendar_calendar_all_events: { pattern: '/calendrier' , page: 'calendar/index'}
};
