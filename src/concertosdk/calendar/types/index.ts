import {Connection, ContentResourceNode, Edge, PageInfo} from '../../common/types';

export interface Calendar extends ContentResourceNode {
    workflowState: string;
}

export interface CalendarConnection extends Connection {
    edges: CalendarEdge[];
    pageInfo: CalendarPageInfo;
}

export interface CalendarEdge extends Edge {
    node: Calendar;
}

export interface CalendarPageInfo  extends PageInfo {
}

export interface Event extends ContentResourceNode {
    calendar: Calendar;
    begin: string;
    end: string;
    latitude: number;
    longitude: number;
    endDateValid: boolean;
}

export interface EventConnection extends Connection {
    edges: EventEdge[];
    pageInfo: EventPageInfo;
}

export interface EventEdge extends Edge {
    node: Event;
}

export interface EventPageInfo extends PageInfo {
}
