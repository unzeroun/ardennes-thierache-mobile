import {Connection, ContentResourceNode, Edge, PageInfo} from '../../common/types';

export interface Directory extends ContentResourceNode {
    id: string;
    template: string;
    entries: DirectoryEntryConnection;
    pageParams: {
        maxPerPage: number|null,
        sortBy: string[][],
    };
}

export interface DirectoryConnection extends Connection {
    edges: DirectoryEdge[];
    pageInfo: DirectoryPageInfo;
}

export interface DirectoryEdge extends Edge {
    node: Directory;
}

export interface DirectoryEntry extends ContentResourceNode {
    id: string;
    type: string;
    published: boolean;
    directoryMap: {
        [key: string]: string;
    };
    valuesMap: {
        [key: string]: any;
    };
}

export interface DirectoryEntryConnection extends Connection {
    edges: DirectoryEntryEdge[];
    pageInfo: DirectoryEntryPageInfo;
}

export interface DirectoryEntryEdge extends Edge {
    node: DirectoryEntry;
}

export interface DirectoryEntryPageInfo extends PageInfo {
}

export interface DirectoryPageInfo extends PageInfo {
}
