module.exports = {
    soloist_directory_show: { pattern: '/directory/:slug', page: 'directory/show' },
    soloist_entry_show: { pattern: '/directory/entry/:slug', page: 'directory/entry/show' },
};
