import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';
import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';
import {HasResourceProps, ResourceConnection} from '../../resource/types';
import routes from '../../routes';
import {Directory, DirectoryConnection} from '../types';

const {getDirectoryBySlug, getDirectoryById} = require('../queries/index.graphql');

export type DirectoryProps = { directory: Directory } & HasResourceProps;
export type DirectoryDecoratedProps = {
    slug?: string,
    id?: string,
};

export default function withDirectory(WrappedComponent: ComponentType<DirectoryProps>):
    ComponentType<DirectoryDecoratedProps> {
    return ({slug, id}: DirectoryDecoratedProps) => {
        if (id) {
            return (
                <Query query={getDirectoryById} variables={{id: '/api/directories/' + id}}>
                    {withCommonLifecycle(
                        ({data}: ConsolidatedQueryResult<{
                            directory: Directory,
                        }>) => (
                            <WrappedComponent directory={data.directory}/>
                        ),
                    )}
                </Query>
            );
        }

        let url: string | null = null;

        if (slug) {
            url = routes.soloist_directory_show.pattern.replace(/:slug/i, slug);
        }

        return (
            <Query query={getDirectoryBySlug} variables={{slug, url}}>
                {withCommonLifecycle(
                    ({data}: ConsolidatedQueryResult<{
                        directories: DirectoryConnection,
                        resources?: ResourceConnection,
                    }>) => (
                        <WrappedComponent directory={data.directories.edges[0].node} resources={data.resources}/>
                    ),
                )}
            </Query>
        );
    };
}
