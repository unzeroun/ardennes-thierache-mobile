import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';
import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';
import {HasResourceProps, ResourceConnection} from '../../resource/types';
import routes from '../../routes';
import {DirectoryEntry, DirectoryEntryConnection} from '../types';

const {getDirectoryEntryBySlug} = require('../queries/index.graphql');

export type DirectoryEntryProps = { directoryEntry: DirectoryEntry } & HasResourceProps;
export type DirectoryEntryDecoratedProps = { slug: string };

export default function withDirectoryEntry(WrappedComponent: ComponentType<DirectoryEntryProps>):
    ComponentType<DirectoryEntryDecoratedProps> {
    return ({slug}: DirectoryEntryDecoratedProps) => {
        const url = routes.soloist_entry_show.pattern.replace(/:slug/i, slug);

        return (
            <Query query={getDirectoryEntryBySlug} variables={{slug, url}}>
                {withCommonLifecycle(
                    ({data}: ConsolidatedQueryResult<{
                        directoryEntries: DirectoryEntryConnection,
                        resources?: ResourceConnection,
                    }>) => (
                        <WrappedComponent directoryEntry={data.directoryEntries.edges[0].node}
                                          resources={data.resources}/>
                    ),
                )}
            </Query>
        );
    };
}
