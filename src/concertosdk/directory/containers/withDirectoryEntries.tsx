import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';

import withCommonLifecycle, {ConsolidatedQueryResult, fetchMore} from '../../common/containers/withCommonLifecycle';
import {DirectoryEntryConnection} from '../types';

const {getDirectoryEntries} = require('../queries/index.graphql');

export type VariableProps = {
    first?: number,
    after?: string | null,
    directory?: string | null,
    pageParams?: {
        maxPerPage: number|null,
        sortBy: string[][],
    },
};

export type DirectoryEntryDecoratedProps = {
    [key: string]: any,
} & VariableProps;

export type DirectoryEntryProps = {
    directoryEntries: DirectoryEntryConnection,
};

export type WrappedProps = DirectoryEntryProps & {
    loadMore: null | (() => void),
};

function loadMore(data: DirectoryEntryProps, fetchMore: fetchMore<DirectoryEntryProps, VariableProps>) {
    return () => {
        if (data.directoryEntries.edges.length >= data.directoryEntries.totalCount
            || !data.directoryEntries.pageInfo.hasNextPage) {
            return;
        }

        const lastCursor = data.directoryEntries.edges[data.directoryEntries.edges.length - 1].cursor;

        fetchMore({
            updateQuery: (prev, {fetchMoreResult}) => {
                if (!fetchMoreResult) {
                    return prev;
                }

                return {
                    ...prev,
                    directoryEntries: {
                        ...prev.directoryEntries,
                        edges: [...prev.directoryEntries.edges, ...fetchMoreResult.directoryEntries.edges],
                        pageInfo: {
                            ...prev.directoryEntries.pageInfo,
                            hasNextPage: fetchMoreResult.directoryEntries.pageInfo.hasNextPage,
                        },
                    },
                };
            },
            variables: {
                after: lastCursor,
            },
        });
    };
}

export default function withDirectoryEntries(WrappedComponent: ComponentType<WrappedProps>) {
    return ({first = 10, after = null, directory = null, pageParams, ...props}: DirectoryEntryDecoratedProps) => {
        const order: {[key: string]: string} = {};
        if (pageParams) {
            pageParams.sortBy.forEach(sort => order[sort[0]] = sort[1]);

            if (pageParams.maxPerPage) {
                first = pageParams.maxPerPage;
            }
        }

        return (
            <Query query={getDirectoryEntries} variables={{first, after, directory, order}}>
                {withCommonLifecycle(
                    ({data, fetchMore}: ConsolidatedQueryResult<DirectoryEntryProps, VariableProps>) => {
                        return (
                            <WrappedComponent directoryEntries={data.directoryEntries}
                                              loadMore={data.directoryEntries.pageInfo.hasNextPage
                                                  ? loadMore(data, fetchMore) : null} {...props} />
                        );
                    },
                )}
            </Query>
        );
    };
}
