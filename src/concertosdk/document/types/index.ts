import {Connection, ContentResourceNode, Edge, HasTimestamp, PageInfo} from '../../common/types';

export interface DocumentCategory extends ContentResourceNode {
    id: string;
    documents: DocumentConnection;
}

export interface DocumentCategoryConnection extends Connection {
    edges: DocumentCategoryEdge[];
    pageInfo: DocumentCategoryPageInfo;
}

export interface DocumentCategoryEdge extends Edge {
    node: DocumentCategory;
}

export interface Document extends ContentResourceNode {
    id: string;
    files: DocumentFileConnection;
    category: DocumentCategory;
}

export interface DocumentConnection extends Connection {
    edges: DocumentEdge[];
    pageInfo: DocumentPageInfo;
}

export interface DocumentEdge extends Edge {
    node: Document;
}

export interface DocumentFile extends HasTimestamp {
    _id: number;
    id: string;
    filename: string;
    name: string;
}

export interface DocumentFileConnection extends Connection {
    edges: DocumentFileEdge[];
    pageInfo: DocumentFilePageInfo;
}

export interface DocumentFileEdge extends Edge {
    node: DocumentFile;
}

export interface DocumentPageInfo extends PageInfo {
}

export interface DocumentCategoryPageInfo extends PageInfo {
}

export interface DocumentFilePageInfo extends PageInfo {
}
