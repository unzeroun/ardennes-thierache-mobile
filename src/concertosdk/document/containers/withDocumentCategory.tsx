import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';
import {Text, View} from 'react-native';
import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';
import {HasResourceProps, ResourceConnection} from '../../resource/types';
import routes from '../../routes';
import {getDocumentCategoryBySlug} from '../queries/index.graphql';
import {DocumentCategory, DocumentCategoryConnection} from '../types';

export type DocumentCategoryProps = { documentCategory: DocumentCategory } & HasResourceProps;
export type DocumentCategoryDecoratedProps = { slug: string };

export default function withDocumentCategory(WrappedComponent: ComponentType<DocumentCategoryProps>):
    ComponentType<DocumentCategoryDecoratedProps> {
    return ({slug}: DocumentCategoryDecoratedProps) => {
        const url = routes.soloist_document_category.pattern.replace(/:slug/i, slug);

        return (
            <Query query={getDocumentCategoryBySlug} variables={{slug, url}}>
                {withCommonLifecycle(
                    ({data}: ConsolidatedQueryResult<{
                        documentCategories: DocumentCategoryConnection,
                        resources?: ResourceConnection,
                    }>) => {
                        return data.documentCategories.edges.length > 0 ?
                            <WrappedComponent documentCategory={data.documentCategories.edges[0].node}
                                              resources={data.resources}/> :
                            <View style={{ alignSelf: 'center', padding: 20 }}>
                                <Text style={{ color: 'red' }}>Aucun document disponible</Text>
                            </View>;
                    },
                )
                }
            </Query>
        );
    };
}
