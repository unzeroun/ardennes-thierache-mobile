import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';
import withCommonLifecycle, {ConsolidatedQueryResult, fetchMore} from '../../common/containers/withCommonLifecycle';
import {getDocuments} from '../queries/index.graphql';
import {DocumentConnection} from '../types';

export type VariableProps = {
    first?: number,
    after?: string | null,
    category?: string | null,
    order?: {
        title?: string,
        category?: string,
    },
};

export type DocumentsDecoratedProps = {
    [key: string]: any,
} & VariableProps;

export type DocumentsProps = {
    documents: DocumentConnection,
};

export type WrappedProps = DocumentsProps & {
    loadMore: null | (() => void),
};

function loadMore(data: DocumentsProps, fetchMore: fetchMore<DocumentsProps, VariableProps>) {
    return () => {
        if (data.documents.edges.length >= data.documents.totalCount || !data.documents.pageInfo.hasNextPage) {
            return;
        }

        const lastCursor = data.documents.edges[data.documents.edges.length - 1].cursor;

        fetchMore({
            updateQuery: (prev, {fetchMoreResult}) => {
                if (!fetchMoreResult) {
                    return prev;
                }

                return {
                    ...prev,
                    documents: {
                        ...prev.documents,
                        edges: [...prev.documents.edges, ...fetchMoreResult.documents.edges],
                        pageInfo: {
                            ...prev.documents.pageInfo,
                            hasNextPage: fetchMoreResult.documents.pageInfo.hasNextPage,
                        },
                    },
                };
            },
            variables: {
                after: lastCursor,
            },
        });
    };
}

export default function withDocuments(WrappedComponent: ComponentType<WrappedProps>) {
    return ({first = 10, after = null, category = null, order = {}, ...props}: DocumentsDecoratedProps) => {
        return (
            <Query query={getDocuments} variables={{first, after, category, order}}>
                {withCommonLifecycle(
                    ({data, fetchMore}: ConsolidatedQueryResult<DocumentsProps, VariableProps>) => {
                        return (
                            <WrappedComponent documents={data.documents}
                                              loadMore={data.documents.pageInfo.hasNextPage
                                                  ? loadMore(data, fetchMore) : null} {...props} />
                        );
                    },
                )}
            </Query>
        );
    };
}
