import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';
import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';
import {HasResourceProps, ResourceConnection} from '../../resource/types';
import routes from '../../routes';
import {getDocumentBySlug} from '../queries/index.graphql';
import {Document, DocumentConnection} from '../types';

export type DocumentProps = { document: Document } & HasResourceProps;
export type DocumentDecoratedProps = { slug: string };

export default function withDocument(WrappedComponent: ComponentType<DocumentProps>):
    ComponentType<DocumentDecoratedProps> {
        return ({slug}: DocumentDecoratedProps) => {
            const url = routes.soloist_document_document.pattern.replace(/:slug/i, slug);

            return (
                <Query query={getDocumentBySlug} variables={{slug, url}}>
                    {withCommonLifecycle(
                        ({data}: ConsolidatedQueryResult<{
                            documents: DocumentConnection,
                            resources?: ResourceConnection,
                        }>) => {
                            return (
                                <WrappedComponent document={data.documents.edges[0].node} resources={data.resources}/>
                            );
                        },
                    )}
                </Query>
            );
        };
}
