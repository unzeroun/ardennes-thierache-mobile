module.exports = {
    soloist_document_category: {pattern: '/document/categorie/:slug', page: 'document/category/show'},
    soloist_document_document: {pattern: '/document/document/:slug', page: 'document/show'},
    // soloist_document_file_download: {pattern: process.env.API_BASE_URL.replace(/\/+\s*$/, '')
    //         + '/document/fichier/:id/telecharger'},
};
