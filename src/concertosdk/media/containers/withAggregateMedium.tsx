import React, {ComponentType} from 'react';
import {Query} from 'react-apollo';
import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';
import {MenuPageBlock} from '../../core/types';
import {getAggregateMediumById} from '../queries/index.graphql';
import {AggregateMediaHasMediumConnection, AggregateMedium} from '../types';

export type OuterProps = {
    id?: number | string,
    sliderProps?: {
        [key: string]: MenuPageBlock,
    },
};

export type ChildProps = {
    medium: AggregateMedium,
};

export default function withAggregateMedium<P extends object>(WrappedComponent: ComponentType<(ChildProps & P) | any>):
    ComponentType<OuterProps & P> {
    return ({id, ...props}: (OuterProps & P)) => {
        return (
            <Query query={getAggregateMediumById} variables={{id: '/api/aggregate_media/' + id, numericId: id}}>
                {withCommonLifecycle(
                    ({data}: ConsolidatedQueryResult<{
                        aggregateMedium: AggregateMedium,
                        aggregateMediaHasMedia: AggregateMediaHasMediumConnection,
                    }>) => {
                        return (
                            <WrappedComponent
                                medium={{...data.aggregateMedium, medias: data.aggregateMediaHasMedia}}
                                {...props} />
                        );
                    },
                )}
            </Query>
        );
    };
}
