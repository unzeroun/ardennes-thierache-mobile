import React, {Fragment, ReactNode} from 'react';

import {Medium, MediumData} from '../types';
import withAggregateMedium from './withAggregateMedium';

type OuterProps = {
    medium?: Medium | number
    children: ({medium}: {medium: MediumData}) => ReactNode,
};
type Props = {
    medium: Medium
    children: ({medium}: {medium: MediumData}) => ReactNode,
};

function InnerMedia({medium, children}: Props) {
    if (!(medium && medium.data)) {
        return null;
    }

    return (
        <>
            {medium.data.medias.map(medium => (
                <Fragment key={medium.id}>
                    {children({medium})}
                </Fragment>
            ))}
        </>
    );
}

export default function Media({medium, children}: OuterProps) {
    if (!medium) {
        return null;
    }

    if ('number' === typeof medium) {
        const Component = withAggregateMedium<{children: ({medium}: {medium: Medium}) => ReactNode}>(InnerMedia);

        return <Component id={medium}>{children}</Component>;
    }

    return <InnerMedia medium={medium}>{children}</InnerMedia>;
}
