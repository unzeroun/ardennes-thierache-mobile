import {Connection, Edge, HasTimestamp, PageInfo} from '../../common/types';

export interface AggregateMediaHasMedium extends Node {
    id: string;
    _id: number;
//    aggregateMedia: AggregateMedium
    media: Medium;
    rank: number;
}

// Connection for AggregateMediaHasMedia.
export interface AggregateMediaHasMediumConnection extends Connection {
    edges: AggregateMediaHasMediumEdge[];
    pageInfo: AggregateMediaHasMediumPageInfo;
}

export interface AggregateMediaHasMediumEdge extends Edge {
    node: AggregateMediaHasMedium;
}

export interface AggregateMediaHasMediumPageInfo extends PageInfo {
}

export interface AggregateMedium extends Node, HasTimestamp {
    id: string;
    _id: number;
    template: string;
    aggregateMediaHasMedias: [];
    name: string;
    alt: string;
    copyright: string;
    medias: AggregateMediaHasMediumConnection;
}

export interface AggregateMediumConnection extends Connection {
    edges: AggregateMediumEdge[];
    pageInfo: AggregateMediumPageInfo;
}

export interface AggregateMediumEdge extends Edge {
    node: AggregateMedium;
}

export interface AggregateMediumPageInfo extends PageInfo {
}

export interface MediumData {
    id: number;
    type: string;
    name: string;
    alt: string;
    copyright: string | null;
    path: string;
    ref?: string;
    provider?: string;
}
export interface AggregateMediumData {
    id: number;
    type: string;
    title: string;
    alt: string;
    ref: string | null;
    copyright: string | null;
    template: string | null;
    medias: MediumData[];
}

export interface Medium extends MediumData, Node, HasTimestamp {
    _id: number;
    thumbnail: string;
    data: AggregateMediumData;
}

export interface MediumConnection extends Connection {
    edges: MediumEdge[];
    pageInfo: MediumPageInfo;
}

export interface MediumEdge extends Edge {
    node: Medium;
}

export interface MediumPageInfo extends PageInfo {
}

export interface Tag extends Node, HasTimestamp {
    id: string;
    _id: number;
    name: string;
}

export interface TagConnection extends Connection {
    edges: TagEdge[];
    pageInfo: TagPageInfo;
}

export interface TagEdge extends Edge {
    node: Tag;
}

export interface TagPageInfo extends PageInfo {

}
