import {ComponentType} from 'react';
import {Query} from 'react-apollo';
import {Action} from '../../../actions';
import withCommonLifecycle, {ConsolidatedQueryResult} from '../../common/containers/withCommonLifecycle';
import {SearchDocumentConnection} from '../types';

const {search} = require('../queries/index.graphql');

export interface WrappedProps {
    searchDocuments?: SearchDocumentConnection;
    setCurrentSearch?: (currentSearch: string) => Action;
    setSearchType: (type: string|null) => void;
    currentSearch?: string | null;
    searchType?: string | null;
    [key: string]: any;
}

export interface SearchDecoratedProps {
    currentSearch?: string | null;
    searchType?: string | null;
    [key: string]: any;
}

export default function withSearch(WrappedComponent: ComponentType<WrappedProps>) {
    return ({setSearchType, currentSearch, searchType, ...props}: SearchDecoratedProps) => {
       return currentSearch && currentSearch.length > 3 ?
            (
                <Query query={search} variables={{q: currentSearch}}>
                    {withCommonLifecycle(
                        ({data}: ConsolidatedQueryResult<{searchDocuments: SearchDocumentConnection}>) => {
                            return (
                                <WrappedComponent currentSearch={currentSearch}
                                                  searchDocuments={data.searchDocuments}
                                                  setSearchType={setSearchType ? setSearchType : () => {}}
                                                  searchType={searchType}
                                                  {...props} />
                            );
                        },
                    )}
                </Query>
            ) : (
                <WrappedComponent setSearchType={setSearchType ? setSearchType : () => {}}
                                  searchType={searchType}
                                  {...props} />
            );
    };
}
