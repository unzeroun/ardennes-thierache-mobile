import {Connection, Edge, PageInfo} from '../../common/types';

export interface SearchDocument extends Node {
    id: string;
    _id: string;
    originalId: number;
    type: string;
    slug: string;
    title: string;
    description: string;
}

export interface SearchDocumentConnection extends Connection {
    edges: SearchDocumentEdge[];
    pageInfo: SearchDocumentPageInfo;
}

export interface SearchDocumentEdge extends Edge {
    node: SearchDocument;
}

export interface SearchDocumentPageInfo extends PageInfo {
}
