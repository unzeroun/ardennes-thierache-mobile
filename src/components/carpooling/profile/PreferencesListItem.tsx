import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {DRIVER_PREFERENCE} from '../../../../config';
import styles from '../../../styles/carpooling/components/profile/PreferencesListItem';

type OwnProps = {
    addPreference: (preference: string) => void,
    preference: DRIVER_PREFERENCE,
    removePreference: (value: string) => void,
    state: boolean,
};

type Props = OwnProps;

export default function PreferencesListItem({
                                                addPreference,
                                                preference,
                                                removePreference,
                                                state,
                                            }: Props) {

    function onSwitch() {
        if (state) {
            removePreference(preference.value);
        } else {
            addPreference(preference.value);
        }
    }

    return (
        <View style={styles.container}>
            <Text style={styles.label}>{preference.label}</Text>
            <View style={styles.separator}/>
            <TouchableOpacity
                style={[styles.button, state ? styles.buttonEnabled : {}]}
                onPress={() => onSwitch()}
            >
                <Image style={styles.icon} source={state ? preference.enabledIcon : preference.disabledIcon}/>
            </TouchableOpacity>
        </View>
    );
}
