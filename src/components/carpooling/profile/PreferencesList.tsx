import React from 'react';
import {StyleProp, View, ViewStyle} from 'react-native';
import {DRIVER_CAR_PREFERENCES} from '../../../../config';
import styles from '../../../styles/carpooling/components/profile/PreferencesList';
import PreferencesListItem from './PreferencesListItem';

type OwnProps = {
    addPreference: (preference: string) => void,
    outerPreferencesList: string[],
    outerStyle?: StyleProp<ViewStyle>,
    preferences: DRIVER_CAR_PREFERENCES,
    removePreference: (value: string) => void,
};

type Props = OwnProps;

export default function PreferencesList({
                                            addPreference,
                                            outerPreferencesList,
                                            outerStyle = {},
                                            preferences,
                                            removePreference,
                                        }: Props) {
    function onAddPreference(value: string) {
        addPreference(value);
    }

    function onRemovePreference(value: string) {
        removePreference(value);
    }

    return (
        <View style={[styles.container, outerStyle]}>
            {
                Object.keys(preferences).map((preferenceKey, index) => {
                    return <PreferencesListItem
                        addPreference={onAddPreference}
                        key={index}
                        preference={preferences[preferenceKey]}
                        removePreference={onRemovePreference}
                        state={outerPreferencesList.indexOf(preferenceKey) !== -1}
                    />;
                })
            }
        </View>
    );
}
