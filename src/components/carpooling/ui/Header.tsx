import React from 'react';
import {Image, View} from 'react-native';
import styles from '../../../styles/carpooling/components/ui/Header';

export default function CarpoolingHeader() {
    return (
        <View style={styles.container}>
            <Image
                source={require('../../../../assets/images/header_logo.png')}
                style={styles.logo}
                resizeMode="contain"
            />
        </View>
    );
}
