import React from 'react';
import {Text, View} from 'react-native';
import {connect} from 'react-redux';
import {Link} from 'react-router-native';
import Redux, {Action, bindActionCreators} from 'redux';
import {setCurrentJourneyRequest} from '../../../actions/carpooling/journeyRequest';
import styles from '../../../styles/carpooling/components/ui/ChatButton';
import {JourneyRequest} from '../../../types/types';
import MessageStatusBadge from './MessageStatusBadge';

type DispatchProps = {
    setCurrentJourneyRequest: (journeyRequest: JourneyRequest | null) => void,
};

type OwnProps = {
    addressee: {
        '@id': string,
        firstName: string,
        lastName: string,
    },
    journeyIri: string,
    endUserIri: string,
};

type Props = DispatchProps & OwnProps;

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        setCurrentJourneyRequest,
    }, dispatch);
}

export default connect<{}, DispatchProps, OwnProps>(null, mapDispatchToProps)(
    function ChatButton({addressee, journeyIri, endUserIri, setCurrentJourneyRequest}: Props) {

        function onClick() {
            setCurrentJourneyRequest(null);
        }

        return (
            <Link
                onPress={() => onClick()}
                to={{
                    pathname: '/message/show/:iri',
                    state: {
                        addressee,
                        endUserIri,
                        journeyIri,
                    },
                }}
            >
                <View style={styles.chatButton}>
                    <MessageStatusBadge
                        addressee={addressee}
                        endUserIri={endUserIri}
                        journeyIri={journeyIri}
                        outerStyle={styles.statusBadge}
                        outerUnreadStyle={styles.statusBadgeUnread}
                        journeyRequest={null}
                    />
                    <Text style={styles.chatButtonLabel}>MESSAGERIE</Text>
                </View>
            </Link>
        );
    },
);
