import React from 'react';
import {Image, StyleProp, TouchableOpacity, View, ViewStyle} from 'react-native';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-native';
import {OwnRouteComponentProps} from '../../../MainRouter';
import {State} from '../../../reducers';
import {CLOSE_BUTTON_STYLES} from '../../../reducers/carpooling/ui';
import styles from '../../../styles/carpooling/components/ui/CloseButton';

type OwnProps = {
    containerStyle?: StyleProp<ViewStyle> | null,
};

type StateProps = {
    closeButtonStyle: CLOSE_BUTTON_STYLES,
    isCloseButtonVisible: boolean,
};

type Props = OwnProps & StateProps & OwnRouteComponentProps;

function mapStateToProps(state: State): StateProps {
    return {
        closeButtonStyle: state.ui.closeButtonStyle,
        isCloseButtonVisible: state.ui.isCloseButtonVisible,
    };
}

export default withRouter(connect<StateProps, {}, {}, State>(mapStateToProps)(
    function CloseButton({closeButtonStyle, containerStyle = null, history, isCloseButtonVisible}: Props) {
        if (!isCloseButtonVisible) {
            return null;
        }

        return (
            <View style={[styles[closeButtonStyle].backButtonContainer, containerStyle]}>
                <TouchableOpacity style={styles[closeButtonStyle].backButton} onPress={() => history.goBack()}>
                    <Image source={require('../../../../assets/images/plus_icon.png')}
                           style={styles[closeButtonStyle].backIcon}/>
                </TouchableOpacity>
            </View>
        );
    },
));
