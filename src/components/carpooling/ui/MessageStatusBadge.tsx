import React from 'react';
import {Image, StyleProp, Text, ViewStyle} from 'react-native';
import {connect} from 'react-redux';
import {Link} from 'react-router-native';
import Redux, {Action, bindActionCreators} from 'redux';
import {setCurrentJourneyRequest} from '../../../actions/carpooling/journeyRequest';
import {State} from '../../../reducers';
import styles from '../../../styles/carpooling/components/ui/MessageStatusBadge';
import {JourneyRequest, Message, MessageListType, User} from '../../../types/types';

type StateProps = {
    messageList: MessageListType,
    user: User | null,
};

type DispatchProps = {
    setCurrentJourneyRequest: (journeyRequest: JourneyRequest | null) => void,
};

type OwnProps = {
    addressee: {
        '@id': string,
        firstName: string,
        lastName: string,
    },
    endUserIri: string,
    journeyIri: string,
    journeyRequest: JourneyRequest | null,
    outerStyle?: StyleProp<ViewStyle>,
    outerUnreadStyle?: StyleProp<ViewStyle>,
};

type MergeProps = StateProps & DispatchProps & OwnProps & {
    messages: Message[],
};

type Props = MergeProps;

function mapStateToProps(state: State): StateProps {
    return {
        messageList: state.message.messageList,
        user: state.profile.user,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        setCurrentJourneyRequest,
    }, dispatch);
}

function mergeProps(state: StateProps, dispatch: DispatchProps, ownProps: OwnProps): MergeProps {
    return {
        ...state,
        ...dispatch,
        ...ownProps,
        endUserIri: ownProps.endUserIri,
        journeyIri: ownProps.journeyIri,
        messages: state.messageList[ownProps.endUserIri],
    };
}

export default connect<StateProps, DispatchProps, OwnProps, MergeProps, State>(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps,
)(
    function MessageStatusBadge({
                                    addressee,
                                    messages,
                                    endUserIri,
                                    journeyIri,
                                    journeyRequest,
                                    outerStyle,
                                    outerUnreadStyle = {},
                                    setCurrentJourneyRequest,
                                    user,
                                }: Props) {

        function hasUnreadMessages() {
            if (!user || !messages) {
                return false;
            }

            return messages.filter(message => {
                return message.writer['@id'] !== user.iri && !message.read;
            }).length > 0;
        }

        function onClick() {
            if (journeyRequest) {
                setCurrentJourneyRequest(journeyRequest);
            } else {
                setCurrentJourneyRequest(null);
            }
        }

        function getMessagesCount() {
            if (!messages) {
                return '0';
            }
            return messages.length.toString();
        }

        function getUnreadMessagesCount() {
            if (!messages) {
                return '0';
            }

            return messages.filter(message => !message.read).length.toString();
        }

        return (
            <Link
                to={{
                    pathname: '/message/show/:iri',
                    state: {
                        addressee,
                        endUserIri,
                        journeyIri,
                    },
                }}
                onPress={() => onClick()}
                style={[
                    styles.container, outerStyle,
                    hasUnreadMessages() ? [styles.unreadContainer, outerUnreadStyle] : {},
                ]}>
                <>
                    <Image
                        style={styles.icon}
                        source={
                            hasUnreadMessages() ? require('../../../../assets/images/chat_icon_unread.png')
                                : require('../../../../assets/images/chat_icon.png')
                        }
                        resizeMode="contain"
                    />
                    <Text style={[
                        styles.count,
                        hasUnreadMessages() ? styles.unreadCount : {},
                    ]}>{hasUnreadMessages() ? getUnreadMessagesCount() : getMessagesCount()}</Text>
                </>
            </Link>
        );
    },
);
