import React from 'react';
import {Image, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import Redux, {Action, bindActionCreators} from 'redux';
import {NavigationItemType} from '../../../../config';
import {setAppSwitchingModalVisibility, setMainRoute} from '../../../actions/routing';
import TopOval from '../../../lib/TopOval';
import {OwnRouteComponentProps} from '../../../MainRouter';
import {State} from '../../../reducers';
import styles from '../../../styles/carpooling/components/ui/Footer';
import {User} from '../../../types/types';
import AppSwitchingModal from '../../AppSwitchingModal';
import NavigationItem from '../../main/NavigationItem';

type OwnProps = {
    navigationItems: NavigationItemType[],
};

type StateProps = {
    user: User | null,
};

type DispatchProps = {
    setAppSwitchingModalVisibility: (visibility: boolean) => void,
    setMainRoute: () => void,
};

type Props = OwnProps & StateProps & DispatchProps & OwnRouteComponentProps;

function mapStateToProps(state: State): StateProps {
    return {
        user: state.profile.user,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        setAppSwitchingModalVisibility,
        setMainRoute,
    }, dispatch);
}

export default withRouter(connect<StateProps, DispatchProps, OwnProps, State>(mapStateToProps, mapDispatchToProps)(
    function CarpoolingFooter({location, navigationItems, setAppSwitchingModalVisibility, setMainRoute, user}: Props) {
        return (
            <View style={styles.container}>
                <View style={{bottom: 0, position: 'absolute'}}>
                    <TopOval
                        height={75}
                        color="#2C3840"
                        backgroundColor="transparent"
                        widthFactor={1.6}
                    />
                </View>
                <View style={styles.content}>
                    {
                        user && (
                            navigationItems.map((navigationItem, index) => (
                                <NavigationItem
                                    key={index}
                                    isActive={location.pathname === navigationItem.location.pathname}
                                    navigationItem={navigationItem}
                                    activeStyle={styles.activeItem}
                                />
                            ))
                        )
                    }
                    <TouchableOpacity style={styles.quitButton} onPress={() => setAppSwitchingModalVisibility(true)}>
                        <Image style={styles.icon} source={require('../../../../assets/images/logout_icon.png')}/>
                    </TouchableOpacity>
                </View>
                <AppSwitchingModal
                    callback={() => setMainRoute()}
                    text="Souhaitez vous revenir sur l'application Ardennes Thiérache ?"
                />
            </View>
        );
    },
));
