import React from 'react';
import {Linking, StyleProp, Text, TouchableOpacity, ViewStyle} from 'react-native';
import styles from '../../../styles/carpooling/components/ui/PhoneButton';

type OwnProps = {
    phoneNumber: string,
    outerStyle?: StyleProp<ViewStyle>,
};

type Props = OwnProps;

export default function PhoneButton({phoneNumber, outerStyle = {}}: Props) {
    return (
        <TouchableOpacity
            onPress={() => Linking.openURL('tel:' + phoneNumber)}
            style={[styles.container, outerStyle]}
        >
            <Text style={styles.label}>TELEPHONE</Text>
        </TouchableOpacity>
    );
}
