import React from 'react';
import {Image, TextInput, TouchableOpacity, View} from 'react-native';
import styles from '../../../styles/carpooling/components/Message/MessageInput';

type OwnProps = {
    sendMessage: () => void,
    setInput: (input: TextInput | null) => void,
    setValue: (value: string) => void,
    value: string,
};

type Props = OwnProps;

export default function MessageInput({
                                         sendMessage,
                                         setInput,
                                         setValue,
                                         value,
                                     }: Props) {
    return (
        <View style={styles.container}>
            <TextInput
                style={styles.input}
                value={value}
                autoCorrect={false}
                onChangeText={(input) => setValue(input)}
                ref={(input) => setInput(input)}
                multiline={true}
                placeholder="Texte du message"
                placeholderTextColor="#DCE7EE"
            />
            <TouchableOpacity onPress={() => sendMessage()} style={styles.send}>
                <Image
                    style={styles.chatIcon}
                    source={require('../../../../assets/images/chat_icon.png')}
                    resizeMode="contain"
                />
                <Image
                    style={styles.plusIcon}
                    source={require('../../../../assets/images/plus_bold_icon.png')}
                    resizeMode="contain"
                />
            </TouchableOpacity>
        </View>
    );
}
