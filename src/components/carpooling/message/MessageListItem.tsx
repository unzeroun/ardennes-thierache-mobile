import moment from 'moment';
import React from 'react';
import {Text, View} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {increaseDisplayedMessagesCount} from '../../../actions/carpooling/message';
import styles from '../../../styles/carpooling/components/Message/MessageListItem';
import {Message} from '../../../types/types';

type DispatchProps = {
    increaseDisplayedMessagesCount: () => void,
};

type OwnProps = {
    message: Message,
    writtenByUser: boolean,
};

type Props = DispatchProps & OwnProps;

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        increaseDisplayedMessagesCount,
    }, dispatch);
}

export default connect<{}, DispatchProps, OwnProps>(null, mapDispatchToProps)(
    function MessageListItem({message, increaseDisplayedMessagesCount, writtenByUser}: Props) {

        return (
            <View
                style={[styles.container, !writtenByUser ? styles.received : {}]}
                onLayout={() => increaseDisplayedMessagesCount()}
            >
                <Text style={styles.time}>
                    {
                        moment(message.sentAt).isAfter(moment().subtract(7, 'm')) ?
                            moment(message.sentAt).format('dddd à hh[h]mm') :
                            moment(message.sentAt).format('DD MMMM à hh[h]mm')
                    }
                </Text>
                <Text style={[styles.content, !writtenByUser ? styles.contentReceived : {}]}>{message.content}</Text>
            </View>
        );
    },
);
