import React from 'react';
import {ScrollView, View} from 'react-native';
import {connect} from 'react-redux';
import {State} from '../../../reducers';
import styles from '../../../styles/carpooling/components/Message/MessageList';
import {Message, User} from '../../../types/types';
import MessageListItem from './MessageListItem';

type StateProps = {
    user: User | null,
};

type OwnProps = {
    messages: Message[],

};

type Props = StateProps & OwnProps;

function mapStateToProps(state: State): StateProps {
    return {
        user: state.profile.user,
    };
}

export default connect<StateProps, {}, OwnProps, State>(mapStateToProps)(
    function MessageList({messages, user}: Props) {

        if (!user) {
            return null;
        }

        return (
            <View>
                {
                    messages.map((message, index) => {
                        return (
                            <MessageListItem
                                key={index}
                                message={message}
                                writtenByUser={user.iri === message.writer['@id']}
                            />
                        );
                    })
                }
            </View>
        );
    },
);
