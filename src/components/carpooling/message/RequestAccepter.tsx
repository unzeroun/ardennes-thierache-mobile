import React from 'react';
import {Image, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {acceptJourneyRequest} from '../../../actions/carpooling/journeyRequest';
import LoadingComponent from '../../../components/main/LoadingComponent';
import {State} from '../../../reducers';
import styles from '../../../styles/carpooling/components/Message/RequestAccepter';
import {Journey, JourneyRequest} from '../../../types/types';

type StateProps = {
    loading: boolean,
};

type DispatchToProps = {
    acceptJourneyRequest: (journeyRequestIri: string, journeyIri: string) => void,
};

type OwnProps = {
    journey: Journey,
    journeyRequest: JourneyRequest,
};

type Props = StateProps & DispatchToProps & OwnProps;

function mapStateToProps(state: State): StateProps {
    return {
        loading: state.journeyRequest.loading,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        acceptJourneyRequest,
    }, dispatch);
}

export default connect<StateProps, DispatchToProps, OwnProps, State>(mapStateToProps, mapDispatchToProps)(
    function RequestAccepter({acceptJourneyRequest, journey, journeyRequest, loading}: Props) {
        return (
            <View style={styles.container}>
                {
                    loading && <LoadingComponent/> || (
                        <TouchableOpacity
                            onPress={() => {
                                acceptJourneyRequest(journeyRequest['@id'], journey['@id']);
                            }}>
                            <Image
                                style={styles.icon}
                                source={require('../../../../assets/images/car_seat.png')}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                    )
                }
            </View>
        );
    },
);
