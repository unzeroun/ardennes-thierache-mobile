import React, {useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {acceptJourneyRequest, denyJourneyRequest} from '../../../actions/carpooling/journeyRequest';
import {State} from '../../../reducers';
import styles from '../../../styles/carpooling/components/journey/JourneyRequestListItem';
import {Journey, JourneyRequest, User} from '../../../types/types';
import MessageStatusBadge from '../ui/MessageStatusBadge';

type StateProps = {
    user: User | null,
};

type DispatchProps = {
    acceptJourneyRequest: (journeyRequestIri: string, journeyIri: string) => void,
    denyJourneyRequest: (journeyRequestIri: string, journeyIri: string) => void,
};

type OwnProps = {
    journey: Journey,
    journeyRequest: JourneyRequest,
};

type Props = StateProps & DispatchProps & OwnProps;

function mapStateToProps(state: State): StateProps {
    return {
        user: state.profile.user,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        acceptJourneyRequest,
        denyJourneyRequest,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, OwnProps, State>(mapStateToProps, mapDispatchToProps)(
    function JourneyRequestListItem({journey, journeyRequest, user}: Props) {

        const [endUserIri, setEndUserIri] = useState<string | null>(null);

        useEffect(() => {
            if (user && journey) {
                setEndUserIri(isOwnedByUser() ? journeyRequest.requester['@id'] : journey.endUser['@id']);
            }
        }, []);

        function isOwnedByUser(): boolean {
            if (!user) {
                return false;
            }

            return journey.endUser['@id'] === user.iri;
        }

        if (!user) {
            return null;
        }

        return (
            <View style={styles.container}>
                <View style={styles.nameContainer}>
                    <Text style={[styles.name, styles.firstName]}>{journeyRequest.requester.firstName}</Text>
                    <Text style={[styles.name]}>{journeyRequest.requester.lastName}</Text>
                </View>
                {
                    endUserIri && (
                        <MessageStatusBadge
                            addressee={journeyRequest.requester}
                            endUserIri={endUserIri}
                            journeyIri={journey['@id']}
                            journeyRequest={journeyRequest ? journeyRequest : null}
                        />
                    )
                }
            </View>
        );
    },
);
