import moment from 'moment';
import React from 'react';
import {Image, StyleProp, Text, View, ViewStyle} from 'react-native';
import {Link} from 'react-router-native';
import styles from '../../../styles/carpooling/components/journey/JourneyListItem';
import {Journey} from '../../../types/types';

type Props = {
    journey: Journey,
    unreadMessagesCount?: number,
    outerStyle?: StyleProp<ViewStyle>,
    timeContainerStyle?: StyleProp<ViewStyle>,
};

export default function JourneyListItem({
                                            journey,
                                            outerStyle = {},
                                            timeContainerStyle = {},
                                            unreadMessagesCount = 0,
                                        }: Props) {
    return (
        <Link style={[styles.container, outerStyle]} to={{
            pathname: '/journey/show/:iri',
            state: {journeyId: journey['@id']},
        }}>
            <>
                <View style={styles.timeAndPlaceContainer}>
                    <View style={[styles.timeContainer, timeContainerStyle]}>
                        <View style={styles.time}>
                            <Text style={styles.day}>{moment(journey.departureDate).format('DD MMM')}</Text>
                            <Text style={styles.hour}>
                                {moment(journey.departureDate)
                                    .format('HH')}h{moment(journey.departureDate).format('mm')}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.stepsContainer}>
                        <Image
                            style={styles.icon}
                            source={require('../../../../assets/images/journey_list_item_icon.png')}
                            resizeMode="contain"
                        />
                        <View style={styles.locations}>
                            <Text style={styles.text}>{journey.departure.city}</Text>
                            <Text style={styles.text}>{journey.arrival.city}</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.dataContainer}>
                    <View style={styles.availablePlaces}>
                        <Image
                            style={styles.carSeat}
                            source={require('../../../../assets/images/car_seat.png')}
                            resizeMode="contain"
                        />
                        <Text>{journey.availablePlaces}</Text>
                    </View>
                    {
                        unreadMessagesCount > 0 && (
                            <View style={styles.chatContainer}>
                                <Image
                                    source={require('../../../../assets/images/chat_icon_unread.png')}
                                    resizeMode="contain"
                                    style={styles.chatIcon}
                                />
                                <Text style={styles.unreadCount}>{unreadMessagesCount}</Text>
                            </View>
                        )
                    }
                </View>
            </>
        </Link>
    );
}
