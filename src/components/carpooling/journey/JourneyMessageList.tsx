import React from 'react';
import {View} from 'react-native';
import {Message} from '../../../types/types';
import JourneyMessageListItem from './JourneyMessageListItem';

type OwnProps = {
    journeyIri: string,
    messageList: Message[][],
};

type Props = OwnProps;

export default function JourneyMessageList({journeyIri, messageList}: Props) {
    return (
        <View>
            {
                messageList.map((messages, index) => {
                    return (
                        <JourneyMessageListItem
                            key={index}
                            journeyIri={journeyIri}
                            messages={messages}
                        />
                    );
                })
            }
        </View>
    );
}
