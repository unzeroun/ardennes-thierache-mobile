import React from 'react';
import {StyleProp, Text, TouchableOpacity, View, ViewStyle} from 'react-native';
import {JOURNEY_REQUEST_STATUSES} from '../../../../config';
import styles from '../../../styles/carpooling/components/journey/PostJourneyRequestStatusButton';

type OwnProps = {
    requestMethod: () => void,
    status: string | null,
    outerStyle?: StyleProp<ViewStyle>,
};

type Props = OwnProps;

export default function PostJourneyRequestStatusButton({
                                                           requestMethod,
                                                           outerStyle = {},
                                                           status,
                                                       }: Props) {
    if (JOURNEY_REQUEST_STATUSES.PENDING === status) {
        return (
            <View style={[styles.container, styles.pending, outerStyle]}>
                <Text style={styles.label}>DEMANDE EN COURS</Text>
            </View>
        );
    }

    if (JOURNEY_REQUEST_STATUSES.DENIED === status) {
        return (
            <View style={[styles.container, styles.denied, outerStyle]}>
                <Text style={styles.label}>{'refusé'.toUpperCase()}</Text>
            </View>
        );
    }

    if (JOURNEY_REQUEST_STATUSES.ACCEPTED === status) {
        return null;
    }

    return (
        <TouchableOpacity onPress={() => requestMethod()} style={[styles.container, outerStyle]}>
            <Text style={styles.label}>RESERVER !</Text>
        </TouchableOpacity>
    );
}
