import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import styles from '../../../styles/carpooling/components/journey/CutySuggestion';

export type CitySuggestionType = {
    city: string,
    coordinates: {
        latitude: number | null,
        longitude: number | null,
    },
    postcode: string,
};

type OwnProps = {
    callback: (city: CitySuggestionType | null) => void,
    citySuggestion: CitySuggestionType,
};

type Props = OwnProps;

export default function CitySuggestion({callback, citySuggestion}: Props) {

    if (!citySuggestion) {
        return null;
    }

    return (
        <TouchableOpacity onPress={() => callback(citySuggestion)} style={styles.container}>
            <Text style={styles.label}>{citySuggestion.city} - {citySuggestion.postcode}</Text>
        </TouchableOpacity>
    );
}
