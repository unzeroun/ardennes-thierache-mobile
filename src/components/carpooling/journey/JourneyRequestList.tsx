import React from 'react';
import {View} from 'react-native';
import {Journey, JourneyRequest} from '../../../types/types';
import JourneyRequestListItem from './JourneyRequestListItem';

type OwnProps = {
    journey: Journey,
    journeyRequests: JourneyRequest[],
};

type Props = OwnProps;

export default function JourneyRequestList({journey, journeyRequests}: Props) {
    return (
        <View>
            {
                journeyRequests.map(journeyRequest => {
                    return <
                        JourneyRequestListItem
                        key={journeyRequest['@id']}
                        journey={journey}
                        journeyRequest={journeyRequest}
                    />;
                })
            }
        </View>
    );
}
