import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {resetCitySuggestions} from '../../../actions/carpooling/journey';
import styles from '../../../styles/carpooling/components/journey/CitySuggesionList';
import CitySuggestion, {CitySuggestionType} from './CitySuggestion';

type DispatchProps = {
    resetCitySuggestions: () => void,
};

type OwnProps = {
    callback: (citySuggestion: CitySuggestionType | null) => void,
    citySuggestions: CitySuggestionType[],
};

type Props = DispatchProps & OwnProps;

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        resetCitySuggestions,
    }, dispatch);
}

export default connect<{}, DispatchProps, OwnProps>(null, mapDispatchToProps)(
    function CitySuggestionList({callback, citySuggestions, resetCitySuggestions}: Props) {

        if (citySuggestions.length === 0) {
            return null;
        }

        return (
            <View style={styles.container}>
                <View style={styles.listContainer}>
                    <TouchableOpacity onPress={() => resetCitySuggestions()} style={styles.backdrop}/>
                    <View>
                        {
                            citySuggestions.map((suggestion, index) => {
                                return <CitySuggestion key={index} callback={callback} citySuggestion={suggestion}/>;
                            })
                        }
                    </View>
                </View>
            </View>
        );
    },
);
