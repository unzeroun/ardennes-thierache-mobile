import React, {useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import styles from '../../../styles/carpooling/components/journey/JourneyMessageListItem';
import {Message} from '../../../types/types';
import MessageStatusBadge from '../ui/MessageStatusBadge';

type OwnProps = {
    journeyIri: string,
    messages: Message[],
};

type Props = OwnProps;

export default function JourneyMessageListItem({journeyIri, messages}: Props) {

    const [referenceMessage, setReferenceMessage] = useState<Message | null>(null);

    useEffect(() => {
        if (messages.length > 0) {
            setReferenceMessage(messages[0]);
        }
    }, [messages]);

    if (!referenceMessage) {
        return null;
    }

    return (
        <View style={styles.container}>
            <View style={styles.nameContainer}>
                <Text style={[styles.name, styles.firstName]}>{referenceMessage.withEndUser.firstName}</Text>
                <Text style={styles.name}>{referenceMessage.withEndUser.lastName}</Text>
            </View>
            <MessageStatusBadge
                addressee={referenceMessage.withEndUser}
                endUserIri={referenceMessage.withEndUser['@id']}
                journeyIri={journeyIri}
                journeyRequest={null}
            />
        </View>
    );
}
