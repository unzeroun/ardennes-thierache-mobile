import React from 'react';
import {StyleProp, View, ViewStyle} from 'react-native';
import {Journey, MessageListType} from '../../../types/types';
import JourneyListItem from './JourneyListItem';

type OwnProps = {
    journeys: Journey[],
    itemStyle?: StyleProp<ViewStyle>,
    itemTimeContainerStyle?: StyleProp<ViewStyle>,
    unreadMessages?: MessageListType,
};

type Props = OwnProps;

export default function JourneyList({
                                        itemStyle = {},
                                        journeys,
                                        itemTimeContainerStyle = {},
                                        unreadMessages = {},
                                    }: Props) {
    return (
        <View>
            {
                journeys.map((journey, index) => {
                    return <JourneyListItem
                        key={index}
                        journey={journey}
                        outerStyle={itemStyle}
                        timeContainerStyle={itemTimeContainerStyle}
                        unreadMessagesCount={unreadMessages[journey['@id']] ? unreadMessages[journey['@id']].length : 0}
                    />;
                })
            }
        </View>
    );
}
