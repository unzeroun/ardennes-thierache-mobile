import React from 'react';
import {IsocontentNative} from 'react-isocontent-native';
import {Image, Text, View} from 'react-native';
import {Link} from 'react-router-native';
import {Document} from '../../../../concertosdk/document/types';
import styles from '../../../../styles/main/components/document/category/DocumentListItem';

type Props = {
    document: Document,
};

export default function DocumentListItem({document}: Props) {
    const parsedDescription = JSON.parse(document.description);
    return (
        <Link to={{
            pathname: '/document/document/' + document.slug,
            state: {
                page: document.title,
                slug: document.slug,
            },
        }}>
            <View style={styles.container}>
                <View style={styles.content}>
                    <Text style={styles.title}>{document.title}</Text>
                    <IsocontentNative content={parsedDescription}/>
                </View>
                <View style={styles.arrowContainer}>
                    <Image
                        source={require('../../../../../assets/images/arrow_right.png')}
                        style={styles.arrow}
                    />
                </View>
            </View>
        </Link>
    );
}
