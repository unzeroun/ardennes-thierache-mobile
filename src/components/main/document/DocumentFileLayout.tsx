import React from 'react';
import {Image, Linking, Text, TouchableOpacity, View} from 'react-native';
import {ROUTE_MAP} from '../../../../config';
import {DocumentFile} from '../../../concertosdk/document/types';
import styles from '../../../styles/main/components/document/DocumentFileLayout';

type Props = {
    documentFile: DocumentFile,
};

function getDownloadLink(id: string) {
    return ROUTE_MAP.soloist_document_file_download.replace(':id', id);
}

function onPress(link: string) {
    Linking.openURL(link);
}

export default function DocumentFileLayout({documentFile}: Props) {
    return (
        <TouchableOpacity
            onPress={() => onPress(getDownloadLink(documentFile._id.toString()))}
            style={styles.container}
        >
            <Text style={styles.title}>{documentFile.name}</Text>
            <View style={styles.arrowContainer}>
                <Image
                    source={require('../../../../assets/images/arrow_right.png')}
                    style={styles.arrow}
                />
            </View>
        </TouchableOpacity>
    );
}
