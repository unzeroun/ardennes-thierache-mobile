import React from 'react';
import {Text, View} from 'react-native';
import styles from '../../styles/main/components/ErrorComponent';

export default function ErrorComponent() {
    return (
        <View>
            <Text style={styles.error}>
                Une erreur est survenue. Veuillez réessayer ultérieurement
            </Text>
        </View>
    );
}
