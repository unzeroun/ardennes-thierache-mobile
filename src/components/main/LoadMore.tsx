import React from 'react';
import {StyleProp, Text, TouchableOpacity, ViewStyle} from 'react-native';
import styles from '../../styles/main/components/LoadMore';

type Props = {
    loadMore: () => void,
    outerStyle?: StyleProp<ViewStyle> | null,
};

export default function LoadMore({loadMore, outerStyle = null}: Props) {
    return (
        <TouchableOpacity
            onPress={() => loadMore()}
            style={[styles.container, outerStyle]}
        >
            <Text style={styles.title}>Afficher plus</Text>
        </TouchableOpacity>
    );
}
