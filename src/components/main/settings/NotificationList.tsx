import React, {useEffect, useState} from 'react';
import {ActivityIndicator, Text, View} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {
    NotificationSubscriberRecord,
    requestNotificationSubscriber,
    updateNotificationSubscriber,
} from '../../../actions/main/notification';
import withNotificationTopics from '../../../lib/notification/containers/withNotificationTopics';
import {WrappedProps} from '../../../lib/notification/containers/withNotificationTopics';
import {NotificationSubscriber, NotificationTopic} from '../../../lib/notification/types';
import useTimeout from '../../../lib/useTimeout';
import {State} from '../../../reducers';
import styles from '../../../styles/main/components/settings/NotificationList';
import NotificationListItem from './NotificationListItem';

type StateProps = {
    godMode: boolean,
    notificationPermission: boolean,
    notificationSubscriber: NotificationSubscriber | null,
    requestingSubscriber: boolean,
};

type DispatchProps = {
    requestNotificationSubscriber: (identifier: string | null) => void,
    updateNotificationSubscriber: (record: NotificationSubscriberRecord, id: number) => void,
};

type Props = StateProps & DispatchProps & WrappedProps;

function mapStateToProps(state: State): StateProps {
    return {
        godMode: state.main.godMode,
        notificationPermission: state.permission.notificationPermission,
        notificationSubscriber: state.notification.notificationSubscriber,
        requestingSubscriber: state.notification.requestingSubscriber,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        requestNotificationSubscriber,
        updateNotificationSubscriber,
    }, dispatch);
}

const ConnectedComponent = connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function Settings({
                          godMode,
                          notificationPermission,
                          notificationSubscriber,
                          notificationTopics,
                          requestNotificationSubscriber,
                          requestingSubscriber,
                          updateNotificationSubscriber,
                      }: Props) {

        const [enabledTopics, setEnabledTopics] = useState<string[]>([]);
        const [delayUpdateNotificationSubscriber] = useTimeout((topics: string[]) => {
            if (notificationSubscriber) {
                updateNotificationSubscriber({
                    identifier: notificationSubscriber.identifier,
                    provider: notificationSubscriber.provider,
                    topics,
                }, notificationSubscriber._id);
            }
        }, 1000);

        useEffect(() => {
            requestNotificationSubscriber(null);
        }, []);

        useEffect(() => {
            if (notificationSubscriber) {
                setEnabledTopics(
                    notificationSubscriber.topics.edges.map(({node: topic}) => topic.id),
                );
            }
        }, [notificationSubscriber]);

        function isTopicActive(topic: NotificationTopic): boolean {
            if (!notificationSubscriber) {
                return false;
            }

            return notificationSubscriber.topics.edges.filter(({node: currentTopic}) => {
                return currentTopic.id === topic.id;
            }).length > 0;
        }

        function setAndDelayEnabledTopics(topics: string[]) {
            setEnabledTopics(topics);
            delayUpdateNotificationSubscriber(topics);
        }

        function removeEnabledTopic(topic: NotificationTopic) {
            setAndDelayEnabledTopics([...enabledTopics.filter((enableTopic) => enableTopic !== topic.id)]);
        }

        function addEnabledTopic(topic: NotificationTopic) {
            if (-1 === enabledTopics.indexOf(topic.id)) {
                setAndDelayEnabledTopics([...enabledTopics, topic.id]);
            }
        }

        if (!notificationTopics) {
            return null;
        }

        return (
            <View style={styles.container}>
                <Text style={styles.title}>Notifications</Text>
                {
                    requestingSubscriber && (
                        <ActivityIndicator color="#5882BD" size="large"/>
                    ) || (!notificationSubscriber || !notificationPermission) && (
                        <View>
                            <Text style={styles.warning}>
                                Veuillez autoriser l'utilisation des notifications depuis les paramètres
                                de votre téléphone
                            </Text>
                        </View>
                    ) || (
                        <View>
                            {
                                notificationTopics.edges.filter(({node: topic}) => !topic.private || godMode)
                                    .map(({node: topic}) => {
                                        return <NotificationListItem
                                            key={topic.id}
                                            isActive={isTopicActive(topic)}
                                            topic={topic}
                                            onEnable={() => addEnabledTopic(topic)}
                                            onDisable={() => removeEnabledTopic(topic)}
                                        />;
                                    })
                            }
                        </View>
                    )
                }
            </View>
        );
    },
);

export default withNotificationTopics(ConnectedComponent);
