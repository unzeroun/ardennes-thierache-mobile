import React, {useEffect, useState} from 'react';
import {Animated, Text, View} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {setToaster} from '../../../actions/main/settings';
import useTimeout from '../../../lib/useTimeout';
import {State} from '../../../reducers';
import styles from '../../../styles/main/components/settings/SettingsBanner';

type StateProps = {
    errored: boolean,
    message: string | null,
};

type DispatchProps = {
    setToaster: (message: string | null) => void,
};

type Props = DispatchProps & StateProps;

function mapStateToProps(state: State): StateProps {
    return {
        errored: state.settings.errored,
        message: state.settings.message,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        setToaster,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function Toaster({errored, message, setToaster}: Props) {

        const [position] = useState<Animated.Value>(new Animated.Value(-75));
        const [delayResetToaster] = useTimeout(() => resetToaster(), 3000);

        useEffect(() => {
            if (message) {
                Animated.timing(position, {
                    duration: 500,
                    toValue: 0,
                    useNativeDriver: false,
                }).start();

                delayResetToaster();
            }
        }, [message]);

        function resetToaster() {
            Animated.timing(position, {
                duration: 500,
                toValue: -75,
                useNativeDriver: false,
        }).start(() => setToaster(null));
        }

        return (
            <View>
                <Animated.View style={[styles.container, errored ? styles.errored : {}, {bottom: position}]}>
                    <Text style={styles.label}>{message}</Text>
                </Animated.View>
            </View>
        );
    },
);
