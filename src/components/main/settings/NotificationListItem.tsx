import React, {useEffect, useState} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {NotificationTopic} from '../../../lib/notification/types';
import styles from '../../../styles/main/components/settings/NotificationListItem';

type OwnProps = {
    isActive: boolean,
    topic: NotificationTopic,
    onEnable: () => void,
    onDisable: () => void,
};

type Props = OwnProps;

export default function NotificationListItem({isActive, topic, onEnable, onDisable}: Props) {
    const [isTopicEnabled, setIsTopicEnabled] = useState<boolean>(false);

    useEffect(() => setIsTopicEnabled(isActive), []);

    function toggleTopic() {
        isTopicEnabled ? disableTopic() : enableTopic();
    }

    function enableTopic() {
        setIsTopicEnabled(true);
        onEnable();
    }

    function disableTopic() {
        setIsTopicEnabled(false);
        onDisable();
    }

    return (
        <TouchableOpacity style={styles.container} onPress={() => toggleTopic()}>
            {
                isTopicEnabled && (
                    <Image
                        style={[styles.checkbox]}
                        source={require('../../../../assets/images/checkmark_icon.png')}
                    />
                ) || <View style={[styles.checkbox, styles.disabled]}/>
            }
            <Text style={styles.label}>{topic.name}</Text>
        </TouchableOpacity>
    );
}
