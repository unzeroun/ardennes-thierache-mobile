import moment from 'moment';
import React, {Fragment} from 'react';
import {Image, Text, View} from 'react-native';
import {Link} from 'react-router-native';
import {Event} from '../../../concertosdk/calendar/types';
import {getMediaThumbnailUrl} from '../../../lib/mediaHelper';
import styles from '../../../styles/main/components/homepage/HomepageUpcomingEventsItem';

type Props = {
    event: Event,
};

export default function HomepageUpcomingEventsItem({event}: Props) {
    return (
        <Link to={{
            pathname: '/calendrier/evenement/' + event.slug,
            state: {
                page: event.title,
                slug: event.slug,
            },
        }}>
            <View style={styles.container}>
                <View style={styles.imageContainer}>
                    {
                        event.media && (
                            <Fragment>
                                <Image
                                    source={{uri: getMediaThumbnailUrl(event)}}
                                    style={styles.image}
                                />
                                <View style={styles.arrowContainer}>
                                    <Image
                                        source={require('../../../../assets/images/arrow_right.png')}
                                        style={styles.arrow}
                                    />
                                </View>
                            </Fragment>
                        )
                    }
                </View>
                <Text style={styles.date}>Le {moment(event.begin).format('DD MMM')}</Text>
                <Text style={styles.title}>{event.title}</Text>
            </View>
        </Link>
    );
}
