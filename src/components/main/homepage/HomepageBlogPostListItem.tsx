import React from 'react';
import {Image, Text, View} from 'react-native';
import {Link} from 'react-router-native';
import {Post} from '../../../concertosdk/blog/types';
import {getMediaThumbnailUrl} from '../../../lib/mediaHelper';
import styles from '../../../styles/main/components/homepage/HomepageBlogPostListItem';

type Props = {
    post: Post,
};

export default function HomepageBlogPostListItem({post}: Props) {
    return (
        <Link to={{
            pathname: '/blog/posts/' + post._id,
            state: {
                id: post._id,
                page: post.title,
            },
        }}>
            <View style={styles.container}>
                {
                    post.media && (
                        <View style={styles.imageContainer}>
                            <Image
                                source={{uri: getMediaThumbnailUrl(post)}}
                                style={styles.image}
                            />
                            <View style={styles.arrowContainer}>
                                <Image
                                    source={require('../../../../assets/images/arrow_right.png')}
                                    style={styles.arrow}
                                />
                            </View>
                        </View>
                    )
                }
                <Text style={styles.title}>{post.title}</Text>
            </View>
        </Link>
    );
}
