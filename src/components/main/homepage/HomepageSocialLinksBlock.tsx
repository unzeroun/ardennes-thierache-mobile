import React from 'react';
import {View} from 'react-native';
import {SOCIAL_LINKS_DATA} from '../../../../config';
import styles from '../../../styles/main/components/homepage/HomepageSocialLinksBlock';
import HomepageSocialLinksItem from './HomepageSocialLinksItem';

export default function HomepageSocialLinksBlock() {
    return (
        <View style={styles.container}>
            {
                SOCIAL_LINKS_DATA.map((socialLinkData, index) => {
                    return <HomepageSocialLinksItem key={index} socialLinkData={socialLinkData}/>;
                })
            }
        </View>
    );
}
