import moment from 'moment';
import React from 'react';
import {Text, View} from 'react-native';
import {Link} from 'react-router-native';
import {Post} from '../../../concertosdk/blog/types';
import styles from '../../../styles/main/components/homepage/HomepageFlashInfoItem';

type Props = {
    post: Post,
};

export default function HomepageFlashInfoItem({post}: Props) {
    const publishedAt = moment(post.publishedAt);

    return (
        <Link to={{
            pathname: '/blog/posts/' + post._id,
            state: {
                id: post._id,
                page: post.title,
            },
        }}>
            <View style={styles.container}>
                <View style={styles.shortDateContainer}>
                    <Text style={styles.shortDate}>{publishedAt.format('DD')}</Text>
                    <Text style={styles.shortDate}>{publishedAt.format('MMM')}</Text>
                </View>
                <View>
                    <Text style={styles.title}>{post.title}</Text>
                    <Text style={styles.fromNow}>{publishedAt.fromNow()}</Text>
                </View>
            </View>
        </Link>
    );
}
