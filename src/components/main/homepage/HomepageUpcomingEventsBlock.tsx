import React from 'react';
import {Image, ScrollView, Text, View} from 'react-native';
import {Link} from 'react-router-native';
import withEvents, {WrappedProps} from '../../../concertosdk/calendar/containers/withEvents';
import styles from '../../../styles/main/components/homepage/HomepageUpcomingEventsBlock';
import HomepageUpcomingEventsItem from './HomepageUpcomingEventsItem';

type OuterProps = {
    first?: number,
};

type Props = OuterProps & WrappedProps;

const Component = withEvents(
    function HomepageUpcomingEventsBlock({events}: Props) {
        return (
            <View style={styles.container}>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>Evènements</Text>
                    <Link to={{pathname: '/calendrier', state: {page: 'Evènements'}}}>
                        <View style={styles.linkContainer}>
                            <Text style={styles.linkLabel}>Tout</Text>
                            <Image
                                source={require('../../../../assets/images/arrow_right_orange.png')}
                                style={styles.arrow}
                            />
                        </View>
                    </Link>
                </View>
                <ScrollView horizontal={true}>
                    {
                        events.edges.map(({node: event}) => {
                            return <HomepageUpcomingEventsItem key={event.id} event={event}/>;
                        })
                    }
                </ScrollView>
            </View>
        );
    },
);

export default ({first}: OuterProps) => <Component first={first}/>;
