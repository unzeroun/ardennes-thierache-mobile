import React from 'react';
import {Image, ScrollView, Text, View} from 'react-native';
import {Link} from 'react-router-native';
import withPosts, {WrappedProps} from '../../../concertosdk/blog/containers/withPosts';
import styles from '../../../styles/main/components/homepage/HomepageBlogPostListBlock';
import HomepageBlogPostListItem from './HomepageBlogPostListItem';

type OuterProps = {
    first?: number,
};

type Props = WrappedProps & OuterProps;

const Component = withPosts(
    function HomepageBlogPostListBlock({posts}: Props) {
        return (
            <View style={styles.container}>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>Actualités</Text>
                    <Link to={
                        {
                            pathname: '/blog/category/4',
                            state: {
                                page: 'Actualités',
                                slug: 'actus',
                            },
                        }}>
                        <View style={styles.linkContainer}>
                            <Text style={styles.linkLabel}>Tout</Text>
                            <Image
                                source={require('../../../../assets/images/arrow_right_orange.png')}
                                style={styles.arrow}
                            />
                        </View>
                    </Link>
                </View>
                <ScrollView horizontal={true}>
                    {
                        posts.edges.map(({node: post}) => <HomepageBlogPostListItem key={post.id} post={post}/>)
                    }
                </ScrollView>
            </View>
        );
    },
);

export default ({first}: OuterProps) => <Component first={first} categories_slug="actus"/>;
