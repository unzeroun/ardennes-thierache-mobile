import React from 'react';
import {Image, Linking, TouchableOpacity} from 'react-native';
import {Link} from 'react-router-native';
import {SocialLinksDataType} from '../../../../config';
import styles from '../../../styles/main/components/homepage/HomepageSocialLinksItem';

type Props = {
    socialLinkData: SocialLinksDataType,
};

function onPress(link?: string) {
    if (link) {
        Linking.openURL(link);
    }
}

export default function HomepageSocialLinksItem({socialLinkData}: Props) {
    if (socialLinkData.link) {
        return (
            <TouchableOpacity onPress={() => onPress(socialLinkData.link)} style={styles.container}>
                <Image
                    source={socialLinkData.icon}
                    style={styles.image}
                />
            </TouchableOpacity>
        );
    }

    if (socialLinkData.internalLink) {
        return (
            <Link to={socialLinkData.internalLink} style={styles.container}>
                <Image
                    source={socialLinkData.icon}
                    style={styles.image}
                />
            </Link>
        );
    }

    return null;
}
