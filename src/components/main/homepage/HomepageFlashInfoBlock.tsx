import React from 'react';
import {Image, Text, View} from 'react-native';
import {Link} from 'react-router-native';
import withPosts, {WrappedProps} from '../../../concertosdk/blog/containers/withPosts';
import styles from '../../../styles/main/components/homepage/HomepageFlashInfoBlock';
import HomepageFlashInfoItem from './HomepageFlashInfoItem';

type OuterProps = {
    first?: number,
};

type Props = WrappedProps & OuterProps;

const Component = withPosts(
    function HomepageFlashInfoBlock({posts}: Props) {
        return (
            <View style={styles.container}>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>Actualités flash</Text>
                    <Link to={
                        {
                            pathname: '/blog/category/flash',
                            state: {
                                page: 'Actualités flash',
                                slug: 'flash',
                            },
                        }}>
                        <View style={styles.linkContainer}>
                            <Text style={styles.linkLabel}>Tout</Text>
                            <Image
                                source={require('../../../../assets/images/arrow_right_orange.png')}
                                style={styles.arrow}
                            />
                        </View>
                    </Link>
                </View>
                {
                    posts.edges.map(({node: post}) => <HomepageFlashInfoItem key={post.id} post={post}/>)
                }
            </View>
        );
    },
);

export default ({first}: OuterProps) => <Component first={first} categories_slug="flash"/>;
