import React from 'react';
import {ActivityIndicator, View} from 'react-native';
import styles from '../../styles/main/components/LoadingComponent';

export default function LoadingComponent() {
    return (
        <View style={styles.container}>
            <ActivityIndicator
                color="#5883bd"
                size="large"
            />
        </View>
    );
}
