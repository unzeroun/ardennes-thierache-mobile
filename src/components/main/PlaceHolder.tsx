import React, {Fragment} from 'react';
import {StyleProp, View, ViewStyle} from 'react-native';
import styles from '../../styles/main/components/PlaceHolder';

type PLACEHOLDER_TYPE = 'text' | 'image';

type Props = {
    lines: number,
    outerStyle?: StyleProp<ViewStyle>,
    type: PLACEHOLDER_TYPE,
};

export default function PlaceHolder({lines, outerStyle = {}, type}: Props) {
    return (
        <Fragment>
            {
                Array(lines).fill(0).map((_, index) => <View key={index} style={[
                    type === 'image' ? styles.image : styles.text,
                    outerStyle,
                ]}/>)
            }
        </Fragment>
    );
}
