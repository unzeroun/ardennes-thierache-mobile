import React, {Fragment} from 'react';
import {Image, ImageStyle, StyleProp, Text, View, ViewStyle} from 'react-native';
import {Link} from 'react-router-native';
import {NavigationItemType} from '../../../config';
import styles from '../../styles/main/components/NavigationItem';

type Props = {
    isActive: boolean,
    navigationItem: NavigationItemType,
    standardStyle?: StyleProp<ImageStyle>,
    activeStyle?: StyleProp<ViewStyle>,
};

export default function NavigationItem({isActive, navigationItem, standardStyle, activeStyle}: Props) {

    const content = (
        <Fragment>
            <Image
                style={[styles.icon, standardStyle]}
                source={isActive ? navigationItem.iconActive : navigationItem.icon}
            />
        </Fragment>
    );

    if (!isActive) {
        return (
            <Link
                style={[
                    styles.itemContainer,
                    isActive ? styles.itemContainerActive : null,
                    activeStyle,
                ]}
                to={navigationItem.location}
            >
                {content}
            </Link>
        );
    }

    return (
        <View style={[styles.itemContainer, isActive ? styles.itemContainerActive : null, activeStyle]}>
            {content}
        </View>
    );
}
