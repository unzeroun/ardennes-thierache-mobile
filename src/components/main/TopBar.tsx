import React, {useState} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router-native';
import Redux, {Action, bindActionCreators} from 'redux';
import {activateGodMode, openSideMenu} from '../../actions/main/main';
import {setPoiPanelExpansion} from '../../actions/main/poi';
import useTimeout from '../../lib/useTimeout';
import {OwnRouteComponentProps} from '../../MainRouter';
import {State} from '../../reducers';
import styles from '../../styles/main/TopBar';

type StateProps = {
    poiPanelExpanded: boolean,
};

type DispatchProps = {
    activateGodMode: () => void,
    openSideMenu: () => void,
    setPoiPanelExpansion: (expanded: boolean) => void,
};

type Props = StateProps & DispatchProps & OwnRouteComponentProps;

function mapStateToProps(state: State): StateProps {
    return {
        poiPanelExpanded: state.poi.poiPanelExpanded,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        activateGodMode,
        openSideMenu,
        setPoiPanelExpansion,
    }, dispatch);
}

export default withRouter(
    connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
        function TopBar({
                            activateGodMode,
                            history,
                            location,
                            openSideMenu,
                            poiPanelExpanded,
                            setPoiPanelExpansion,
                        }: Props) {

            const [godModeCounter, setGodModeCounter] = useState<number>(0);
            const [delayGodModeResetCounter] = useTimeout(() => setGodModeCounter(0), 200);

            function onBackPress() {
                if (poiPanelExpanded) {
                    setPoiPanelExpansion(false);
                } else {
                    history.goBack();
                }
            }

            function shouldGoBack(): boolean {
                return (location.pathname !== '/' && (location.state && !location.state.inMenu)) ||
                    poiPanelExpanded;
            }

            function incrementGodModeCounter() {
                setGodModeCounter(godModeCounter + 1);
                delayGodModeResetCounter();
                if (godModeCounter + 1 >= 10) {
                    activateGodMode();
                }
            }

            let routeName = 'Ardennes Thiérache';

            if (location.pathname === '/') {
                routeName = 'Accueil';
            } else {
                if (location.state && location.state.page) {
                    routeName = location.state.page;
                }
            }

            return (
                <View style={styles.container}>
                    <View style={styles.iconContainer}>
                        {
                            shouldGoBack() && (
                                <TouchableOpacity onPress={() => onBackPress()}>
                                    <Image
                                        source={require('../../../assets/images/arrow_back.png')}
                                        style={styles.arrow}
                                    />
                                </TouchableOpacity>
                            ) || (
                                <TouchableOpacity onPress={() => openSideMenu()}>
                                    <Image
                                        source={require('../../../assets/images/menu_icon.png')}
                                        style={styles.menu}
                                    />
                                </TouchableOpacity>
                            )
                        }
                    </View>
                    <Text onPress={() => incrementGodModeCounter()} style={styles.title}>
                        {
                            routeName ? routeName : 'Ardennes Thiérache'
                        }
                    </Text>
                    <Link to="/reporting" style={styles.reportIconContainer}>
                        <Image style={styles.reportIcon} source={require('../../../assets/images/camera_stroke.png')}/>
                    </Link>
                    <View/>
                </View>
            );
        },
    ),
);
