import React, {ReactElement} from 'react';
import {ROUTE_MAP} from '../../../../../config';
import {MenuNode} from '../../../../concertosdk/core/types';
import Link from '../../../../lib/Link';

type Props = {
    menuNode: MenuNode,
    children: ReactElement,
};

export default function CoreLink({children, menuNode}: Props) {

    if ('action' === menuNode.type) {
        const {route, ...params} = menuNode.specificData.params;
        const mappedRoute = ROUTE_MAP[route];

        if (mappedRoute) {
            if ('soloist_blog_list_category' === route) {
                return (
                    <Link
                        page={menuNode.title}
                        route={mappedRoute}
                        slug={params.category.replace('c-', '')}>
                        {children}
                    </Link>
                );
            }

            return (
                <Link page={menuNode.title} route={mappedRoute} slug={params.slug}>
                    {children}
                </Link>
            );
        }
    }

    if ('page' === menuNode.type) {
        return (
            <Link page={menuNode.title} route={'/core/page/' + menuNode.slug} slug={menuNode.slug}>
                {children}
            </Link>
        );
    }

    if ('shortcut' === menuNode.type && menuNode.specificData.node) {
        return (
            <CoreLink menuNode={menuNode.specificData.node}>
                {children}
            </CoreLink>
        );
    }

    return null;
}
