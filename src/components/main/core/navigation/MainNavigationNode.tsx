import React from 'react';
import {Text, View} from 'react-native';
import {MenuNode} from '../../../../concertosdk/core/types';
import styles from '../../../../styles/main/components/core/navigation/MainNavigationNode';
import CoreLink from './CoreLink';

type Props = {
    menuNode: MenuNode,
};

export default function MainNavigationNode({menuNode}: Props) {
    if (menuNode.children && menuNode.children.length > 0) {
        return (
            <View>
                <Text style={styles.label}>{menuNode.title}</Text>
                {
                    menuNode.children.map(node => (
                        <View key={node.id}>
                            <MainNavigationNode menuNode={node}/>
                        </View>
                    ))
                }
            </View>
        );
    }

    return (
        <CoreLink menuNode={menuNode}>
            <View style={styles.linkContainer}>
                <View style={styles.linkLabelContainer}>
                    <Text style={styles.linkLabel}>{menuNode.title}</Text>
                </View>
            </View>
        </CoreLink>
    );
}
