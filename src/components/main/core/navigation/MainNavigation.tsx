import React from 'react';
import {ScrollView} from 'react-native';
import withMenuNodes, {MenuNodesProps} from '../../../../concertosdk/core/containers/withMenuNodes';
import MainNavigationNode from './MainNavigationNode';

type Props = MenuNodesProps;

const Component = withMenuNodes(
    function MainNavigation({menuNodes}: Props) {
        return (
            <ScrollView>
                {
                    menuNodes.edges.map(({node: node}, index) => (
                        <MainNavigationNode key={index} menuNode={node}/>
                    ))
                }
            </ScrollView>
        );
    },
);

export default () => <Component asTree specialKey="mobile_app"/>;
