import React, {Component, ReactElement} from 'react';
import {Animated, Dimensions, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {closeSideMenu} from '../../../actions/main/main';
import {setAppSwitchingModalVisibility, setCarpoolingRoute} from '../../../actions/routing';
import {isIOS} from '../../../lib/Platform';
import {State} from '../../../reducers';
import styles from '../../../styles/main/components/core/SideMenu';
import AppSwitchingModal from '../../AppSwitchingModal';

type StateProps = {
    isSideMenuOpen: boolean,
};

type DispatchProps = {
    closeSideMenu: () => void,
    setAppSwitchingModalVisibility: (visible: boolean) => void,
    setCarpoolingRoute: () => void,
};

type OwnProps = {
    children: ReactElement,
};

type Props = StateProps & DispatchProps & OwnProps;

type InnerState = {
    filterOffset: Animated.Value,
    offset: Animated.Value,
    opacity: Animated.Value,
};

function mapStateToProps(state: State): StateProps {
    return {
        isSideMenuOpen: state.main.isSideMenuOpen,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        closeSideMenu,
        setAppSwitchingModalVisibility,
        setCarpoolingRoute,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, OwnProps, State>(mapStateToProps, mapDispatchToProps)(
    class SideMenu extends Component<Props> {

        public state: InnerState = {
            filterOffset: new Animated.Value(-Dimensions.get('window').width),
            offset: new Animated.Value(-Dimensions.get('window').width),
            opacity: new Animated.Value(0.8),
        };

        public componentDidUpdate(prevProps: Props) {
            if (prevProps.isSideMenuOpen !== this.props.isSideMenuOpen) {
                if (this.props.isSideMenuOpen) {
                    this.openAnim();
                } else {
                    this.closeAnim();
                }
            }
        }

        public render() {
            return (
                <Animated.View
                    style={[
                        styles.container,
                        isIOS ? {left: this.state.filterOffset} : {translateX: this.state.filterOffset},
                    ]}
                >
                    <View style={styles.statusBar}/>
                    <Animated.View
                        style={[
                            styles.filterContainer,
                            {opacity: this.state.opacity},
                        ]}
                    >
                        <TouchableOpacity
                            style={styles.filter}
                            onPress={() => this.props.closeSideMenu()}
                        />
                    </Animated.View>
                    <Animated.View style={[
                        styles.content,
                        isIOS ? {left: this.state.offset} : {translateX: this.state.offset},
                    ]}>
                        {/* TODO: mettre en place point d'entrée carpooling */}
                        {/*<TouchableOpacity*/}
                        {/*    onPress={() => this.props.setAppSwitchingModalVisibility(true)}*/}
                        {/*    style={styles.carpoolingButton}*/}
                        {/*>*/}
                        {/*    <Text style={styles.carpoolingLabel}>Covoiturage</Text>*/}
                        {/*</TouchableOpacity>*/}
                        {this.props.children}
                    </Animated.View>
                    <AppSwitchingModal
                        callback={() => this.onOpenCarpooling()}
                        text="Souhaitez vous accéder à l'application de covoiturage ?"
                    />
                </Animated.View>
            );
        }

        private onOpenCarpooling() {
            this.props.closeSideMenu();
            this.props.setCarpoolingRoute();
        }

        private openAnim() {
            Animated.timing(
                this.state.opacity,
                {
                    duration: 400,
                    toValue: .85,
                    useNativeDriver: !isIOS,
                },
            ).start();

            Animated.timing(
                this.state.offset,
                {
                    duration: 400,
                    toValue: 0,
                    useNativeDriver: !isIOS,
                },
            ).start();

            Animated.timing(
                this.state.filterOffset,
                {
                    duration: 1,
                    toValue: 0,
                    useNativeDriver: !isIOS,
                },
            ).start();
        }

        private closeAnim() {
            Animated.timing(
                this.state.opacity,
                {
                    duration: 250,
                    toValue: 0,
                    useNativeDriver: !isIOS,
                },
            ).start();

            Animated.timing(
                this.state.offset,
                {
                    duration: 250,
                    toValue: -Dimensions.get('window').width,
                    useNativeDriver: !isIOS,
                },
            ).start();

            setTimeout(() => {
                Animated.timing(
                    this.state.filterOffset,
                    {
                        duration: 1,
                        toValue: -Dimensions.get('window').width,
                        useNativeDriver: !isIOS,
                    },
                ).start();
            }, 250);
        }
    },
);
