import React from 'react';
import {IsocontentNative} from 'react-isocontent-native';
import {ImageResizeMode, ImageStyle, StyleProp, View, ViewStyle} from 'react-native';
import {MenuPageBlock} from '../../../concertosdk/core/types';
import Media from '../../../lib/media';
import {getMediaUrl} from '../../../lib/mediaHelper';

type Props = {
    block: MenuPageBlock,
    imageResizeMode?: ImageResizeMode | null,
    imageStyle?: StyleProp<ImageStyle> | null,
    webViewStyle?: StyleProp<ViewStyle> | null,
};

export default function PageBlock({
                                      block,
                                      imageResizeMode = null,
                                      imageStyle = null,
                                      webViewStyle = null,
                                  }: Props) {
    if (!block.displayableData) {
        return null;
    }

    if ('media' === block.type) {
        return (
            <Media
                imageResizeMode={imageResizeMode || 'cover'}
                imageStyle={imageStyle}
                mediaUrl={getMediaUrl(block.displayableData.media)}
                webViewStyle={webViewStyle}
            />
        );
    }

    if ('text' === block.type) {
        if (block.displayableData && block.displayableData.value) {
            const parsedContent = JSON.parse(block.displayableData.value);
            return (
                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                    <IsocontentNative content={parsedContent} />
                </View>
            );
        }
    }

    return null;
}
