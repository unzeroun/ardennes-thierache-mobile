import React from 'react';
import {View} from 'react-native';
import {withRouter} from 'react-router';
import {NavigationItemType} from '../../../../config';
import {OwnRouteComponentProps} from '../../../MainRouter';
import styles from '../../../styles/main/components/core/BottomBar';
import NavigationItem from '../NavigationItem';

type OwnProps = {
    navigationItems: NavigationItemType[],
};

type Props = OwnProps & OwnRouteComponentProps;

export default withRouter(
    function BottomBar({location, navigationItems}: Props) {
        return (
            <View style={styles.container}>
                {
                    navigationItems.map((navigationItem, index) => (
                        <NavigationItem
                            isActive={location.pathname === navigationItem.location.pathname}
                            key={index}
                            navigationItem={navigationItem}
                        />
                    ))
                }
            </View>
        );
    },
);
