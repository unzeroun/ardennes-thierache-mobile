import moment from 'moment';
import React from 'react';
import {Image, Text, View} from 'react-native';
import {Link} from 'react-router-native';
import {Event} from '../../../concertosdk/calendar/types';
import {dateWithHour} from '../../../lib/date';
import {getMediaThumbnailUrl} from '../../../lib/mediaHelper';
import styles from '../../../styles/main/components/calendar/EventListItem';

type Props = {
    event: Event,
};

export default function EventListItem({event}: Props) {

    function isHappening(event: Event): boolean {
        return moment(event.begin).isBefore(moment()) && moment(event.end).isAfter(moment());
    }

    return (
        <Link to={{
            pathname: '/calendrier/evenement/' + event.slug,
            state: {
                page: event.title,
                slug: event.slug,
            },
        }}>
            <View style={isHappening(event) ? styles.containerHappening : {}}>
                {
                    isHappening(event) && (
                        <Text style={styles.happeningLabel}>En cours</Text>
                    )
                }
                <View style={styles.container}>
                    {
                        event.media && (
                            <View style={styles.imageContainer}>
                                <Image
                                    source={{uri: getMediaThumbnailUrl(event)}}
                                    style={styles.image}
                                />
                                <View style={styles.arrowContainer}>
                                    <Image
                                        source={require('../../../../assets/images/arrow_right.png')}
                                        style={styles.arrow}
                                    />
                                </View>
                            </View>
                        )
                    }
                    <View style={styles.content}>
                        <Text style={styles.eventTitle}>{event.title}</Text>
                        <Text style={styles.place}>{event.content.place}</Text>
                        <Text style={styles.date}>{dateWithHour({begin: event.begin, end: event.end})}</Text>
                    </View>
                </View>
            </View>
        </Link>
    );
}
