import {useEffect} from 'react';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {
    NotificationSubscriberRecord,
    registerNotificationSubscriber,
    requestNotificationIdentifier,
    requestNotificationSubscriber,
} from '../../actions/main/notification';
import {getNotificationPermission} from '../../actions/main/permission';
import withNotificationTopics, {WrappedProps} from '../../lib/notification/containers/withNotificationTopics';
import {NotificationSubscriber} from '../../lib/notification/types';
import {State} from '../../reducers';

type StateProps = {
    notificationIdentifier: string | null,
    notificationPermission: boolean,
    notificationSubscriber: NotificationSubscriber| null,
    requestingSubscriber: boolean,
};

type DispatchProps = {
    getNotificationPermission: () => void,
    registerNotificationSubscriber: (subscriber: NotificationSubscriberRecord) => void,
    requestNotificationIdentifier: () => void,
    requestNotificationSubscriber: (identifier: string) => void,
};

type Props = StateProps & DispatchProps & WrappedProps;

function mapStateToProps(state: State): StateProps {
    return {
        notificationIdentifier: state.notification.notificationIdentifier,
        notificationPermission: state.permission.notificationPermission,
        notificationSubscriber: state.notification.notificationSubscriber,
        requestingSubscriber: state.notification.requestingSubscriber,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        getNotificationPermission,
        registerNotificationSubscriber,
        requestNotificationIdentifier,
        requestNotificationSubscriber,
    }, dispatch);
}

const ConnectedComponent = connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function RegisterNotificationSubscriber({
                                                getNotificationPermission,
                                                notificationIdentifier,
                                                notificationPermission,
                                                notificationSubscriber,
                                                notificationTopics,
                                                registerNotificationSubscriber,
                                                requestNotificationIdentifier,
                                                requestNotificationSubscriber,
                                                requestingSubscriber,
                                            }: Props) {
        useEffect(() => {
            getNotificationPermission();
        }, []);

        useEffect(() => {
            if (notificationPermission) {
                requestNotificationIdentifier();
            }
        }, [notificationPermission]);

        useEffect(() => {
            if (notificationIdentifier) {
                requestNotificationSubscriber(notificationIdentifier);
            }
        }, [notificationIdentifier]);

        useEffect(() => {
            if (requestingSubscriber || notificationSubscriber || !notificationIdentifier) {
                return;
            }

            const registeredTopics: string[] = notificationTopics.edges.filter(
                ({node: topic}) => !topic.private).map(({node: topic}) => topic.id,
            );

            registerNotificationSubscriber({
                identifier: notificationIdentifier,
                provider: 'expo',
                topics: registeredTopics,
            });
        }, [requestingSubscriber]);

        return null;
    },
);

export default withNotificationTopics(ConnectedComponent);
