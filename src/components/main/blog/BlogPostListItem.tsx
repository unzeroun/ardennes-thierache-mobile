import {LinearGradient} from 'expo-linear-gradient';
import React from 'react';
import {IsocontentNative} from 'react-isocontent-native';
import {Image, Text, View} from 'react-native';
import {Link} from 'react-router-native';
import {Post} from '../../../concertosdk/blog/types';
import {getMediaThumbnailUrl} from '../../../lib/mediaHelper';
import styles from '../../../styles/main/components/blog/BlogPostListitem';

type Props = {
    post: Post,
};

export default function BlogPostListItem({post}: Props) {
    const parsedDescription = JSON.parse(post.description);

    return (
        <Link to={{
            pathname: '/blog/posts/' + post._id,
            state: {
                id: post._id,
                page: post.title,
            },
        }} style={styles.container}>
            <View style={styles.contentContainer}>
                {
                    post.media && (
                        <View style={styles.imageContainer}>
                            <Image
                                source={{uri: getMediaThumbnailUrl(post)}}
                                style={styles.image}
                            />
                            <View style={styles.arrowContainer}>
                                <Image
                                    source={require('../../../../assets/images/arrow_right.png')}
                                    style={styles.arrow}
                                />
                            </View>
                        </View>
                    )
                }
                <Text style={styles.title}>{post.title}</Text>
                <IsocontentNative content={parsedDescription} />
                <LinearGradient
                    colors={['rgba(232, 106, 82, 0)', '#F26C4F']}
                    style={styles.gradient}
                />
            </View>
        </Link>
    );
}
