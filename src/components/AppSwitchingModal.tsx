import React from 'react';
import {Modal, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {setAppSwitchingModalVisibility} from '../actions/routing';
import {State} from '../reducers';
import styles from '../styles/components/AppSwitchingModal';

type OwnProps = {
    callback: () => void,
    text: string,
};

type StateProps = {
    appSwitchingModalVisible: boolean,
};

type DispatchProps = {
    setAppSwitchingModalVisibility: (visible: boolean) => void,
};

type Props = OwnProps & StateProps & DispatchProps;

function mapStateToProps(state: State): StateProps {
    return {
        appSwitchingModalVisible: state.routing.appSwitchingModalVisible,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        setAppSwitchingModalVisibility,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function AppSwitchingModal({
                                   appSwitchingModalVisible,
                                   callback,
                                   setAppSwitchingModalVisibility,
                                   text,
                               }: Props) {

        function onAccept() {
            setAppSwitchingModalVisibility(false);
            callback();
        }

        return (
            <Modal visible={appSwitchingModalVisible} transparent={true}>
                <TouchableOpacity
                    onPress={() => setAppSwitchingModalVisibility(false)}
                    style={styles.backdrop}
                />
                <View style={styles.container}>
                    <View style={styles.content}>
                        <Text style={styles.text}>{text}</Text>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity
                                onPress={() => onAccept()}
                                style={styles.button}
                            >
                                <Text style={styles.buttonLabel}>Oui</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => setAppSwitchingModalVisibility(false)}
                                style={styles.button}
                            >
                                <Text style={styles.buttonLabel}>Non</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    },
);
