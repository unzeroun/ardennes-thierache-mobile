import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    backdrop: {
        backgroundColor: '#000',
        height: '100%',
        opacity: .5,
        position: 'absolute',
        width: '100%',
    },

    container: {
        alignItems: 'center',
        height: '100%',
        justifyContent: 'center',
        width: '100%',
    },

    content: {
        alignItems: 'center',
        backgroundColor: '#FFF',
        maxWidth: 300,
        paddingVertical: 50,
        width: Dimensions.get('window').width * .8,
    },

    text: {
        fontFamily: 'ubuntu_regular',
        fontSize: 18,
        marginBottom: 50,
        paddingHorizontal: 20,
        textAlign: 'center',
    },

    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 60,
        width: '100%',
    },

    button: {
        alignSelf: 'center',
        backgroundColor: '#2C3840',
        marginBottom: 25,
        padding: 15,
    },

    buttonLabel: {
        color: '#FFF',
        fontFamily: 'ubuntu_regular',
        textAlign: 'center',
    },
});
