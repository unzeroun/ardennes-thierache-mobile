import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        position: 'absolute',
        width: '100%',
        zIndex: 1,
    },

    headerExpandButtonContainer: {
        alignSelf: 'center',
        backgroundColor: '#F26C4F',
        borderColor: '#FFF',
        borderRadius: 30,
        borderWidth: 1,
        height: 40,
        marginVertical: 10,
        width: 40,
    },

    headerExpandButton: {
        padding: 10,
    },

    arrow: {
        height: '100%',
        transform: [{rotateZ: '180deg'}],
        width: '100%',
    },

    poiName: {
        color: '#F26C4F',
        fontFamily: 'ubuntu_regular',
        fontSize: 28,
        marginBottom: 40,
    },

    mediaContainer: {
        height: Dimensions.get('window').width * .5,
        width: Dimensions.get('window').width,
    },

    poiDescription: {
        padding: 10,
    },

    additionalData: {
        fontFamily: 'ubuntu_bold',
        paddingBottom: 5,
        paddingLeft: 10,
    },

    city: {
        display: 'flex',
        flexDirection: 'row',
    },
});
