import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    header: {
        alignItems: 'center',
        backgroundColor: '#2c3840',
        bottom: 0,
        display: 'flex',
        flexDirection: 'row',
        height: 140,
        justifyContent: 'space-between',
        paddingHorizontal: 5,
        position: 'absolute',
        width: '100%',
    },

    headerExpandButtonContainer: {
        backgroundColor: '#F26C4F',
        borderColor: '#FFF',
        borderRadius: 30,
        borderWidth: 1,
        left: Dimensions.get('window').width / 2,
        position: 'absolute',
        top: -20,
        transform: [{translateX: -20}],
    },

    headerExpandButton: {
        height: 40,
        padding: 10,
        width: 40,
    },

    contentContainer: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
    },

    textContainer: {
        width: '80%',
    },

    arrow: {
        height: '100%',
        width: '100%',
    },

    headerLabel: {
        color: '#F26C4F',
        flexShrink: 1,
        fontFamily: 'ubuntu_bold',
        fontSize: 20,
        marginBottom: 10,
    },

    headerDescription: {
        color: '#FFF',
        fontFamily: 'ubuntu_regular',
        fontSize: 14,
    },

    carButton: {
        alignSelf: 'center',
        height: 60,
        marginLeft: 10,
        padding: 10,
        width: 60,
    },

    carIcon: {
        height: '100%',
        width: '100%',
    },
});
