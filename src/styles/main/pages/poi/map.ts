import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        height: '100%',
        position: 'relative',
        width: '100%',
    },

    map: {
        height: '100%',
        width: '100%',
    },

    icon: {
        height: 30,
        width: 30,
    },

    dotCircleContainer: {
        bottom: 10,
        height: 40,
        position: 'absolute',
        right: 10,
        width: 40,
    },

    dotCircle: {
        height: '100%',
        width: '100%',
    },
});
