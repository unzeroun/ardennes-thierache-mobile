import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    title: {
        color: '#DFDFDF',
        fontFamily: 'ubuntu_light',
        fontSize: 30,
        marginBottom: 10,
        paddingLeft: 20,
        paddingTop: 10,
    },

    separator: {
        borderBottomColor: '#DFDFDF',
        borderBottomWidth: 4,
    },

    content: {
      paddingVertical: 10,
    },

    loadMore: {
        marginVertical: 15,
    },
});
