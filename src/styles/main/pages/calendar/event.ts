import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        paddingVertical: 10,
    },

    titleContainer: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    title: {
        borderBottomColor: '#EDEDED',
        borderBottomWidth: 4,
        color: '#DFDFDF',
        fontFamily: 'ubuntu_light',
        fontSize: 30,
        marginBottom: 10,
        marginLeft: 10,
        paddingBottom: 10,
        paddingLeft: 30,
    },

    separator: {
        borderBottomColor: '#DFDFDF',
        borderBottomWidth: 4,
    },

    content: {
        paddingHorizontal: 15,
        paddingVertical: 30,
    },

    eventTitle: {
        color: '#F26C4F',
        fontSize: 40,
        marginBottom: 25,
    },

    imageContainer: {
        backgroundColor: '#CCC',
        height: (Dimensions.get('window').width - 60) * .66,
        width: Dimensions.get('window').width,
    },

    image: {
        height: '100%',
        width: '100%',
    },

    textContainer: {
        paddingHorizontal: 15,
        paddingVertical: 30,
    },

    description: {
        fontFamily: 'ubuntu_regular',
        lineHeight: 20,
        opacity: .8,
    },

    date: {
        fontFamily: 'ubuntu_bold',
        marginVertical: 15,
        opacity: .8,
    },

    place: {
        fontFamily: 'ubuntu_bold',
        fontSize: 20,
        opacity: .8,
    },

    contactLabel: {
        fontFamily: 'ubuntu_italic',
        marginVertical: 15,
        opacity: .8,
    },

    organizerData: {
        fontFamily: 'ubuntu_regular',
        marginBottom: 4,
        opacity: .8,
    },

    map: {
        height: 300,
        width: '100%',
    },

    marker: {
        height: 30,
        width: 30,
    },
});
