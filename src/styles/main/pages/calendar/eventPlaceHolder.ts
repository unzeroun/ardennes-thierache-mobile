import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        paddingVertical: 10,
    },

    content: {
        paddingHorizontal: 30,
    },

    titleContainer: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 30,
    },

    title: {
        borderBottomColor: '#EDEDED',
        borderBottomWidth: 4,
        color: '#DFDFDF',
        fontFamily: 'ubuntu_light',
        fontSize: 30,
        marginBottom: 10,
        marginLeft: 10,
        paddingBottom: 10,
        paddingLeft: 30,
    },

    separator: {
        borderBottomColor: '#DFDFDF',
        borderBottomWidth: 4,
        marginBottom: 35,
    },

    titlePlaceholder: {
        marginBottom: 20,
    },

    eventTitle: {
        backgroundColor: '#CCC',
        borderRadius: 15,
        height: 15,
        marginTop: 15,
        maxWidth: 300,
        width: Dimensions.get('window').width * .8,
    },

    imageContainer: {
        marginVertical: 30,
    },

    textPlaceholder: {
        marginBottom: 10,
    },

    mapPlaceholder: {
        marginTop: 30,
    },
});
