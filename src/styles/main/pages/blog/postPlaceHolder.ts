import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        paddingVertical: 15,
    },

    title: {
        color: '#DFDFDF',
        fontFamily: 'ubuntu_light',
        fontSize: 30,
        marginBottom: 10,
        paddingLeft: 30,
    },

    separator: {
        borderBottomColor: '#DFDFDF',
        borderBottomWidth: 4,
    },

    content: {
        padding: 30,
        paddingTop: 60,
    },

    header: {
        marginBottom: 10,
        marginTop: 10,
        transform: [{translateX: 30}],
        width: Dimensions.get('window').width * .5,
    },

    imageContainer: {
        marginVertical: 10,
        width: '100%',
    },

    textContainer: {
        marginTop: 10,
    },
});
