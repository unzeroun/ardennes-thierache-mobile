import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        paddingVertical: 15,
    },

    title: {
        color: '#DFDFDF',
        fontFamily: 'ubuntu_light',
        fontSize: 30,
        marginBottom: 10,
        paddingLeft: 30,
    },

    separator: {
        borderBottomColor: '#DFDFDF',
        borderBottomWidth: 4,
    },

    content: {
        padding: 30,
    },

    header: {
        padding: 30,
    },

    date: {
        fontFamily: 'ubuntu_italic',
    },

    postTitle: {
        color: '#F26C4F',
        fontFamily: 'ubuntu_regular',
        fontSize: 28,
    },

    imageContainer: {
        backgroundColor: '#CCC',
        height: (Dimensions.get('window').width - 60) * .66,
        position: 'relative',
        width: Dimensions.get('window').width,
    },

    image: {
        height: '100%',
        width: '100%',
    },

    textContainer: {
        padding: 30,
    },

    body: {
        fontFamily: 'ubuntu_regular',
    },
});
