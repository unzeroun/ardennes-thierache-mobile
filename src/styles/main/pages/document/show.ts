import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        paddingHorizontal: 30,
        paddingVertical: 40,
    },

    title: {
        color: '#F26C4F',
        fontFamily: 'ubuntu_regular',
        fontSize: 28,
        marginBottom: 40,
    },

    description: {
        fontFamily: 'ubuntu_regular',
        marginBottom: 30,
        opacity: .8,
    },
});
