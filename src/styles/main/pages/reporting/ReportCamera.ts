import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#000',
        display: 'flex',
        height: '100%',
        position: 'relative',
        width: Dimensions.get('window').width,
    },

    camera: {
        height: '100%',
    },

    linkContainer: {
        backgroundColor: 'rgba(255, 255, 255, .5)',
        left: 10,
        padding: 20,
        position: 'absolute',
        top: 10,
    },

    snapButton: {
        backgroundColor: '#FFF',
        borderRadius: 40,
        bottom: 20,
        height: 80,
        left: Dimensions.get('window').width / 2,
        opacity: .5,
        position: 'absolute',
        transform: [{translateX: -40}],
        width: 80,
    },

    activityIndicator: {
        backgroundColor: 'transparent',
        bottom: 20,
        height: 80,
        left: Dimensions.get('window').width / 2,
        opacity: .5,
        position: 'absolute',
        transform: [{translateX: -40}],
        width: 80,
    },

    noCameraContainer: {
        alignItems: 'center',
        backgroundColor: '#262626',
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
    },

    noCameraLabel: {
        color: '#FFF',
        fontFamily: 'ubuntu_bold',
        marginBottom: 35,
    },

    noCameraLinkContainer: {
        backgroundColor: '#2C3840',
        padding: 20,
    },

    noCameraLinkLabel: {
        color: '#FFF',
        fontFamily: 'ubuntu_regular',
    },
});
