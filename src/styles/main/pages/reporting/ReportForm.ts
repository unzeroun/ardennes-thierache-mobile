import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#262626',
        flex: 1,
        padding: 25,
        paddingBottom: 0,
    },

    title: {
        color: '#FFF',
        fontFamily: 'ubuntu_bold',
        fontSize: 24,
        marginBottom: 25,
        textAlign: 'center',
    },

    image: {
        height: 200,
        width: '100%',
    },

    inputLabel: {
        color: '#FFF',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
    },

    input: {
        borderColor: 'rgba(0, 0, 0, .2)',
        borderWidth: 1,
        color: '#FFF',
        fontFamily: 'ubuntu_regular',
        marginVertical: 15,
        padding: 10,
        width: '100%',
    },

    map: {
        height: 200,
        marginBottom: 25,
        width: '100%',
    },

    marker: {
        height: 30,
        width: 30,
    },

    button: {
        alignSelf: 'center',
        backgroundColor: '#2C3840',
        marginBottom: 25,
        padding: 15,
    },

    buttonLabel: {
        color: '#FFF',
        fontFamily: 'ubuntu_regular',
        textAlign: 'center',
    },
});
