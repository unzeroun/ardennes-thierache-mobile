import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'flex-start',
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        height: '100%',
    },

    button: {
        alignItems: 'center',
        display: 'flex',
        height: `${100 / 3}%`,
        justifyContent: 'space-between',
        overflow: 'hidden',
        padding: 35,
        width: Dimensions.get('window').width / 2,
    },

    image: {
        flexShrink: 1,
    },

    label: {
        fontFamily: 'ubuntu_bold',
        textAlign: 'center',
    },
});
