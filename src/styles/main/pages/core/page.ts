import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        paddingVertical: 30,
        width: '100%',
    },

    title: {
        color: '#F26C4F',
        fontFamily: 'ubuntu_bold',
        fontSize: 22,
        paddingHorizontal: 30,
    },

    imageBlock: {
        height: Dimensions.get('window').width - 60,
    },

});
