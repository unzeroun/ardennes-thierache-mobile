import Constants from 'expo-constants';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    statusBar: {
        backgroundColor: '#2C3840',
        elevation: 2,
        height: Constants.statusBarHeight,
        zIndex: 2,
    },
});
