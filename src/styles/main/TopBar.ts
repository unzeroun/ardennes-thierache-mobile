import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#2C3840',
        display: 'flex',
        elevation: 2,
        flexDirection: 'row',
        justifyContent: 'center',
        paddingHorizontal: 10,
        paddingVertical: 15,
        position: 'relative',
        zIndex: 2,
    },

    iconContainer: {
        left: 10,
        position: 'absolute',
    },

    arrow: {
        height: 35,
        width: 35,
    },

    menu: {
        height: 35,
        width: 35,
    },

    title: {
        alignSelf: 'center',
        color: '#FFF',
        fontFamily: 'ubuntu_bold',
        paddingVertical: 5,
        textAlign: 'center',
        width: '60%',
    },

    reportIconContainer: {
        height: 35,
        position: 'absolute',
        right: 10,
        width: 35,
    },

    reportIcon: {
        height: '100%',
        width: '100%',
    },
});
