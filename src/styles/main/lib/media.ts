import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    webView: {
        height: '100%',
        width: '100%',
    },

    image: {
        height: '100%',
        width: '100%',
    },
});
