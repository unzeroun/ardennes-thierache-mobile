import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    image: {
        backgroundColor: '#CCC',
        height: 200,
        marginVertical: 5,
        width: '100%',
    },

    text: {
        backgroundColor: '#CCC',
        borderRadius: 15,
        height: 15,
        marginVertical: 5,
        maxWidth: 300,
        width: Dimensions.get('window').width * .8,
    },
});
