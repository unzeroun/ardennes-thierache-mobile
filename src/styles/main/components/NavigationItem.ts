import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    itemContainer: {
        alignItems: 'center',
        display: 'flex',
        flex: 1,
        paddingVertical: 13,
    },

    itemContainerActive: {
        backgroundColor: '#5882BD',
    },

    icon: {
        height: 24,
        width: 24,
    },
});
