import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {

    },

    error: {
        color: '#F26C4F',
        fontFamily: 'ubuntu_bold',
        padding: 15,
        textAlign: 'center',
    },
});
