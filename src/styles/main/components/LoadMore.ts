import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignSelf: 'center',
    },

    title: {
        color: '#F26C4F',
        fontFamily: 'ubuntu_bold',
        padding: 15,
        textDecorationLine: 'underline',
    },
});
