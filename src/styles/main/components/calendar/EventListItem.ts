import {Dimensions, StyleSheet} from 'react-native';

const imageWidth = Dimensions.get('window').width * .4;

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        marginHorizontal: 10,
        marginVertical: 20,
    },

    happeningLabel: {
        color: '#F26C4F',
        fontFamily: 'ubuntu_bold',
        fontSize: 20,
    },

    containerHappening: {
        alignItems: 'center',
        backgroundColor: '#666',
        display: 'flex',
        marginBottom: 10,
        padding: 15,
    },

    imageContainer: {
        backgroundColor: '#CCC',
        height: imageWidth + imageWidth * .33,
        marginRight: 20,
        position: 'relative',
        width: imageWidth,
    },

    image: {
        height: '100%',
        width: '100%',
    },

    arrowContainer: {
        alignItems: 'center',
        backgroundColor: '#F26C4F',
        borderColor: '#F6947F',
        borderRadius: 20,
        borderWidth: 1,
        bottom: '50%',
        display: 'flex',
        height: 40,
        justifyContent: 'center',
        position: 'absolute',
        right: 0,
        transform: [{translateX: 20}, {translateY: 20}],
        width: 40,
    },

    arrow: {
        height: 15,
        width: 15,
    },

    content: {
        flex: 1,
    },

    eventTitle: {
        color: '#F26C4F',
        fontFamily: 'ubuntu_light',
        fontSize: 20,
    },

    place: {
        alignSelf: 'center',
        borderBottomColor: '#000',
        borderBottomWidth: 5,
        color: 'rgba(0, 0, 0, .5)',
        fontFamily: 'ubuntu_regular',
        paddingBottom: 10,
        paddingTop: 5,
    },

    date: {
        fontSize: 13,
        fontWeight: 'bold',
    },
});
