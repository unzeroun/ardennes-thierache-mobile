import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20,
    },

    title: {
        fontFamily: 'ubuntu_regular',
        fontSize: 20,
        opacity: .8,
    },

    arrowContainer: {
        alignItems: 'center',
        backgroundColor: '#F26C4F',
        borderColor: '#F6947F',
        borderRadius: 15,
        borderWidth: 1,
        display: 'flex',
        height: 25,
        justifyContent: 'center',
        marginLeft: 15,
        width: 25,
    },

    arrow: {
        height: '50%',
        width: '50%',
    },
});
