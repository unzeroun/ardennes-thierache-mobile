import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10,
        width: Dimensions.get('window').width,
    },

    content: {
        flex: 1,
    },

    title: {
        color:      '#F26C4F',
        fontFamily: 'ubuntu_bold',
        fontSize:   20,
    },

    description: {
        fontFamily: 'ubuntu_italic',
        opacity: .8,
    },

    arrowContainer: {
        alignItems: 'center',
        backgroundColor: '#F26C4F',
        borderColor: '#F6947F',
        borderRadius: 15,
        borderWidth: 1,
        display: 'flex',
        height: 25,
        justifyContent: 'center',
        marginLeft: 15,
        width: 25,
    },

    arrow: {
        height: '50%',
        width: '50%',
    },
});
