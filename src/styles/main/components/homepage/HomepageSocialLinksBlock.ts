import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#F2C84B',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 25,
    },
});
