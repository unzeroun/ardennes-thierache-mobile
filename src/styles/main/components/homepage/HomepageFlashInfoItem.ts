import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        marginBottom: 10,
        width: '90%',
    },

    shortDateContainer: {
        alignItems: 'center',
        backgroundColor: 'rgb(233,107,84)',
        borderRadius: 100,
        flexDirection: 'column',
        height: 50,
        justifyContent: 'center',
        marginRight: 10,
        width: 50,
    },

    shortDate: {
        color: '#FFF',
        fontFamily: 'ubuntu_light',
    },

    title: {
        color: 'rgb(77,77,77)',
        fontFamily: 'ubuntu_bold',
        fontSize: 14,
    },

    fromNow: {
        color: 'rgb(77,77,77)',
        fontFamily: 'ubuntu_italic',
        fontSize: 14,
    },
});
