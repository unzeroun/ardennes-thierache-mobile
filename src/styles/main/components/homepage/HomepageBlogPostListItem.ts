import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#F26C4F',
        display: 'flex',
        height: '100%',
        marginHorizontal: 10,
        width: 150,
    },

    imageContainer: {
        backgroundColor: '#CCC',
        height: '50%',
        marginBottom: 15,
        position: 'relative',
        width: '100%',
    },

    image: {
        height: '100%',
        width: '100%',
    },

    arrowContainer: {
        alignItems: 'center',
        backgroundColor: '#F26C4F',
        borderColor: '#F6947F',
        borderRadius: 15,
        borderWidth: 1,
        bottom: 0,
        display: 'flex',
        height: 25,
        justifyContent: 'center',
        left: '50%',
        position: 'absolute',
        transform: [{translateX: -10}, {translateY: 10}],
        width: 25,
    },

    arrow: {
        height: 15,
        width: 15,
    },

    title: {
        alignItems: 'center',
        backgroundColor: '#F26C4F',
        color: 'white',
        flex: 1,
        fontFamily: 'ubuntu_light',
        fontSize: 14,
        justifyContent: 'center',
        paddingBottom: 15,
        paddingHorizontal: 15,
    },
});
