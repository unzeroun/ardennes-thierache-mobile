import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        marginBottom: 10,
        maxHeight: 220,
    },

    title: {
        color: '#CCC',
        fontFamily: 'ubuntu_light',
        fontSize: 25,
        margin: 10,
    },

    titleContainer: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
    },

    linkContainer: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        marginRight: 10,
    },

    linkLabel: {
        color: '#E86A52',
        fontFamily: 'ubuntu_bold',
        marginRight: 5,
    },

    arrow: {
        height: 10,
        width: 10,
    },
});
