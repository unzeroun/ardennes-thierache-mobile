import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: 'transparent',
        display: 'flex',
        marginHorizontal: 10,
        width: 130,
    },

    imageContainer: {
        backgroundColor: '#CCC',
        height: 150,
        marginBottom: 15,
        position: 'relative',
        width: '100%',
    },

    image: {
        height: '100%',
        width: '100%',
    },

    arrowContainer: {
        alignItems: 'center',
        backgroundColor: '#F26C4F',
        borderColor: '#F6947F',
        borderRadius: 15,
        borderWidth: 1,
        bottom: 0,
        display: 'flex',
        height: 25,
        justifyContent: 'center',
        left: '50%',
        position: 'absolute',
        transform: [{translateX: -10}, {translateY: 10}],
        width: 25,
    },

    arrow: {
        height: 15,
        width: 15,
    },

    date: {
        color: '#4D4D4D',
        fontFamily: 'ubuntu_bold',
        textAlign: 'center',
    },

    title: {
        color: '#F26C4F',
        fontFamily: 'ubuntu_light',
        textAlign: 'center',
    },
});
