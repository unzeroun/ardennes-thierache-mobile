import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        width: 40,
    },
    image: {
        height: 40,
        width: 40,
    },
});
