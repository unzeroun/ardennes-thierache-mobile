import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        padding: 10,
        width: '100%',
    },

    titleContainer: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20,
    },

    linkContainer: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
    },

    linkLabel: {
        color: '#E86A52',
        fontFamily: 'ubuntu_bold',
        marginRight: 5,
    },

    arrow: {
        height: 10,
        width: 10,
    },

    title: {
        color: '#CCC',
        fontFamily: 'ubuntu_light',
        fontSize: 25,
    },
});
