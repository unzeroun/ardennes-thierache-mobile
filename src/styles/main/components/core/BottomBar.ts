import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#2C3840',
        display: 'flex',
        flexDirection: 'row',
    },
});
