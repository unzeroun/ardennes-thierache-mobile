import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    linkContainer: {
        borderBottomColor: 'rgba(26, 36, 52, .3)',
        borderBottomWidth: 1,
        paddingLeft: 60,
    },

    label: {
        color: '#5883bd',
        fontWeight: 'bold',
        paddingLeft: 50,
        paddingVertical: 20,
    },

    linkLabelContainer: {
        borderLeftColor: '#5883bd',
        borderLeftWidth: 1,
    },

    linkLabel: {
        color: '#5883bd',
        paddingLeft: 15,
        paddingVertical: 20,
    },
});
