import Constants from 'expo-constants';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    statusBar: {
        backgroundColor: '#2C3840',
        height: Constants.statusBarHeight,
    },

    container: {
        elevation: 2,
        height: '100%',
        position: 'absolute',
        width: '100%',
        zIndex: 2,
    },

    containerActive: {
        left: 0,
    },

    filterContainer: {
        backgroundColor: '#000',
        height: '100%',
        position: 'absolute',
        top: Constants.statusBarHeight,
        width: '100%',
    },

    filter: {
        height: '100%',
        width: '100%',
    },

    content: {
        backgroundColor: '#2C3840',
        flex: 1,
        width: '80%',
    },

    carpoolingButton: {
        alignItems: 'center',
        backgroundColor: '#1F2E40',
        borderBottomColor: 'rgba(26, 36, 52, .3)',
        borderBottomWidth: 1,
        display: 'flex',
        justifyContent: 'center',
        paddingLeft: 15,
        paddingVertical: 20,
        width: '100%',
    },

    carpoolingLabel: {
        color: '#5883bd',
        fontWeight: 'bold',
    },
});
