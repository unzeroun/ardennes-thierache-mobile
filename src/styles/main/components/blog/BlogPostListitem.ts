import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#F26C4F',
        marginVertical: 5,
        maxHeight: 200,
        overflow: 'hidden',
    },

    contentContainer: {
        height: '100%',
        position: 'relative',
    },

    imageContainer: {
        backgroundColor: '#CCC',
        height: 90,
        marginBottom: 25,
        position: 'relative',
        width: '100%',
    },

    image: {
        height: '100%',
        width: '100%',
    },

    arrowContainer: {
        alignItems: 'center',
        backgroundColor: '#F26C4F',
        borderColor: '#F6947F',
        borderRadius: 25,
        borderWidth: 1,
        bottom: 0,
        display: 'flex',
        height: 50,
        justifyContent: 'center',
        left: '50%',
        position: 'absolute',
        transform: [{translateX: -25}, {translateY: 25}],
        width: 50,
    },

    arrow: {
        height: 15,
        width: 15,
    },

    title: {
        color: '#FFF',
        fontFamily: 'ubuntu_light',
        fontSize: 20,
    },

    gradient: {
        bottom: 0,
        height: 50,
        position: 'absolute',
        width: '100%',
    },
});
