import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {},

    warning: {
        color: '#FFF',
        fontFamily: 'ubuntu_light',
    },

    title: {
        color: '#FFF',
        fontFamily: 'ubuntu_bold',
        fontSize: 18,
        marginBottom: 25,
    },
});
