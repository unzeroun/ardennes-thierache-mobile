import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#4CAF50',
        position: 'absolute',
        width: Dimensions.get('window').width,
    },

    errored: {
        backgroundColor: '#ED1B24',
    },

    label: {
        color: '#FFF',
        fontFamily: 'ubuntu_regular',
        padding: 15,
    },
});
