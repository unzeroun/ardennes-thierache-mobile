import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        marginBottom: 30,
    },

    checkbox: {
        height: 25,
        marginRight: 15,
        width: 25,
    },

    disabled: {
        borderColor: '#FFF',
        borderWidth: 2,
    },

    label: {
        color: '#FFF',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
    },
});
