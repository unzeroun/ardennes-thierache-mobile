import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    filler: {
        flex: 1,
    },
});
