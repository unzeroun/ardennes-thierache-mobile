import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingBottom: 10,
    },

    errorLabel: {
        color: '#FF9494',
        textAlign: 'center',
        width: 150,
    },
});
