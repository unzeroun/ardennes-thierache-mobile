import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, .75)',
        height: Dimensions.get('window').height,
        left: 0,
        position: 'absolute',
        top: 0,
        width: Dimensions.get('window').width,
    },

    modal: {
        left: 0,
        position: 'absolute',
        top: 0,
    },

    container: {
        alignItems: 'center',
        height: Dimensions.get('window').height,
        justifyContent: 'center',
        width: Dimensions.get('window').width,
    },

    labelContainer: {
        position: 'absolute',
        textAlign: 'center',
        top: 0,
    },

    label: {
        backgroundColor: '#404B52',
        color: '#DCE7EE',
        fontFamily: 'ubuntu_regular',
        fontSize: 18,
        textAlign: 'center',
        width: '100%',
    },

    pickerContainer: {
        backgroundColor: '#FFF',
        borderRadius: 2,
        marginBottom: 25,
        paddingHorizontal: 60,
        paddingVertical: 40,
    },

    submit: {
        alignSelf: 'center',
        backgroundColor: '#F3C045',
        borderRadius: 20,
        paddingHorizontal: 40,
        paddingVertical: 10,
    },

    submitLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
        textAlign: 'center',
    },
});
