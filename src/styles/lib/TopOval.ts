import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#F00',
        borderRadius: Dimensions.get('window').width * 2.5,
        height: Dimensions.get('window').width * 5,
        left: '50%',
        position: 'absolute',
        top: 0,
        transform: [{translateX: -Dimensions.get('window').width * 2.5 }],
        width: Dimensions.get('window').width * 5,
    },
});
