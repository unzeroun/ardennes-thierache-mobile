import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    chatButton: {
        alignSelf: 'center',
        backgroundColor: '#F3C045',
        borderRadius: 22,
        marginVertical: 20,
        paddingHorizontal: 45,
        paddingVertical: 12,
    },

    chatButtonLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
    },

    statusBadge: {
        display: 'none',
    },

    statusBadgeUnread: {
        backgroundColor: 'transparent',
        display: 'flex',
        height: 25,
        position: 'absolute',
        top: 0,
    },
});
