import {StyleSheet} from 'react-native';

export default {
    white: StyleSheet.create({
        backButtonContainer: {
            alignItems: 'center',
            position: 'absolute',
            right: 20,
            top: 50,
        },

        backButton: {
            alignItems: 'center',
            backgroundColor: '#FFF',
            borderRadius: 20,
            bottom: -15,
            display: 'flex',
            height: 40,
            justifyContent: 'center',
            position: 'relative',
            right: 20,
            top: 10,
            width: 40,
        },

        backIcon: {
            height: 15,
            transform: [{rotateZ: '45deg'}],
            width: 15,
        },
    }),
    yellow: StyleSheet.create({
        backButtonContainer: {
            alignItems: 'flex-end',
            position: 'absolute',
            right: 5,
            top: 130,
        },

        backButton: {
            alignItems: 'center',
            backgroundColor: '#F3C045',
            borderRadius: 15,
            bottom: -15,
            display: 'flex',
            height: 30,
            justifyContent: 'center',
            position: 'relative',
            right: 20,
            top: 10,
            width: 30,
        },

        backIcon: {
            height: 15,
            transform: [{rotateZ: '45deg'}],
            width: 15,
        },
    }),
};
