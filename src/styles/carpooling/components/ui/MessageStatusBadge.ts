import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#F8F8F8',
        borderRadius: 50,
        height: 50,
        justifyContent: 'center',
        padding: 15,
        position: 'relative',
        width: 50,
    },

    unreadContainer: {
        backgroundColor: '#F3C045',
    },

    icon: {
        height: 20,
        top: 3,
    },

    count: {
        color: '#404B52',
        fontFamily: 'ubuntu_bold',
        fontSize: 12,
        position: 'absolute',
    },

    unreadCount: {
        color: '#DCE7EE',
    },
});
