import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        bottom: 0,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'absolute',
        width: '100%',
    },

    content: {
        flexDirection: 'row',
        width: Dimensions.get('window').width * .8,
    },

    activeItem: {
        backgroundColor: 'transparent',
    },

    quitButton: {
        alignItems: 'center',
        display: 'flex',
        flex: 1,
        paddingVertical: 13,
    },

    icon: {
        height: 24,
        width: 24,
    },
});
