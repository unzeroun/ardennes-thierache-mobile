import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#F3C045',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: -20,
        paddingVertical: 20,
        width: Dimensions.get('window').width,
    },

    logo: {
        width: Dimensions.get('window').width * .5,
    },
});
