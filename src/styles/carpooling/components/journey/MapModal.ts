import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    backdrop: {
        backgroundColor: '#F00',
        height: Dimensions.get('window').height,
        position: 'absolute',
        width: Dimensions.get('window').width,
    },

    map: {
        height: 200,
    },
});
