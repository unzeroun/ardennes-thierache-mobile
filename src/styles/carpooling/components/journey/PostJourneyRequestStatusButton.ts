import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignSelf: 'center',
        backgroundColor: '#F3C045',
        borderRadius: 20,
        paddingHorizontal: 30,
        paddingVertical: 12,
    },

    pending: {
        backgroundColor: '#DCE7EE',
    },

    denied: {
        backgroundColor: '#FFBABA',
    },

    label: {
        color: '#2C3840',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
    },
});
