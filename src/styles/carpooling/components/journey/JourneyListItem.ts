import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#FFF',
        display: 'flex',
        flexDirection: 'row',
        height: 60,
        justifyContent: 'space-between',
    },

    timeContainer: {
        alignItems: 'center',
        backgroundColor: '#2C3840',
        borderBottomRightRadius: 28,
        borderTopRightRadius: 28,
        display: 'flex',
        height: 60,
        justifyContent: 'center',
        paddingLeft: 50,
        position: 'relative',
        width: 85,
    },

    time: {
        alignItems: 'center',
        backgroundColor: '#F0BE2B',
        borderRadius: 28,
        display: 'flex',
        height: 56,
        justifyContent: 'center',
        position: 'absolute',
        right: 3,
        width: 56,
    },

    day: {
        color: '#2C3840',
        fontFamily: 'ubuntu_light',
    },

    hour: {
        color: '#2C3840',
        fontFamily: 'ubuntu_medium',
        fontSize: 12,
    },

    timeAndPlaceContainer: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
    },

    stepsContainer: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        paddingLeft: 25,
    },

    icon: {
        height: 40,
    },

    locations: {
        alignItems: 'flex-start',
        display: 'flex',
    },

    text: {
        color: '#2C3840',
        fontFamily: 'ubuntu_medium',
    },

    dataContainer: {
        paddingRight: 25,
    },

    availablePlaces: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
    },

    carSeat: {
        height: 15,
        width: 15,
    },

    chatContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
    },

    chatIcon: {
        height: 20,
        top: 3,
        width: 20,
    },

    unreadCount: {
        color: '#FFF',
        fontFamily: 'ubuntu_bold',
        fontSize: 12,
        position: 'absolute',
    },
});
