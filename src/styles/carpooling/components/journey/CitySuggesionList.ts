import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        // zIndex: 2,
        height: Dimensions.get('window').height,
        overflow: 'visible',
        width: Dimensions.get('window').width,
    },

    backdrop: {
        height: Dimensions.get('window').height,
        left: 0,
        position: 'absolute',
        top: 0,
        width: Dimensions.get('window').width,
    },

    listContainer: {
        elevation: 2,
        height: '100%',
        paddingHorizontal: 50,
        position: 'relative',
        top: 25,
        zIndex: 2,
    },
});
