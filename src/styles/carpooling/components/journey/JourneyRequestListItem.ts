import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%',
    },

    nameContainer: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
    },

    name: {
        color: '#404B52',
        fontFamily: 'ubuntu_regular',
        fontSize: 20,
    },

    firstName: {
        paddingRight: 5,
    },
});
