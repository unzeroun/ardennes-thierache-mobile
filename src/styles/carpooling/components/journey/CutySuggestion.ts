import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#EEE',
        padding: 10,
    },

    label: {
        color: '#404B52',
        fontFamily: 'ubuntu_light',
        fontSize: 16,
    },
});
