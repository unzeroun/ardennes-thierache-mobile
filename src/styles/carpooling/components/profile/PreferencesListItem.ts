import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
    },

    button: {
        alignItems: 'center',
        backgroundColor: '#00000029',
        borderRadius: 20,
        height: 40,
        justifyContent: 'center',
        width: 40,
    },

    buttonEnabled: {
        backgroundColor: '#DCE7EE',
    },

    icon: {
        height: 20,
        width: 20,
    },

    label: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_light',
        fontSize: 14,
    },

    separator: {
        borderColor: '#DCE7EE',
        borderTopWidth: 1,
        height: 1,
        marginVertical: 7,
        width: 65,
    },
});
