import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#404B52',
        paddingBottom: 15,
        paddingTop: 25,
    },
});
