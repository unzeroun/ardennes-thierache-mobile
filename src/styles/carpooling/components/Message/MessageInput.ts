import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        alignSelf: 'flex-end',
        flexDirection: 'row',
        paddingTop: 10,
        transform: [{translateX: -20}],
        width: Dimensions.get('window').width * .9,
    },

    input: {
        borderColor: '#DCE7EE',
        borderRadius: Dimensions.get('window').width * .3,
        borderWidth: 1,
        color: '#DCE7EE',
        flex: 1,
        fontFamily: 'ubuntu_light',
        fontSize: 14,
        height: 50,
        marginRight: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },

    send: {
        backgroundColor: '#F3C045',
        borderRadius: 25,
        height: 50,
        justifyContent: 'center',
        padding: 10,
        position: 'relative',
        width: 50,
    },

    chatIcon: {
        height: 20,
        position: 'absolute',
        transform: [{rotateY: '180deg'}, {translateY: 2}],
        width: 50,
    },

    plusIcon: {
        height: 7,
        position: 'absolute',
        width: 50,
    },
});
