import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignSelf: 'flex-end',
        marginBottom: 15,
        width: Dimensions.get('window').width * .5,
    },

    received: {
        alignSelf: 'flex-start',
    },

    time: {
        color: '#DBE7ED',
        fontFamily: 'ubuntu_light',
        fontSize: 10,
        marginBottom: 5,
        paddingLeft: 20,
    },

    content: {
        backgroundColor: '#DBE7ED',
        borderRadius: Dimensions.get('window').width * .03,
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
        padding: 20,
    },

    contentReceived: {
        backgroundColor: '#FFF',
    },
});
