import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#F3C045',
        borderRadius: 25,
        bottom: 150,
        height: 50,
        justifyContent: 'center',
        padding: 10,
        position: 'absolute',
        right: 20,
        width: 50,
    },

    icon: {
        width: 20,
    },
});
