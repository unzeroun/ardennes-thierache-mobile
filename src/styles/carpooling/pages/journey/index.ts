import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        flex: 1,
    },

    header: {
        backgroundColor: '#F0BE2B',
        paddingVertical: 16,
    },

    headerText: {
        color: '#2C3840',
        fontFamily: 'ubuntu_light',
        textAlign: 'center',
    },

    journeyList: {
        // paddingBottom: 10,
        position: 'relative',
    },

    pendingList: {
    },

    userJourneys: {},

    upcomingTargets: {
        backgroundColor: '#F3C045',
        paddingBottom: 25,
    },

    pendingTargets: {
        paddingBottom: 25,
        paddingTop: 25,
    },

    closeTargets: {
        paddingBottom: 25,
        paddingTop: 25,
    },

    closeTargetsContainer: {
        backgroundColor: '#F8F8F8',
    },

    requestsHeadTitle: {
        marginTop: -25,
    },

    item: {
        elevation: 3,
        marginBottom: 5,
        shadowColor: '#000',
        shadowOffset: {
            height: 1,
            width: 0,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
    },

    pendingTimeContainer: {
        backgroundColor: '#DCE7EE',
    },

    closeTimeContainer: {
        backgroundColor: '#F3C045',
    },

    addContainer: {
        alignItems: 'center',
        backgroundColor: '#F3C045',
        borderRadius: 25,
        bottom: 100,
        display: 'flex',
        height: 50,
        justifyContent: 'center',
        position: 'absolute',
        right: 10,
        width: 50,
    },

    addIcon: {
        height: 25,
        width: 25,
    },
});
