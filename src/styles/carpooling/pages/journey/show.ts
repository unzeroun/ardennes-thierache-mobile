import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        // backgroundColor: '#FFF',
        flex: 1,
        paddingBottom: 100,
    },

    loadingContainer: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    },

    backButtonContainer: {
        alignItems: 'flex-end',
        backgroundColor: '#FFF',
        display: 'flex',
        width: '100%',
    },

    backButton: {
        alignItems: 'center',
        backgroundColor: '#F3C045',
        borderRadius: 15,
        bottom: -15,
        display: 'flex',
        height: 30,
        justifyContent: 'center',
        position: 'relative',
        right: 20,
        top: 10,
        width: 30,
    },

    topContent: {
        backgroundColor: '#FFF',
        width: Dimensions.get('window').width,
    },

    backIcon: {
        height: 15,
        transform: [{rotateZ: '45deg'}],
        width: 15,
    },

    timeAndPlaceContainer: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        marginBottom: 15,
    },

    timeContainer: {
        alignItems: 'center',
        backgroundColor: '#F3C045',
        borderBottomRightRadius: 28,
        borderTopRightRadius: 28,
        display: 'flex',
        height: 60,
        justifyContent: 'center',
        paddingLeft: 50,
        paddingRight: 10,
        position: 'relative',
    },

    day: {
        color: '#2C3840',
        fontFamily: 'ubuntu_light',
    },

    hour: {
        color: '#2C3840',
        fontFamily: 'ubuntu_medium',
        fontSize: 12,
    },

    availablePlaces: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        paddingLeft: 15,
    },

    carSeat: {
        height: 15,
        width: 15,
    },

    placesLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_bold',
        fontSize: 12,
    },

    stepContainer: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 15,
        width: '100%',
    },

    stepIcon: {
        height: 60,
    },

    stepLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_medium',
        fontSize: 20,
    },

    price: {
        color: '#232323',
        fontFamily: 'ubuntu_light',
        fontSize: 36,
        marginBottom: 15,
        textAlign: 'center',
        width: '100%',
    },

    requestButtonContainer: {
        backgroundColor: '#FFF',
    },

    content: {
        alignItems: 'center',
        paddingTop: 20,
    },

    requestList: {
        paddingBottom: 25,
    },

    pendingRequestList: {
        backgroundColor: '#F8F8F8',
    },

    messagesList: {
        paddingTop: 25,
    },

    requestListLabel: {
        color: '#404B52',
        fontFamily: 'ubuntu_light',
        fontSize: 14,
        marginBottom: 20,
        textAlign: 'center',
    },

    statusButton: {
        marginBottom: 20,
    },

    driverData: {
        alignItems: 'center',
        backgroundColor: '#404C52',
        display: 'flex',
        flexDirection: 'column',
        paddingBottom: 10,
        position: 'relative',
    },

    driverHeaderLabel: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_light',
        fontSize: 14,
        marginBottom: 10,
        marginTop: -20,
    },

    driverName: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_regular',
        fontSize: 20,
    },

    chatButton: {
        alignSelf: 'center',
        backgroundColor: '#F3C045',
        borderRadius: 22,
        marginVertical: 20,
        paddingHorizontal: 45,
        paddingVertical: 12,
    },

    chatButtonLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
    },

    phoneButton: {
        alignSelf: 'stretch',
    },

    carData: {
        alignItems: 'center',
    },

    carIdentity: {
        alignItems: 'center',
        flexDirection: 'row',
    },

    carBrand: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_light',
        fontSize: 14,
        marginRight: 5,
    },

    carModel: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_regular',
        fontSize: 20,
    },

    carPreferences: {
        alignItems: 'center',
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingHorizontal: 75,
        paddingVertical: 15,
        width: Dimensions.get('window').width,
    },

    carPreferenceContainer: {
        alignItems: 'center',
        backgroundColor: '#DCE7EE',
        borderRadius: 20,
        height: 40,
        justifyContent: 'center',
        width: 40,
    },

    carPreference: {
        height: 20,
        width: 20,
    },

    map: {
        height: 300,
        width: Dimensions.get('window').width,
    },
});
