import {Dimensions, Platform, StyleSheet} from 'react-native';

export default StyleSheet.create({
    header: {
        alignItems: 'center',
        backgroundColor: '#F3C045',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingBottom: 40,
        paddingTop: 35,
        position: 'relative',
    },

    headerLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_bold',
        fontSize: 18,
    },

    container: {
        alignItems: 'center',
        backgroundColor: '#FFF',
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        position: 'relative',
    },

    scrollContainer: {
        alignItems: 'center',
        paddingBottom: 20,
        width: '100%',
    },

    inputContainer: {
        alignItems: 'center',
        marginBottom: 15,
        position: 'relative',
        textAlign: 'center',
        zIndex: 1,
    },

    inputLabel: {
        color: '#404B52',
        fontFamily: 'ubuntu_light',
    },

    input: {
        color: '#404B52',
        fontFamily: 'ubuntu_regular',
        fontSize: 18,
        paddingHorizontal: 20,
        paddingVertical: 10,
        textAlign: 'center',
        ...Platform.select({
            ios: {
                paddingBottom: 15,
                paddingHorizontal: 75,
                width: '100%',
            },
        }),
    },

    dateContainer: {
        borderWidth: 0,
        zIndex: 1,
    },

    errorLabel: {
        color: '#FF9494',
        textAlign: 'center',
        width: 150,
    },

    dateInput: {
        borderWidth: 0,
        color: '#404B52',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
        textAlign: 'center',
    },

    separator: {
        borderColor: '#404B52',
        borderTopWidth: 1,
        height: 0,
        marginVertical: 5,
        width: 65,
    },

    submit: {
        alignSelf: 'center',
        backgroundColor: '#F3C045',
        borderRadius: 20,
        marginBottom: 100,
        paddingHorizontal: 40,
        paddingVertical: 10,
    },

    submitLabel: {
        color: '#404B52',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
        textAlign: 'center',
    },
});
