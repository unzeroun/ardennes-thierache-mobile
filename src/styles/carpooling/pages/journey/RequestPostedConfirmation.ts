import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
    },

    header: {
        marginBottom: 15,
    },

    journey: {
        elevation: 2,
        marginVertical: 25,
        shadowColor: '#000',
        shadowOffset: {
            height: 1,
            width: 0,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
    },

    content: {
        alignItems: 'center',
        backgroundColor: '#2C3840',
        flex: 1,
        justifyContent: 'center',
    },

    image: {
        height: 50,
    },

    label: {
        color: '#F0BE2B',
        fontFamily: 'ubuntu_bold',
        fontSize: 14,
        marginBottom: 15,
        textAlign: 'center',
        width: 130,
    },

    name: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
        marginBottom: 25,
    },

    submit: {
        alignSelf: 'center',
        backgroundColor: '#F3C045',
        borderRadius: 20,
        paddingHorizontal: 40,
        paddingVertical: 10,
    },

    submitLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
        textAlign: 'center',
    },
});
