import {Dimensions, Platform, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#404B52',
        flex: 1,
    },

    scrollContainer: {
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
        marginTop: -20,
        paddingBottom: 30,
    },

    header: {
        alignItems: 'center',
        backgroundColor: '#F3C045',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingBottom: 20,
        width: Dimensions.get('window').width,
    },

    headerLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_light',
        fontSize: 14,
    },

    backButtonContainer: {
        alignItems: 'center',
        position: 'absolute',
        right: 20,
        top: '-50%',
    },

    backButton: {
        alignItems: 'center',
        backgroundColor: '#FFF',
        borderRadius: 20,
        bottom: -15,
        display: 'flex',
        height: 40,
        justifyContent: 'center',
        position: 'relative',
        right: 20,
        top: 10,
        width: 40,
    },

    backIcon: {
        height: 15,
        transform: [{rotateZ: '45deg'}],
        width: 15,
    },

    editIcon: {
        height: 20,
        width: 20,
    },

    modelContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 20,
    },

    modelInput: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_regular',
        fontSize: 18,
        padding: 20,
        textAlign: 'center',
    },

    inputContainer: {
        alignItems: 'center',
        marginBottom: 15,
    },

    input: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_regular',
        fontSize: 18,
        paddingHorizontal: 25,
        paddingVertical: 10,
        textAlign: 'center',
        ...Platform.select({
            ios: {
                paddingBottom: 15,
                paddingHorizontal: 75,
                width: '100%',
            },
        }),
    },

    dateInput: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_regular',
        fontSize: 18,
        textAlign: 'center',
    },

    separator: {
        borderColor: '#DCE7EE',
        borderTopWidth: 1,
        height: 1,
        marginVertical: 5,
        width: 65,
    },

    inputLabel: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_light',
        fontSize: 14,
    },

    preferencesList: {
        marginBottom: 20,
    },

    submit: {
        alignSelf: 'center',
        backgroundColor: '#F3C045',
        borderRadius: 20,
        marginBottom: 80,
        paddingHorizontal: 40,
        paddingVertical: 10,
    },

    submitLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
        textAlign: 'center',
    },
});
