import {Platform, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#404B52',
        flex: 1,
        paddingBottom: 80,
    },

    header: {
        alignItems: 'center',
        backgroundColor: '#F3C045',
        justifyContent: 'center',
        paddingBottom: 20,
    },

    headerLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_light',
        fontSize: 14,
    },

    scrollContainer: {
        backgroundColor: '#404B52',
        display: 'flex',
        justifyContent: 'center',
        marginTop: -20,
        paddingBottom: 20,
        width: '100%',
    },

    inputContainer: {
        alignItems: 'center',
        ...Platform.select({
            android: {
                marginBottom: 15,
            },
        }),
    },

    descriptionContainer: {
        ...Platform.select({
            ios: {
                paddingTop: 15,
            },
        }),
    },

    inputNameContainer: {
        marginBottom: 0,
    },

    nameContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 20,
    },

    nameLabel: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_light',
        fontSize: 24,
        padding: 15,
        textAlign: 'center',
    },

    nameOffset: {
        paddingHorizontal: 10,
    },

    editIcon: {
        height: 20,
        width: 20,
    },

    input: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_regular',
        fontSize: 18,
        paddingHorizontal: 35,
        textAlign: 'center',

        ...Platform.select({
            ios: {
                paddingBottom: 15,
                paddingHorizontal: 75,
                width: '100%',
            },
        }),
    },

    dateInput: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_regular',
        fontSize: 18,
        textAlign: 'center',
    },

    separator: {
        borderColor: '#DCE7EE',
        borderTopWidth: 1,
        height: 1,
        marginVertical: 5,
        width: 65,
    },

    inputLabel: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_light',
        fontSize: 14,
    },

    dateContainer: {
        borderWidth: 0,
    },

    genderContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        ...Platform.select({
            ios: {
                marginBottom: 15,
            },
        }),
    },

    genderPicker: {
        backgroundColor: 'transparent',
        elevation: -1,
        position: 'relative',
        textAlign: 'center',
        width: 150,
    },

    genderLabel: {
        backgroundColor: '#404B52',
        color: '#DCE7EE',
        fontFamily: 'ubuntu_regular',
        fontSize: 18,
        position: 'absolute',
        textAlign: 'center',
        width: '100%',
    },

    descriptionInput: {
        paddingHorizontal: 50,
    },

    submit: {
        alignSelf: 'center',
        backgroundColor: '#F3C045',
        borderRadius: 20,
        paddingHorizontal: 40,
        paddingVertical: 10,
    },

    submitLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
        textAlign: 'center',
    },
});
