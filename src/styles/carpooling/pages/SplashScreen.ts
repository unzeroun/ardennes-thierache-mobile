import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#F3C045',
        flex: 1,
        justifyContent: 'center',
    },

    logo: {
        width: Dimensions.get('window').width * .66,
    },
});
