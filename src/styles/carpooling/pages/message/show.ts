import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
    },

    scroller: {
        backgroundColor: '#404B52',
    },

    header: {
        backgroundColor: '#FFF',
        paddingBottom: 30,
        paddingTop: 20,
    },

    ovalContainer: {
        marginTop: -10,
    },

    backButtonContainer: {
        right: 0,
        top: 105,
    },

    contentContainer: {
        alignItems: 'center',
        backgroundColor: '#404C52',
        justifyContent: 'space-between',
        paddingHorizontal: 35,
    },

    innerContainer: {
        backgroundColor: '#404B52',
        width: '100%',
    },

    journey: {
        elevation: 3,
        marginBottom: 5,
        shadowColor: '#000',
        shadowOffset: {
            height: 1,
            width: 0,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
    },

    driverLabel: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_light',
        fontSize: 12,
    },

    driverName: {
        color: '#DCE7EE',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
    },

    messageContainer: {
        backgroundColor: '#404B52',
        flex: 1,
        paddingHorizontal: 35,
        width: Dimensions.get('window').width,
    },

    requestContainer: {
        alignItems: 'center',
        paddingVertical: 25,
    },

    requestIcon: {
        height: 25,
        marginBottom: 10,
        width: 25,
    },

    request: {
        color: '#F3C045',
        fontFamily: 'ubuntu_bold',
        paddingHorizontal: 10,
    },

    loader: {
        bottom: 100,
        justifyContent: 'center',
        position: 'absolute',
        right: 20,
    },

    inputContainer: {
        backgroundColor: '#404B52',
        width: '100%',
    },
});
