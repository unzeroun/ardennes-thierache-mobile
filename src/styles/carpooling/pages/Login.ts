import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
    },

    header: {
        alignItems: 'center',
        backgroundColor: '#F0BE2B',
        paddingVertical: 80,
        width: '100%',
    },

    logoContainer: {
        alignItems: 'center',
    },

    logo: {
        height: 50,
        marginBottom: 15,
        width: 100,
    },

    baseline: {
        height: 70,
        width: 150,
    },

    content: {
        alignItems: 'center',
        position: 'relative',
    },

    label: {
        color: '#404B52',
        fontFamily: 'ubuntu_light',
        fontSize: 14,
    },

    separator: {
        borderColor: '#404B52',
        borderTopWidth: 1,
        height: 1,
        marginVertical: 5,
        width: 55,
    },

    input: {
        color: '#404B52',
        flex: 1,
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
        justifyContent: 'center',
        marginBottom: 35,
        paddingHorizontal: 75,
        textAlign: 'center',
    },

    requestedInput: {
        color: '#F3C045',
    },

    submit: {
        alignSelf: 'center',
        backgroundColor: '#F3C045',
        borderRadius: 23,
        marginBottom: 15,
        paddingHorizontal: 40,
        paddingVertical: 15,
    },

    submitLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_regular',
        fontSize: 16,
        textAlign: 'center',
    },

    confirmContainer: {
        alignItems: 'center',
        width: '100%',
    },

    confirmLabel: {
        color: '#2C3840',
        fontFamily: 'ubuntu_regular',
        fontSize: 14,
    },

    confirmInput: {
        alignItems: 'center',
        color: '#2C3840',
        fontFamily: 'ubuntu_regular',
        fontSize: 14,
        justifyContent: 'center',
        marginTop: 15,
        paddingHorizontal: 5,
        textAlign: 'center',
        width: '100%',
    },

    inputUnderline: {
        borderColor: '#404B52',
        borderTopWidth: 1,
        height: 1,
        marginBottom: 15,
        width: 100,
    },
});
