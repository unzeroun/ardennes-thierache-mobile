import {Dimensions, StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        flex: 1,
    },

    content: {
        minHeight: Dimensions.get('window').height,
        paddingBottom: 80,
    },

    inputContainer: {
        alignItems: 'center',
        backgroundColor: '#F3C045',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingVertical: 15,
    },

    label: {
        color: '#2C3840',
        fontFamily: 'ubuntu_light',
        paddingVertical: 15,
        textAlign: 'center',
    },

    input: {
        backgroundColor: '#FFF',
        borderRadius: 20,
        marginRight: 5,
        paddingHorizontal: 10,
        paddingVertical: 15,
        width: Dimensions.get('window').width * .8,
    },

    btn: {
        alignItems: 'center',
        backgroundColor: '#2C3840',
        borderRadius: 20,
        height: 40,
        justifyContent: 'center',
        width: 40,
    },

    icon: {
        height: 20,
        width: 20,
    },

    title: {
        color: '#2C3840',
        fontFamily: 'ubuntu_light',
        paddingVertical: 15,
        textAlign: 'center',
    },

    item: {
        marginBottom: 15,
    },
});
