import {
    Action,
    History, Href,
    Location as OriginalLocation,
    LocationDescriptorObject,
    LocationKey, LocationListener,
    Path,
    Pathname,
    Search,
    TransitionPromptHook, UnregisterCallback,
} from 'history';
import React from 'react';
import {View} from 'react-native';
import {StaticContext} from 'react-router';
import {match, NativeRouter, Route, Switch} from 'react-router-native';
import {BOTTOM_BAR_NAVIGATION_ITEMS} from '../config';
import BottomBar from './components/main/core/BottomBar';
import MainNavigation from './components/main/core/navigation/MainNavigation';
import SideMenu from './components/main/core/SideMenu';
import Toaster from './components/main/settings/Toaster';
import TopBar from './components/main/TopBar';
import BackButton from './lib/BackButton';
import BlogPostList from './pages/main/blog/index';
import BlogPost from './pages/main/blog/post';
import CalendarEvent from './pages/main/calendar/event';
import CalendarEventIndex from './pages/main/calendar/index';
import MenuPage from './pages/main/core/page';
import DocumentCategory from './pages/main/document/category/show';
import Document from './pages/main/document/show';
import Home from './pages/main/Home';
import Map from './pages/main/poi/map';
import ReportCamera from './pages/main/reporting/ReportCamera';
import ReportForm from './pages/main/reporting/ReportForm';
import Reporting from './pages/main/reporting/Reporting';
import ReportTypes from './pages/main/reporting/ReportTypes';
import Settings from './pages/main/Settings';
import styles from './styles/main/Router';

type State = {
    page?: string,
    id?: number,
    slug?: string,
    inMenu?: boolean,
};

export type Location = {
    pathname: Pathname,
    search: Search,
    state: State,
    hash: History.Hash
    key?: LocationKey,
};

export type OwnHistory<State> = {
    length: number,
    action: Action,
    location: OriginalLocation<State>,
    index?: number,
    push(path: Path, state?: State): void,
    push(location: LocationDescriptorObject<State>): void,
    replace(path: Path, state?: State): void,
    replace(location: LocationDescriptorObject<State>): void,
    go(n: number): void,
    goBack(): void,
    goForward(): void,
    block(prompt?: boolean | string | TransitionPromptHook): UnregisterCallback,
    listen(listener: LocationListener): UnregisterCallback,
    createHref(location: LocationDescriptorObject<State>): Href,
};

export type OwnRouteComponentProps<Params extends {
    [K in keyof Params]?: string
} = {}, C extends StaticContext = StaticContext> = {
    history: History,
    location: Location,
    match: match<Params>,
    staticContext?: C,
};

export default function Router() {
    return (
        <NativeRouter>
            <BackButton/>
            <TopBar/>
            <View style={styles.filler}>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/blog/category/:slug" component={BlogPostList}/>
                    <Route exact path="/blog/posts/:slug" component={BlogPost}/>
                    <Route exact path="/calendrier" component={CalendarEventIndex}/>
                    <Route exact path="/calendrier/evenement/:slug" component={CalendarEvent}/>
                    <Route exact path="/document/categorie/:slug" component={DocumentCategory}/>
                    <Route exact path="/document/document/:slug" component={Document}/>
                    <Route exact path="/core/page/:slug" component={MenuPage}/>
                    <Route exact path="/poi/map" component={Map}/>
                    <Route exact path="/reporting" component={Reporting}/>
                    <Route exact path="/reporting/types" component={ReportTypes}/>
                    <Route exact path="/reporting/camera" component={ReportCamera}/>
                    <Route exact path="/reporting/form" component={ReportForm}/>
                    <Route exact path="/settings" component={Settings}/>
                </Switch>
            </View>
            <Toaster/>
            <BottomBar navigationItems={BOTTOM_BAR_NAVIGATION_ITEMS}/>
            <SideMenu>
                <MainNavigation/>
            </SideMenu>
        </NativeRouter>
    );
}
