import * as Notifications from 'expo-notifications';
import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import Redux, {Action, bindActionCreators} from 'redux';
import {NOTIFICATION_TYPES} from '../config';
import {setActiveRouter, setOpenedNotification} from './actions/routing';
import CarpoolingRouter from './CarpoolingRouter';
import MainRouter from './MainRouter';
import {State} from './reducers';
import {ActiveRoute, NotificationParamsType, User} from './types/types';

type StateProps = {
    activeRoute: ActiveRoute,
    openedNotification: NotificationParamsType | null,
    user: User | null,
};

type DispatchProps = {
    setActiveRouter: (activeRoute: ActiveRoute) => void,
    setOpenedNotification: (notification: NotificationParamsType | null) => void,
};

type Props = DispatchProps & StateProps;

function mapStateToProps(state: State): StateProps {
    return {
        activeRoute: state.routing.activeRoute,
        openedNotification: state.routing.openedNotification,
        user: state.profile.user,
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Action>) {
    return bindActionCreators({
        setActiveRouter,
        setOpenedNotification,
    }, dispatch);
}

export default connect<StateProps, DispatchProps, {}, State>(mapStateToProps, mapDispatchToProps)(
    function Entrypoint({activeRoute, setActiveRouter, setOpenedNotification}: Props) {

        useEffect(() => {
            Notifications.addNotificationResponseReceivedListener((notif) => {
                const data = notif.notification.request.content.data as NotificationParamsType;
                const type = data.type;
                if (type === NOTIFICATION_TYPES.JOURNEY_REQUEST_RECEIVED
                    || type === NOTIFICATION_TYPES.NEW_MESSAGE_RECEIVED) {
                    setActiveRouter(ActiveRoute.CARPOOLING);
                }
                mapNotificationToRoute(data);
            });
        }, []);

        function mapNotificationToRoute(notification: NotificationParamsType) {
            setOpenedNotification(notification);
        }

        if (activeRoute === ActiveRoute.MAIN) {
            return <MainRouter/>;
        }

        if (activeRoute === ActiveRoute.CARPOOLING) {
            return <CarpoolingRouter/>;
        }

        return null;
    },
);
