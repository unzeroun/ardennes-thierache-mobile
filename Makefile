run:
	expo start

cc:
	rm -rf /tmp/metro-cache

hook: hooks/pre-commit
	ln -vs ../../hooks/pre-commit .git/hooks/pre-commit
