import {ImageSourcePropType} from 'react-native';
import {Location} from './src/MainRouter';

export const GEO_API_GOUV_URL = 'https://api-adresse.data.gouv.fr';
// export const API_BASE_URL = 'http://192.168.101.135';
// export const API_ENDPOINT = 'http://192.168.101.135/api';
// export const API_ENTRYPOINT = 'http://192.168.101.135/api/graphql';
export const API_BASE_URL = 'https://www.ardennes-thierache.com';
export const API_ENDPOINT = 'https://www.ardennes-thierache.com/api';
export const API_ENTRYPOINT = 'https://www.ardennes-thierache.com/api/graphql';
export const REACHY_BASE_URL = 'https://api.reachy.xyz';
export const REACHY_CLIENT_ID = 'be1968ea-8794-11e9-bc42-526af7764f64';
export const THUMBNAIL_BASE_URL = 'https://www.ardennes-thierache.com/media/cache/resolve/retina/';
export const YOUTUBE_THUMBNAIL_URL = 'https://img.youtube.com/vi/:id/0.jpg';
export const USED_MAIL_ERROR_LABEL = 'email: Cette valeur est déjà utilisée.';
export const JWT_EXPIRE_TIME_LIMIT = 300;

export enum JOURNEY_REQUEST_STATUSES {
    PENDING = 'pending',
    ACCEPTED = 'accepted',
    DENIED = 'denied',
}

export enum NOTIFICATION_TYPES {
    JOURNEY_REQUEST_RECEIVED = 'journey_request_received',
    NEW_MESSAGE_RECEIVED = 'new_message_received',
}

export enum NOTIFICATION_TYPES_ROUTES_MAPPING {
    JOURNEY_REQUEST_RECEIVED = '/journey/show/',
}

export type SocialLinksDataType = {
    icon: ImageSourcePropType,
    link?: string,
    internalLink?: string,
};

export const SOCIAL_LINKS_DATA: SocialLinksDataType[] = [
    {
        icon: require('./assets/images/facebook_icon.png'),
        link: 'https://www.facebook.com/ComComArdennesThierache',
    },
    {
        icon: require('./assets/images/instagram_icon.png'),
        link: 'https://www.instagram.com/ardennesthierache/',
    },
    {
        icon: require('./assets/images/camera_filled.png'),
        internalLink: '/reporting',
    },
    {
        icon: require('./assets/images/twitter_icon.png'),
        link: 'https://twitter.com/ardenthierache',
    },
    {
        icon: require('./assets/images/youtube_icon.png'),
        link: 'https://www.youtube.com/channel/UCFkYNTnUb7FAoRu6b9hwGxQ',
    },
];

export type NavigationItemType = {
    icon: ImageSourcePropType,
    iconActive: ImageSourcePropType,
    label: string,
    location: Location,
};

export const BOTTOM_BAR_NAVIGATION_ITEMS: NavigationItemType[] = [
    {
        icon: require('./assets/images/home.png'),
        iconActive: require('./assets/images/home_active.png'),
        label: 'Acceuil',
        location:  {
            hash: '',
            pathname: '/',
            search: '',
            state: {
                inMenu: true,
                page: 'Accueil',
            },
        },
    },
    {
        icon: require('./assets/images/calendar.png'),
        iconActive: require('./assets/images/calendar_active.png'),
        label: 'Évènements',
        location: {
            hash: '',
            pathname: '/calendrier/',
            search: '',
            state: {
                inMenu: true,
                page: 'Évènements',
            },
        },
    },
    {
        icon: require('./assets/images/map_icon.png'),
        iconActive: require('./assets/images/map_icon_active.png'),
        label: 'Maps',
        location: {
            hash: '',
            pathname: '/poi/map/',
            search: '',
            state: {
                inMenu: true,
                page: '',
            },
        },
    },
    {
        icon: require('./assets/images/settings_icon.png'),
        iconActive: require('./assets/images/settings_icon_active.png'),
        label: 'Réglages',
        location: {
            hash: '',
            pathname: '/settings/',
            search: '',
            state: {
                inMenu: true,
                page: 'Réglages',
            },
        },
    },
];

export const CARPOOLING_FOOTER_NAVIGATION_ITEMS: NavigationItemType[] = [
    {
        icon: require('./assets/images/journey_icon.png'),
        iconActive: require('./assets/images/journey_icon_active.png'),
        label: '',
        location: {
            hash: '',
            pathname: '/routes',
            search: '',
            state: {},
        },
    },
    {
        icon: require('./assets/images/search_icon.png'),
        iconActive: require('./assets/images/search_icon_active.png'),
        label: '',
        location: {
            hash: '',
            pathname: '/search',
            search: '',
            state: {},
        },
    },
    {
        icon: require('./assets/images/user_icon.png'),
        iconActive: require('./assets/images/user_icon_active.png'),
        label: '',
        location: {
            hash: '',
            pathname: '/profile/user',
            search: '',
            state: {},
        },
    },
];

export type RouteMapType = {
    [key: string]: string,
};

export const ROUTE_MAP: RouteMapType = {
    soloist_blog_list_category: '/blog/category/:slug',
    soloist_document_category: '/document/categorie/:slug',
    soloist_document_file_download: API_BASE_URL.replace(/\/+\s*$/, '')
        + '/fr/document/fichier/:id/telecharger',
    soloist_document_show_category: '/document/categorie/:slug',
};

export type Type = {
    label: string,
    image: ImageSourcePropType,
};

export const REPORT_TYPES: Type[] = [
    {
        image: require('./assets/reporting/truck.png'),
        label: 'TRANSPORTS',
    },
    {
        image: require('./assets/reporting/message.png'),
        label: 'DIVERS',
    },
    {
        image: require('./assets/reporting/warning.png'),
        label: 'TRAVAUX',
    },
    {
        image: require('./assets/reporting/car.png'),
        label: 'CIRCULATION',
    },
    {
        image: require('./assets/reporting/tree.png'),
        label: 'NATURE',
    },
    {
        image: require('./assets/reporting/photo.png'),
        label: 'CONCOURS',
    },
];

export const CAMERA_RATIO: string = '4:3';

export const CAR_PREFERENCES: string[] = [
    'discussion',
    'music',
    'smoker',
    'pet',
];

export type DRIVER_PREFERENCE = {
    disabledIcon: ImageSourcePropType,
    enabledIcon: ImageSourcePropType,
    label: string,
    value: string,
};

export type DRIVER_CAR_PREFERENCES = {
    [key: string]: DRIVER_PREFERENCE,
};

export const DRIVER_CAR_PREFERENCES: DRIVER_CAR_PREFERENCES = {
    discussion: {
        disabledIcon: require('./assets/images/discussion_disabled.png'),
        enabledIcon: require('./assets/images/discussion_enabled.png'),
        label: 'BLABLA',
        value: 'discussion',
    },
    music: {
        disabledIcon: require('./assets/images/music_disabled.png'),
        enabledIcon: require('./assets/images/music_enabled.png'),
        label: 'MUSIQUE',
        value: 'music',
    },
    pet: {
        disabledIcon: require('./assets/images/pet_disabled.png'),
        enabledIcon: require('./assets/images/pet_enabled.png'),
        label: 'ANIMAUX',
        value: 'pet',
    },
    smoker: {
        disabledIcon: require('./assets/images/smoker_disabled.png'),
        enabledIcon: require('./assets/images/smoker_enabled.png'),
        label: 'FUMEUR',
        value: 'smoker',
    },
};
